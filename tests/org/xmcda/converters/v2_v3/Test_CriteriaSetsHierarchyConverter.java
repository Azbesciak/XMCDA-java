package org.xmcda.converters.v2_v3;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.xmcda.CriteriaSet;
import org.xmcda.CriteriaSetHierarchyNode;
import org.xmcda.CriteriaSetsHierarchy;
import org.xmcda.parsers.xml.xmcda_v2.TestUtils;
import org.xmcda.v2.XMCDA;
import org.xml.sax.SAXException;

public class Test_CriteriaSetsHierarchyConverter
{
	private final String xmcda_v2_header = 
			"<xmcda:XMCDA" +
			"  xsi:schemaLocation=\"http://www.decision-deck.org/2012/XMCDA-2.2.1 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.1.xsd\"\n" + 
			"  xmlns:xmcda=\"http://www.decision-deck.org/2012/XMCDA-2.2.1\"\n" +
			"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";

	private final String xmcda_v2_footer = "</xmcda:XMCDA>";
	private final String hierarchy_1 = xmcda_v2_header +
			"	<hierarchy>\n" + 
			"		<node id=\"nodes\">\n" + 
			"			<node id=\"nodes1\">\n" + 
			"				<node id=\"nodes11\">\n" + 
			"					<criteriaSetID>cS01</criteriaSetID>\n" + 
			"				</node>\n" + 
			"				<node id=\"nodes12\">\n" + 
			"					<criteriaSetID>cS02</criteriaSetID>\n" + 
			"				</node>\n" + 
			"			</node>\n" + 
			"			<node id=\"nodes2\">\n" + 
			"				<criteriaSetID>cS03</criteriaSetID>\n" + 
			"			</node>\n" + 
			"		</node>\n" + 
			"		<node id=\"nodes\">\n" + 
			"			<criteriaSetID>cS04</criteriaSetID>\n" + 
			"		</node>" +
			"	</hierarchy>\n" + 
			xmcda_v2_footer;

	@Test
	public void testConvertTo_v3() throws UnsupportedEncodingException, IOException, JAXBException, SAXException
	{
		XMCDA xmcda_v2 = TestUtils.readXMCDA(hierarchy_1);
		//Hierarchy hierarchy_v2 = (Hierarchy) xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().get(0).getValue();
		//new CriteriaHierarchyConverter().convertTo_v3(hierarchy_v2, xmcda_v3);

		org.xmcda.XMCDA xmcda_v3 = XMCDAConverter.convertTo_v3(xmcda_v2);
		assertEquals(6, xmcda_v3.criteriaSets.size());
		assertEquals(1, xmcda_v3.criteriaSetsHierarchiesList.size());
		CriteriaSetsHierarchy hierarchy = xmcda_v3.criteriaSetsHierarchiesList.get(0);
		assertEquals(2, hierarchy.getRootNodes().size());

		Iterator<CriteriaSetHierarchyNode> rootNodes = hierarchy.getRootNodes().iterator();

		CriteriaSetHierarchyNode firstNode = rootNodes.next();
		assertNotNull(firstNode);
		assertEquals("hierarchy criteriaSet nodes", firstNode.getCriteriaSet().id()); // a criterion was created with the (v2) node's id

		// now just check the structure
		assertEquals(2, firstNode.getChildren().size());
		assertEquals(2, firstNode.getChild("hierarchy criteriaSet nodes1").getChildren().size());

		assertNull(firstNode.getChild("hierarchy criteriaSet nodes2"));
		assertNotNull(firstNode.getChild("cS03"));
		assertEquals(0, firstNode.getChild("cS03").getChildren().size());

		CriteriaSetHierarchyNode secondNode = rootNodes.next();
		assertEquals("cS04", secondNode.getCriteriaSet().id());

		assertFalse(rootNodes.hasNext());
	}
	
	private final String hierarchy_2 = xmcda_v2_header +
			"	<hierarchy>\n" + 
			"		<node>\n" + 
			"			<criteriaSetID>cS01</criteriaSetID>\n" + 
			"		</node>\n" + 
			"		<node id=\"nodes12\">\n" + 
			"			<criteriaSetID>cS02</criteriaSetID>\n" + 
			"		</node>\n" + 
			"		<node>\n" + 
			"			<criteriaSet id=\"cS03\">" + 
			"				<element><criterionID>c03</criterionID></element>\n" + 
			"				<element><criterionID>c04</criterionID></element>\n" + 
			"			</criteriaSet>" +
			"		</node>\n" + 
			"	</hierarchy>\n" + 
			xmcda_v2_footer;

	@Test
	public void testConvertTo_v3_with_criteriaSet() throws UnsupportedEncodingException, IOException, JAXBException, SAXException
	{
		XMCDA xmcda_v2 = TestUtils.readXMCDA(hierarchy_2);

		org.xmcda.XMCDA xmcda_v3 = XMCDAConverter.convertTo_v3(xmcda_v2);

		assertEquals(3, xmcda_v3.criteriaSets.size());
		assertEquals(0, xmcda_v3.criteriaHierarchiesList.size());
		assertEquals(1, xmcda_v3.criteriaSetsHierarchiesList.size());

		CriteriaSetsHierarchy hierarchy = xmcda_v3.criteriaSetsHierarchiesList.get(0);
		assertEquals(3, hierarchy.getRootNodes().size());

		CriteriaSet<?> cs03 = xmcda_v3.criteriaSets.get("cS03");
		assertEquals(2, cs03.size());
	}

	private static org.xmcda.XMCDA getHierarchy_1()
	{
		org.xmcda.XMCDA xmcda = new org.xmcda.XMCDA();
		CriteriaSetsHierarchy hierarchy_1 = new CriteriaSetsHierarchy();
		xmcda.criteriaSetsHierarchiesList.add(hierarchy_1);

		org.xmcda.Description d = new org.xmcda.Description();
		d.setComment("cSH1 comment");
		hierarchy_1.setDescription(d);

		hierarchy_1.setId("cSH1");
		hierarchy_1.setName("criteria sets hierarchy 1");
		hierarchy_1.setMcdaConcept("cSH1 mcdaConcept");

		hierarchy_1.addChild(xmcda.criteriaSets.get("cS01", true));
		CriteriaSetHierarchyNode node = hierarchy_1.getChild("cS01");
		node.addChild(xmcda.criteriaSets.get("cS11", true));
		node.addChild(xmcda.criteriaSets.get("cS12", true));

		hierarchy_1.addChild(xmcda.criteriaSets.get("cS02", true));
		return xmcda;
	}

	/**
	 * Checks the criteria hierarchy returned by {@link #getHierarchy_1()} after it has been converted to xmcda v2
	 * 
	 * @param hierarchy the criteria hierarchy to be checked
	 */
	private static void checkHierarchy_1(org.xmcda.v2.XMCDA xmcda_v2)
	{
		// 2 created: criteria & hierarchy
		assertEquals(2, xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().size());
		org.xmcda.v2.CriteriaSets criteriaSets = null;
		org.xmcda.v2.Hierarchy hierarchy = null;
		Iterator<JAXBElement<?>> iterator = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().iterator();
		Object o = iterator.next().getValue();
		if (o instanceof org.xmcda.v2.CriteriaSets) criteriaSets = (org.xmcda.v2.CriteriaSets) o;
		else hierarchy = (org.xmcda.v2.Hierarchy) o;

		o = iterator.next().getValue();
		if (o instanceof org.xmcda.v2.CriteriaSets) criteriaSets = (org.xmcda.v2.CriteriaSets) o;
		else hierarchy = (org.xmcda.v2.Hierarchy) o;
		
		assertEquals("cSH1", hierarchy.getId());
		assertEquals("criteria sets hierarchy 1", hierarchy.getName());
		assertEquals("cSH1 mcdaConcept", hierarchy.getMcdaConcept());

		assertEquals(1, hierarchy.getDescription().getUserOrAuthorOrVersion().size());
		assertEquals("comment", hierarchy.getDescription().getUserOrAuthorOrVersion().get(0).getName().getLocalPart());
		assertEquals("cSH1 comment", hierarchy.getDescription().getUserOrAuthorOrVersion().get(0).getValue());
		
		assertEquals(4, criteriaSets.getCriteriaSet().size());
		assertEquals(2, hierarchy.getNode().size());
		
		BiFunction<List<org.xmcda.v2.Node>, String, org.xmcda.v2.Node> getNode = 
				(List<org.xmcda.v2.Node> nodes, String id) -> nodes.stream().filter(n -> id.equals(n.getCriteriaSetID())).findFirst().orElse(null); 

		org.xmcda.v2.Node design = getNode.apply(hierarchy.getNode(), "cS01");
		assertNotNull(design);
		assertEquals(2, design.getNode().size());

		org.xmcda.v2.Node shape = getNode.apply(design.getNode(), "cS11");
		org.xmcda.v2.Node brand = getNode.apply(design.getNode(), "cS12");

		assertNotNull(shape);
		assertNotNull(brand);
		assertTrue(shape.getNode().isEmpty()); // shape has no child
		assertTrue(brand.getNode().isEmpty()); // brand has no child

		org.xmcda.v2.Node price = getNode.apply(hierarchy.getNode(), "cS02");
		assertNotNull(price);
		assertTrue(price.getNode().isEmpty());
	}

	@Test
	public void testConvertTo_v2()
	{
		XMCDA xmcda_v2 = XMCDAConverter.convertTo_v2(getHierarchy_1());
		checkHierarchy_1(xmcda_v2);
	}
}
