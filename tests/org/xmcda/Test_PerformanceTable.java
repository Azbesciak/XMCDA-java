package org.xmcda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;


public class Test_PerformanceTable
{
	Alternative a1 = new Alternative("a01");
	Alternative a2 = new Alternative("a02");
	Criterion c1 = new Criterion("c01");
	Criterion c2 = new Criterion("c02");
	PerformanceTable<Object> p;

	@Before
	public void setUp()
	{
		p = new PerformanceTable<>();
	}
	
	@Test(expected=NullPointerException.class)
	public void test_put_null_alternative_and_criteria()
	{
		p.put(null, null, 1.0);
	}
	
	@Test(expected=NullPointerException.class)
	public void test_put_null_criteria()
	{
		p.put(a1, null, 1.0);
	}

	@Test(expected=NullPointerException.class)
	public void test_put_null_alternative()
	{
		p.put(null, c1, 1.0);
	}

	@Test
	public void test_put_non_null_args()
	{
		p.put(a1, c1, 1.0);
	}

	@Test
	public void test_put_null_value()
	{
		p.put(a1, c1, (Object) null);
	}

	// get()
	@Test(expected=NullPointerException.class)
	public void test_get_null_alternative_and_criteria()
	{
		p.get(null, null);
	}
	
	@Test(expected=NullPointerException.class)
	public void test_get_null_criteria()
	{
		p.get(a1, null);
	}

	@Test(expected=NullPointerException.class)
	public void test_get_null_alternative()
	{
		p.get(null, c1);
	}

	@Test
	public void test_get_non_null_value()
	{
		PerformanceTable<String> p = new PerformanceTable<>();
		p.put(a1, c1, "42");
		assertEquals("42", p.get(a1,c1).get(0).getValue());
	}

	@Test
	public void test_get_null_value()
	{
		PerformanceTable<Object> p = new PerformanceTable<>();
		assertEquals(null, p.get(a1, c1));
	}

	@Test
	public void test_Integer_PerformanceTable_put_get()
	{
		PerformanceTable<Integer> p = new PerformanceTable<>();
		p.put(a1, c1, 42);
		assertEquals((Integer)42, p.get(a1, c1).get(0).getValue());
		assertEquals(null, p.get(a1, c2));
	}

	//
	
	@Test
	public void testGetQValue()
	{
		p.put(a1, c1, 42);
		assertEquals(42, p.getQValue(a1, c1).getValue());
		assertEquals(null, p.getQValue(a1, c2)); // TODO ou alors un object singleton NullQValue ?
	}

	@Test
	public void testGetValue()
	{
		p.put(a1, c1, 42);
		assertEquals(42, p.getValue(a1, c1));
		assertEquals(null, p.getValue(a1, c2));
	}

	@Test
	public void testDescription()
	{
		assertNull(p.getDescription());
	}
	
}
