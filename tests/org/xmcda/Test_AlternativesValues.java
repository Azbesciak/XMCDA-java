package org.xmcda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.xml.bind.JAXBElement;

import org.junit.Before;
import org.junit.Test;
import org.xmcda.converters.v2_v3.XMCDAConverter;

public class Test_AlternativesValues
{
	XMCDA xmcda;
	AlternativesValues<Integer> alternativesValues = null;

	@Before
	public void init()
	{
		xmcda = new XMCDA();
		alternativesValues = new AlternativesValues<>();
		xmcda.alternativesValuesList.add(alternativesValues);
	}

	public org.xmcda.v2.AlternativeValue alternativeValue(org.xmcda.v2.XMCDA xmcda_v2)
	{
		org.xmcda.v2.AlternativesValues alternativesValues_v2 = null;
		for (JAXBElement<?> element: xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters())
		{
			if (element.getValue() instanceof org.xmcda.v2.AlternativesValues)
				alternativesValues_v2 = (org.xmcda.v2.AlternativesValues) element.getValue();
		}
		assertNotNull(alternativesValues_v2);
		assertEquals(1, alternativesValues_v2.getAlternativeValue().size());

		return alternativesValues_v2.getAlternativeValue().get(0);
	}

	@Test
	public void testToV2WithOrWithoutValues()
	{
		Alternative a1 = xmcda.alternatives.get("a01", true);
		alternativesValues.put(a1, 42);
		org.xmcda.v2.XMCDA xmcda_v2;

		// 1 value, no <values>
		XMCDAConverter.toV2Parameters.get().setOmitValuesInAlternativesValues(true);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		org.xmcda.v2.AlternativeValue alternativeValue_v2 = alternativeValue(xmcda_v2);
		assertEquals(1, alternativeValue_v2.getValueOrValues().size());
		assertTrue(alternativeValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Value);

		// 2 values, no <values>
		alternativesValues.clear();
		alternativesValues.put(a1, 42);
		alternativesValues.get(a1).add(new QualifiedValue<>(77));
		XMCDAConverter.toV2Parameters.get().setOmitValuesInAlternativesValues(true);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		alternativeValue_v2 = alternativeValue(xmcda_v2);
		assertEquals(2, alternativeValue_v2.getValueOrValues().size());
		assertTrue(alternativeValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Value);
		assertTrue(alternativeValue_v2.getValueOrValues().get(1) instanceof org.xmcda.v2.Value);

		// 1 value, with <values>
		alternativesValues.clear();
		alternativesValues.put(a1, 42);
		XMCDAConverter.toV2Parameters.get().setOmitValuesInAlternativesValues(false);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		alternativeValue_v2 = alternativeValue(xmcda_v2);
		assertEquals(1, alternativeValue_v2.getValueOrValues().size());
		assertTrue(alternativeValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Values);
		assertEquals(1, ((org.xmcda.v2.Values) alternativeValue_v2.getValueOrValues().get(0)).getValue().size());

		// 2 values, with <values>
		alternativesValues.clear();
		alternativesValues.put(a1, 42);
		alternativesValues.get(a1).add(new QualifiedValue<>(77));
		XMCDAConverter.toV2Parameters.get().setOmitValuesInAlternativesValues(false);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		alternativeValue_v2 = alternativeValue(xmcda_v2);
		assertEquals(1, alternativeValue_v2.getValueOrValues().size());
		assertTrue(alternativeValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Values);
		assertEquals(2, ((org.xmcda.v2.Values) alternativeValue_v2.getValueOrValues().get(0)).getValue().size());
	}

}
