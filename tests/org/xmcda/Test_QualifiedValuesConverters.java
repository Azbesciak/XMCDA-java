/**
 * 
 */
package org.xmcda;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xmcda.utils.ValueConverters.ConversionException;
import org.xmcda.value.Rational;

/**
 * @author big
 *
 */
public class Test_QualifiedValuesConverters
{
	@Before
	public void setUp() throws Exception
	{}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{}

	/**
	 * Test method for {@link org.xmcda.utils.ValueConverters#convert(java.lang.Object, java.lang.Class)}.
	 * @throws ConversionException 
	 */
	@Test
	public void testConvert_value() throws ConversionException
	{
		QualifiedValue<Integer> i = new QualifiedValue<>(1);
		QualifiedValue<Double> d = i.convertTo(Double.class);
		
		QualifiedValue<Rational> r = new QualifiedValue<>(new Rational(1,0));
		assertThrows(ConversionException.class, () -> r.convertTo(Double.class));
	}

	private QualifiedValues _get_qvs()
	{
		@SuppressWarnings("rawtypes")
		QualifiedValues qvs = new QualifiedValues<>();
		qvs.add(new QualifiedValue(1));
		qvs.add(new QualifiedValue(2.));
		return qvs;
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testConvert_values() throws ConversionException
	{
		QualifiedValues<?> qvs = _get_qvs();
		qvs.convertTo(String.class);
		assertTrue(qvs.isNumeric());

		qvs.add(new QualifiedValue("moo"));
		assertFalse(qvs.isNumeric());
		qvs.remove(qvs.size()-1);

		qvs = _get_qvs();
		Rational r = new Rational(1,0);
		QualifiedValue qr = new QualifiedValue<>(r);
		assertThrows(ConversionException.class, () -> qr.convertTo(Double.class));

		qvs.add(qr);
		assertTrue(qvs.isNumeric());
		assertThrows(ConversionException.class, qvs::convertToDouble);
		// the content should not have changed
		assertEquals(Integer.class, qvs.get(0).getValue().getClass());
		assertEquals(Double.class, qvs.get(1).getValue().getClass());
		assertEquals(Rational.class, qvs.get(2).getValue().getClass());
		
		r.setDenominator(2);
		assertTrue(qvs.isNumeric());
		assertEquals(1, qvs.get(0).getValue());
		assertEquals(2., qvs.get(1).getValue());
		qvs.convertToDouble();
		assertEquals(Double.class, qvs.get(0).getValue().getClass());
		assertEquals(Double.class, qvs.get(1).getValue().getClass());
		assertEquals(Double.class, qvs.get(2).getValue().getClass());
		assertEquals(1., qvs.get(0).getValue());
		assertEquals(2., qvs.get(1).getValue());
		assertEquals(0.5, qvs.get(2).getValue());
	}
}
