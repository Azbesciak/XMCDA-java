package org.xmcda.parsers.xml.xmcda_v2;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.xmcda.CriteriaSet;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import org.xmcda.converters.v2_v3.XMCDAConverter;

public class Test_Values
{

	/**
	 * tests that the XMCDA v2 file written is valid
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void test_v3tov2_valid() throws Exception
	{
		final File tmpFile = File.createTempFile("test_XMCDA_Values", ".xml");
		try
		{
			XMCDA xmcda_v3 = new XMCDA();
			CriteriaSet criteriaSet_v3 = Factory.criteriaSet();
			criteriaSet_v3.setId("cS1");
			xmcda_v3.criteriaSets.add(criteriaSet_v3);
			criteriaSet_v3.put(xmcda_v3.criteria.get("c01", true), null);

			org.xmcda.v2.XMCDA xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda_v3);
			XMCDAParser.writeXMCDA(xmcda_v2, tmpFile);
			XMCDAParser.validate(tmpFile);
		}
		finally
		{
			tmpFile.delete();
		}
	}

}
