package org.xmcda.parsers.xml.xmcda_v2;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.xml.sax.SAXException;

public class test_ReadXMCDAv2
{
	private final static String alternatives_1_filename = "data/tests/v2_to_v3/alternatives-1.in.v2.xml";

	/**
	 * Returns the same XMCDA as the one stored in {@link #hierarchy_1_filename}.
	 */
	@Test
	public void readAlternatives_1() throws IOException, JAXBException, SAXException
	{		
		org.xmcda.v2.XMCDA xmcda_v2 = org.xmcda.parsers.xml.xmcda_v2.XMCDAParser.readXMCDA(alternatives_1_filename);
		List<JAXBElement<?>> all = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		assertEquals(1, all.size());
		org.xmcda.v2.Alternatives alternatives = ((JAXBElement<org.xmcda.v2.Alternatives>) all.get(0)).getValue();
		// 3 descriptions (the last two are ignored), 7 alternatives
		assertEquals(3+7, alternatives.getDescriptionOrAlternative().size());
	}

}
