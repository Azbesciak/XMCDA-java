package org.xmcda.parsers.xml.xmcda_v3;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;

import org.junit.Test;
import org.xmcda.CriteriaSetHierarchyNode;
import org.xmcda.CriteriaSetsHierarchy;
import org.xmcda.Description;
import org.xmcda.XMCDA;
import org.xml.sax.SAXException;

public class Test_CriteriaSetsHierarchyParser
{
	private final static String hierarchy_1_filename = "data/tests/v2_to_v3/criteriaSetsHierarchy-1.in.v3.xml";

	/**
	 * Returns the same XMCDA as the one stored in {@link #hierarchy_1_filename}.
	 */
	private XMCDA getHierarchy_1()
	{
		XMCDA xmcda = new XMCDA();
		CriteriaSetsHierarchy hierarchy_1 = new CriteriaSetsHierarchy();
		xmcda.criteriaSetsHierarchiesList.add(hierarchy_1);

		Description d = new Description();
		d.setComment("cSH1 comment");
		hierarchy_1.setDescription(d);

		hierarchy_1.setId("cSH1");
		hierarchy_1.setName("criteria sets hierarchy 1");
		hierarchy_1.setMcdaConcept("cSH1 mcdaConcept");

		hierarchy_1.addChild(xmcda.criteriaSets.get("cS1", true));
		CriteriaSetHierarchyNode node = hierarchy_1.getChild("cS1");
		node.addChild(xmcda.criteriaSets.get("cS11", true));
		node.addChild(xmcda.criteriaSets.get("cS12", true));

		hierarchy_1.addChild(xmcda.criteriaSets.get("cS2", true));
		return xmcda;
	}

	/**
	 * Checks the criteria hierarchy loaded from the file {@link #hierarchy_1_filename}.
	 * 
	 * @param hierarchy the criteria hierarchy to be checked
	 */
	private void checkHierarchy1(CriteriaSetsHierarchy hierarchy)
	{
		assertEquals("cSH1", hierarchy.id());
		assertEquals("criteria sets hierarchy 1", hierarchy.name());
		assertEquals("cSH1 mcdaConcept", hierarchy.mcdaConcept());

		assertEquals("cSH1 comment", hierarchy.getDescription().getComment());
		
		assertEquals(2, hierarchy.getRootNodes().size());
		
		assertNotNull(hierarchy.getChild("cS1"));
		assertEquals(2, hierarchy.getChild("cS1").getChildren().size());
		assertNotNull(hierarchy.getChild("cS1").getChild("cS11"));
		assertNotNull(hierarchy.getChild("cS1").getChild("cS12"));
		assertTrue(hierarchy.getChild("cS1").getChild("cS11").getChildren().isEmpty()); // cS11 has no child
		assertTrue(hierarchy.getChild("cS1").getChild("cS12").getChildren().isEmpty()); // cS12 has no child

		assertNotNull(hierarchy.getChild("cS2"));
		assertTrue(hierarchy.getChild("cS2").getChildren().isEmpty());
	}

	@Test
	public void test_fromXML() throws XMLStreamException, FileNotFoundException
	{
		XMLEventReader eventReader = TestUtils.getReader(new File(hierarchy_1_filename));
		XMCDA xmcda = new XMCDA();
		TestUtils.firstStartEvent(eventReader);
		CriteriaSetsHierarchy hierarchy = new CriteriaSetsHierarchyParser().fromXML(xmcda, TestUtils.firstStartEvent(eventReader), eventReader);

		checkHierarchy1(hierarchy);
	}

	@Test
	public void test_XMCDAfromXML() throws XMLStreamException, IOException, SAXException
	{
		XMCDA xmcda = new XMCDAParser().readXMCDA(new XMCDA(), new File(hierarchy_1_filename));

		assertEquals(1, xmcda.criteriaSetsHierarchiesList.size());
		checkHierarchy1(xmcda.criteriaSetsHierarchiesList.get(0));
	}

	@Test
	public void test_XMCDAtoXML() throws XMLStreamException, IOException, SAXException
	{
		// to test it, we write & load a XMCDA object with a CriteriaSetsHierarchy, and make sure it is the same
		XMCDA xmcda = getHierarchy_1();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(8192); // arbitrary size, large enough

		new XMCDAParser().writeXMCDA(xmcda, outputStream, CriteriaSetsHierarchyParser.CRITERIA_SETS_HIERARCHY);
		XMCDA readXMCDA = new XMCDAParser().readXMCDA(new XMCDA(), new ByteArrayInputStream(outputStream.toByteArray()), null);

		assertEquals(1, readXMCDA.criteriaSetsHierarchiesList.size());
		checkHierarchy1(readXMCDA.criteriaSetsHierarchiesList.get(0));
	}

}
