package org.xmcda.parsers.xml.xmcda_v3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.StringReader;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public abstract class TestUtils
{
	public static XMLEventReader getReader(String xmlString) throws XMLStreamException
	{
		return XMLInputFactory.newInstance().createXMLEventReader(new StringReader(xmlString));
	}

	public static XMLEventReader getReader(File xmlfile) throws XMLStreamException, FileNotFoundException
	{
		return XMLInputFactory.newInstance().createXMLEventReader(new BufferedReader(new FileReader(xmlfile)));
	}

	public static StartElement firstStartEvent(XMLEventReader eventReader) throws XMLStreamException
	{
		XMLEvent element = null;
		// If the eventReader is incorrect this will throw XMLStreamException or
		// NoSuchElementException and that's ok in tests, so we don't check anything here
		while ( !( element = eventReader.nextEvent() ).isStartElement() );
		return element.asStartElement();
	}
}
