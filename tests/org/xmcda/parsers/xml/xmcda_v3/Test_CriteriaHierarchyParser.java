package org.xmcda.parsers.xml.xmcda_v3;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;

import org.junit.Test;
import org.xmcda.CriteriaHierarchy;
import org.xmcda.CriterionHierarchyNode;
import org.xmcda.Description;
import org.xmcda.XMCDA;
import org.xml.sax.SAXException;

public class Test_CriteriaHierarchyParser
{
	private final static String hierarchy_1_filename = "data/tests/v2_to_v3/criteriaHierarchy-1.in.v3.xml";

	/**
	 * Returns the same XMCDA as the one stored in {@link #hierarchy_1_filename}.
	 */
	private XMCDA getHierarchy_1()
	{
		XMCDA xmcda = new XMCDA();
		CriteriaHierarchy hierarchy_1 = new CriteriaHierarchy();
		xmcda.criteriaHierarchiesList.add(hierarchy_1);

		Description d = new Description();
		d.setComment("cH1 comment");
		hierarchy_1.setDescription(d);

		hierarchy_1.setId("cH1");
		hierarchy_1.setName("hierarchy 1");
		hierarchy_1.setMcdaConcept("cH1 mcdaConcept");

		hierarchy_1.addChild(xmcda.criteria.get("design", true));
		CriterionHierarchyNode node = hierarchy_1.getChild("design");
		node.addChild(xmcda.criteria.get("shape", true));
		node.addChild(xmcda.criteria.get("brand", true));

		hierarchy_1.addChild(xmcda.criteria.get("price", true));
		return xmcda;
	}

	/**
	 * Checks the criteria hierarchy loaded from the file {@link #hierarchy_1_filename}.
	 * 
	 * @param hierarchy the criteria hierarchy to be checked
	 */
	private void checkHierarchy1(CriteriaHierarchy hierarchy)
	{
		assertEquals("cH1", hierarchy.id());
		assertEquals("hierarchy 1", hierarchy.name());
		assertEquals("cH1 mcdaConcept", hierarchy.mcdaConcept());

		assertEquals("cH1 comment", hierarchy.getDescription().getComment());
		
		assertEquals(2, hierarchy.getRootNodes().size());
		
		assertNotNull(hierarchy.getChild("design"));
		assertEquals(2, hierarchy.getChild("design").getChildren().size());
		assertNotNull(hierarchy.getChild("design").getChild("shape"));
		assertNotNull(hierarchy.getChild("design").getChild("brand"));
		assertTrue(hierarchy.getChild("design").getChild("shape").getChildren().isEmpty()); // shape has no child
		assertTrue(hierarchy.getChild("design").getChild("brand").getChildren().isEmpty()); // brand has no child

		assertNotNull(hierarchy.getChild("price"));
		assertTrue(hierarchy.getChild("price").getChildren().isEmpty());
	}

	@Test
	public void test_fromXML() throws XMLStreamException, FileNotFoundException
	{
		XMLEventReader eventReader = TestUtils.getReader(new File(hierarchy_1_filename));
		XMCDA xmcda = new XMCDA();
		TestUtils.firstStartEvent(eventReader);
		CriteriaHierarchy hierarchy = new CriteriaHierarchyParser().fromXML(xmcda, TestUtils.firstStartEvent(eventReader), eventReader);

		checkHierarchy1(hierarchy);
	}

	@Test
	public void test_XMCDAfromXML() throws XMLStreamException, IOException, SAXException
	{
		XMCDA xmcda = new XMCDAParser().readXMCDA(new XMCDA(), new File(hierarchy_1_filename));

		assertEquals(1, xmcda.criteriaHierarchiesList.size());
		checkHierarchy1(xmcda.criteriaHierarchiesList.get(0));
	}

	@Test
	public void test_XMCDAtoXML() throws XMLStreamException, IOException, SAXException
	{
		// to test it, we write & load a XMCDA object with a criteriaHierarchy, and make sure it is the same
		XMCDA xmcda = getHierarchy_1();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(8192); // arbitrary size, large enough

		new XMCDAParser().writeXMCDA(xmcda, outputStream, CriteriaHierarchyParser.CRITERIA_HIERARCHY);
		XMCDA readXMCDA = new XMCDAParser().readXMCDA(new XMCDA(), new ByteArrayInputStream(outputStream.toByteArray()), null);

		assertEquals(1, readXMCDA.criteriaHierarchiesList.size());
		checkHierarchy1(readXMCDA.criteriaHierarchiesList.get(0));
	}

}
