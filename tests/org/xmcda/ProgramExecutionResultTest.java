package org.xmcda;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class ProgramExecutionResultTest
{
	ProgramExecutionResult p = null;

	@Before
	public void init()
	{
		p = new ProgramExecutionResult();
	}

	@org.junit.Test
	public void getStatus() throws Exception
	{
		Assert.assertEquals(ProgramExecutionResult.Status.OK, p.getStatus());
	}

	@Test
	public void updateStatus() throws Exception
	{
		// Prg exec. result's status escalades from OK to warning then ERROR...
		p.addDebug("debug msg");
		Assert.assertEquals(ProgramExecutionResult.Status.OK, p.getStatus());

		p.addInfo("info msg");
		Assert.assertEquals(ProgramExecutionResult.Status.OK, p.getStatus());

		p.addWarning("warning msg");
		Assert.assertEquals(ProgramExecutionResult.Status.WARNING, p.getStatus());

		p.addInfo("info msg");
		Assert.assertEquals(ProgramExecutionResult.Status.WARNING, p.getStatus());

		p.addError("error msg");
		Assert.assertEquals(ProgramExecutionResult.Status.ERROR, p.getStatus());

		// Once the Status is ERROR,
		p.addWarning("warning msg");
		Assert.assertEquals(ProgramExecutionResult.Status.ERROR, p.getStatus());

		p.addInfo("info msg");
		Assert.assertEquals(ProgramExecutionResult.Status.ERROR, p.getStatus());

		p.addDebug("debug msg");
		Assert.assertEquals(ProgramExecutionResult.Status.ERROR, p.getStatus());
	}

	@Test
	public void forceStatus() throws Exception
	{

	}

	@Test
	public void addMessage() throws Exception
	{

	}

	@Test
	public void addDebug() throws Exception
	{

	}

	@Test
	public void addInfo() throws Exception
	{

	}

	@Test
	public void addWarning() throws Exception
	{

	}

	@Test
	public void addError() throws Exception
	{

	}

}
