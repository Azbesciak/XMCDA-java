package org.xmcda;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.xmcda.utils.ValueConverters.ConversionException;
import org.xmcda.value.Rational;

public class Test_AlternativesMatrix
{
	AlternativesMatrix<Object> matrix;
	Alternative a1, a2, a3, a4;

	@Before
	public void setUp() throws Exception
	{
		a1 = new Alternative("a1");
		a2 = new Alternative("a2");
		matrix = new AlternativesMatrix<>();
		matrix.put(a1, a1, new QualifiedValue<>("11"));
		matrix.put(a1, a2, new QualifiedValue<>(12));
		matrix.put(a2, a1, new QualifiedValue<>(2.1));
	}
	
	@Test
	public void testCheckConversion() throws ConversionException
	{
		matrix.checkConversion(Double.class);
		matrix.checkConversion(String.class);
		// doesn't how to to convert to integer, yet
		assertThrows(ConversionException.class, ()->matrix.checkConversion(Integer.class));
		
		matrix.put(a2, a2, new QualifiedValue<>("invalid double"));
		assertThrows(ConversionException.class, ()->matrix.checkConversion(Double.class));
		matrix.checkConversion(String.class);

		QualifiedValue<Rational> r = new QualifiedValue<>(new Rational(1,0));
		matrix.put(a2, a2, new QualifiedValue<>(r));
		assertThrows(ConversionException.class, () -> matrix.checkConversion(Double.class));
	}

	@Test
	public void testConvertToDouble() throws ConversionException
	{
		matrix.convertTo(Double.class);
		assertEquals(11., matrix.get(a1, a1).get(0).getValue());
		assertEquals(12., matrix.get(a1, a2).get(0).getValue());
		assertEquals(2.1, matrix.get(a2, a1).get(0).getValue());
	}

	@Test
	public void testConvertRationalToDouble() throws ConversionException
	{
		Rational r = new Rational(1,0);
		matrix.put(a2, a2, r);
		assertThrows(ConversionException.class, () -> matrix.convertTo(Double.class));

		r.setDenominator(2);
		matrix.convertTo(Double.class);
		assertEquals(11., matrix.get(a1, a1).get(0).getValue());
		assertEquals(12., matrix.get(a1, a2).get(0).getValue());
		assertEquals(2.1, matrix.get(a2, a1).get(0).getValue());
		assertEquals(0.5, matrix.get(a2, a2).get(0).getValue());
	}

	@Test
	public void testConvertToString() throws ConversionException
	{
		matrix.convertTo(String.class);
		assertEquals("11", matrix.get(a1, a1).get(0).getValue());
		assertEquals("12", matrix.get(a1, a2).get(0).getValue());
		assertEquals("2.1", matrix.get(a2, a1).get(0).getValue());
	}

}
