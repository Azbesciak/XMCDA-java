package org.xmcda;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class Test_Alternatives
{
	Alternatives alternatives = null;

	@Before
	public void init()
	{
		alternatives = new Alternatives();
	}

	@SuppressWarnings("static-access")
	@Test
	public void test_TAG()
	{
		assertEquals("alternatives", Alternatives.TAG);
		assertEquals("alternatives", alternatives.TAG);
	}

}
