#! /bin/bash
. test_base.sh


idx=0
failed=()

function T
{
    local _ret
    test_file "$@"
    _ret=$?
    if [ ${_ret} -ne 0 ]; then
        failed[${idx}]="$*"
        ((idx++))
    fi
    return ${_ret}
}

T alternatives-1.in.v2.xml
T alternatives-2.in.v3.xml

T alternativesComparisons-1.in.v2.xml
T alternativesComparisons-1.qualitative.rankedLabel.in.v2.xml
T alternativesComparisons-2.qualitative.fuzzyLabel.in.v2.xml
T alternativesComparisons-1.in.v3.xml

T alternativesComparisons-3.no_value.in.v2.xml
T alternativesComparisons-3.no_value.in.v3.xml

T alternativesSets-1.in.v2.xml
T alternativesSets-1.in.v3.xml
T alternativesSets-2.in.v2.xml
T alternativesSets-2.in.v3.xml

T categories-1.in.v2.xml
T categories-2.in.v3.xml

T categoriesSets-1.in.v2.xml
T categoriesSets-1.in.v3.xml
T categoriesSets-2.in.v2.xml
T categoriesSets-2.in.v3.xml

T criteria-1.in.v2.xml
T criteria-1.in.v3.xml

# these test criteriaComparisons AND scale v3->v2

T criteriaComparisons-criteriaMatrix-1.scale.qualitative.rankedLabel.in.v2.xml
T criteriaComparisons-criteriaMatrix-2.scale.qualitative.fuzzyLabel.in.v2.xml
T criteriaComparisons-criteriaMatrix-3.scale.quantitative.in.v2.xml
T criteriaComparisons-criteriaMatrix-4.scale.nominal.in.v2.xml

# TODO option spéciale
# no scale
#T criteriaMatrix-1_to_criteriaComparisons.in.v3.xml

#
T criteriaMatrix-criteriaComparisons-1.scale.qualitative.rankedLabel.in.v3.xml
T criteriaMatrix-criteriaComparisons-2.scale.qualitative.fuzzyLabel.in.v3.xml
T criteriaMatrix-criteriaComparisons-3.scale.quantitative.in.v3.xml
T criteriaMatrix-criteriaComparisons-4.scale.nominal.in.v3.xml
T criteriaComparisons-criteriaMatrix-6.in.v2.xml


## compare v2 -> v3 -> v2 for criteriaComparisons
# tkdiff criteriaComparisons-1.scale.qualitative.rankedLabel.in.v2.xml criteriaMatrix-1.scale.qualitative.rankedLabel.out.v2.xml
# tkdiff criteriaComparisons-2.scale.qualitative.fuzzyLabel.in.v2.xml criteriaMatrix-2.scale.qualitative.fuzzyLabel.out.v2.xml
# tkdiff criteriaComparisons-3.scale.quantitative.in.v2.xml criteriaMatrix-3.scale.quantitative.out.v2.xml
# tkdiff criteriaComparisons-4.scale.nominal.in.v2.xml criteriaMatrix-4.scale.nominal.out.v2.xml

# criteriaComparisons w/ criteriaSets(Ids) v2 -> v3 criteriaSetsMatrices
T criteriaComparisons-criteriaMatrix-5.criteriaSets.in.v2.xml

T criteriaSetsMatrix-criteriaComparisons-5.in.v3.xml
T criteriaComparisons-criteriaSetsMatrix-5.in.v2.xml

T criteriaSetsMatrix-criteriaComparisons.in.v3.xml


# criteriaSets
T criteriaSets-1.in.v2.xml
T criteriaSets-1.in.v3.xml
T criteriaSets-2.in.v2.xml
T criteriaSets-2.in.v3.xml

# criterionValue
T criterionValue-1.in.v2.xml

# criteriaValues
T criteriaValues-1.in.v2.xml
T criteriaValues-1.in.v3.xml

# criteriaValues v2 / criteriaSetsValues v3
T criteriaValues-2.in.v2.xml
T criteriaValues-2.in.v3.xml # fail

T criteriaValues-3.in.v2.xml

# criteriaFunctions
T criteria_functions-1.in.v2.xml
T criteria_functions-1.in.v3.xml

T performanceTable-1.in.v2.xml
T performanceTable-2.in.v3.xml

# criteriaMatrix
T criteriaMatrix-1.in.v2.xml
T criteriaMatrix-1.in.v3.xml

# methodParameters <-> programParameters
T methodParameters-1.in.v2.xml
T methodParameters-1.in.v3.xml

# methodMessages
T methodMessages-1.in.v2.xml
T methodMessages-1.in.v3.xml
T methodParameters-2.empty_label.in.v3.xml

# criteria: functions
T criteria_functions-1.in.v2.xml
T criteria_functions-1.in.v3.xml

# criteria: thresholds
T criteria_thresholds-1.in.v2.xml
T criteria_thresholds-1.in.v3.xml

# criteria: scales
T criteria_scales-1.in.v2.xml
T criteria_scales-1.in.v3.xml

# alternativesValues
T alternativesValues-1.in.v2.xml
T alternativesValues-1.in.v3.xml

# categoriesValues
T categoriesValues-1.in.v2.xml
T categoriesValues-1.in.v3.xml

# categoriesProfiles
T categoriesProfiles-1.in.v2.xml
T categoriesProfiles-1.in.v3.xml

# criteriaHierarchy
T criteriaHierarchy-1.in.v2.xml
T criteriaHierarchy-1.in.v3.xml

# criteriaSetsHierarchy
T criteriaSetsHierarchy-1.in.v2.xml
T criteriaSetsHierarchy-2.in.v2.xml
T criteriaSetsHierarchy-1.in.v3.xml

if [ ${idx} -ne 0 ]; then
    echo "# failed tests: "
    for failed in "${failed[@]}"; do
        echo "${failed}"
    done
    exit 1
else
    echo "# All tests passed"
fi

exit 0


# instructions to use nailgun

XMCDA_JAVA_DIR=/home/TB/projects/mcda/xmcda/XMCDA-java
NAILGUN_DIR=/home/TB/projects/mcda/xmcda/XMCDA-java/tmp/nailgun
NG=${NAILGUN_DIR}/ng
# default is 2113
PORT=2114

# launching the server
java -cp ${NAILGUN_DIR}/nailgun-server/target/nailgun-server-0.9.3-SNAPSHOT.jar:${XMCDA_JAVA_DIR}/dist/XMCDA-java.latest.jar com.martiansoftware.nailgun.NGServer 127.0.0.1:${PORT:?}

# command-line
${NG} --nailgun-port ${PORT:?} org.xmcda.converters.v2_v3.XMCDAConverter --v2 "${infiles[@]}" --v3 "${outfiles[@]}" --no-values
