#! /bin/bash

# check validity of the xml files
ret=0

SCHEMA_DIR="${0%/*}/../../../xsd"
for f in *.xml; do
    version=$(grep -E 'xmlns:xmcda' "$f" | sed -E 's#.*xmlns:xmcda="http://www.decision-deck.org/20[0-9][0-9]/XMCDA-([.0-9]*)".*$#\1#')
    if ! xmllint --noout \
               --schema "${SCHEMA_DIR}/XMCDA-${version}.xsd" \
               "${f}" >/dev/null 2>&1
    then
        echo "validation failed: $f "
        ((ret++))
    fi
done
exit $ret
