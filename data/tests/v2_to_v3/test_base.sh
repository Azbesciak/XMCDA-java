#! /bin/bash

XMCDA_JAVA_PATH=/home/TB/projects/mcda/xmcda/XMCDA-java

RM=/bin/rm
JAVA=/usr/local/java8/bin/java


XMCDA_JAVA_DIR=/home/TB/projects/mcda/xmcda/XMCDA-java
NAILGUN_DIR=/home/TB/projects/mcda/xmcda/XMCDA-java/tmp/nailgun
NG=${NAILGUN_DIR}/ng

# launch the server
# java -cp ${NAILGUN_DIR}/nailgun-server/target/nailgun-server-0.9.3-SNAPSHOT.jar:${XMCDA_JAVA_DIR}/dist/XMCDA-java.latest.jar com.martiansoftware.nailgun.NGServer 127.0.0.1

CMD="${NG} --nailgun-port ${PORT:-2113} org.xmcda.converters.v2_v3.XMCDAConverter " #--v2 "${infiles[@]}" --v3 "${outfiles[@]}"' #--no-values

# Pure java
#CMD="$JAVA -classpath ${XMCDA_JAVA_PATH}/bin:${XMCDA_JAVA_PATH} org.xmcda.converters.v2_v3.XMCDAConverter"

function test_file()
{
  local f opt1 opt2 f_name test_file ref_file ret
  f=$1
  shift

  f=${f#\./}
  opt1=${f%\.xml}
  opt1=${opt1##*\.}
  if [ "$opt1" = "v2" ]; then
      opt2="v3"
  else
      opt2="v2"
  fi
  f_name=${f%\.in*}

  #echo
  #echo --$opt1 $f --$opt2 $f_name.test.$opt2.xml

  test_file="$f_name.test.$opt2.xml"
  ref_file="$f_name.out.$opt2.xml"
  $CMD "$@" --$opt1 "$f" --$opt2 "$test_file"

  xmllint --c14n "${ref_file}"  > "${ref_file}.c14n"
  xmllint --c14n "${test_file}" > "${test_file}.c14n"
  diff -q "${ref_file}.c14n" "${test_file}.c14n"
  ret=$?
  if [ $ret -ne 0 ]; then
      echo FAILED: --$opt1 $f --$opt2 $test_file
      diff -u "${ref_file}.c14n" "${test_file}.c14n"
      echo
  else
      echo SUCCESS: --$opt1 $f --$opt2 $test_file
  fi
  $RM "${test_file}" "${ref_file}".c14n "${test_file}".c14n
  echo; echo
  echo '---------------------------------------'
  echo; echo
  return $ret
}
