#! /bin/bash

source test_base.sh

find . -name '[a-z]*.in.*.xml' | while read f; do
  test_file "$f"
done
