0.8
---

- Add a way to check if a conversion can be successful without
  modifiying the values to QualifiedValue, QualifiedValues and Matrix

- Matrix has learned how to convert its values

- Matrix can now tell if it isNumeric()

- Make sure ValueConverters.convetr() embed any RuntimeException into a
  ConversionException

- Add conversion from int, double, boolean and Rational to String

- Fix ValueConverter's conversion of a Rational to a Double

0.7
---

- XMCDA Converter CLI:

  + Add the verbose flag (-v), showing read/write errors in details

  + Add the --use-value-if-possible: when converting to XMCDA v2,
    single <value> are not embedded in the tag <values>

  + bugfix: When converting multiple files at a time, a conversion error
    on a file (for example, happening on an invalid input file) do not
    stop the process abruptly anymore, possibly leaving some file not
    converted even when they can be converted.


- Fix writeXMCDA() which could fail when using some StAX API
  implementation, such as Woodstox.

- Fix the v2/v3 conversion with --input-tags-only
  The fix is for the tags:
  + v2: alternativesValues, criteriaComparisons, criteriaValues
  + v3: criteriaSetsMatrix, criteriaSetsValues.
  
- Add the conversion of XMCDAv2 criterionValue to v3 criteriaValues

0.6
---

- XMCDA v2 parser/writer refactored.
  Note that the environment variable XMCDA_lib_writes_v2_VERSION is
  replaced by XMCDAv2_VERSION.

- Add the ability to write XMCDA v3 using a specific schema.  It can
  be done programmatically or by setting the environment variable
  XMCDAv3_VERSION

0.5.1
-----

- Fix: included XML Schema XMCDA v3.1.1 was incorrect

0.5
---

- XMCDA v3 to v2 conversion of <programExecutionResult> now adds by
  default a <logMessage> or a <errorMessage> to <methodMessages>.

- Add support for XMCDA v3.1.1

- Add support for XMCDA v2.2.2


0.4
---
- Add support for XMCDA v3.1.0: criteriaHierarchy and criteriaSetsHierarchy

- Add the conversion between categoryComparison (v2) and
  categoriesMatrix (v3)

- Fix: v2: do not write invalid empty <values/> anymore in xxxSets and
  xxxComparisons

- Fix: v2 criteriaSets were not converted to v3

- reading and writing XMCDA files has been optimised, especially when
  multiple files need to be be read or written.

- XMCDAConverter: can now convert multiple files at once.  This allows
  to speed up the conversion when a lot of files must be translated,
  since the time overhead relative to the launching of the JVM does
  not accumulate anymore.

- Fix: performance tables do not lose their ids anymore when converted
  from v2 to v3


0.3.0
-----

- It is now possible to export to the desired v2.x.y version; see
  commit 15d8d1b4b8b29db43842bf0e694f9780762fe60d for details.

- When exporting to XMCDA v2, all <value> tags are now always embedded
  in the (optional) <values> tag.

- Added the XMCDA v2/v3 conversion for categorieValues.

- Added support for XMCDA v2.0.0, v2.1.0, v2.2.0, v2.2.2
  and v3.0.1, v3.0.2

- categoriesProfile bounds, categoriesInterval and interval: at least
  one lower bound or a upper bound is now required.

- Referenceable now has a new field 'marker', set by default on creation.

  This allows to mark any referenceable object, namely: alternative,
  criterion, category, alternativesSet, criteriaSet and categoriesSet
  etc. at their creation time.

  All details, including a use-case and the corresponding code, can be
  found in the commit 434c79d7892e0eb66011c28cf4be9fdb68e9a455
  message.

- lots of packages renamed
    + org.xmcda.converters.v2_2_1_v3_0 -> org.xmcda.converters.v2_v3
    + org.xmcda.v2_2_1 → org.xmcda.v2
    + org.xmcda.parsers.xml.xmcda_2_2_1 t→ org.xmcda.parsers.xml.xmcda_v2
    + org.xmcda.parsers.xml.xmcda_3_0 → org.xmcda.parsers.xml.xmcda_v3


0.2.0
-----
- ProgramExecutionResult.Status.exitStatus() semantic changes: it now
  returns 0 for both OK and WARNING status, 1 for ERROR and 2 for
  TERMINATED.
  This allows programs to use it as their own return status,
  considering that both OK and WARNING status indicates a valid
  execution.


0.1.0
-----
First public release.
