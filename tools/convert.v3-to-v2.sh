#! /bin/bash
if [ $# -ne 2 ]; then
    echo "Usage: $0 xmcda_v3.xml xmcda_v2.xml" >&2
    exit 1
fi
script_dir="${0%/*}"  # yes: we know this does not work universally
XMCDA_lib="${script_dir}/../dist/XMCDA-java-latest.jar"
java -cp "${XMCDA_lib}" \
    org.xmcda.converters.v2_v3.XMCDAConverter \
    --v3 "$1" --v2 "$2"
