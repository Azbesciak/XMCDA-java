
import os, time
script_dir=os.path.dirname(os.path.abspath(__file__))

def code_for_marker(marker):
    "Returns the code associated to this marker"
    f = os.path.join(script_dir, 'data', marker+'.code')
    return open(f).read()

markers=( 'Referenceable', 'CommonAttributes', 'HasDescription')
markers_code = { marker: code_for_marker(marker) for marker in markers }

import sys

filename=sys.argv[1]
f=open(filename)
new_f=open(filename+'.generated', 'w')

c=f.readlines()
f.close()

def marker_start(m):
    return '// %s (start)'%m

def marker_end(m):
    return '// %s (end)'%m

def handle_marker(c, marker, code, indent):
    line = c.pop(0)
    end=marker_end(marker)

    # ignore everything up to the end marker
    while end not in line:
        line = c.pop(0)

    # write content
    new_f.write(indent + marker_start(marker) + '\n')
    code = markers_code[marker]
    code = code.replace('@Generated',
                        '@Generated(value="tools/generate_common_code.py", date="%s")'%time.strftime("%Y-%m-%dT%H:%M:%S"))

    for line in code.split('\n'):
        if line.strip():
            new_f.write(indent + line + '\n')
        else:
            new_f.write('\n')

    new_f.write(indent + end + '\n')

while c:
    line = c.pop(0)
    for marker in markers_code.keys():
        idx = line.find(marker_start(marker))
        if idx != -1:
            handle_marker(c, marker, markers_code[marker], line[:idx])
            line = None
            break
    if line:
        new_f.write(line)

new_f.close()
import os
os.rename(filename+'.generated', filename)
