package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaThresholds;
import org.xmcda.Criterion;
import org.xmcda.CriterionThresholds;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaThresholdsParser
{
	public static final String CRITERIA_THRESHOLDS = "criteriaThresholds";

	public CriteriaThresholds fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CriteriaThresholds thresholds = Factory.criteriaThresholds();
		new CommonAttributesParser().handleAttributes(thresholds, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERIA_THRESHOLDS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				thresholds.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CriterionThresholdsParser.CRITERION_THRESHOLD.equals(startElement.getName().getLocalPart()))
			    new CriterionThresholdsParser().fromXML(xmcda, thresholds, startElement, eventReader);
		}
		return thresholds;

	}

	public void toXML(List<CriteriaThresholds> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CriteriaThresholds criteriaThresholds: list)
			toXML(criteriaThresholds, writer);
	}

	public void toXML(CriteriaThresholds criteriaThresholds, XMLStreamWriter writer) throws XMLStreamException
	{
		if (criteriaThresholds == null)
		    return;

		writer.writeStartElement(CRITERIA_THRESHOLDS);
		new CommonAttributesParser().toXML(criteriaThresholds, writer);
		writer.writeln();

		new DescriptionParser().toXML(criteriaThresholds.getDescription(), writer);

		for (Entry<Criterion, CriterionThresholds> entry: criteriaThresholds.entrySet())
			new CriterionThresholdsParser().toXML(entry.getKey(), entry.getValue(), writer);

		writer.writeEndElement();
		writer.writeln();
	}

}
