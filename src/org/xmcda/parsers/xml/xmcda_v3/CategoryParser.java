package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Category;
import org.xmcda.CommonAttributes;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CategoryParser
{
	public static final String CATEGORY = "category";

	public static final String ACTIVE   = "active";

	public Category fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		CommonAttributes attribueHolder = new ReferenceableParser().getAttributes(startElement);
		Category category = xmcda.categories.get(attribueHolder.id());
		category.setName(attribueHolder.name());
		category.setMcdaConcept(attribueHolder.mcdaConcept());

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			// description
			// type
			// active
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CATEGORY.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				category.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (ACTIVE.equals(startElement.getName().getLocalPart()))
			{
				event = eventReader.nextEvent();
				final String active = event.asCharacters().getData();
				category.setIsActive("true".equals(active) || "1".equals(active));
				continue;
			}
		}
		return category;
	}

	public void toXML(Category category, XMLStreamWriter writer) throws XMLStreamException
	{
		if (category == null)
		    return; // TODO normal ça?

		writer.writeStartElement(CATEGORY);
		new ReferenceableParser().toXML(category, writer);
		writer.writeln();
		new DescriptionParser().toXML(category.getDescription(), writer);
		writer.writeElementBoolean(ACTIVE, category.isActive());
		writer.writeEndElement();
		writer.writeln();
	}
}
