/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.QualifiedValue;
import org.xmcda.XMCDA;
import org.xmcda.value.Rational;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * @author big
 */
public class RationalParser
{
	public static final String RATIONAL    = QualifiedValue.XMCDATypes.RATIONAL.getTag();

	public static final String NUMERATOR   = "numerator";

	public static final String DENOMINATOR = "denominator";

	public Rational fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Rational rational = new Rational();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement() && RATIONAL.equals(event.asEndElement().getName().getLocalPart()))
				break;

			if (! event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (NUMERATOR.equals(startElement.getName().getLocalPart()))
			{
				rational.setNumerator(Integer.parseInt(Utils.getTextContent(startElement, eventReader)));
				continue;
			}
			if (DENOMINATOR.equals(startElement.getName().getLocalPart()))
			{
				rational.setDenominator(Integer.parseInt(Utils.getTextContent(startElement, eventReader)));
				continue;
			}
		}
		return rational;
	}

	public void toXML(Rational rational, XMLStreamWriter writer) throws XMLStreamException
	{
		if (rational == null)
		    return;
		writer.writeStartElement(RATIONAL);
		writer.writeln();
		writer.writeElementChars(NUMERATOR, "" + rational.getNumerator());
		writer.writeElementChars(DENOMINATOR, "" + rational.getDenominator());
		writer.writeEndElement();
		writer.writeln();

	}

}
