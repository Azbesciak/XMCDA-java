/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;

/**
 * @author big
 */
public class XMLStreamWriter
    implements javax.xml.stream.XMLStreamWriter
{
	private javax.xml.stream.XMLStreamWriter writer;

	private int                              indent_level = 0;

	private String                           indent_str   = "\t";

	private boolean                          mayIndent    = true;

	public XMLStreamWriter(javax.xml.stream.XMLStreamWriter proxiedXMLStreamWriter)
	{
		writer = proxiedXMLStreamWriter;
	}

	public void increaseIndentLevel()
	{
		indent_level += 1;
	}

	public void decreaseIndentLevel()
	{
		if (indent_level > 0)
		    indent_level -= 1;
	}

	protected String indentation()
	{
		if (indent_level == 0)
		    return "";
		final StringBuffer sb = new StringBuffer(indent_level * indent_str.length());
		for (int i = 0; i < indent_level; i++)
			sb.append(indent_str);
		return sb.toString();
	}

	public void writeIndentation() throws XMLStreamException
	{
		if (mayIndent)
		    writeCharacters(indentation());
		mayIndent = false;
	}

	public void writeNonNullAttribute(String localName, String value) throws XMLStreamException
	{
		if (value != null)
		    writeAttribute(localName, value);
	}

	public void writeElementChars(String localName, String string) throws XMLStreamException
	{
		writeStartElement(localName);
		writeCharacters(string);
		writeEndElement();
		writeln();
	}

	public void writeElementBoolean(String localName, boolean value) throws XMLStreamException
	{
		writeStartElement(localName);
		if (value)
			writeCharacters("true");
		else
			writeCharacters("false");
		writeEndElement();
		writeln();
	}

	public void writeln() throws XMLStreamException
	{
		writeCharacters("\n");
		mayIndent = true;
	}


	/* proxied methods follows */

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String)
	 */
	@Override
	public void writeStartElement(String localName) throws XMLStreamException
	{
		this.writeIndentation();
		writer.writeStartElement(localName);
		increaseIndentLevel();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException
	{
		this.writeIndentation();
		writer.writeStartElement(namespaceURI, localName);
		increaseIndentLevel();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void writeStartElement(String prefix, String localName, String namespaceURI) throws XMLStreamException
	{
		this.writeIndentation();
		writer.writeStartElement(prefix, localName, namespaceURI);
		this.increaseIndentLevel();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeEmptyElement(String namespaceURI, String localName) throws XMLStreamException
	{
		this.writeIndentation();
		writer.writeEmptyElement(namespaceURI, localName);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void writeEmptyElement(String prefix, String localName, String namespaceURI) throws XMLStreamException
	{
		this.writeIndentation();
		writer.writeEmptyElement(prefix, localName, namespaceURI);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String)
	 */
	@Override
	public void writeEmptyElement(String localName) throws XMLStreamException
	{
		this.writeIndentation();
		writer.writeEmptyElement(localName);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeEndElement()
	 */
	@Override
	public void writeEndElement() throws XMLStreamException
	{
		this.decreaseIndentLevel();
		this.writeIndentation();
		writer.writeEndElement();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeEndDocument()
	 */
	@Override
	public void writeEndDocument() throws XMLStreamException
	{
		writer.writeEndDocument();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#close()
	 */
	@Override
	public void close() throws XMLStreamException
	{
		writer.close();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#flush()
	 */
	@Override
	public void flush() throws XMLStreamException
	{
		writer.flush();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeAttribute(String localName, String value) throws XMLStreamException
	{
		writer.writeAttribute(localName, value);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void writeAttribute(String prefix, String namespaceURI, String localName, String value)
	    throws XMLStreamException
	{
		writer.writeAttribute(prefix, namespaceURI, localName, value);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void writeAttribute(String namespaceURI, String localName, String value) throws XMLStreamException
	{
		writer.writeAttribute(namespaceURI, localName, value);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeNamespace(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeNamespace(String prefix, String namespaceURI) throws XMLStreamException
	{
		writer.writeNamespace(prefix, namespaceURI);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeDefaultNamespace(java.lang.String)
	 */
	@Override
	public void writeDefaultNamespace(String namespaceURI) throws XMLStreamException
	{
		writer.writeDefaultNamespace(namespaceURI);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeComment(java.lang.String)
	 */
	@Override
	public void writeComment(String data) throws XMLStreamException
	{
		writer.writeComment(data);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeProcessingInstruction(java.lang.String)
	 */
	@Override
	public void writeProcessingInstruction(String target) throws XMLStreamException
	{
		writer.writeProcessingInstruction(target);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeProcessingInstruction(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeProcessingInstruction(String target, String data) throws XMLStreamException
	{
		writer.writeProcessingInstruction(target, data);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeCData(java.lang.String)
	 */
	@Override
	public void writeCData(String data) throws XMLStreamException
	{
		writer.writeCData(data);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeDTD(java.lang.String)
	 */
	@Override
	public void writeDTD(String dtd) throws XMLStreamException
	{
		writer.writeDTD(dtd);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeEntityRef(java.lang.String)
	 */
	@Override
	public void writeEntityRef(String name) throws XMLStreamException
	{
		writer.writeEntityRef(name);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeStartDocument()
	 */
	@Override
	public void writeStartDocument() throws XMLStreamException
	{
		writer.writeStartDocument();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeStartDocument(java.lang.String)
	 */
	@Override
	public void writeStartDocument(String version) throws XMLStreamException
	{
		writer.writeStartDocument(version);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeStartDocument(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeStartDocument(String encoding, String version) throws XMLStreamException
	{
		writer.writeStartDocument(encoding, version);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeCharacters(java.lang.String)
	 */
	@Override
	public void writeCharacters(String text) throws XMLStreamException
	{
		writer.writeCharacters(text);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#writeCharacters(char[], int, int)
	 */
	@Override
	public void writeCharacters(char[] text, int start, int len) throws XMLStreamException
	{
		writer.writeCharacters(text, start, len);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#getPrefix(java.lang.String)
	 */
	@Override
	public String getPrefix(String uri) throws XMLStreamException
	{
		return writer.getPrefix(uri);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#setPrefix(java.lang.String, java.lang.String)
	 */
	@Override
	public void setPrefix(String prefix, String uri) throws XMLStreamException
	{
		writer.setPrefix(prefix, uri);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#setDefaultNamespace(java.lang.String)
	 */
	@Override
	public void setDefaultNamespace(String uri) throws XMLStreamException
	{
		writer.setDefaultNamespace(uri);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#setNamespaceContext(javax.xml.namespace.NamespaceContext)
	 */
	@Override
	public void setNamespaceContext(NamespaceContext context) throws XMLStreamException
	{
		writer.setNamespaceContext(context);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#getNamespaceContext()
	 */
	@Override
	public NamespaceContext getNamespaceContext()
	{
		return writer.getNamespaceContext();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.stream.XMLStreamWriter#getProperty(java.lang.String)
	 */
	@Override
	public Object getProperty(String name) throws IllegalArgumentException
	{
		return writer.getProperty(name);
	}

}
