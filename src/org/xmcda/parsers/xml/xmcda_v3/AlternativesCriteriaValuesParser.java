package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Alternative;
import org.xmcda.AlternativesCriteriaValues;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesCriteriaValuesParser<VALUE_TYPE>
{
	public static final String ALTERNATIVES_CRITERIA_VALUES = AlternativesCriteriaValues.TAG;
	public static final String ALTERNATIVE_CRITERIA_VALUES = "alternativeCriteriaValues";
	public static final String ALTERNATIVE_ID = "alternativeID";

	public AlternativesCriteriaValues<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		AlternativesCriteriaValues<VALUE_TYPE> acvs = Factory.alternativesCriteriaValues();
		new CommonAttributesParser().handleAttributes(acvs, startElement);
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVES_CRITERIA_VALUES.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			
			if ( ! event.isStartElement())
				continue;
			
			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (DescriptionParser.DESCRIPTION.equals(currentTagName))
			{
				acvs.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			else if (ALTERNATIVE_CRITERIA_VALUES.equals(currentTagName))
			{
				fromXML(xmcda, acvs, startElement, eventReader);
			}

		}
		return acvs;
	}

	public void fromXML(XMCDA xmcda, AlternativesCriteriaValues<VALUE_TYPE> acvs, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Alternative alternative = null;
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVE_CRITERIA_VALUES.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			
			if ( ! event.isStartElement())
				continue;
			
			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (ALTERNATIVE_ID.equals(currentTagName))
			{
				alternative = xmcda.alternatives.get(Utils.getTextContent(startElement, eventReader));
			}
			else if (CriteriaValuesParser.CRITERIA_VALUES.equals(currentTagName))
			{
				acvs.put(alternative, new CriteriaValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}
		}
	}

	public void toXML(List<AlternativesCriteriaValues<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (AlternativesCriteriaValues<VALUE_TYPE> acvs: list)
			toXML(acvs, writer);
	}

	public void toXML(AlternativesCriteriaValues<VALUE_TYPE> acvs, XMLStreamWriter writer) throws XMLStreamException
	{
		if ( acvs == null )
			return;
		writer.writeStartElement(ALTERNATIVES_CRITERIA_VALUES);
		new CommonAttributesParser().toXML(acvs, writer);
		writer.writeln();
		new DescriptionParser().toXML(acvs.getDescription(), writer);

		for (Alternative alternative: acvs.keySet())
		{
			writer.writeStartElement(ALTERNATIVE_CRITERIA_VALUES);
			writer.writeln();

			writer.writeElementChars(ALTERNATIVE_ID, alternative.id());
			new CriteriaValuesParser<VALUE_TYPE>().toXML(acvs.get(alternative), writer);
			writer.writeEndElement(); // ALTERNATIVE_CRITERIA_VALUES
			writer.writeln();
		}
		
		writer.writeEndElement(); // ALTERNATIVES_CRITERIA_VALUES
		writer.writeln();

	}
}
