package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaSet;
import org.xmcda.CriteriaSets;
import org.xmcda.XMCDA;

public class CriteriaSetsParser
{
	public static final String CRITERIA_SETS = "criteriaSets";

	public String rootTag()
	{
		return CRITERIA_SETS;
	}

	public String elementTag()
	{
		return CriteriaSetParser.CRITERIA_SET;
	}

	public CriteriaSetParser setParser()
	{
		return new CriteriaSetParser();
	}

	public void fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CriteriaSets<?> sets = xmcda.criteriaSets;
		new CommonAttributesParser().handleAttributes(sets, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (rootTag().equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				sets.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (elementTag().equals(startElement.getName().getLocalPart()))
			    sets.add(setParser().fromXML(xmcda, startElement, eventReader));
		}
	}

	public <VALUE_TYPE> void toXML(CriteriaSets<VALUE_TYPE> sets, XMLStreamWriter writer) throws XMLStreamException
	{
		if (sets == null)
		    return;
		if (sets.isVoid())
			return;
		
		writer.writeStartElement(rootTag());
		new CommonAttributesParser().toXML(sets, writer);
		writer.writeln();

		new DescriptionParser().toXML(sets.getDescription(), writer);

		for (CriteriaSet<VALUE_TYPE> set: sets)
			new CriteriaSetParser<VALUE_TYPE>().toXML(set, writer);

		writer.writeEndElement();
		writer.writeln();
	}

}
