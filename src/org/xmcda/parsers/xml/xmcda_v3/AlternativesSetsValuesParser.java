package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.AlternativesSet;
import org.xmcda.AlternativesSetsValues;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesSetsValuesParser <ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE>
{
	public static final String ALTERNATIVES_SETS_VALUES = "alternativesSetsValues";

	public static final String VALUES               = "values";

	public AlternativesSetsValues<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		AlternativesSetsValues<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE> values = Factory.alternativesSetsValues();
		new CommonAttributesParser().handleAttributes(values, startElement);

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			// alternativesSetFunction
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVES_SETS_VALUES.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				values.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (AlternativesSetValuesParser.ALTERNATIVES_SET_VALUE.equals(startElement.asStartElement().getName().getLocalPart()))
			{
				new AlternativesSetValuesParser<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE>().fromXML(xmcda, values, startElement, eventReader);
			}
		}
		return values;
	}

	public void toXML(List<AlternativesSetsValues<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE>> list, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (list == null || list.size() == 0)
		    return;
		for (AlternativesSetsValues<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE> values: list)
			toXML(values, writer);
	}

	public void toXML(AlternativesSetsValues<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE> values, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (values == null)
		    return; // TODO normal ça?

		writer.writeStartElement(ALTERNATIVES_SETS_VALUES);
		new CommonAttributesParser().toXML(values, writer);
		writer.writeln();
		new DescriptionParser().toXML(values.getDescription(), writer);
		for (Entry<AlternativesSet<ALTERNATIVES_SETS_VALUE_TYPE>, LabelledQValues<VALUE_TYPE>> entry: values.entrySet())
			new AlternativesSetValuesParser<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE>().toXML(entry.getKey(), entry.getValue(), writer);

		writer.writeEndElement();
		writer.writeln();
	}


}
