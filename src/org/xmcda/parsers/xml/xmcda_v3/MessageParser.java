package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Message;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class MessageParser
{
	public static final String MESSAGE = "message";
	public static final String LEVEL = "level";
	public static final String TEXT = "text";

	public Message fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		Message message = new Message();
		new CommonAttributesParser().handleAttributes(message, startElement);
		Attribute level = startElement.getAttributeByName(new QName(LEVEL));
		if (level != null)
			message.setLevel(Message.Level.valueOf(level.getValue().toUpperCase()));
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (MESSAGE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (DescriptionParser.DESCRIPTION.equals(currentTagName))
			{
				message.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (TEXT.equals(currentTagName))
			{
				message.setText(Utils.getTextContent(startElement, eventReader));
			}
		}
		return message;
	}

	public void toXML(Message message, XMLStreamWriter writer) throws XMLStreamException
	{
		if (message == null)
		    return;

		writer.writeStartElement(MESSAGE);
		new CommonAttributesParser().toXML(message, writer);
		writer.writeNonNullAttribute(LEVEL, message.getLevel().name().toLowerCase());
		writer.writeln();
		new DescriptionParser().toXML(message.getDescription(), writer);

		writer.writeElementChars(TEXT, message.getText());

		writer.writeEndElement(); // MESSAGE
		writer.writeln();
	}
}
