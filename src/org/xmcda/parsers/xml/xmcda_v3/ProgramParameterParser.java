package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Factory;
import org.xmcda.ProgramParameter;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class ProgramParameterParser <VALUE_TYPE>
{
	public static final String PROGRAM_PARAMETER = "parameter";

	public ProgramParameter<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		ProgramParameter<VALUE_TYPE> programParameter = Factory.<VALUE_TYPE> programParameter();
		new CommonAttributesParser().handleAttributes(programParameter, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (PROGRAM_PARAMETER.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (DescriptionParser.DESCRIPTION.equals(currentTagName))
			{
				programParameter.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (QualifiedValuesParser.VALUES.equals(currentTagName))
			{
				programParameter.setValues(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}
		}
		return programParameter;
	}

	public void toXML(ProgramParameter<VALUE_TYPE> programParameter, XMLStreamWriter writer) throws XMLStreamException
	{
		if (programParameter == null)
		    return;

		writer.writeStartElement(PROGRAM_PARAMETER);
		new CommonAttributesParser().toXML(programParameter, writer);
		writer.writeln();
		new DescriptionParser().toXML(programParameter.getDescription(), writer);

		new QualifiedValuesParser<VALUE_TYPE>().toXML(programParameter.getValues(), writer);

		writer.writeEndElement(); // PROGRAM_PARAMETER
		writer.writeln();
	}
}
