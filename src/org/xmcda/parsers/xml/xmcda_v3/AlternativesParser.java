package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Alternative;
import org.xmcda.Alternatives;
import org.xmcda.XMCDA;

public class AlternativesParser
{
	public static final String ALTERNATIVES = "alternatives";
	public void fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Alternatives alternatives = xmcda.alternatives;
		new CommonAttributesParser().handleAttributes(alternatives, startElement);
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVES.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			
			if ( ! event.isStartElement())
				continue;
			
			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				alternatives.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (AlternativeParser.ALTERNATIVE.equals(startElement.getName().getLocalPart()))
			{
				// TODO make sure we do not read HERE twice the same id (merge() will not detect this if project is authorized to create instances)
				alternatives.merge(new AlternativeParser().fromXML(xmcda, startElement, eventReader));
			}
		}
	}

	public void toXML(Alternatives alternatives, XMLStreamWriter writer) throws XMLStreamException
	{
		if (alternatives == null)
			return;
		if (alternatives.isVoid())
			return;

		writer.writeStartElement(ALTERNATIVES);
		new CommonAttributesParser().toXML(alternatives, writer);
		writer.writeln();
		new DescriptionParser().toXML(alternatives.getDescription(), writer);

		for (Alternative alternative: alternatives)
			new AlternativeParser().toXML(alternative, writer);
		
		writer.writeEndElement();
		writer.writeln();

	}

}
