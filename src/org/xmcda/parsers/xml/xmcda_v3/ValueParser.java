package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.QualifiedValue;
import org.xmcda.XMCDA;
import org.xmcda.value.*;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * This is the class reading and writing XMCDA 3.x tags with type {@code value}. It directly handles integers, reals,
 * labels, boolean and {@link NA} values, and delegates the other tags to their dedicated converters, namely:
 * {@link IntervalParser}, {@link RationalParser}, {@link ValuedLabelParser}and {@link FuzzyNumberParser}.
 * 
 * @author Sébastien Bigaret
 */
public class ValueParser<VALUE_TYPE>
{

	/**
	 * Extracts the value stored in the coming tag with type xmcda:value. It may be called:
	 * <ul>
	 * <li>after this tag has been read, for example, after reading an {@code <integer>} tag,</li>
	 * <li>or just after a tag expecting a value, for example after the {@code slope} tag has been read (cf. XPath:
	 * {@code //function/affine/slope} in XMCDA).
	 * <li>
	 * </ul>
	 * 
	 * @param startElement
	 *            the last start element which has been read before this method is called
	 * @param eventReader
	 *            the object parsing the xml stream
	 * @return the extracted value
	 * @throws XMLStreamException
	 */
	public VALUE_TYPE fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		final String initialTag = startElement.getName().getLocalPart();

		Object value = null;
		XMLEvent event = startElement;
		while ( eventReader.hasNext() )
		{
			if (event.isEndElement())
			    if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (value != null)
			    // already got it, just wait for the event for </value>
			    break;

			if (!event.isStartElement())
			{
				event = eventReader.nextEvent();
				continue;
			}
			// if we are called after the 
			String currentTag = event.asStartElement().getName().getLocalPart();
			if ( QualifiedValue.XMCDATypes.INTEGER.getTag().equals(currentTag) )
			{
				value = Integer.parseInt(Utils.getTextContent(startElement, eventReader));
				continue; // no break here: make sure we've read </value> before continuing
			}
			if ( QualifiedValue.XMCDATypes.REAL.getTag().equals(currentTag) )
			{
				value = Double.parseDouble(Utils.getTextContent(startElement, eventReader));
				continue;
			}
			if ( QualifiedValue.XMCDATypes.INTERVAL.getTag().equals(currentTag) )
			{
				value = new IntervalParser().fromXML(xmcda, startElement, eventReader);
				continue;
			}
			if ( QualifiedValue.XMCDATypes.RATIONAL.getTag().equals(currentTag) )
			{
				value = new RationalParser().fromXML(xmcda, startElement, eventReader);
				continue;
			}
			if ( QualifiedValue.XMCDATypes.LABEL.getTag().equals(currentTag) )
			{
				value = Utils.getTextContent(startElement, eventReader);
				if ( value == null )
					value = "";
				continue;
			}
			if ( QualifiedValue.XMCDATypes.VALUED_LABEL.getTag().equals(currentTag) )
			{
				value = new ValuedLabelParser().fromXML(xmcda, startElement, eventReader);
				continue;
			}
			if ( QualifiedValue.XMCDATypes.BOOLEAN.getTag().equals(currentTag) )
			{
				final String s = Utils.getTextContent(startElement, eventReader);
				value = s != null && ( "true".equals(s) || "1".equals(s) );
				continue;
			}
			if ( QualifiedValue.XMCDATypes.NA.getTag().equals(currentTag) )
			{
				value = org.xmcda.value.NA.na;
				continue;
			}
			if ( QualifiedValue.XMCDATypes.FUZZY_NUMBER.getTag().equals(currentTag) )
			{
				value = new FuzzyNumberParser().fromXML(xmcda, startElement, eventReader);
				continue;
			}
			// if we have not found anything one the first pass, we were called with a startElement expecting a
			// <value/>, not directly on a <value>
			event = eventReader.nextEvent();
		}
		return (VALUE_TYPE) value;
	}

	public <T> void toXML(T value, XMLStreamWriter writer) throws XMLStreamException
	{
		if (value == null)
		    return; // TODO normal ça?

		if (value instanceof Integer)
			writeInteger((Integer) value, writer);
		else if (value instanceof Double)
			writeReal((Double) value, writer);
		else if (value instanceof Float)
			writeReal((Float) value, writer);
		else if (value instanceof Interval)
			writeInterval((Interval<?>) value, writer);
		else if (value instanceof Rational)
			writeRational((Rational) value, writer);
		else if (value instanceof String)
			writeLabel((String) value, writer);
		else if (value instanceof ValuedLabel)
			writeValuedLabel((ValuedLabel<?>) value, writer);
		else if (value instanceof Boolean)
			writeBoolean((Boolean) value, writer);
		else if (value instanceof NA)
			writeNA((org.xmcda.value.NA) value, writer);
		else if (value instanceof FuzzyNumber)
			writeFuzzyNumber((FuzzyNumber<?, ?>) value, writer);
		else
			throw new RuntimeException("Unknown type: "+value.getClass());

	}

	protected void writeInteger(Integer i, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeElementChars(QualifiedValue.XMCDATypes.INTEGER.getTag(), i.toString());
	}

	protected void writeReal(Double d, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeElementChars(QualifiedValue.XMCDATypes.REAL.getTag(), d.toString());
	}
	protected void writeReal(Float f, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeElementChars(QualifiedValue.XMCDATypes.REAL.getTag(), f.toString());
	}

	protected void writeInterval(Interval<?> interval, XMLStreamWriter writer) throws XMLStreamException
	{
		new IntervalParser().toXML(interval, writer);
	}

	protected void writeRational(Rational rational, XMLStreamWriter writer) throws XMLStreamException
	{
		new RationalParser().toXML(rational, writer);
	}

	protected void writeLabel(String label, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeElementChars(QualifiedValue.XMCDATypes.LABEL.getTag(), label);
	}

	protected void writeValuedLabel(ValuedLabel<?> valuedLabel, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		new ValuedLabelParser().toXML(valuedLabel, writer);
	}

	protected void writeBoolean(Boolean bool, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeElementChars(QualifiedValue.XMCDATypes.BOOLEAN.getTag(), bool ? "true" : "false");
	}

	protected void writeNA(NA na, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeEmptyElement(QualifiedValue.XMCDATypes.NA.getTag());
		writer.writeln();
	}

	protected void writeFuzzyNumber(FuzzyNumber<?, ?> fuzzyNumber, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		new FuzzyNumberParser().toXML(fuzzyNumber, writer);
	}

}
