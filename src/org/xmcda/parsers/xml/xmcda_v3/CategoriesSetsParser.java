package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CategoriesSet;
import org.xmcda.CategoriesSets;
import org.xmcda.XMCDA;

public class CategoriesSetsParser
{
	public static final String CATEGORIES_SETS = "categoriesSets";

	public String rootTag()
	{
		return CATEGORIES_SETS;
	}

	public String elementTag()
	{
		return CategoriesSetParser.CATEGORIES_SET;
	}

	public CategoriesSetParser setParser()
	{
		return new CategoriesSetParser();
	}

	public void fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CategoriesSets<?> sets = xmcda.categoriesSets;
		new CommonAttributesParser().handleAttributes(sets, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (rootTag().equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				sets.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (elementTag().equals(startElement.getName().getLocalPart()))
			    sets.add(setParser().fromXML(xmcda, startElement, eventReader));
		}
	}

	public <VALUE_TYPE> void toXML(CategoriesSets<VALUE_TYPE> sets, XMLStreamWriter writer) throws XMLStreamException
	{
		if (sets == null)
		    return;
		if (sets.isVoid())
			return;
		
		writer.writeStartElement(rootTag());
		new CommonAttributesParser().toXML(sets, writer);
		writer.writeln();

		new DescriptionParser().toXML(sets.getDescription(), writer);

		for (CategoriesSet<VALUE_TYPE> set: sets)
			new CategoriesSetParser<VALUE_TYPE>().toXML(set, writer);

		writer.writeEndElement();
		writer.writeln();
	}

}
