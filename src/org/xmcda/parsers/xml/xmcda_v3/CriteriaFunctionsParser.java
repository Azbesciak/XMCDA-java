package org.xmcda.parsers.xml.xmcda_v3;


import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaFunctions;
import org.xmcda.Criterion;
import org.xmcda.CriterionFunctions;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaFunctionsParser
{
	public static final String CRITERIA_FUNCTIONS = CriteriaFunctions.TAG;

	public static final String CRITERION_ID       = "criterionID";

	public static final String FUNCTIONS          = "functions";

	public CriteriaFunctions fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CriteriaFunctions criteriaFunctions = Factory.criteriaFunctions();
		new CommonAttributesParser().handleAttributes(criteriaFunctions, startElement);

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			// criterionFunction
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERIA_FUNCTIONS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				criteriaFunctions.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CriterionFunctionsParser.CRITERION_FUNCTION.equals(startElement.asStartElement().getName().getLocalPart()))
			{
				new CriterionFunctionsParser().fromXML(xmcda, criteriaFunctions, startElement, eventReader);
			}
		}
		return criteriaFunctions;
	}

	public void toXML(List<CriteriaFunctions> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CriteriaFunctions criteriaFunctions: list)
			toXML(criteriaFunctions, writer);
	}

	public void toXML(CriteriaFunctions criteriaFunctions, XMLStreamWriter writer) throws XMLStreamException
    {
		if (criteriaFunctions == null)
			return; // TODO normal ça?

		writer.writeStartElement(CRITERIA_FUNCTIONS);
		new CommonAttributesParser().toXML(criteriaFunctions, writer);
		writer.writeln();
		new DescriptionParser().toXML(criteriaFunctions.getDescription(), writer);
		for (Entry<Criterion, CriterionFunctions> entry: criteriaFunctions.entrySet())
			new CriterionFunctionsParser().toXML(entry.getKey().id(), entry.getValue(), writer);

		writer.writeEndElement();
		writer.writeln();
    }

}
