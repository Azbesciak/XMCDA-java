package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Factory;
import org.xmcda.QuantitativeScale;
import org.xmcda.Scale;
import org.xmcda.Scale.PreferenceDirection;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class QuantitativeScaleParser
{
	public static final String QUANTITATIVE = "quantitative";

	public static final String MINIMUM      = "minimum";

	public static final String MAXIMUM      = "maximum";

	public QuantitativeScale<?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		QuantitativeScale scale = Factory.quantitativeScale();

		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (QUANTITATIVE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (Scale.PREFERENCE_DIRECTION.equals(startElement.getName().getLocalPart()))
			{
				final String prefDir = Utils.getTextContent(startElement, eventReader).toUpperCase();
				scale.setPreferenceDirection(PreferenceDirection.valueOf(prefDir));
			}
			else if (MINIMUM.equals(startElement.getName().getLocalPart()))
			{
				scale.setMinimum(new QualifiedValueParser().fromXML(xmcda, startElement, eventReader));
			}
			else if (MAXIMUM.equals(startElement.getName().getLocalPart()))
			{
				scale.setMaximum(new QualifiedValueParser().fromXML(xmcda, startElement, eventReader));
			}
		}
		return scale;
	}


	public void toXML(QuantitativeScale<?> scale, XMLStreamWriter writer) throws XMLStreamException
	{
		if (scale == null)
		    return;

		writer.writeStartElement(QUANTITATIVE);
		writer.writeln();
		
		writer.writeElementChars(Scale.PREFERENCE_DIRECTION, scale.getPreferenceDirection().toString().toLowerCase());
		new QualifiedValueParser().toXML(MINIMUM, scale.getMinimum(), writer);
		new QualifiedValueParser().toXML(MAXIMUM, scale.getMaximum(), writer);

		writer.writeEndElement(); // QUANTITATIVE
		writer.writeln();
	}

}
