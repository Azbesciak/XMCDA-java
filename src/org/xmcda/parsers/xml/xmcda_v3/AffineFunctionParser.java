package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.XMCDA;
import org.xmcda.value.AffineFunction;

public class AffineFunctionParser
{
	public static final String AFFINE = "affine";
	public static final String SLOPE = "slope";
	public static final String INTERCEPT = "intercept";
	
	public AffineFunction<?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		AffineFunction function = new AffineFunction();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (AFFINE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			if (SLOPE.equals(startElement.getName().getLocalPart()))
				function.setSlope(new ValueParser().fromXML(xmcda, startElement, eventReader));
			if (INTERCEPT.equals(startElement.getName().getLocalPart()))
				function.setIntercept(new ValueParser().fromXML(xmcda, startElement, eventReader));
		}
		if (!function.getSlope().getClass().equals(function.getIntercept().getClass()))
			throw new RuntimeException("AffineFunction slope and intercept must have the same type");
		return function;

	}

	public void toXML(AffineFunction<?> function, XMLStreamWriter writer) throws XMLStreamException
	{
		if (function == null)
			return;
		writer.writeStartElement(AFFINE);
		writer.writeln();

		writer.writeStartElement(SLOPE);
		writer.writeln();
		new ValueParser().toXML(function.getSlope(), writer);
		writer.writeEndElement();
		writer.writeln();
		
		writer.writeStartElement(INTERCEPT);
		writer.writeln();
		new ValueParser().toXML(function.getIntercept(), writer);
		writer.writeEndElement();
		writer.writeln();

		writer.writeEndElement();
		writer.writeln();		
    }
}
