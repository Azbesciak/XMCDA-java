package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaValues;
import org.xmcda.Criterion;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaValuesParser<VALUE_TYPE>
{
	public static final String CRITERIA_VALUES = CriteriaValues.TAG;

	public static final String VALUES          = "values";

	public CriteriaValues<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	throws XMLStreamException
	{
		CriteriaValues<VALUE_TYPE> values = Factory.criteriaValues();
		new CommonAttributesParser().handleAttributes(values, startElement);

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			// criterionFunction
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (CRITERIA_VALUES.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				values.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CriterionValuesParser.CRITERION_VALUE.equals(startElement.asStartElement().getName().getLocalPart()))
			{
				new CriterionValuesParser<VALUE_TYPE>().fromXML(xmcda, values, startElement, eventReader);
			}
		}
		return values;
	}

	public void toXML(List<CriteriaValues<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CriteriaValues<VALUE_TYPE> values: list)
			toXML(values, writer);
	}

	public void toXML(CriteriaValues<VALUE_TYPE> values, XMLStreamWriter writer) throws XMLStreamException
	{
		if (values == null)
			return; // TODO normal ça?

		writer.writeStartElement(CRITERIA_VALUES);
		new CommonAttributesParser().toXML(values, writer);
		writer.writeln();
		new DescriptionParser().toXML(values.getDescription(), writer);
		for (Entry<Criterion, LabelledQValues<VALUE_TYPE>> entry: values.entrySet())
			new CriterionValuesParser<VALUE_TYPE>().toXML(entry.getKey(), entry.getValue(), writer);

		writer.writeEndElement();
		writer.writeln();
	}


}
