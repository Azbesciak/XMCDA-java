/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.Alternative;
import org.xmcda.AlternativesMatrix;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import org.xmcda.utils.Matrix;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import java.util.List;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesMatrixParser <VALUE_TYPE>
    extends MatrixParser<Alternative, VALUE_TYPE>
{
	public static final String ALTERNATIVES_MATRIX = AlternativesMatrix.TAG;

	@Override
	public String rootTag()
	{
		return ALTERNATIVES_MATRIX;
	}

	@Override
	public String dimensionTag()
	{
		return "alternativeID";
	}

	@Override
	public Alternative buildDimension(XMCDA xmcda, String id)
	{
		return xmcda.alternatives.get(id);
	}

	@Override
	public String dimensionID(Alternative alternative)
	{
		return alternative.id();
	}

	@Override
	public AlternativesMatrix<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		Matrix<Alternative, VALUE_TYPE> matrix = super.fromXML(xmcda, startElement, eventReader);
		AlternativesMatrix<VALUE_TYPE> alternativesMatrix = Factory.alternativesMatrix();

		alternativesMatrix.setId(matrix.id());
		alternativesMatrix.setName(matrix.name());
		alternativesMatrix.setMcdaConcept(matrix.mcdaConcept());
		alternativesMatrix.setDescription(matrix.getDescription());
		alternativesMatrix.setValuation(matrix.getValuation());
		alternativesMatrix.putAll(matrix);
		return alternativesMatrix;
	}

	public void toXML(List<AlternativesMatrix<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (AlternativesMatrix<VALUE_TYPE> matrix: list)
			toXML(matrix, writer);
	}

	public void toXML(AlternativesMatrix<VALUE_TYPE> matrix, XMLStreamWriter writer) throws XMLStreamException
	{
		super.toXML(matrix, writer);
	}
}
