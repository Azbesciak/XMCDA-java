package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.BibliographyEntry;
import org.xmcda.Description;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

public class DescriptionParser
{
	public static final String DESCRIPTION            = "description";

	public static final String AUTHOR                 = "author";

	public static final String COMMENT                = "comment";

	public static final String KEYWORD                = "keyword";

	public static final String BIBLIOGRAPHY           = "bibliography";

	public static final String CREATION_DATE          = "creationDate";

	public static final String LAST_MODIFICATION_DATE = "lastModificationDate";

	public static final String BIB_ENTRY              = "bibEntry";

	public Description fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Description description = Factory.description();
		while ( eventReader.hasNext() )
		{
			// author 0..*
			// comment 0..1
			// keyword 0..*
			// bibliography 0..1
			// creationDate 0..1
			// lastModificationdate 0..1
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			{
				if (DESCRIPTION.equals(event.asEndElement().getName().getLocalPart()))
				    break;
			}

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (BIBLIOGRAPHY.equals(startElement.getName().getLocalPart()))
			{
				description.setBibliography(handleBibliography(xmcda, startElement, eventReader));
			}
			// we are only interested in the characters content of children EXCEPT BIBLIOGRAPHY
			event = eventReader.nextEvent();
			String content = "";
			if (event.isCharacters())
			    // if EndElement: empty tag
			    content = event.asCharacters().getData();

			if (AUTHOR.equals(startElement.getName().getLocalPart()))
			{
				description.getAuthors().add(content);
				continue;
			}
			if (COMMENT.equals(startElement.getName().getLocalPart()))
			{
				description.setComment(content);
				continue;
			}
			if (KEYWORD.equals(startElement.getName().getLocalPart()))
			{
				description.getKeywords().add(content);
				continue;
			}
			if (CREATION_DATE.equals(startElement.getName().getLocalPart()))
			{
				DatatypeFactory dtf;
				try
				{
					dtf = DatatypeFactory.newInstance();
					description.setCreationDate(dtf.newXMLGregorianCalendar(content).toGregorianCalendar());
				}
				catch (DatatypeConfigurationException e)
				{
					// TODO
					e.printStackTrace();
				}
				continue;
			}
			if (LAST_MODIFICATION_DATE.equals(startElement.getName().getLocalPart()))
			{
				DatatypeFactory dtf;
				try
				{
					dtf = DatatypeFactory.newInstance();
					description.setLastModificationDate(dtf.newXMLGregorianCalendar(content).toGregorianCalendar());
				}
				catch (DatatypeConfigurationException e)
				{
					// TODO
					e.printStackTrace();
				}
				continue;
			}
		}
		return description;
	}

	public BibliographyEntry handleBibliography(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		BibliographyEntry bibliographyEntry = Factory.bibliographyEntry();
		new CommonAttributesParser().handleAttributes(bibliographyEntry, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (BIBLIOGRAPHY.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			if (!event.isStartElement())
			    continue;
			if (DESCRIPTION.equals(event.asStartElement().getName().getLocalPart()))
			{
				bibliographyEntry.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
				continue;
			}
			if (BIB_ENTRY.equals(event.asStartElement().getName().getLocalPart()))
			{
				event = eventReader.nextEvent();
				String content = "";
				if (event.isCharacters())
				    // if EndElement: empty tag
				    content = event.asCharacters().getData();
				bibliographyEntry.getBibEntries().add(content);
			}
		}
		return bibliographyEntry;
	}

	public void toXML(Description description, XMLStreamWriter writer) throws XMLStreamException
	{
		if (description == null)
		    return;
		writer.writeStartElement(DESCRIPTION);
		writer.writeln();
		for (String author: description.getAuthors())
			writer.writeElementChars(AUTHOR, author);

		if (description.getComment() != null)
		    writer.writeElementChars(COMMENT, description.getComment());

		for (String keyword: description.getKeywords())
			writer.writeElementChars(KEYWORD, keyword);

		toXML(description.getBibliography(), writer);

		if (description.getCreationDate() != null)
		{
			DatatypeFactory dtf;
			try
			{
				dtf = DatatypeFactory.newInstance();
				XMLGregorianCalendar xml_date = dtf.newXMLGregorianCalendar(description.getCreationDate());
				writer.writeElementChars(CREATION_DATE, xml_date.toXMLFormat());
			}
			catch (DatatypeConfigurationException e)
			{
				// TODO
				e.printStackTrace();
			}

		}

		if (description.getLastModificationDate() != null)
		{
			DatatypeFactory dtf;
			try
			{
				dtf = DatatypeFactory.newInstance();
				XMLGregorianCalendar xml_date = dtf.newXMLGregorianCalendar(description.getLastModificationDate());
				writer.writeElementChars(LAST_MODIFICATION_DATE, xml_date.toXMLFormat());
			}
			catch (DatatypeConfigurationException e)
			{
				// TODO
				e.printStackTrace();
			}
		}
		writer.writeEndElement();
		writer.writeln();
	}

	public void toXML(BibliographyEntry bibliography, XMLStreamWriter writer) throws XMLStreamException
	{
		if (bibliography == null)
		    return;
		writer.writeStartElement(BIBLIOGRAPHY);
		new CommonAttributesParser().toXML(bibliography, writer);
		writer.writeln();
		toXML(bibliography.getDescription(), writer);
		for (String bibEntry: bibliography.getBibEntries())
			writer.writeElementChars(BIB_ENTRY, bibEntry);
		writer.writeEndElement();
		writer.writeln();
	}
}
