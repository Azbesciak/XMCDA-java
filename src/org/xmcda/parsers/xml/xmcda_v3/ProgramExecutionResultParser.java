package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.Message;
import org.xmcda.ProgramExecutionResult;
import org.xmcda.XMCDA;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.List;

/**
 * @author Sébastien Bigaret
 */
public class ProgramExecutionResultParser
{
	public static final String PROGRAM_EXECUTION_RESULT = ProgramExecutionResult.TAG;

	public static final String MESSAGES                 = "messages";

	public static final String STATUS                   = "status";

	public ProgramExecutionResult fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		ProgramExecutionResult executionResult = new ProgramExecutionResult();
		new CommonAttributesParser().handleAttributes(executionResult, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (PROGRAM_EXECUTION_RESULT.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (DescriptionParser.DESCRIPTION.equals(currentTagName))
			{
				executionResult.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (STATUS.equals(currentTagName))
			{
				final String status = Utils.getTextContent(startElement, eventReader);
				executionResult.updateStatus(ProgramExecutionResult.Status.valueOf(status.toUpperCase()));
			}
			if (MESSAGES.equals(currentTagName))
			{
				messagesFromXML(xmcda, executionResult, startElement, eventReader);
			}
		}
		return executionResult;
	}

	void messagesFromXML(XMCDA xmcda, ProgramExecutionResult executionResult, StartElement startElement,
	                     XMLEventReader eventReader) throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (MESSAGES.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (MessageParser.MESSAGE.equals(currentTagName))
			{
				executionResult.add(new MessageParser().fromXML(xmcda, startElement, eventReader));
			}
		}
	}

	public void toXML(List<ProgramExecutionResult> execResults, XMLStreamWriter writer) throws XMLStreamException
	{
		if (execResults == null || execResults.size() == 0)
		    return;

		for (ProgramExecutionResult result: execResults)
			toXML(result, writer);
	}

	public void toXML(ProgramExecutionResult prgExecResults, XMLStreamWriter writer) throws XMLStreamException
	{
		if (prgExecResults == null)
		    return;

		writer.writeStartElement(PROGRAM_EXECUTION_RESULT);
		new CommonAttributesParser().toXML(prgExecResults, writer);
		writer.writeln();

		new DescriptionParser().toXML(prgExecResults.getDescription(), writer);

		if (prgExecResults.getStatus() != null)
		    writer.writeElementChars(STATUS, prgExecResults.getStatus().name().toLowerCase());

		if (prgExecResults != null && prgExecResults.size() > 0)
		{
			writer.writeStartElement(MESSAGES);
			writer.writeln();
			final MessageParser messageParser = new MessageParser();
			for (Message message: prgExecResults)
				messageParser.toXML(message, writer);
			writer.writeEndElement(); // MESSAGES
			writer.writeln();
		}
		writer.writeEndElement(); // PROGRAM_EXECUTION_RESULTS
		writer.writeln();
	}
}
