package org.xmcda.parsers.xml.xmcda_v3;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xmcda.Alternatives;
import org.xmcda.AlternativesAssignments;
import org.xmcda.AlternativesCriteriaValues;
import org.xmcda.AlternativesLinearConstraints;
import org.xmcda.AlternativesMatrix;
import org.xmcda.AlternativesSets;
import org.xmcda.AlternativesSetsLinearConstraints;
import org.xmcda.AlternativesSetsMatrix;
import org.xmcda.AlternativesSetsValues;
import org.xmcda.AlternativesValues;
import org.xmcda.Categories;
import org.xmcda.CategoriesLinearConstraints;
import org.xmcda.CategoriesMatrix;
import org.xmcda.CategoriesProfiles;
import org.xmcda.CategoriesSets;
import org.xmcda.CategoriesSetsLinearConstraints;
import org.xmcda.CategoriesSetsMatrix;
import org.xmcda.CategoriesSetsValues;
import org.xmcda.CategoriesValues;
import org.xmcda.Criteria;
import org.xmcda.CriteriaFunctions;
import org.xmcda.CriteriaHierarchy;
import org.xmcda.CriteriaLinearConstraints;
import org.xmcda.CriteriaMatrix;
import org.xmcda.CriteriaScales;
import org.xmcda.CriteriaSets;
import org.xmcda.CriteriaSetsHierarchy;
import org.xmcda.CriteriaSetsLinearConstraints;
import org.xmcda.CriteriaSetsMatrix;
import org.xmcda.CriteriaSetsValues;
import org.xmcda.CriteriaThresholds;
import org.xmcda.CriteriaValues;
import org.xmcda.PerformanceTable;
import org.xmcda.ProgramExecutionResult;
import org.xmcda.ProgramParameters;
import org.xmcda.XMCDA;
import org.xml.sax.SAXException;

public class XMCDAParser
{
	private final static Logger LOGGER = Logger.getLogger(XMCDAParser.class.getName());

	/**
	 * Used by {@link #validate(File)}: creating a schemaFactory instance is time-consuming, so we create it once and
	 * for all. For example: the method called by the {@link org.xmcda.converters.v2_v3.XMCDAConverter XMCDAConverter}
	 * for each XMCDA v3 files whose conversion to XMCDA v2 is requested..
	 */
	private static Validator schemaValidator = null;

	public static synchronized boolean validate(File file) throws IOException, SAXException
	{
		return validate(new StreamSource(file));
	}

	protected static synchronized boolean validate(InputStream inputStream) throws IOException, SAXException
	{
		return validate(new StreamSource(new BufferedInputStream(inputStream) {
			@Override
			public void close() throws IOException
			{
				// ignore close() so that the stream can be mark()'ed/reset() and re-read, if possible
			}
		}));
	}

	protected static synchronized boolean validate(StreamSource streamSource) throws IOException, SAXException
	{
		if (schemaValidator == null)
		{
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			// create a grammar object.
			Schema schemaGrammar = schemaFactory.newSchema(Version.allSchemas());

			schemaValidator = schemaGrammar.newValidator();
			// schemaValidator.setErrorHandler;
		}

		// validate xml instance against the grammar.
		schemaValidator.validate(streamSource);
		return true;
	}

	public XMCDA readXMCDA(XMCDA xmcda, String filename) throws IOException, XMLStreamException, SAXException
	{
		return readXMCDA(xmcda, new File(filename));
	}

	public XMCDA readXMCDA(XMCDA xmcda, String filename, String... tags) throws IOException, XMLStreamException,
			SAXException
	{
		return readXMCDA(xmcda, new File(filename), tags);
	}

	public XMCDA readXMCDA(XMCDA xmcda, File file) throws IOException, XMLStreamException, SAXException
	{
		return readXMCDA(xmcda, file, new String[]{});
	}

	/**
	 * Checks whether {@code xmlTag} is equal to {@code XMCDATag}, and if it is contained in {@code tagsOnly}.
	 * When {@code tagsOnly} is {@code}, the last check is ignored.
	 *
	 * @param XMCDATag the non-null XMCDA tag
	 * @param xmlTag   a non-null xml tag
	 * @param tagsOnly a list of (non-null) xml tags
	 * @return {@code true} if {@code XMCDATag} is equal to {@code xmlTag},
	 * @throws NullPointerException if {@code xmlTag} or {@code XMCDATag} are {@code null}, or if {@code XMCDATag}
	 *                              contains {@code null} values.
	 */
	private boolean handleTag(String XMCDATag, String xmlTag, String[] tagsOnly)
	throws NullPointerException
	{
		if ( xmlTag == null)
			throw new NullPointerException("xmlTag cannot be null");
		if ( XMCDATag == null)
			throw new NullPointerException("XMCDATag cannot be null");

		if ( ! XMCDATag.equals(xmlTag) )
			return false;
		if ( tagsOnly == null || tagsOnly.length == 0 )
			return true;
		for ( String tagOnly: tagsOnly )
		{
			if ( tagOnly == null )
			    throw new NullPointerException("tagsOnly cannot contain null values");
			if ( xmlTag.equals(tagOnly) )
				return true;
		}
		return false;
	}

	public XMCDA readXMCDA(XMCDA xmcda, File file, String... tagsOnly)
	throws IOException, XMLStreamException, SAXException
	{
		return readXMCDA(xmcda, file, null, tagsOnly);
	}

	/**
	 * Reads a XMCDA file and adds its content to an XMCDA object.
	 * @param xmcda the XMCDA object on which the parsed elements will be put into
	 * @param file the files containing the XMCDA to be read
	 * @param parsedRootElements if non-null, the parsed root elements are appended to this list
	 * @param tagsOnly the list of root tags to read. If empty, everything is read
	 * @return the object passed as parameter {@code xmcda}
	 * @throws IOException
	 * @throws XMLStreamException
	 * @throws SAXException
	 */
	public XMCDA readXMCDA(final XMCDA xmcda, final File file, Set<String> parsedRootElements, final String... tagsOnly)
	throws IOException, XMLStreamException, SAXException
	{
		if ( ! validate(file) ) // TODO never returns false...
			throw new RuntimeException("invalid XMCDA");
		return readXMCDA_noValidation(xmcda, new FileInputStream(file), parsedRootElements, tagsOnly);
	}

	/** The maximum size of the stream that can be read by {@link #readXMCDA(XMCDA, InputStream, Set, String...)} */
	protected int readXMCDAInputStreamMaxSize = 8192;
	
	/**
	 * Validates and parses an inputStream to build a XMCDA object. <br>
	 * <b>Warning:</b> the method works for small inputs only, it relies on an internal buffer whose size is
	 * {@link #readXMCDAInputStreamMaxSize}.
	 * 
	 * @param xmcda the XMCDA object in which the read elements should be put.
	 * @param inputStream the stream to read
	 * @param parsedRootElements a set in which the method puts the elements it parses. It mayu be null.
	 * @param tagsOnly the XMCDA root tags to be read in the stream. If {@code null} or empty,
	 * @return the updated XMCDA object supplied as a parameter
	 * @throws IOException
	 * @throws XMLStreamException
	 * @throws SAXException
	 */
	protected XMCDA readXMCDA(final XMCDA xmcda, final InputStream inputStream, Set<String> parsedRootElements, final String... tagsOnly)
	throws IOException, XMLStreamException, SAXException
	{
		BufferedInputStream bis = new BufferedInputStream(inputStream, 8192);
		bis.mark(0);
		
		if ( ! validate(bis) ) // TODO never returns false...
			throw new RuntimeException("invalid XMCDA");

		bis.reset();
		return readXMCDA_noValidation(xmcda, bis, parsedRootElements, tagsOnly);
	}

	/**
	 * Parses an input stream and populate the supplied XMCDA objects with the elements found in the stream.<br>
	 * Parameters: see {@link #readXMCDA(XMCDA, InputStream, Set, String...)}.
	 */
	protected XMCDA readXMCDA_noValidation(final XMCDA xmcda, InputStream inputStream, Set<String> parsedRootElements, final String... tagsOnly)
	throws IOException, XMLStreamException, SAXException
	{
		// First create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		// Setup a new eventReader
		XMLEventReader eventReader = inputFactory.createXMLEventReader(inputStream);

		if ( parsedRootElements == null )
			parsedRootElements = new HashSet<>();

		// Read the XML document
		while (eventReader.hasNext()) {
			XMLEvent event = eventReader.nextEvent();
			if (!event.isStartElement())
				continue;
			StartElement startElement = event.asStartElement();

			if ( handleTag(Alternatives.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new AlternativesParser().fromXML(xmcda, startElement, eventReader); // TODO factory etc.
				parsedRootElements.add(Alternatives.TAG);
				continue;
			}
			if ( handleTag(AlternativesSets.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new AlternativesSetsParser().fromXML(xmcda, startElement, eventReader);
				parsedRootElements.add(AlternativesSets.TAG);
				continue;
			}
			if ( handleTag(AlternativesSets.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new AlternativesSetsParser().fromXML(xmcda, startElement, eventReader);
				parsedRootElements.add(AlternativesSets.TAG);
				continue;
			}
			if ( handleTag(Criteria.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new CriteriaParser().fromXML(xmcda, startElement, eventReader);
				parsedRootElements.add(Criteria.TAG);
				continue;
			}
			if ( handleTag(CriteriaSets.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new CriteriaSetsParser().fromXML(xmcda, startElement, eventReader);
				parsedRootElements.add(CriteriaSets.TAG);
				continue;
			}
			if ( handleTag(CriteriaSets.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new CriteriaSetsParser().fromXML(xmcda, startElement, eventReader);
				continue;
			}
			if ( handleTag(Categories.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new CategoriesParser().fromXML(xmcda, startElement, eventReader);
				parsedRootElements.add(Categories.TAG);
				continue;
			}
			if ( handleTag(CategoriesSets.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new CategoriesSetsParser().fromXML(xmcda, startElement, eventReader);
				parsedRootElements.add(CategoriesSets.TAG);
				continue;
			}
			if ( handleTag(PerformanceTable.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				new PerformanceTableParser().fromXML(xmcda, startElement, eventReader);
				parsedRootElements.add(PerformanceTable.TAG);
				continue;
			}

			if ( handleTag(AlternativesValues.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.alternativesValuesList.add(new AlternativesValuesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(AlternativesValues.TAG);
				continue;
			}
			if ( handleTag(AlternativesSetsValues.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.alternativesSetsValuesList.add(new AlternativesSetsValuesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(AlternativesSetsValues.TAG);
				continue;
			}
			if ( handleTag(AlternativesLinearConstraints.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.alternativesLinearConstraintsList.add(new AlternativesLinearConstraintsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(AlternativesLinearConstraints.TAG);
				continue;
			}
			if ( handleTag(AlternativesSetsLinearConstraints.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.alternativesSetsLinearConstraintsList.add(new AlternativesSetsLinearConstraintsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(AlternativesSetsLinearConstraints.TAG);
				continue;
			}
			if ( handleTag(AlternativesMatrix.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.alternativesMatricesList.add(new AlternativesMatrixParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(AlternativesMatrix.TAG);
				continue;
			}
			if ( handleTag(AlternativesSetsMatrix.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.alternativesSetsMatricesList.add(new AlternativesSetsMatrixParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(AlternativesSetsMatrix.TAG);
				continue;
			}

			if ( handleTag(CriteriaFunctions.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaFunctionsList.add(new CriteriaFunctionsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaFunctions.TAG);
				continue;
			}
			if ( handleTag(CriteriaScales.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaScalesList.add(new CriteriaScalesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaScales.TAG);
				continue;
			}
			if ( handleTag(CriteriaThresholds.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaThresholdsList.add(new CriteriaThresholdsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaThresholds.TAG);
				continue;
			}
			if ( handleTag(CriteriaValues.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaValuesList.add(new CriteriaValuesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaValues.TAG);
				continue;
			}
			if ( handleTag(CriteriaSetsValues.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaSetsValuesList.add(new CriteriaSetsValuesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaSetsValues.TAG);
				continue;
			}
			if ( handleTag(CriteriaLinearConstraints.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaLinearConstraintsList.add(new CriteriaLinearConstraintsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaLinearConstraints.TAG);
				continue;
			}
			if ( handleTag(CriteriaSetsLinearConstraints.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaSetsLinearConstraintsList.add(new CriteriaSetsLinearConstraintsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaSetsLinearConstraints.TAG);
				continue;
			}
			if ( handleTag(CriteriaMatrix.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaMatricesList.add(new CriteriaMatrixParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaMatrix.TAG);
				continue;
			}
			if ( handleTag(CriteriaSetsMatrix.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaSetsMatricesList.add(new CriteriaSetsMatrixParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaSetsMatrix.TAG);
				continue;
			}
			if ( handleTag(CriteriaHierarchy.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaHierarchiesList.add(new CriteriaHierarchyParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaHierarchy.TAG);
				continue;
			}
			if ( handleTag(CriteriaSetsHierarchy.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.criteriaSetsHierarchiesList.add(new CriteriaSetsHierarchyParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CriteriaSetsHierarchy.TAG);
				continue;
			}

			if ( handleTag(AlternativesCriteriaValues.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.alternativesCriteriaValuesList.add(new AlternativesCriteriaValuesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(AlternativesCriteriaValues.TAG);
				continue;
			}

			if ( handleTag(CategoriesProfiles.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.categoriesProfilesList.add(new CategoriesProfilesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CategoriesProfiles.TAG);
				continue;
			}
			if ( handleTag(AlternativesAssignments.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.alternativesAssignmentsList.add(new AlternativesAssignmentsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(AlternativesAssignments.TAG);
				continue;
			}
			if ( handleTag(CategoriesValues.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.categoriesValuesList.add(new CategoriesValuesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CategoriesValues.TAG);
				continue;
			}
			if ( handleTag(CategoriesSetsValues.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.categoriesSetsValuesList.add(new CategoriesSetsValuesParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CategoriesSetsValues.TAG);
				continue;
			}
			if ( handleTag(CategoriesLinearConstraints.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.categoriesLinearConstraintsList.add(new CategoriesLinearConstraintsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CategoriesLinearConstraints.TAG);
				continue;
			}
			if ( handleTag(CategoriesSetsLinearConstraints.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.categoriesSetsLinearConstraintsList.add(new CategoriesSetsLinearConstraintsParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CategoriesSetsLinearConstraints.TAG);
				continue;
			}
			if ( handleTag(CategoriesMatrix.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.categoriesMatricesList.add(new CategoriesMatrixParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CategoriesMatrix.TAG);
				continue;
			}
			if ( handleTag(CategoriesSetsMatrix.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.categoriesSetsMatricesList.add(new CategoriesSetsMatrixParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(CategoriesSetsMatrix.TAG);
				continue;
			}

			if ( handleTag(ProgramParameters.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.programParametersList.add(new ProgramParametersParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(ProgramParameters.TAG);
				continue;
			}
			if ( handleTag(ProgramExecutionResult.TAG, startElement.getName().getLocalPart(), tagsOnly) )
			{
				xmcda.programExecutionResultsList.add(new ProgramExecutionResultParser().fromXML(xmcda, startElement, eventReader));
				parsedRootElements.add(ProgramExecutionResult.TAG);
				continue;
			}
		}
		return xmcda;
	}

	// Writing XMCDA

	public static final String XMCDAv3_VERSION = "XMCDAv3_VERSION";
	
    /**
	 * The default version to use when writing an XMCDA v3 file. The default is 3.3.1, except when the environment
	 * variable {@value #XMCDAv3_VERSION} is set and has a valid value, in which case this value takes precedence the
	 * first time the class is loaded (there won't be any changes afterwards, except if someone programmatically
	 * change the default.
	 */
	static private Version defaultXMCDAv3Version = Version.XMCDA_3_1_1;

	static
	{
	    String sys_env_v3 = System.getenv().getOrDefault(XMCDAv3_VERSION, null);
	    if ( sys_env_v3 != null )
	    {
	    	Optional<Version> env_version = Version.get(sys_env_v3);
	    	if ( env_version.isPresent() )
	    		defaultXMCDAv3Version = env_version.get();
	    	else
	    		LOGGER.warning(() -> "Invalid value of environment variable "+XMCDAv3_VERSION+" ("+sys_env_v3+")");
	    }
	}

	static public Version getDefaultXMCDAv3Version()
	{
	    return defaultXMCDAv3Version;
	}

    static public void setDefaultXMCDAv3Version(Version version)
    {
        if ( version == null )
            throw new NullPointerException("Parameter 'version' cannot be null");
        defaultXMCDAv3Version = version;
    }

	public void writeXMCDA(XMCDA xmcda, String contextFileName) throws FileNotFoundException, XMLStreamException
	{
		writeXMCDA(xmcda, new File(contextFileName));
	}

	public void writeXMCDA(XMCDA xmcda, String contextFileName, String... tagsOnly)
	throws FileNotFoundException, XMLStreamException
	{
		writeXMCDA(xmcda, new File(contextFileName), tagsOnly);
	}

	public void writeXMCDA(XMCDA xmcda, File contextFile) throws FileNotFoundException,
			XMLStreamException
	{
		writeXMCDA(xmcda,contextFile, (java.lang.String[]) null);
	}

	public void writeXMCDA(XMCDA xmcda, File contextFile, String... tagsOnly) throws FileNotFoundException,
			XMLStreamException
	{
		writeXMCDA(xmcda, new FileOutputStream(contextFile), tagsOnly);
	}

	public void writeXMCDA(XMCDA xmcda, OutputStream stream, String... tagsOnly) throws FileNotFoundException,
	XMLStreamException
	{
		writeXMCDA(xmcda, stream, getDefaultXMCDAv3Version(), tagsOnly);
	}

	public void writeXMCDA(XMCDA xmcda, OutputStream stream, Version version, String... tagsOnly) throws FileNotFoundException,
			XMLStreamException
	{
		List<String> tagsOnlyList;

		if ( tagsOnly == null || tagsOnly.length == 0 )
			tagsOnlyList = XMCDA.ROOT_TAGS;
		else
		{
			tagsOnlyList = new ArrayList<>(tagsOnly.length);
			for ( String tag: tagsOnly )
				tagsOnlyList.add(tag);
		}

		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

		javax.xml.stream.XMLStreamWriter java_writer =  outputFactory.createXMLStreamWriter(stream);
		XMLStreamWriter writer = new XMLStreamWriter(java_writer);
		writer.writeStartDocument();
		writer.writeln();
		// Ecriture de l'élément conteneur
		writer.writeStartElement(version.namespacePrefix, "XMCDA", version.namespace);
		// Ajout d'un namespace
		writer.writeNamespace(version.namespacePrefix, version.namespace);
		writer.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation",
		                      version.namespace + " " + version.schemaURL);
		writer.writeln();

		if ( xmcda.alternatives != null && tagsOnlyList.contains(Alternatives.TAG) )
		    new AlternativesParser().toXML(xmcda.alternatives, writer);
		if ( xmcda.alternativesSets != null && tagsOnlyList.contains(AlternativesSets.TAG) )
			new AlternativesSetsParser().toXML(xmcda.alternativesSets, writer);
		if ( xmcda.criteria != null && tagsOnlyList.contains(Criteria.TAG) )
			new CriteriaParser().toXML(xmcda.criteria, writer);
		if ( xmcda.criteriaSets != null && tagsOnlyList.contains(CriteriaSets.TAG) )
			new CriteriaSetsParser().toXML(xmcda.criteriaSets, writer);
		if ( xmcda.categories != null && tagsOnlyList.contains(Categories.TAG) )
			new CategoriesParser().toXML(xmcda.categories, writer);
		if ( xmcda.categoriesSets != null && tagsOnlyList.contains(CategoriesSets.TAG) )
			new CategoriesSetsParser().toXML(xmcda.categoriesSets, writer);
		if ( xmcda.performanceTablesList != null && tagsOnlyList.contains(PerformanceTable.TAG) )
			new PerformanceTableParser().toXML(xmcda.performanceTablesList, writer);

		if ( xmcda.alternativesValuesList != null && tagsOnlyList.contains(AlternativesValues.TAG) )
			new AlternativesValuesParser().toXML(xmcda.alternativesValuesList, writer);
		if ( xmcda.alternativesSetsValuesList != null && tagsOnlyList.contains(AlternativesSetsValues.TAG) )
			new AlternativesSetsValuesParser().toXML(xmcda.alternativesSetsValuesList, writer);
		if ( xmcda.alternativesLinearConstraintsList != null && tagsOnlyList.contains(AlternativesLinearConstraints.TAG) )
			new AlternativesLinearConstraintsParser().toXML(xmcda.alternativesLinearConstraintsList, writer);
		if ( xmcda.alternativesSetsLinearConstraintsList != null && tagsOnlyList.contains(AlternativesSetsLinearConstraints.TAG) )
			new AlternativesSetsLinearConstraintsParser().toXML(xmcda.alternativesSetsLinearConstraintsList, writer);
		if ( xmcda.alternativesMatricesList != null && tagsOnlyList.contains(AlternativesMatrix.TAG) )
			new AlternativesMatrixParser().toXML(xmcda.alternativesMatricesList, writer);
		if ( xmcda.alternativesSetsMatricesList != null && tagsOnlyList.contains(AlternativesSetsMatrix.TAG) )
			new AlternativesSetsMatrixParser().toXML(xmcda.alternativesSetsMatricesList, writer);

		if ( xmcda.criteriaFunctionsList != null && tagsOnlyList.contains(CriteriaFunctions.TAG) )
			new CriteriaFunctionsParser().toXML(xmcda.criteriaFunctionsList, writer);
		if ( xmcda.criteriaScalesList != null && tagsOnlyList.contains(CriteriaScales.TAG) )
			new CriteriaScalesParser().toXML(xmcda.criteriaScalesList, writer);
		if ( xmcda.criteriaThresholdsList != null && tagsOnlyList.contains(CriteriaThresholds.TAG) )
			new CriteriaThresholdsParser().toXML(xmcda.criteriaThresholdsList, writer);
		if ( xmcda.criteriaValuesList != null && tagsOnlyList.contains(CriteriaValues.TAG) )
			new CriteriaValuesParser().toXML(xmcda.criteriaValuesList, writer);
		if ( xmcda.criteriaSetsValuesList != null && tagsOnlyList.contains(CriteriaSetsValues.TAG) )
			new CriteriaSetsValuesParser().toXML(xmcda.criteriaSetsValuesList, writer);
		if ( xmcda.criteriaLinearConstraintsList != null && tagsOnlyList.contains(CriteriaLinearConstraints.TAG) )
			new CriteriaLinearConstraintsParser().toXML(xmcda.criteriaLinearConstraintsList, writer);
		if ( xmcda.criteriaSetsLinearConstraintsList != null && tagsOnlyList.contains(CriteriaSetsLinearConstraints.TAG) )
			new CriteriaSetsLinearConstraintsParser().toXML(xmcda.criteriaSetsLinearConstraintsList, writer);
		if ( xmcda.criteriaMatricesList != null && tagsOnlyList.contains(CriteriaMatrix.TAG) )
			new CriteriaMatrixParser().toXML(xmcda.criteriaMatricesList, writer);
		if ( xmcda.criteriaSetsMatricesList != null && tagsOnlyList.contains(CriteriaSetsMatrix.TAG) )
			new CriteriaSetsMatrixParser().toXML(xmcda.criteriaSetsMatricesList, writer);
		if ( xmcda.criteriaHierarchiesList != null && tagsOnlyList.contains(CriteriaHierarchy.TAG))
			new CriteriaHierarchyParser().toXML(xmcda.criteriaHierarchiesList, writer);
		if ( xmcda.criteriaSetsHierarchiesList != null && tagsOnlyList.contains(CriteriaSetsHierarchy.TAG))
			new CriteriaSetsHierarchyParser().toXML(xmcda.criteriaSetsHierarchiesList, writer);

		if ( xmcda.alternativesCriteriaValuesList != null && tagsOnlyList.contains(AlternativesCriteriaValues.TAG) )
			new AlternativesCriteriaValuesParser().toXML(xmcda.alternativesCriteriaValuesList, writer);

		if ( xmcda.categoriesProfilesList != null && tagsOnlyList.contains(CategoriesProfiles.TAG) )
			new CategoriesProfilesParser().toXML(xmcda.categoriesProfilesList, writer);
		if ( xmcda.alternativesAssignmentsList != null && tagsOnlyList.contains(AlternativesAssignments.TAG) )
			new AlternativesAssignmentsParser().toXML(xmcda.alternativesAssignmentsList, writer);
		if ( xmcda.categoriesValuesList != null && tagsOnlyList.contains(CategoriesValues.TAG) )
			new CategoriesValuesParser().toXML(xmcda.categoriesValuesList, writer);
		if ( xmcda.categoriesSetsValuesList != null && tagsOnlyList.contains(CategoriesSetsValues.TAG) )
			new CategoriesSetsValuesParser().toXML(xmcda.categoriesSetsValuesList, writer);
		if ( xmcda.categoriesLinearConstraintsList != null && tagsOnlyList.contains(CategoriesLinearConstraints.TAG) )
			new CategoriesLinearConstraintsParser().toXML(xmcda.categoriesLinearConstraintsList, writer);
		if ( xmcda.categoriesSetsLinearConstraintsList != null && tagsOnlyList.contains(CategoriesSetsLinearConstraints.TAG) )
			new CategoriesSetsLinearConstraintsParser().toXML(xmcda.categoriesSetsLinearConstraintsList, writer);
		if ( xmcda.categoriesMatricesList != null && tagsOnlyList.contains(CategoriesMatrix.TAG) )
			new CategoriesMatrixParser().toXML(xmcda.categoriesMatricesList, writer);
		if ( xmcda.categoriesSetsMatricesList != null && tagsOnlyList.contains(CategoriesSetsMatrix.TAG) )
			new CategoriesSetsMatrixParser().toXML(xmcda.categoriesSetsMatricesList, writer);

		if ( xmcda.programParametersList != null && tagsOnlyList.contains(ProgramParameters.TAG) )
			new ProgramParametersParser().toXML(xmcda.programParametersList, writer);
		if ( xmcda.programExecutionResultsList != null && tagsOnlyList.contains(ProgramExecutionResult.TAG) )
			new ProgramExecutionResultParser().toXML(xmcda.programExecutionResultsList, writer);

		writer.writeEndElement();
		writer.writeln();
		writer.writeEndDocument();
		writer.close();
	}

	public static void main(String[] args) throws Throwable
	{
		XMCDA xmcda = new XMCDA();
		XMCDAParser reader = new XMCDAParser();
		reader.readXMCDA(xmcda, "test.xml");
		for (org.xmcda.Alternative alternative: xmcda.alternatives)
		{
			System.out.println(alternative);
		}
		reader.writeXMCDA(xmcda, "test-out.xml");
	}
}
