package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Category;
import org.xmcda.CategoryProfile;
import org.xmcda.CategoryProfile.Profile;
import org.xmcda.CategoryProfile.Type;
import org.xmcda.Description;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

/**
 * 
 * @author Sébastien Bigaret
 *
 */
public class CategoryProfileParser<VALUE_TYPE>
{
	public static final String CATEGORY_PROFILE = "categoryProfile";
	public static final String CATEGORY_ID = "categoryID";
	public static final String CENTRAL = "central";
	public static final String BOUNDING = "bounding";
	public static final String ALTERNATIVE_ID = "alternativeID";
	public static final String LOWER_BOUND = "lowerBound";
	public static final String UPPER_BOUND = "upperBound";
	
	public CategoryProfile<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		final StartElement initialElement = startElement;
		final String initialTag = initialElement.getName().getLocalPart();
		CategoryProfile<VALUE_TYPE> profile = null;
		Category category = null;
		Description description = null;
		while ( eventReader.hasNext() )
		{
			// abscissa
			// ordinate
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
				    break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			final String currentTagName = startElement.asStartElement().getName().getLocalPart();
			if (DescriptionParser.DESCRIPTION.equals(currentTagName))
			{
				// at this point profile is still null
				description = new DescriptionParser().fromXML(xmcda, startElement, eventReader);
			}
			else if (CATEGORY_ID.equals(currentTagName))
			{
				// at this point profile is still null
				category = xmcda.categories.get(Utils.getTextContent(startElement, eventReader));
			}
			else if (CENTRAL.equals(currentTagName))
			{
				profile = Factory.<VALUE_TYPE>categoryProfile(Type.CENTRAL);
				profile.setCentralProfile(profileFromXML(xmcda, CENTRAL, eventReader));
			}
			else if (BOUNDING.equals(currentTagName))
			{
				profile = boundingProfileFromXML(xmcda, startElement, eventReader);
			}
		}
		new CommonAttributesParser().handleAttributes(profile, initialElement);
		profile.setCategory(category);
		profile.setDescription(description);
		return profile;
	}
	
	/**
	 * Searches within the {@code <bounding>} tag the lower and/or the upper bound(s) --only one of these are required,
	 * but both can be present.
	 * @param boundingProfile the bounding profile in which lower and/or upper bounds will be set
	 * @param xmcda the xmcda object
	 * @param startElement the element starting the definition (corresponding to XMCDA tag 'bounding')
	 * @param eventReader the XMLEventreader to use
	 * @throws XMLStreamException
	 */
	public CategoryProfile<VALUE_TYPE> boundingProfileFromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		final CategoryProfile<VALUE_TYPE> boundingProfile = Factory.<VALUE_TYPE>categoryProfile(Type.BOUNDING);

		final StartElement initialElement = startElement;
		final String initialTag = initialElement.getName().getLocalPart();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
				    break;
			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			final String currentTagName = startElement.asStartElement().getName().getLocalPart();
			if ( LOWER_BOUND.equals(currentTagName) )
			{
				boundingProfile.setLowerBound(profileFromXML(xmcda, LOWER_BOUND, eventReader));
			}
			else if ( UPPER_BOUND.equals(currentTagName) )
			{
				boundingProfile.setUpperBound(profileFromXML(xmcda, UPPER_BOUND, eventReader));
			}
		}
		return boundingProfile;
	}

	public Profile<VALUE_TYPE> profileFromXML(XMCDA xmcda, final String initialTag, XMLEventReader eventReader) throws XMLStreamException
	{
		Profile<VALUE_TYPE> profile = new Profile<VALUE_TYPE>();
		while ( eventReader.hasNext() )
		{
			// alternativeID
			// values
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
				    break;

			if (!event.isStartElement())
				continue;
			final StartElement startElement = event.asStartElement();
			if (ALTERNATIVE_ID.equals(startElement.asStartElement().getName().getLocalPart()))
				profile.setAlternative(xmcda.alternatives.get(Utils.getTextContent(startElement, eventReader)));
			else if (QualifiedValuesParser.VALUES.equals(startElement.asStartElement().getName().getLocalPart()))
				profile.setValues(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
		}
		return profile;
	}

	public void toXML(CategoryProfile<VALUE_TYPE> categoryProfile, XMLStreamWriter writer) throws XMLStreamException
	{
		if (categoryProfile == null)
			return;

		writer.writeStartElement(CATEGORY_PROFILE);
		new CommonAttributesParser().toXML(categoryProfile, writer);
		writer.writeln();
		new DescriptionParser().toXML(categoryProfile.getDescription(), writer);

		writer.writeElementChars(CATEGORY_ID, categoryProfile.getCategory().id());
		if (categoryProfile.getType().equals(CategoryProfile.Type.CENTRAL))
		{
			profileToXML(CENTRAL, categoryProfile.getCentralProfile(), writer);
		}
		else
		{
			writer.writeStartElement(BOUNDING);
			writer.writeln();
			profileToXML(LOWER_BOUND, categoryProfile.getLowerBound(), writer);
			profileToXML(UPPER_BOUND, categoryProfile.getUpperBound(), writer);
			writer.writeEndElement(); // BOUNDING
			writer.writeln();
		}
		writer.writeEndElement(); // CATEGORY_PROFILE
		writer.writeln();
	}

	public void profileToXML(String tag, CategoryProfile.Profile<VALUE_TYPE> profile, XMLStreamWriter writer) throws XMLStreamException
	{
		if (profile == null)  // this is legitimate for a bounding profile without a lower or upper bound.
			return;

		writer.writeStartElement(tag);
		writer.writeln();

		writer.writeElementChars(ALTERNATIVE_ID, profile.getAlternative().id());
		new QualifiedValuesParser<VALUE_TYPE>().toXML(profile.getValues(), writer);

		writer.writeEndElement(); // tag
		writer.writeln();
	}
}
