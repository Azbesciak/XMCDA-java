package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

import org.xmcda.XMCDA;
import org.xmcda.value.EndPoint;

/**
 * 
 * @author Sébastien Bigaret
 *
 */
public class EndPointParser
{
	public static final String OPEN = "open";
	
	public EndPoint fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		final StartElement head_element = startElement;
		EndPoint endPoint = new EndPoint(new PointParser().fromXML(xmcda, startElement, eventReader));
		final Attribute open = head_element.getAttributeByName(new QName(OPEN));
		if (open != null)
			endPoint.setOpen(Utils.booleanValue(open.getValue()));
		
		return endPoint;
	}

	public void toXML(String startTag, EndPoint endPoint, XMLStreamWriter writer) throws XMLStreamException
	{
		if (endPoint == null)
			return;
		
		writer.writeStartElement(startTag);
		writer.writeAttribute(OPEN, endPoint.isOpen() ? "true" : "false");
		writer.writeln();

		writer.writeStartElement(PointParser.ABSCISSA);
		writer.writeln();
		new ValueParser().toXML(endPoint.getAbscissa().getValue(), writer);
		writer.writeEndElement();
		writer.writeln();
		
		writer.writeStartElement(PointParser.ORDINATE);
		writer.writeln();
		new ValueParser().toXML(endPoint.getOrdinate().getValue(), writer);
		writer.writeEndElement();
		writer.writeln();

		writer.writeEndElement();
		writer.writeln();
	}

}
