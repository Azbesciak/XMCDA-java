package org.xmcda.parsers.xml.xmcda_v3;


import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.AlternativesSet;
import org.xmcda.AlternativesSetsLinearConstraints;
import org.xmcda.Factory;
import org.xmcda.LinearConstraint;
import org.xmcda.XMCDA;
import org.xmcda.parsers.xml.xmcda_v3.LinearConstraintParser.LinearConstraintParserHelper;

import java.util.List;

public class AlternativesSetsLinearConstraintsParser <ALTERNATIVESSET_VALUE_TYPE, VALUE_TYPE>
{
	private class Helper
	    extends LinearConstraintParserHelper<AlternativesSet<ALTERNATIVESSET_VALUE_TYPE>>
	{
		@Override
		public AlternativesSet<ALTERNATIVESSET_VALUE_TYPE> buildObject(XMCDA xmcda, String id)
		{
			return (AlternativesSet<ALTERNATIVESSET_VALUE_TYPE>) xmcda.alternativesSets.get(id);
		}
	}

	public static final String ALTERNATIVES_SETS_LINEAR_CONSTRAINTS = AlternativesSetsLinearConstraints.TAG;

	public static final String ALTERNATIVES_SET_ID                  = "alternativesSetID";

	public LinearConstraintParserHelper<AlternativesSet<ALTERNATIVESSET_VALUE_TYPE>> helper()
	{
		return new Helper();
	}

	public AlternativesSetsLinearConstraints<ALTERNATIVESSET_VALUE_TYPE, VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement,
	                                                                                 XMLEventReader eventReader)
	    throws XMLStreamException
	{
		AlternativesSetsLinearConstraints<ALTERNATIVESSET_VALUE_TYPE, VALUE_TYPE> constraints = Factory
		        .alternativesSetsLinearConstraints();
		new CommonAttributesParser().handleAttributes(constraints, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVES_SETS_LINEAR_CONSTRAINTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				constraints.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (VariablesParser.VARIABLES.equals(startElement.getName().getLocalPart()))
			{
				constraints.setVariables(new VariablesParser().fromXML(xmcda, startElement, eventReader));
			}
			if (LinearConstraintParser.CONSTRAINTS.equals(startElement.getName().getLocalPart()))
			{
				readConstraintsFromXML(xmcda, constraints, startElement, eventReader);
			}

		}
		return constraints;
	}

	public void readConstraintsFromXML(XMCDA xmcda,
	                                   AlternativesSetsLinearConstraints<ALTERNATIVESSET_VALUE_TYPE, VALUE_TYPE> constraints,
	                                   StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (LinearConstraintParser.CONSTRAINTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (LinearConstraintParser.CONSTRAINT.equals(startElement.getName().getLocalPart()))
			{
				constraints.add(new LinearConstraintParser<AlternativesSet<ALTERNATIVESSET_VALUE_TYPE>, VALUE_TYPE>()
				        .constraintFromXML(xmcda, helper(), startElement, eventReader));
			}
		}
	}

	public void toXML(List<AlternativesSetsLinearConstraints<ALTERNATIVESSET_VALUE_TYPE, VALUE_TYPE>> list, XMLStreamWriter writer)
		    throws XMLStreamException
	{
		if (list == null || list.size() == 0)
		    return;
		for (AlternativesSetsLinearConstraints<ALTERNATIVESSET_VALUE_TYPE, VALUE_TYPE> values: list)
			toXML(values, writer);
	}

	public void toXML(AlternativesSetsLinearConstraints<ALTERNATIVESSET_VALUE_TYPE, VALUE_TYPE> constraints,
	                  XMLStreamWriter writer) throws XMLStreamException
	{
		if (constraints == null)
		    return;

		writer.writeStartElement(ALTERNATIVES_SETS_LINEAR_CONSTRAINTS);
		new CommonAttributesParser().toXML(constraints, writer);
		writer.writeln();

		new DescriptionParser().toXML(constraints.getDescription(), writer);

		new VariablesParser().toXML(constraints.getVariables(), writer);

		writer.writeStartElement(LinearConstraintParser.CONSTRAINTS);
		writer.writeln();
		for (LinearConstraint<AlternativesSet<ALTERNATIVESSET_VALUE_TYPE>, VALUE_TYPE> constraint: constraints)
		{
			new LinearConstraintParser<AlternativesSet<ALTERNATIVESSET_VALUE_TYPE>, VALUE_TYPE>().toXML(ALTERNATIVES_SET_ID,
			                                                                                    constraint, writer);
		}
		writer.writeEndElement(); // CONSTRAINTS
		writer.writeln();
		writer.writeEndElement(); // ALTERNATIVES_LINEAR_CONSTRAINTS
		writer.writeln();
	}
}
