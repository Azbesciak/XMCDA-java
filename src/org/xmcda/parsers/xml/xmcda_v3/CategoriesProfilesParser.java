package org.xmcda.parsers.xml.xmcda_v3;


import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CategoriesProfiles;
import org.xmcda.CategoryProfile;
import org.xmcda.Criteria;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import java.util.List;

public class CategoriesProfilesParser <VALUE_TYPE>
{
	public static final String CATEGORIES_PROFILES = CategoriesProfiles.TAG;

	public CategoriesProfiles<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CategoriesProfiles<VALUE_TYPE> categoriesProfiles = Factory.<VALUE_TYPE> categoriesProfiles();
		new CommonAttributesParser().handleAttributes(categoriesProfiles, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CATEGORIES_PROFILES.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				categoriesProfiles.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CategoryProfileParser.CATEGORY_PROFILE.equals(startElement.getName().getLocalPart()))
			{
				categoriesProfiles.add(new CategoryProfileParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}

		}
		return categoriesProfiles;
	}

	public void toXML(List<CategoriesProfiles<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CategoriesProfiles<VALUE_TYPE> categoriesProfiles: list)
			toXML(categoriesProfiles, writer);
	}

	public void toXML(CategoriesProfiles<VALUE_TYPE> categoriesProfiles, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (categoriesProfiles == null)
		    return;
		writer.writeStartElement(CATEGORIES_PROFILES);
		new CommonAttributesParser().toXML(categoriesProfiles, writer);
		writer.writeln();
		new DescriptionParser().toXML(categoriesProfiles.getDescription(), writer);

		for (CategoryProfile<VALUE_TYPE> categoryProfile: categoriesProfiles)
			new CategoryProfileParser<VALUE_TYPE>().toXML(categoryProfile, writer);

		writer.writeEndElement(); // CATEGORIES_PROFILES
		writer.writeln();

	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class type)
	{
		return type == Criteria.class;
	}

}
