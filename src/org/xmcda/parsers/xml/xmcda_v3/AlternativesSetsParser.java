package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.AlternativesSet;
import org.xmcda.AlternativesSets;
import org.xmcda.XMCDA;

public class AlternativesSetsParser
{
	public static final String ALTERNATIVES_SETS = "alternativesSets";

	public String rootTag()
	{
		return ALTERNATIVES_SETS;
	}

	public String elementTag()
	{
		return AlternativesSetParser.ALTERNATIVES_SET;
	}

	public AlternativesSetParser setParser()
	{
		return new AlternativesSetParser();
	}

	public void fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		AlternativesSets<?> sets = xmcda.alternativesSets;
		new CommonAttributesParser().handleAttributes(sets, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (rootTag().equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				sets.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (elementTag().equals(startElement.getName().getLocalPart()))
			    sets.add(setParser().fromXML(xmcda, startElement, eventReader));
		}
	}

	public <VALUE_TYPE> void toXML(AlternativesSets<VALUE_TYPE> sets, XMLStreamWriter writer) throws XMLStreamException
	{
		if (sets == null)
		    return;
		if (sets.isVoid())
			return;
		
		writer.writeStartElement(rootTag());
		new CommonAttributesParser().toXML(sets, writer);
		writer.writeln();

		new DescriptionParser().toXML(sets.getDescription(), writer);

		for (AlternativesSet<VALUE_TYPE> set: sets)
			new AlternativesSetParser<VALUE_TYPE>().toXML(set, writer);

		writer.writeEndElement();
		writer.writeln();
	}

}
