/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import java.util.ArrayList;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Alternative;
import org.xmcda.Criterion;
import org.xmcda.Description;
import org.xmcda.PerformanceTable;
import org.xmcda.QualifiedValues;
import org.xmcda.XMCDA;
import org.xmcda.utils.PerformanceTableCoord;

/**
 * @author Sébastien Bigaret
 */
public class PerformanceTableParser
{
	public static final String PERFORMANCE_TABLE        = "performanceTable";

	public static final String ALTERNATIVE_PERFORMANCES = "alternativePerformances";

	public static final String ALTERNATIVE_ID           = "alternativeID";
	public static final String CRITERION_ID           = "criterionID";

	public static final String PERFORMANCE              = "performance";

	public PerformanceTable<?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		PerformanceTable< ? > perfTable = new PerformanceTable<Object>();
		new CommonAttributesParser().handleAttributes(perfTable, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (PERFORMANCE_TABLE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				perfTable.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (ALTERNATIVE_PERFORMANCES.equals(startElement.getName().getLocalPart()))
			{
				fromXML(xmcda, perfTable, startElement, eventReader);
			}
		}
		xmcda.performanceTablesList.add(perfTable);
		return perfTable;
	}

	protected void fromXML(XMCDA xmcda, PerformanceTable<?> perfTable, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		// on est sur alternativeOnCriteriaPerformances

		Alternative alternative = null;

		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVE_PERFORMANCES.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (ALTERNATIVE_ID.equals(startElement.getName().getLocalPart()))
			{
				// TODO wait until END of alternative_ID (test w/ comments e.g.)
				event = eventReader.nextEvent();
				String alt_id = "";
				if (event.isCharacters())
				    // if EndElement: empty tag
				    alt_id = event.asCharacters().getData();
				alternative = xmcda.alternatives.get(alt_id);
			}
			if (PERFORMANCE.equals(startElement.getName().getLocalPart()))
			{
				// description: will be bound to PerformanceTableCoord
				// criterionID: will be used to build a Criterion so that a PerformanceTableCoord is ultimately built
				// values: the values attached to (alternative, criteria)
				altOnCritPerf_fromXML(xmcda, perfTable, alternative, startElement, eventReader);
			}

		}
	}
	protected void altOnCritPerf_fromXML(XMCDA xmcda, PerformanceTable<?> perfTable, Alternative alternative, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Criterion criterion = null;
		
		Description description = null;
		
		QualifiedValues values = null;
		
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (PERFORMANCE.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			if ( ! event.isStartElement() )
				continue;
			startElement = event.asStartElement();
			if (CRITERION_ID.equals(startElement.getName().getLocalPart()))
			{
				// TODO wait until END of alternative_ID (test w/ comments e.g.)
				event = eventReader.nextEvent();
				String crit_id = "";
				if (event.isCharacters())
				    // if EndElement: empty tag
				    crit_id = event.asCharacters().getData();
				criterion = xmcda.criteria.get(crit_id);
			}
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				description = new DescriptionParser().fromXML(xmcda, startElement, eventReader);
			}
			if (QualifiedValuesParser.VALUES.equals(startElement.getName().getLocalPart()))
			{
				values = new QualifiedValuesParser().fromXML(xmcda, startElement, eventReader);
			}

		}
		// check that we have both alt & crit
		// assign the 
		PerformanceTableCoord coord = new PerformanceTableCoord(alternative, criterion);
		coord.setDescription(description);
		perfTable.put(coord, values);
	}

	public void toXML(ArrayList<PerformanceTable<?>> performanceTables, XMLStreamWriter writer) throws XMLStreamException
	{
		if (performanceTables == null || performanceTables.size()==0)
			return;
		for (PerformanceTable perfTable: performanceTables)
			toXML(perfTable, writer);
	}

	public <T> void toXML(PerformanceTable<T> performanceTable, XMLStreamWriter writer) throws XMLStreamException
	{
		if (performanceTable == null)
			return;
		writer.writeStartElement(PERFORMANCE_TABLE);
		new CommonAttributesParser().toXML(performanceTable, writer);
		writer.writeln();
		new DescriptionParser().toXML(performanceTable.getDescription(), writer);
		
		for (Alternative alternative: performanceTable.getAlternatives())
		{
			writer.writeStartElement(ALTERNATIVE_PERFORMANCES);
			writer.writeln();
			writer.writeElementChars(ALTERNATIVE_ID, alternative.id());
			for (Criterion criterion: performanceTable.getCriteria())
			{
				final QualifiedValues<T> qvalue = performanceTable.get(alternative, criterion);
				if ( qvalue==null )
					continue;
				writer.writeStartElement(PERFORMANCE);
				writer.writeln();
				new DescriptionParser().toXML(performanceTable.getCoord(alternative,criterion).getDescription(), writer);
				writer.writeElementChars(CRITERION_ID, criterion.id());
				new QualifiedValuesParser().toXML(qvalue, writer);
				writer.writeEndElement();
				writer.writeln();
			}
			writer.writeEndElement();
			writer.writeln();
		}
		writer.writeEndElement();
		writer.writeln();
	}

}
