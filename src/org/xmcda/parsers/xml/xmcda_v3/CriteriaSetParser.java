package org.xmcda.parsers.xml.xmcda_v3;

import java.util.Map.Entry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Criterion;
import org.xmcda.CriteriaSet;
import org.xmcda.Factory;
import org.xmcda.QualifiedValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaSetParser<VALUE_TYPE>
{
	public static final String CRITERIA_SET = "criteriaSet";

	public static final String ELEMENT          = "element";

	public static final String CRITERION_ID   = "criterionID";

	public static final String VALUES           = "values";

	public String rootTag()
	{
		return CRITERIA_SET;
	}

	public String elementTag()
	{
		return CRITERION_ID;
	}

	public Class<Criterion> elementClass()
	{
		return Criterion.class;
	}

	public String elementID(Criterion obj)
	{
		return obj.id();
	}

	public CriteriaSet<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
			throws XMLStreamException
	{
		CriteriaSet<VALUE_TYPE> set = Factory.<VALUE_TYPE>criteriaSet();
		new CommonAttributesParser().handleAttributes(set, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (rootTag().equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				set.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}

			if (ELEMENT.equals(startElement.getName().getLocalPart()))
				elementFromXML(xmcda, set, startElement, eventReader);
		}
		return set;
	}

	protected void elementFromXML(XMCDA xmcda, CriteriaSet<VALUE_TYPE> set, StartElement startElement, XMLEventReader eventReader)
			throws XMLStreamException
	{
		Criterion element = null;
		QualifiedValues<VALUE_TYPE> qvalues = null;
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (ELEMENT.equals(event.asEndElement().getName().getLocalPart()))
					break;
			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (elementTag().equals(startElement.getName().getLocalPart()))
				element = xmcda.criteria.get(Utils.getTextContent(startElement, eventReader));
			else if (QualifiedValuesParser.VALUES.equals(startElement.getName().getLocalPart()))
				qvalues = new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader);
		}
		set.put(element, qvalues);
	}

	public void toXML(CriteriaSet<VALUE_TYPE> set, XMLStreamWriter writer) throws XMLStreamException
	{
		if (set == null)
			return;

		writer.writeStartElement(rootTag());
		new CommonAttributesParser().toXML(set, writer);
		writer.writeln();

		new DescriptionParser().toXML(set.getDescription(), writer);

		for (Entry<Criterion, QualifiedValues<VALUE_TYPE>> entry: set.entrySet())
		{
			writer.writeStartElement(ELEMENT);
			writer.writeln();

			writer.writeElementChars(elementTag(), elementID(entry.getKey()));
			new QualifiedValuesParser<VALUE_TYPE>().toXML(entry.getValue(), writer);
			writer.writeEndElement();
			writer.writeln();
		}

		writer.writeEndElement();
		writer.writeln();
	}
}
