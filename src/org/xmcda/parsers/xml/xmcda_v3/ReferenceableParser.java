/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import java.util.Iterator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

import org.xmcda.CommonAttributes;
import org.xmcda.Referenceable;

/**
 * @author Sébastien Bigaret
 *
 */
public class ReferenceableParser
{
	public static final String ID           = "id";

	public static final String NAME         = "name";

	public static final String MCDA_CONCEPT = "mcdaConcept";

	private static final class ReferenceableAttributeHolder implements CommonAttributes
	{
		// CommonAttributes (start)

		/** The id attribute allows to identify the underlying piece of data by a program. */
		private String            id;

		/** The name attribute contains the human readable name of the object or concept. */
		private String            name;

		/**
		 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
		 * make choices which will have an influence on the output. The documentation of the program should therefore
		 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
		 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
		 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
		 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
		 * mcdaConcepts, as this will reduce the compatibility between the various programs.
		 */
		private String            mcdaConcept;

		public String id()
		{
			return id;
		}

		public void setId(String id)
		{
			this.id = id;
		}

		public String name()
		{
			return name;
		}

		public String mcdaConcept()
		{
			return mcdaConcept;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public void setMcdaConcept(String mcdaConcept)
		{
			this.mcdaConcept = mcdaConcept;
		}

		// CommonAttributes (end)

	}
	
	public CommonAttributes getAttributes(StartElement startElement)
	{
		ReferenceableAttributeHolder holder = new ReferenceableAttributeHolder();
		@SuppressWarnings("unchecked")
		Iterator<Attribute> attributes = startElement.getAttributes();
		while (attributes.hasNext()) {
			Attribute attribute = attributes.next();
			/* ignore */ new ReferenceableParser().handleAttribute(holder, attribute);
		}
		return holder;
	}
	
	public boolean handleAttribute(CommonAttributes obj, Attribute attribute)
	{
		final String attributeName = attribute.getName().toString();
		if (ID.equals(attributeName))
		{
			obj.setId(attribute.getValue());
			return true;
		}
		if (MCDA_CONCEPT.equals(attributeName))
		{
			obj.setMcdaConcept(attribute.getValue());
			return true;
		}
		if (NAME.equals(attributeName))
		{
			obj.setName(attribute.getValue());
			return true;
		}
		return false;
	}
	
	public void toXML(Referenceable obj, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeNonNullAttribute(ID, obj.id());
		writer.writeNonNullAttribute(NAME, obj.name());
		writer.writeNonNullAttribute(MCDA_CONCEPT, obj.mcdaConcept());
	}
}
