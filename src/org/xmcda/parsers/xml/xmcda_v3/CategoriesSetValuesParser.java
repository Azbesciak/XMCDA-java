package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CategoriesSetsValues;
import org.xmcda.CategoriesSet;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesSetValuesParser<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE>
{
	public static final String CATEGORIES_SET_VALUE = "categoriesSetValue";

	public static final String CATEGORIES_SET_ID       = "categoriesSetID";

	/**
	 * Builds the list of values attached to an ELEMENT and inserts it into the provided objectsValues object
	 * using the ELEMENT as the key.
	 * @param objectsValues
	 * @param startElement
	 * @param eventReader
	 * @return the ELEMENT object that was used to update the objectsValues with the built list of values
	 * @throws XMLStreamException
	 */
	public CategoriesSet<CATEGORIES_SET_VALUE_TYPE> fromXML(XMCDA xmcda, CategoriesSetsValues<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> objectsValues, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		LabelledQValues<VALUE_TYPE> categoriesSetValues = Factory.labelledQValues();
		new CommonAttributesParser().handleAttributes(categoriesSetValues, startElement);

		CategoriesSet<CATEGORIES_SET_VALUE_TYPE> categoriesSet = null;
		
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CATEGORIES_SET_VALUE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				categoriesSetValues.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (QualifiedValuesParser.VALUES.equals(startElement.asStartElement().getName()
			        .getLocalPart()))
			{
				categoriesSetValues.addAll(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}

			if (CATEGORIES_SET_ID.equals(startElement.getName().getLocalPart()))
			{
				final String id = Utils.getTextContent(startElement, eventReader);
				categoriesSet = Factory.categoriesSet();
				categoriesSet.setId(id);
			}
		}
		objectsValues.put(categoriesSet, categoriesSetValues);
		return categoriesSet;
	}

	public void toXML(CategoriesSet<CATEGORIES_SET_VALUE_TYPE> categoriesSet, LabelledQValues<VALUE_TYPE> categoriesSetValues, XMLStreamWriter writer) throws XMLStreamException
    {
		if (categoriesSetValues == null)
			return;

		writer.writeStartElement(CATEGORIES_SET_VALUE);
		new CommonAttributesParser().toXML(categoriesSetValues, writer);
		writer.writeln();
		new DescriptionParser().toXML(categoriesSetValues.getDescription(), writer);
		writer.writeElementChars(CATEGORIES_SET_ID, categoriesSet.id());
		new QualifiedValuesParser<VALUE_TYPE>().toXML(categoriesSetValues, writer);
		writer.writeEndElement();
		writer.writeln();
    }

}
