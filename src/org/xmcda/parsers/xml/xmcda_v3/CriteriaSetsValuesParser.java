package org.xmcda.parsers.xml.xmcda_v3;


import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaSet;
import org.xmcda.CriteriaSetsValues;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaSetsValuesParser <CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE>
{
	public static final String CRITERIA_SETS_VALUES = org.xmcda.CriteriaSetsValues.TAG;

	public static final String VALUES               = "values";

	public CriteriaSetsValues<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CriteriaSetsValues<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE> values = Factory.criteriaSetsValues();
		new CommonAttributesParser().handleAttributes(values, startElement);

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			// criteriaSetFunction
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERIA_SETS_VALUES.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				values.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CriteriaSetValuesParser.CRITERIA_SET_VALUE.equals(startElement.asStartElement().getName().getLocalPart()))
			{
				new CriteriaSetValuesParser<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE>().fromXML(xmcda, values, startElement, eventReader);
			}
		}
		return values;
	}

	public void toXML(List<CriteriaSetsValues<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE>> list, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (list == null || list.size() == 0)
		    return;
		for (CriteriaSetsValues<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE> values: list)
			toXML(values, writer);
	}

	public void toXML(CriteriaSetsValues<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE> values, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (values == null)
		    return; // TODO normal ça?

		writer.writeStartElement(CRITERIA_SETS_VALUES);
		new CommonAttributesParser().toXML(values, writer);
		writer.writeln();
		new DescriptionParser().toXML(values.getDescription(), writer);
		for (Entry<CriteriaSet<CRITERIA_SETS_VALUE_TYPE>, LabelledQValues<VALUE_TYPE>> entry: values.entrySet())
			new CriteriaSetValuesParser<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE>().toXML(entry.getKey(), entry.getValue(), writer);

		writer.writeEndElement();
		writer.writeln();
	}


}
