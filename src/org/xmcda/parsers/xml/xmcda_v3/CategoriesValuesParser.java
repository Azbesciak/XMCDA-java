package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CategoriesValues;
import org.xmcda.Category;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesValuesParser<VALUE_TYPE>
{
	public static final String CATEGORIES_VALUES = CategoriesValues.TAG;

	public static final String VALUES          = "values";

	public CategoriesValues<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	throws XMLStreamException
	{
		CategoriesValues<VALUE_TYPE> values = Factory.categoriesValues();
		new CommonAttributesParser().handleAttributes(values, startElement);

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			// categoryFunction
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (CATEGORIES_VALUES.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				values.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CategoryValuesParser.CATEGORY_VALUE.equals(startElement.asStartElement().getName().getLocalPart()))
			{
				new CategoryValuesParser<VALUE_TYPE>().fromXML(xmcda, values, startElement, eventReader);
			}
		}
		return values;
	}

	public void toXML(List<CategoriesValues<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CategoriesValues<VALUE_TYPE> values: list)
			toXML(values, writer);
	}

	public void toXML(CategoriesValues<VALUE_TYPE> values, XMLStreamWriter writer) throws XMLStreamException
	{
		if (values == null)
			return; // TODO normal ça?

		writer.writeStartElement(CATEGORIES_VALUES);
		new CommonAttributesParser().toXML(values, writer);
		writer.writeln();
		new DescriptionParser().toXML(values.getDescription(), writer);
		for (Entry<Category, LabelledQValues<VALUE_TYPE>> entry: values.entrySet())
			new CategoryValuesParser<VALUE_TYPE>().toXML(entry.getKey(), entry.getValue(), writer);

		writer.writeEndElement();
		writer.writeln();
	}


}
