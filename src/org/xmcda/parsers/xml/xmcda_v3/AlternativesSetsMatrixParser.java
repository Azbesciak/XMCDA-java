/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;

import org.xmcda.AlternativesSet;
import org.xmcda.AlternativesSetsMatrix;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesSetsMatrixParser <ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE>
    extends MatrixParser<AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE>, VALUE_TYPE>
{
	public static final String ALTERNATIVES_SETS_MATRIX = AlternativesSetsMatrix.TAG;

	@Override
	public String rootTag()
	{
		return ALTERNATIVES_SETS_MATRIX;
	}

	@Override
	public String dimensionTag()
	{
		return "alternativesSetID";
	}

	@Override
	public AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE> buildDimension(XMCDA xmcda, String id)
	{
		return (AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE>) xmcda.alternativesSets.get(id);
	}

	@Override
	public String dimensionID(AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE> alternativesSet)
	{
		return alternativesSet.id();
	}

	@Override
	public AlternativesSetsMatrix<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		Matrix<AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE>, VALUE_TYPE> matrix = super.fromXML(xmcda, startElement, eventReader);
		AlternativesSetsMatrix<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> alternativesSetsMatrix = Factory.alternativesSetsMatrix();

		alternativesSetsMatrix.setId(matrix.id());
		alternativesSetsMatrix.setName(matrix.name());
		alternativesSetsMatrix.setMcdaConcept(matrix.mcdaConcept());
		alternativesSetsMatrix.setDescription(matrix.getDescription());
		alternativesSetsMatrix.setValuation(matrix.getValuation());
		alternativesSetsMatrix.putAll(matrix);
		return alternativesSetsMatrix;
	}

	public void toXML(List<AlternativesSetsMatrix<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE>> matricesList, XMLStreamWriter writer)
		    throws XMLStreamException
	{
		if (matricesList == null || matricesList.size() == 0)
		    return;

		for (AlternativesSetsMatrix<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> matrix: matricesList)
			toXML(matrix, writer);
	}

	public void toXML(AlternativesSetsMatrix<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> matrix, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		super.toXML(matrix, writer);
	}
}
