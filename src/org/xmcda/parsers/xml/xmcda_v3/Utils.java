package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;


public class Utils
{
	public static String getTextContent(StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		String content = null;
		final String tagName = startElement.getName().getLocalPart();
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if ( tagName.equals(event.asEndElement().getName().getLocalPart()) )
					break;
			if (content != null)
				continue;
			if (event.isCharacters())
				content = event.asCharacters().getData(); // TODO check multi-line text???
		}
		return content;
	}
	
	public static StartElement getNextStartElement(StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isStartElement())
			{
				startElement = event.asStartElement();
				break;
			}
		}
		return startElement;
	}
	
	public static boolean booleanValue(String boolean_str)
	{
		return "true".equals(boolean_str) || "1".equals(boolean_str);
	}
}
