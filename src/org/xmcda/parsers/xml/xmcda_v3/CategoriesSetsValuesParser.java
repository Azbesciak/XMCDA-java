package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CategoriesSet;
import org.xmcda.CategoriesSetsValues;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesSetsValuesParser <CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE>
{
	public static final String CATEGORIES_SETS_VALUES = CategoriesSetsValues.TAG;

	public static final String VALUES               = "values";

	public CategoriesSetsValues<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CategoriesSetsValues<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE> values = Factory.categoriesSetsValues();
		new CommonAttributesParser().handleAttributes(values, startElement);

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			// categoriesSetFunction
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CATEGORIES_SETS_VALUES.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				values.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CategoriesSetValuesParser.CATEGORIES_SET_VALUE.equals(startElement.asStartElement().getName().getLocalPart()))
			{
				new CategoriesSetValuesParser<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE>().fromXML(xmcda, values, startElement, eventReader);
			}
		}
		return values;
	}

	public void toXML(List<CategoriesSetsValues<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE>> list, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (list == null || list.size() == 0)
		    return;
		for (CategoriesSetsValues<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE> values: list)
			toXML(values, writer);
	}

	public void toXML(CategoriesSetsValues<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE> values, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (values == null)
		    return; // TODO normal ça?

		writer.writeStartElement(CATEGORIES_SETS_VALUES);
		new CommonAttributesParser().toXML(values, writer);
		writer.writeln();
		new DescriptionParser().toXML(values.getDescription(), writer);
		for (Entry<CategoriesSet<CATEGORIES_SETS_VALUE_TYPE>, LabelledQValues<VALUE_TYPE>> entry: values.entrySet())
			new CategoriesSetValuesParser<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE>().toXML(entry.getKey(), entry.getValue(), writer);

		writer.writeEndElement();
		writer.writeln();
	}


}
