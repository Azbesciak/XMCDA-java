package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaThresholds;
import org.xmcda.Criterion;
import org.xmcda.CriterionThresholds;
import org.xmcda.Factory;
import org.xmcda.Threshold;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriterionThresholdsParser
{
	public static final String CRITERION_THRESHOLD = "criterionThreshold";

	public static final String CRITERION_ID        = "criterionID";

	public static final String THRESHOLDS          = "thresholds";

	public Criterion fromXML(XMCDA xmcda, CriteriaThresholds criteriaThresholds, StartElement startElement,
	                                XMLEventReader eventReader) throws XMLStreamException
	{
		CriterionThresholds thresholds = Factory.criterionThresholds();
		new CommonAttributesParser().handleAttributes(thresholds, startElement);
		Criterion criterion = null;
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERION_THRESHOLD.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				thresholds.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CriterionThresholdsParser.CRITERION_ID.equals(startElement.getName().getLocalPart()))
			    criterion = xmcda.criteria.get(Utils.getTextContent(startElement, eventReader));
			if (THRESHOLDS.equals(startElement.getName().getLocalPart()))
			    thresholdsFromXML(xmcda, thresholds, startElement, eventReader);
		}
		criteriaThresholds.put(criterion, thresholds);
		return criterion;
	}

	public void thresholdsFromXML(XMCDA xmcda, CriterionThresholds criterionThresholds, StartElement startElement,
	                                     XMLEventReader eventReader) throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (THRESHOLDS.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (ThresholdParser.THRESHOLD.equals(startElement.getName().getLocalPart()))
			    criterionThresholds.add(new ThresholdParser().fromXML(xmcda, startElement, eventReader));
		}
	}

	public void toXML(Criterion criterion, CriterionThresholds criterionThresholds, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (criterionThresholds == null)
		    return;

		writer.writeStartElement(CRITERION_THRESHOLD);
		new CommonAttributesParser().toXML(criterionThresholds, writer);
		writer.writeln();

		new DescriptionParser().toXML(criterionThresholds.getDescription(), writer);

		writer.writeElementChars(CRITERION_ID, criterion.id());

		writer.writeStartElement(THRESHOLDS);
		writer.writeln();

		for (Threshold threshold: criterionThresholds)
			new ThresholdParser().toXML(threshold, writer);

		writer.writeEndElement(); // THRESHOLDS
		writer.writeln();

		writer.writeEndElement(); // CRITERION_THRESHOLD
		writer.writeln();
	}
}
