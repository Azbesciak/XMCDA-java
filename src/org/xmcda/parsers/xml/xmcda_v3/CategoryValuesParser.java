package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CategoriesValues;
import org.xmcda.Category;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CategoryValuesParser<VALUE_TYPE>
{
	public static final String CATEGORY_VALUE = "categoryValue";

	public static final String CATEGORY_ID       = "categoryID";

	/**
	 * Builds the list of values attached to an ELEMENT and inserts it into the provided objectsValues object
	 * using the ELEMENT as the key.
	 * @param objectsValues
	 * @param startElement
	 * @param eventReader
	 * @return the ELEMENT object that was used to update the objectsValues with the built list of values
	 * @throws XMLStreamException
	 */
	public Category fromXML(XMCDA xmcda, CategoriesValues<VALUE_TYPE> objectsValues, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		LabelledQValues<VALUE_TYPE> categoryValues = Factory.labelledQValues();
		new CommonAttributesParser().handleAttributes(categoryValues, startElement);

		Category category = null;
		
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CATEGORY_VALUE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				categoryValues.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (QualifiedValuesParser.VALUES.equals(startElement.asStartElement().getName()
			        .getLocalPart()))
			{
				categoryValues.addAll(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}

			if (CATEGORY_ID.equals(startElement.getName().getLocalPart()))
			{
				final String id = Utils.getTextContent(startElement, eventReader);
				category = xmcda.categories.get(id);
			}
		}
		objectsValues.put(category, categoryValues);
		return category;
	}

	public void toXML(Category category, LabelledQValues<VALUE_TYPE> categoryValues, XMLStreamWriter writer) throws XMLStreamException
    {
		if (categoryValues == null)
			return;

		writer.writeStartElement(CATEGORY_VALUE);
		new CommonAttributesParser().toXML(categoryValues, writer);
		writer.writeln();
		new DescriptionParser().toXML(categoryValues.getDescription(), writer);
		writer.writeElementChars(CATEGORY_ID, category.id());
		new QualifiedValuesParser<VALUE_TYPE>().toXML(categoryValues, writer);
		writer.writeEndElement();
		writer.writeln();
    }

}
