package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaScales;
import org.xmcda.Criterion;
import org.xmcda.CriterionScales;
import org.xmcda.Factory;
import org.xmcda.Scale;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriterionScalesParser
{
	public static final String CRITERION_SCALE = "criterionScale";

	public static final String CRITERION_ID    = "criterionID";

	public static final String SCALES          = "scales";

	public Criterion fromXML(XMCDA xmcda, CriteriaScales criteriaScales, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CriterionScales scales = Factory.criterionScales();
		new CommonAttributesParser().handleAttributes(scales, startElement);
		Criterion criterion = null;
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERION_SCALE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				scales.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CriterionScalesParser.CRITERION_ID.equals(startElement.getName().getLocalPart()))
			    criterion = xmcda.criteria.get(Utils.getTextContent(startElement, eventReader));
			if (SCALES.equals(startElement.getName().getLocalPart()))
			    scalesFromXML(xmcda, scales, startElement, eventReader);
		}
		criteriaScales.put(criterion, scales);
		return criterion;
	}

	public void scalesFromXML(XMCDA xmcda, CriterionScales criterionScales, StartElement startElement,
	                                XMLEventReader eventReader) throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (SCALES.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (ScaleParser.SCALE.equals(startElement.getName().getLocalPart()))
				criterionScales.add(new ScaleParser().fromXML(xmcda, startElement, eventReader));
		}
	}

	public void toXML(Criterion criterion, CriterionScales criterionScales, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (criterionScales == null)
		    return;

		writer.writeStartElement(CRITERION_SCALE);
		new CommonAttributesParser().toXML(criterionScales, writer);
		writer.writeln();

		new DescriptionParser().toXML(criterionScales.getDescription(), writer);

		writer.writeElementChars(CRITERION_ID, criterion.id());

		writer.writeStartElement(SCALES);
		writer.writeln();

		for (Scale scale: criterionScales)
			new ScaleParser().toXML(scale, writer);

		writer.writeEndElement(); // SCALES
		writer.writeln();

		writer.writeEndElement(); // CRITERION_SCALE
		writer.writeln();
	}
}
