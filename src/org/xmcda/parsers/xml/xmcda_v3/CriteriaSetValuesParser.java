package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaSetsValues;
import org.xmcda.CriteriaSet;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaSetValuesParser<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE>
{
	public static final String CRITERIA_SET_VALUE = "criteriaSetValue";

	public static final String CRITERIA_SET_ID       = "criteriaSetID";

	/**
	 * Builds the list of values attached to an ELEMENT and inserts it into the provided objectsValues object
	 * using the ELEMENT as the key.
	 * @param objectsValues
	 * @param startElement
	 * @param eventReader
	 * @return the ELEMENT object that was used to update the objectsValues with the built list of values
	 * @throws XMLStreamException
	 */
	public CriteriaSet<CRITERIA_SET_VALUE_TYPE> fromXML(XMCDA xmcda, CriteriaSetsValues<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE> objectsValues, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		LabelledQValues<VALUE_TYPE> criteriaSetValues = Factory.labelledQValues();
		new CommonAttributesParser().handleAttributes(criteriaSetValues, startElement);

		CriteriaSet<CRITERIA_SET_VALUE_TYPE> criteriaSet = null;
		
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERIA_SET_VALUE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				criteriaSetValues.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (QualifiedValuesParser.VALUES.equals(startElement.asStartElement().getName()
			        .getLocalPart()))
			{
				criteriaSetValues.addAll(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}

			if (CRITERIA_SET_ID.equals(startElement.getName().getLocalPart()))
			{
				final String id = Utils.getTextContent(startElement, eventReader);
				criteriaSet = Factory.criteriaSet();
				criteriaSet.setId(id);
			}
		}
		objectsValues.put(criteriaSet, criteriaSetValues);
		return criteriaSet;
	}

	public void toXML(CriteriaSet<CRITERIA_SET_VALUE_TYPE> criteriaSet, LabelledQValues<VALUE_TYPE> criteriaSetValues, XMLStreamWriter writer) throws XMLStreamException
    {
		if (criteriaSetValues == null)
			return;

		writer.writeStartElement(CRITERIA_SET_VALUE);
		new CommonAttributesParser().toXML(criteriaSetValues, writer);
		writer.writeln();
		new DescriptionParser().toXML(criteriaSetValues.getDescription(), writer);
		writer.writeElementChars(CRITERIA_SET_ID, criteriaSet.id());
		new QualifiedValuesParser<VALUE_TYPE>().toXML(criteriaSetValues, writer);
		writer.writeEndElement();
		writer.writeln();
    }

}
