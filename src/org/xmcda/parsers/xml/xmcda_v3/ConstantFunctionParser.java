package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.XMCDA;
import org.xmcda.value.ConstantFunction;

/**
 * @author Sébastien Bigaret
 */
public class ConstantFunctionParser
{
	public static final String CONSTANT = "constant";

	@SuppressWarnings({ "rawtypes", "unchecked" })
    public ConstantFunction<?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
        ConstantFunction function = new ConstantFunction();
		new CommonAttributesParser().handleAttributes(function, startElement);

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (CONSTANT.equals(event.asEndElement().getName().getLocalPart()))
				    break;

			if (!event.isStartElement())
				continue;
			if (function.getValue() != null) // got it, wait until the tag is closed
				continue;

			startElement = event.asStartElement();
			function.setValue(new ValueParser().fromXML(xmcda, startElement, eventReader));
		}
		return function;
	}

	
	public void toXML(ConstantFunction<?> function, XMLStreamWriter writer) throws XMLStreamException
    {
		if (function == null)
			return; // TODO normal ça?

		writer.writeStartElement(CONSTANT);
		writer.writeln();
		new ValueParser().toXML(function.getValue(), writer);
		writer.writeEndElement();
		writer.writeln();
    }

	
}
