/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.CriteriaMatrix;
import org.xmcda.Criterion;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import org.xmcda.utils.Matrix;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import java.util.List;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaMatrixParser <VALUE_TYPE>
    extends MatrixParser<Criterion, VALUE_TYPE>
{
	public static final String CRITERIA_MATRIX = CriteriaMatrix.TAG;

	@Override
	public String rootTag()
	{
		return CRITERIA_MATRIX;
	}

	@Override
	public String dimensionTag()
	{
		return "criterionID";
	}

	@Override
	public Criterion buildDimension(XMCDA xmcda, String id)
	{
		return xmcda.criteria.get(id);
	}

	@Override
	public String dimensionID(Criterion criterion)
	{
		return criterion.id();
	}

	@Override
	public CriteriaMatrix<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		Matrix<Criterion, VALUE_TYPE> matrix = super.fromXML(xmcda, startElement, eventReader);
		CriteriaMatrix<VALUE_TYPE> criteriaMatrix = Factory.criteriaMatrix();

		criteriaMatrix.setId(matrix.id());
		criteriaMatrix.setName(matrix.name());
		criteriaMatrix.setMcdaConcept(matrix.mcdaConcept());
		criteriaMatrix.setDescription(matrix.getDescription());
		criteriaMatrix.setValuation(matrix.getValuation());
		criteriaMatrix.putAll(matrix);
		return criteriaMatrix;
	}

	public void toXML(List<CriteriaMatrix<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CriteriaMatrix<VALUE_TYPE> matrix: list)
			toXML(matrix, writer);
	}

	public void toXML(CriteriaMatrix<VALUE_TYPE> matrix, XMLStreamWriter writer) throws XMLStreamException
	{
		super.toXML(matrix, writer);
	}
}
