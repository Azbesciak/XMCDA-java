package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaFunctions;
import org.xmcda.Criterion;
import org.xmcda.CriterionFunctions;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriterionFunctionsParser
{
	public static final String CRITERION_FUNCTION = "criterionFunction";

	public static final String CRITERION_ID       = "criterionID";


	/**
	 * Builds the list of functions attached to a criterion and inserts it into the provided criteriaFunctions object
	 * using the criterion as the key.
	 * @param criteriaFunctions
	 * @param startElement
	 * @param eventReader
	 * @return the criterion that was used to update the criteriaFunctions with the built list of functions
	 * @throws XMLStreamException
	 */
	public Criterion fromXML(XMCDA xmcda, CriteriaFunctions criteriaFunctions, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CriterionFunctions criterionFunctions = new CriterionFunctions();
		new CommonAttributesParser().handleAttributes(criterionFunctions, startElement);

		Criterion criterion = null;
		
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERION_FUNCTION.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				criterionFunctions.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (FunctionsParser.FUNCTIONS.equals(startElement.asStartElement().getName()
			        .getLocalPart()))
			{
				criterionFunctions.addAll(new FunctionsParser().fromXML(xmcda, startElement, eventReader));
			}

			if (CRITERION_ID.equals(startElement.getName().getLocalPart()))
			{
				final String criterionID = Utils.getTextContent(startElement, eventReader);
				criterion = xmcda.criteria.get(criterionID);
			}
		}
		criteriaFunctions.put(criterion, criterionFunctions);
		return criterion;
	}

	public void toXML(String criterionID, CriterionFunctions criterionFunctions, XMLStreamWriter writer) throws XMLStreamException
    {
		if (criterionFunctions == null)
			return; // TODO normal ça?

		writer.writeStartElement(CRITERION_FUNCTION);
		new CommonAttributesParser().toXML(criterionFunctions, writer);
		writer.writeln();
		new DescriptionParser().toXML(criterionFunctions.getDescription(), writer);
		writer.writeElementChars(CRITERION_ID, criterionID);
		new FunctionsParser().toXML(criterionFunctions, writer);
		writer.writeEndElement();
		writer.writeln();
    }

}
