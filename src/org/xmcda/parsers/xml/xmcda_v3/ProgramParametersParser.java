package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.Factory;
import org.xmcda.ProgramParameter;
import org.xmcda.ProgramParameters;
import org.xmcda.XMCDA;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.List;

/**
 * @author Sébastien Bigaret
 */
public class ProgramParametersParser <VALUE_TYPE>
{
	public static final String PROGRAM_PARAMETERS = ProgramParameters.TAG;

	public ProgramParameters<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		ProgramParameters<VALUE_TYPE> prgParameters = Factory.<VALUE_TYPE> programParameters();
		new CommonAttributesParser().handleAttributes(prgParameters, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (PROGRAM_PARAMETERS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (DescriptionParser.DESCRIPTION.equals(currentTagName))
			{
				prgParameters.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (ProgramParameterParser.PROGRAM_PARAMETER.equals(currentTagName))
			{
				prgParameters.add(new ProgramParameterParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}
		}
		return prgParameters;
	}

	public void toXML(List<ProgramParameters<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (ProgramParameters<VALUE_TYPE> programParameters: list)
			toXML(programParameters, writer);
	}

	public void toXML(ProgramParameters<VALUE_TYPE> programParameters, XMLStreamWriter writer) throws XMLStreamException
	{
		if (programParameters == null)
		    return;

		writer.writeStartElement(PROGRAM_PARAMETERS);
		new CommonAttributesParser().toXML(programParameters, writer);
		writer.writeln();
		new DescriptionParser().toXML(programParameters.getDescription(), writer);

		final ProgramParameterParser<VALUE_TYPE> programParameterParser = new ProgramParameterParser<VALUE_TYPE>();
		for (ProgramParameter<VALUE_TYPE> programParameter: programParameters)
			programParameterParser.toXML(programParameter, writer);

		writer.writeEndElement(); // PROGRAM_PARAMETERS
		writer.writeln();
	}
}
