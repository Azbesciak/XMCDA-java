package org.xmcda.parsers.xml.xmcda_v3;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.XMCDA;
import org.xmcda.value.Function;

public class FunctionsParser
{
	public static final String FUNCTIONS = "functions";

	public List<Function> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		ArrayList<Function> functions = new ArrayList<Function>();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (FUNCTIONS.equals(event.asEndElement().getName().getLocalPart()))
				    break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (FunctionParser.FUNCTION.equals(startElement.getName().getLocalPart()))
			{
				functions.add(new FunctionParser().fromXML(xmcda, startElement, eventReader));
			}
		}
		
		return functions;
	}

	public void toXML(List<Function> functions, XMLStreamWriter writer) throws XMLStreamException
    {
		if (functions == null)
			return; // TODO normal ça?

		writer.writeStartElement(FUNCTIONS);
		writer.writeln();
		for (Function function: functions)
		{
			new FunctionParser().toXML(FunctionParser.FUNCTION, function, writer);
		}
		writer.writeEndElement();
		writer.writeln();
    }
}
