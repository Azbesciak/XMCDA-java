package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.Alternative;
import org.xmcda.CommonAttributes;
import org.xmcda.XMCDA;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * 
 * @author Sébastien Bigaret
 *
 */
public class AlternativeParser
{
	public static final String ALTERNATIVE = "alternative";

	public static final String TYPE        = "type";

	public static final String ACTIVE      = "active";

	public Alternative fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		CommonAttributes attributeHolder = new ReferenceableParser().getAttributes(startElement);
		Alternative alternative = xmcda.alternatives.get(attributeHolder.id());
		alternative.setName(attributeHolder.name());
		alternative.setMcdaConcept(attributeHolder.mcdaConcept());

		// Attributes handled, examine children
		while ( eventReader.hasNext() )
		{
			// description
			// type
			// active
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (ALTERNATIVE.equals(event.asEndElement().getName().getLocalPart()))
				    break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				alternative.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (TYPE.equals(startElement.getName().getLocalPart()))
			{
				event = eventReader.nextEvent();
				final String type = event.asCharacters().getData();
				alternative.setIsReal("real".equals(type)); // "real" or "fictive"
				continue;
			}
			if (ACTIVE.equals(startElement.getName().getLocalPart()))
			{
				event = eventReader.nextEvent();
				final String active = event.asCharacters().getData();
				alternative.setIsActive("true".equals(active) || "1".equals(active));
				continue;
			}
		}
		return alternative;
	}

	public void toXML(Alternative alternative, XMLStreamWriter writer) throws XMLStreamException
    {
		if (alternative == null)
			return; // TODO normal ça?

		writer.writeStartElement(ALTERNATIVE);
		new ReferenceableParser().toXML(alternative, writer);
		writer.writeln();
		new DescriptionParser().toXML(alternative.getDescription(), writer);
		writer.writeElementChars(TYPE, alternative.isReal() ? "real" : "fictive");
		writer.writeElementBoolean(ACTIVE, alternative.isActive());
		writer.writeEndElement();
		writer.writeln();
    }
}
