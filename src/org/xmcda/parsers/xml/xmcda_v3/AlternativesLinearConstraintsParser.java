package org.xmcda.parsers.xml.xmcda_v3;


import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.AlternativesLinearConstraints;
import org.xmcda.Alternative;
import org.xmcda.Factory;
import org.xmcda.LinearConstraint;
import org.xmcda.XMCDA;
import org.xmcda.parsers.xml.xmcda_v3.LinearConstraintParser.LinearConstraintParserHelper;

import java.util.List;

public class AlternativesLinearConstraintsParser <VALUE_TYPE>
{
	private class Helper
	    extends LinearConstraintParserHelper<Alternative>
	{
		@Override
		public Alternative buildObject(XMCDA xmcda, String id)
		{
			return xmcda.alternatives.get(id);
		}
	}

	public static final String ALTERNATIVES_LINEAR_CONSTRAINTS = AlternativesLinearConstraints.TAG;

	public static final String ALTERNATIVE_ID                = "alternativeID";

	public LinearConstraintParserHelper<Alternative> helper()
	{
		return new Helper();
	}

	public AlternativesLinearConstraints<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		AlternativesLinearConstraints<VALUE_TYPE> constraints = Factory.alternativesLinearConstraints();

		new CommonAttributesParser().handleAttributes(constraints, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVES_LINEAR_CONSTRAINTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				constraints.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (VariablesParser.VARIABLES.equals(startElement.getName().getLocalPart()))
			{
				constraints.setVariables(new VariablesParser().fromXML(xmcda, startElement, eventReader));
			}
			if (LinearConstraintParser.CONSTRAINTS.equals(startElement.getName().getLocalPart()))
			{
				readConstraintsFromXML(xmcda, constraints, startElement, eventReader);
			}

		}
		return constraints;
	}

	public void readConstraintsFromXML(XMCDA xmcda, 
	                                   AlternativesLinearConstraints<VALUE_TYPE> constraints, StartElement startElement,
	                                   XMLEventReader eventReader) throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (LinearConstraintParser.CONSTRAINTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (LinearConstraintParser.CONSTRAINT.equals(startElement.getName().getLocalPart()))
			{
				constraints.add(new LinearConstraintParser<Alternative, VALUE_TYPE>().constraintFromXML(xmcda,
				                                                                                      helper(),
				                                                                                      startElement,
				                                                                                      eventReader));
			}
		}
	}

	public void toXML(List<AlternativesLinearConstraints<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (AlternativesLinearConstraints<VALUE_TYPE> constraints: list)
			toXML(constraints, writer);
	}

	public void toXML(AlternativesLinearConstraints<VALUE_TYPE> constraints, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (constraints == null)
		    return;

		writer.writeStartElement(ALTERNATIVES_LINEAR_CONSTRAINTS);
		new CommonAttributesParser().toXML(constraints, writer);
		writer.writeln();

		new DescriptionParser().toXML(constraints.getDescription(), writer);

		new VariablesParser().toXML(constraints.getVariables(), writer);

		writer.writeStartElement(LinearConstraintParser.CONSTRAINTS);
		writer.writeln();
		for (LinearConstraint<Alternative, VALUE_TYPE> constraint: constraints)
		{
			new LinearConstraintParser<Alternative, VALUE_TYPE>().toXML(ALTERNATIVE_ID, constraint, writer);
		}
		writer.writeEndElement(); // CONSTRAINTS
		writer.writeln();
		writer.writeEndElement(); // ALTERNATIVES_LINEAR_CONSTRAINTS
		writer.writeln();
	}
}
