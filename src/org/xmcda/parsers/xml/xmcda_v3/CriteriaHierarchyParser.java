package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaHierarchy;
import org.xmcda.CriterionHierarchyNode;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

public class CriteriaHierarchyParser
{
	public static final String CRITERIA_HIERARCHY = "criteriaHierarchy";

	public static final String NODES			  = "nodes";

	public static final String NODE				  = "node";

	public CriteriaHierarchy fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
		    throws XMLStreamException
	{
		CriteriaHierarchy criteriaHierarchy = Factory.criteriaHierarchy();
		new CommonAttributesParser().handleAttributes(criteriaHierarchy, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (CRITERIA_HIERARCHY.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				criteriaHierarchy.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}

			if (NODES.equals(startElement.getName().getLocalPart()))
				parseNodes_fromXML(xmcda, criteriaHierarchy, startElement, eventReader);
		}
		return criteriaHierarchy;

	}

	private void parseNodes_fromXML(XMCDA xmcda, CriterionHierarchyNode node, StartElement startElement, XMLEventReader eventReader)
			throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (NODES.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (NODE.equals(startElement.getName().getLocalPart()))
			{
				node.addChild(parseNode_fromXML(xmcda, startElement, eventReader));
			}
		}
	}

	private CriterionHierarchyNode parseNode_fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
			throws XMLStreamException
	{
		CriterionHierarchyNode node = null;
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (NODE.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();

			if ("criterionID".equals(startElement.getName().getLocalPart()))
			{
				final String id = Utils.getTextContent(startElement, eventReader);
				node = new CriterionHierarchyNode(xmcda.criteria.get(id));
			}
			else if (NODES.equals(startElement.getName().getLocalPart()))
			{
				parseNodes_fromXML(xmcda, node, startElement, eventReader);
			}
		}
		return node;
	}

	public void toXML(List<CriteriaHierarchy> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CriteriaHierarchy criteriaHierarchy: list)
			toXML(criteriaHierarchy, writer);
	}

	public void toXML(CriteriaHierarchy criteriaHierarchy, XMLStreamWriter writer) throws XMLStreamException
	{
		if (criteriaHierarchy == null)
		    return;

		writer.writeStartElement(CRITERIA_HIERARCHY);
		new CommonAttributesParser().toXML(criteriaHierarchy, writer);
		writer.writeln();

		new DescriptionParser().toXML(criteriaHierarchy.getDescription(), writer);

		toXML((CriterionHierarchyNode)criteriaHierarchy, writer);
		writer.writeEndElement();
		writer.writeln();
	}

	private void toXML(CriterionHierarchyNode node, XMLStreamWriter writer) throws XMLStreamException
	{
		if (node == null)
		    return;
		if (node.getCriterion() != null)
		{
			writer.writeStartElement(NODE);
			writer.writeln();
			writer.writeElementChars("criterionID", node.getCriterion().id());
		}
		if (!node.getChildren().isEmpty())
		{
			writer.writeStartElement(NODES);
			writer.writeln();
			for (CriterionHierarchyNode child: node.getChildren())
				toXML(child, writer);
			writer.writeEndElement();
			writer.writeln();
		}
		if (node.getCriterion() != null)
		{
			writer.writeEndElement();
			writer.writeln();
		}
		
	}
}
