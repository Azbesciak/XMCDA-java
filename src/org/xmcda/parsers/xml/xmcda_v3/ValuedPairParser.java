package org.xmcda.parsers.xml.xmcda_v3;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CommonAttributes;
import org.xmcda.XMCDA;
import org.xmcda.utils.ValuedPair;

public class ValuedPairParser<ELEMENT, VALUE>
{
	public static final String PAIRS    = "pairs";

	public static final String PAIR     = "pair";

	public static final String INITIAL  = "initial";

	public static final String TERMINAL = "terminal";

	public static abstract class ValuedPairParserHelper<ELEMENT>
	{
		public abstract ELEMENT buildObject(XMCDA xmcda, String id);
	}
	
	public List<ValuedPair<ELEMENT, VALUE>> pairsFromXML(XMCDA xmcda, ValuedPairParserHelper<ELEMENT> helper,
	                                                                             StartElement startElement,
	                                                                             XMLEventReader eventReader)
	    throws XMLStreamException
	{
		ArrayList<ValuedPair<ELEMENT, VALUE>> pairs = new ArrayList<ValuedPair<ELEMENT, VALUE>>();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (PAIRS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (PAIR.equals(startElement.getName().getLocalPart()))
			{
				pairs.add(fromXML(xmcda, helper, startElement, eventReader));
			}
		}
		return pairs;
	}

	public ValuedPair<ELEMENT, VALUE> fromXML(XMCDA xmcda, ValuedPairParserHelper<ELEMENT> helper,
	                                                                  StartElement startElement,
	                                                                  XMLEventReader eventReader)
	    throws XMLStreamException
	{
		ValuedPair<ELEMENT, VALUE> pair = new ValuedPair<ELEMENT, VALUE>();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (PAIR.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			if (INITIAL.equals(startElement.getName().getLocalPart()))
			{
				startElement = Utils.getNextStartElement(startElement, eventReader); // get criterionID
				pair.setInitial(helper.buildObject(xmcda, Utils.getTextContent(startElement, eventReader)));
			}
			if (TERMINAL.equals(startElement.getName().getLocalPart()))
			{
				startElement = Utils.getNextStartElement(startElement, eventReader); // get criterionID
				pair.setTerminal(helper.buildObject(xmcda, Utils.getTextContent(startElement, eventReader)));
			}
			if (QualifiedValuesParser.VALUES.equals(startElement.getName().getLocalPart()))
			{
				pair.setValues(new QualifiedValuesParser().fromXML(xmcda, startElement, eventReader));
			}

		}
		return pair;
	}

	public void toXML(String elementTag, ValuedPair<? extends CommonAttributes, VALUE> pair,
	                                 XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeStartElement(PAIR);
		writer.writeln();

		writer.writeStartElement(INITIAL);
		writer.writeln();
		writer.writeElementChars(elementTag, pair.getInitial().id());
		writer.writeEndElement(); // INITIAL
		writer.writeln();

		writer.writeStartElement(TERMINAL);
		writer.writeln();
		writer.writeElementChars(elementTag, pair.getTerminal().id());
		writer.writeEndElement(); // TERMINAL
		writer.writeln();

		if (pair.getValues() != null && pair.getValues().size() > 0)
		{
			new QualifiedValuesParser().toXML(pair.getValues(), writer);
		}

		writer.writeEndElement(); // PAIR
		writer.writeln();

	}

}
