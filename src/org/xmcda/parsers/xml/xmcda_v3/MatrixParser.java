package org.xmcda.parsers.xml.xmcda_v3;

import java.util.LinkedHashSet;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Factory;
import org.xmcda.QualifiedValues;
import org.xmcda.XMCDA;
import org.xmcda.utils.Coord;
import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public abstract class MatrixParser <DIMENSION, VALUE_TYPE>
{
	public static final String VALUATION = "valuation";

	public static final String ROW       = "row";

	public static final String COLUMN    = "column";

	public abstract String rootTag();

	public abstract String dimensionTag();

	public abstract DIMENSION buildDimension(XMCDA xmcda, String id);

	public abstract String dimensionID(DIMENSION obj);

	public Matrix<DIMENSION, VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		final String initialTag = startElement.getName().getLocalPart();

		Matrix<DIMENSION, VALUE_TYPE> matrix = Factory.<DIMENSION, VALUE_TYPE> matrix();
		new CommonAttributesParser().handleAttributes(matrix, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				matrix.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (VALUATION.equals(startElement.getName().getLocalPart()))
			{
				matrix.setValuation(new ScaleParser().fromXML(xmcda, startElement, eventReader));
			}
			if (ROW.equals(startElement.getName().getLocalPart()))
			{
				readRowsFromXML(xmcda, matrix, startElement, eventReader);
			}
		}
		return matrix;
	}

	public void readRowsFromXML(XMCDA xmcda, Matrix<DIMENSION, VALUE_TYPE> matrix, StartElement startElement,
	                            XMLEventReader eventReader) throws XMLStreamException
	{
		DIMENSION row = null;
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ROW.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			if (dimensionTag().equals(startElement.getName().getLocalPart()))
			{
				row = buildDimension(xmcda, Utils.getTextContent(startElement, eventReader));
			}
			if (COLUMN.equals(startElement.getName().getLocalPart()))
			{
				readColumnsFromXML(xmcda, matrix, row, startElement, eventReader);
			}
		}
	}

	public void readColumnsFromXML(XMCDA xmcda, Matrix<DIMENSION, VALUE_TYPE> matrix, DIMENSION row, StartElement startElement,
	                               XMLEventReader eventReader) throws XMLStreamException
	{
		DIMENSION column = null;
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (COLUMN.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			if (dimensionTag().equals(startElement.getName().getLocalPart()))
			{
				column = buildDimension(xmcda, Utils.getTextContent(startElement, eventReader));

			}
			if (QualifiedValuesParser.VALUES.equals(startElement.getName().getLocalPart()))
			{
				final QualifiedValues<VALUE_TYPE> values = new QualifiedValuesParser<VALUE_TYPE>()
				        .fromXML(xmcda, startElement, eventReader);
				matrix.put(new Coord<DIMENSION, DIMENSION>(row, column), values);
			}
		}
	}

	/**
	 * Returns the columns having values in the matrix for the supplied row. We use this, instead of
	 * {@link Matrix#getColumns()}, to iterate on the columns in the same order as they were inserted i, the matrix
	 * (incl. when they were read from XML). This ensures that reading than writing an XMCDA ...Matrix tag produces the
	 * same XML structure and ordering (except for the formatting).
	 * 
	 * @param matrix
	 * @param row
	 * @return
	 */
	protected LinkedHashSet<DIMENSION> columnsForRow(Matrix<DIMENSION, VALUE_TYPE> matrix, DIMENSION row)
	{
		LinkedHashSet<DIMENSION> set = new LinkedHashSet<DIMENSION>();
		for (Coord<DIMENSION, DIMENSION> coord: matrix.keySet())
		{
			if (coord.x.equals(row))
			    set.add(coord.y);
		}
		return set;
	}

	public void toXML(Matrix<DIMENSION, VALUE_TYPE> matrix, XMLStreamWriter writer) throws XMLStreamException
	{
		if (matrix == null)
		    return;
		writer.writeStartElement(rootTag());
		new CommonAttributesParser().toXML(matrix, writer);
		writer.writeln();
		new DescriptionParser().toXML(matrix.getDescription(), writer);

		new ScaleParser().toXML(VALUATION, matrix.getValuation(), writer);

		for (DIMENSION row: matrix.getRows())
		{
			writer.writeStartElement(ROW);
			writer.writeln();
			writer.writeElementChars(dimensionTag(), dimensionID(row));
			for (DIMENSION column: columnsForRow(matrix, row))
			{
				final QualifiedValues<VALUE_TYPE> values = matrix.get(row, column);
				if (values == null)
				    continue;
				writer.writeStartElement(COLUMN);
				writer.writeln();
				writer.writeElementChars(dimensionTag(), dimensionID(column));
				new QualifiedValuesParser<VALUE_TYPE>().toXML(values, writer);
				writer.writeEndElement(); // COLUMN
				writer.writeln();
			}
			writer.writeEndElement(); // ROW
			writer.writeln();
		}
		writer.writeEndElement(); // rootTag()
		writer.writeln();

	}
}
