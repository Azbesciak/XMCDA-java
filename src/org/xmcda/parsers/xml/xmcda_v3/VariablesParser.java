/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import java.util.ArrayList;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Factory;
import org.xmcda.LinearConstraint.Variable;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 *
 */
public class VariablesParser
{
	public static final String VARIABLES = "variables";
	public static final String VARIABLE = "variable";

	public ArrayList<Variable> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		ArrayList<Variable> variables = new ArrayList<Variable>();
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (VARIABLES.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if ( ! event.isStartElement())
				continue;
			startElement = event.asStartElement();

			if (VARIABLE.equals(startElement.getName().getLocalPart()))
			{
				variables.add(variableFromXML(xmcda, startElement, eventReader));
			}
		}
		
		return variables;
	}
	
	public void toXML(ArrayList<Variable> variables, XMLStreamWriter writer) throws XMLStreamException
    {
		if (variables == null)
			return;

		writer.writeStartElement(VARIABLES);
		writer.writeln();
		for (Variable variable: variables)
			variableToXML(variable, writer);
		writer.writeEndElement(); // VARIABLES
		writer.writeln();
    }

	public Variable variableFromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Variable variable = Factory.linearConstraintVariable();
		new CommonAttributesParser().handleAttributes(variable, startElement);

		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (VARIABLE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if ( ! event.isStartElement())
				continue;
			startElement = event.asStartElement();

			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				variable.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
		}
		return variable;
	}
	
	public void variableToXML(Variable variable, XMLStreamWriter writer) throws XMLStreamException
    {
		if (variable == null)
			return;

		writer.writeStartElement(VARIABLE);
		new CommonAttributesParser().toXML(variable, writer);
		writer.writeln();
		new DescriptionParser().toXML(variable.getDescription(), writer);
		writer.writeEndElement(); // VARIABLE
		writer.writeln();
    }
}
