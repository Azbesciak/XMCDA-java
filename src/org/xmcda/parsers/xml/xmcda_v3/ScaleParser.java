/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.NominalScale;
import org.xmcda.QualitativeScale;
import org.xmcda.QuantitativeScale;
import org.xmcda.Scale;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 *
 */
public class ScaleParser
{
	public static final String SCALE = "scale";

	public Scale fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		// may be called w/ <scale> or <valuation>
		// will be used to fill in the common attributes into the value after it is built
		final StartElement initialElement = startElement;
		final String initialTag = initialElement.getName().getLocalPart();
		Scale scale = null;
		
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if ( scale != null ) // already got it, just wait for the event for </scale>
				continue;
			if ( ! event.isStartElement())
				continue;
			
			startElement = event.asStartElement();
			if (NominalScaleParser.NOMINAL.equals(startElement.getName().getLocalPart()))
				scale = new NominalScaleParser().fromXML(xmcda, startElement, eventReader);
			else if (QualitativeScaleParser.QUALITATIVE.equals(startElement.getName().getLocalPart()))
				scale = new QualitativeScaleParser().fromXML(xmcda, startElement, eventReader);
			else if (QuantitativeScaleParser.QUANTITATIVE.equals(startElement.getName().getLocalPart()))
				scale = new QuantitativeScaleParser().fromXML(xmcda, startElement, eventReader);
		}
		new CommonAttributesParser().handleAttributes(scale, initialElement);
		return scale;
	}
	
	public void toXML(Scale scale, XMLStreamWriter writer) throws XMLStreamException
	{
		toXML(SCALE, scale, writer);
	}
	
	public void toXML(String tag, Scale scale, XMLStreamWriter writer) throws XMLStreamException
    {
		if (scale == null)
			return; // TODO normal ça?

		writer.writeStartElement(tag);
		new CommonAttributesParser().toXML(scale, writer);
		writer.writeln();
		if (scale instanceof NominalScale)
			new NominalScaleParser().toXML((NominalScale) scale, writer);
		else if (scale instanceof QualitativeScale)
			new QualitativeScaleParser().toXML((QualitativeScale) scale, writer);
		else if (scale instanceof QuantitativeScale)
			new QuantitativeScaleParser().toXML((QuantitativeScale) scale, writer);

		writer.writeEndElement();
		writer.writeln();
    }
}
