package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaSetHierarchyNode;
import org.xmcda.CriteriaSetsHierarchy;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

public class CriteriaSetsHierarchyParser
{
	public static final String CRITERIA_SETS_HIERARCHY = "criteriaSetsHierarchy";

	public static final String NODES			  = "nodes";

	public static final String NODE				  = "node";

	public CriteriaSetsHierarchy fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
		    throws XMLStreamException
	{
		CriteriaSetsHierarchy criteriaSetsHierarchy = Factory.criteriaSetsHierarchy();
		new CommonAttributesParser().handleAttributes(criteriaSetsHierarchy, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (CRITERIA_SETS_HIERARCHY.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				criteriaSetsHierarchy.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}

			if (NODES.equals(startElement.getName().getLocalPart()))
				parseNodes_fromXML(xmcda, criteriaSetsHierarchy, startElement, eventReader);
		}
		return criteriaSetsHierarchy;

	}

	private void parseNodes_fromXML(XMCDA xmcda, CriteriaSetHierarchyNode node, StartElement startElement, XMLEventReader eventReader)
			throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (NODES.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (NODE.equals(startElement.getName().getLocalPart()))
			{
				node.addChild(parseNode_fromXML(xmcda, startElement, eventReader));
			}
		}
	}

	private CriteriaSetHierarchyNode parseNode_fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
			throws XMLStreamException
	{
		CriteriaSetHierarchyNode node = null;
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (NODE.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();

			if ("criteriaSetID".equals(startElement.getName().getLocalPart()))
			{
				final String id = Utils.getTextContent(startElement, eventReader);
				node = new CriteriaSetHierarchyNode(xmcda.criteriaSets.get(id));
			}
			else if (NODES.equals(startElement.getName().getLocalPart()))
			{
				parseNodes_fromXML(xmcda, node, startElement, eventReader);
			}
		}
		return node;
	}

	public void toXML(List<CriteriaSetsHierarchy> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CriteriaSetsHierarchy criteriaSetsHierarchy: list)
			toXML(criteriaSetsHierarchy, writer);
	}

	public void toXML(CriteriaSetsHierarchy criteriaSetsHierarchy, XMLStreamWriter writer) throws XMLStreamException
	{
		if (criteriaSetsHierarchy == null)
		    return;

		writer.writeStartElement(CRITERIA_SETS_HIERARCHY);
		new CommonAttributesParser().toXML(criteriaSetsHierarchy, writer);
		writer.writeln();

		new DescriptionParser().toXML(criteriaSetsHierarchy.getDescription(), writer);

		toXML((CriteriaSetHierarchyNode)criteriaSetsHierarchy, writer);
		writer.writeEndElement();
		writer.writeln();
	}

	private void toXML(CriteriaSetHierarchyNode node, XMLStreamWriter writer) throws XMLStreamException
	{
		if (node == null)
		    return;
		if (node.getCriteriaSet() != null)
		{
			writer.writeStartElement(NODE);
			writer.writeln();
			writer.writeElementChars("criteriaSetID", node.getCriteriaSet().id());
		}
		if (!node.getChildren().isEmpty())
		{
			writer.writeStartElement(NODES);
			writer.writeln();
			for (CriteriaSetHierarchyNode child: node.getChildren())
				toXML(child, writer);
			writer.writeEndElement();
			writer.writeln();
		}
		if (node.getCriteriaSet() != null)
		{
			writer.writeEndElement();
			writer.writeln();
		}
		
	}
}
