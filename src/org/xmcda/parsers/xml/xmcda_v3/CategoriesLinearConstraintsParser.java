package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CategoriesLinearConstraints;
import org.xmcda.Category;
import org.xmcda.Factory;
import org.xmcda.LinearConstraint;
import org.xmcda.XMCDA;
import org.xmcda.parsers.xml.xmcda_v3.LinearConstraintParser.LinearConstraintParserHelper;

public class CategoriesLinearConstraintsParser <VALUE_TYPE>
{
	private class Helper
	    extends LinearConstraintParserHelper<Category>
	{
		@Override
		public Category buildObject(XMCDA xmcda, String id)
		{
			return xmcda.categories.get(id);
		}
	}

	public static final String CATEGORIES_LINEAR_CONSTRAINTS = CategoriesLinearConstraints.TAG;

	public static final String CATEGORY_ID                = "categoryID";

	public LinearConstraintParserHelper<Category> helper()
	{
		return new Helper();
	}

	public CategoriesLinearConstraints<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CategoriesLinearConstraints<VALUE_TYPE> constraints = Factory.categoriesLinearConstraints();

		new CommonAttributesParser().handleAttributes(constraints, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CATEGORIES_LINEAR_CONSTRAINTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				constraints.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (VariablesParser.VARIABLES.equals(startElement.getName().getLocalPart()))
			{
				constraints.setVariables(new VariablesParser().fromXML(xmcda, startElement, eventReader));
			}
			if (LinearConstraintParser.CONSTRAINTS.equals(startElement.getName().getLocalPart()))
			{
				readConstraintsFromXML(xmcda, constraints, startElement, eventReader);
			}

		}
		return constraints;
	}

	public void readConstraintsFromXML(XMCDA xmcda, 
	                                   CategoriesLinearConstraints<VALUE_TYPE> constraints, StartElement startElement,
	                                   XMLEventReader eventReader) throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (LinearConstraintParser.CONSTRAINTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (LinearConstraintParser.CONSTRAINT.equals(startElement.getName().getLocalPart()))
			{
				constraints.add(new LinearConstraintParser<Category, VALUE_TYPE>().constraintFromXML(xmcda,
				                                                                                      helper(),
				                                                                                      startElement,
				                                                                                      eventReader));
			}
		}
	}

	public void toXML(List<CategoriesLinearConstraints<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CategoriesLinearConstraints<VALUE_TYPE> constraints: list)
			toXML(constraints, writer);
	}

	public void toXML(CategoriesLinearConstraints<VALUE_TYPE> constraints, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (constraints == null)
		    return;

		writer.writeStartElement(CATEGORIES_LINEAR_CONSTRAINTS);
		new CommonAttributesParser().toXML(constraints, writer);
		writer.writeln();

		new DescriptionParser().toXML(constraints.getDescription(), writer);

		new VariablesParser().toXML(constraints.getVariables(), writer);

		writer.writeStartElement(LinearConstraintParser.CONSTRAINTS);
		writer.writeln();
		for (LinearConstraint<Category, VALUE_TYPE> constraint: constraints)
		{
			new LinearConstraintParser<Category, VALUE_TYPE>().toXML(CATEGORY_ID, constraint, writer);
		}
		writer.writeEndElement(); // CONSTRAINTS
		writer.writeln();
		writer.writeEndElement(); // CATEGORIES_LINEAR_CONSTRAINTS
		writer.writeln();
	}
}
