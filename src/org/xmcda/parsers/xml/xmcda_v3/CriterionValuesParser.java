package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaValues;
import org.xmcda.Criterion;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriterionValuesParser<VALUE_TYPE>
{
	public static final String CRITERION_VALUE = "criterionValue";

	public static final String CRITERION_ID       = "criterionID";

	/**
	 * Builds the list of values attached to an ELEMENT and inserts it into the provided objectsValues object
	 * using the ELEMENT as the key.
	 * @param objectsValues
	 * @param startElement
	 * @param eventReader
	 * @return the ELEMENT object that was used to update the objectsValues with the built list of values
	 * @throws XMLStreamException
	 */
	public Criterion fromXML(XMCDA xmcda, CriteriaValues<VALUE_TYPE> objectsValues, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		LabelledQValues<VALUE_TYPE> criterionValues = Factory.labelledQValues();
		new CommonAttributesParser().handleAttributes(criterionValues, startElement);

		Criterion criterion = null;
		
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERION_VALUE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				criterionValues.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (QualifiedValuesParser.VALUES.equals(startElement.asStartElement().getName()
			        .getLocalPart()))
			{
				criterionValues.addAll(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}

			if (CRITERION_ID.equals(startElement.getName().getLocalPart()))
			{
				final String id = Utils.getTextContent(startElement, eventReader);
				criterion = xmcda.criteria.get(id);
			}
		}
		objectsValues.put(criterion, criterionValues);
		return criterion;
	}

	public void toXML(Criterion criterion, LabelledQValues<VALUE_TYPE> criterionValues, XMLStreamWriter writer) throws XMLStreamException
    {
		if (criterionValues == null)
			return;

		writer.writeStartElement(CRITERION_VALUE);
		new CommonAttributesParser().toXML(criterionValues, writer);
		writer.writeln();
		new DescriptionParser().toXML(criterionValues.getDescription(), writer);
		writer.writeElementChars(CRITERION_ID, criterion.id());
		new QualifiedValuesParser<VALUE_TYPE>().toXML(criterionValues, writer);
		writer.writeEndElement();
		writer.writeln();
    }

}
