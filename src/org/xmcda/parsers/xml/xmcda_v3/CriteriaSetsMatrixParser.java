/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.CriteriaSet;
import org.xmcda.CriteriaSetsMatrix;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import org.xmcda.utils.Matrix;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import java.util.List;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaSetsMatrixParser <CRITERIA_SET_VALUE_TYPE, VALUE_TYPE>
    extends MatrixParser<CriteriaSet<CRITERIA_SET_VALUE_TYPE>, VALUE_TYPE>
{
	public static final String CRITERIA_SETS_MATRIX = CriteriaSetsMatrix.TAG;

	@Override
	public String rootTag()
	{
		return CRITERIA_SETS_MATRIX;
	}

	@Override
	public String dimensionTag()
	{
		return "criteriaSetID";
	}

	@Override
	public CriteriaSet<CRITERIA_SET_VALUE_TYPE> buildDimension(XMCDA xmcda, String id)
	{
		return (CriteriaSet<CRITERIA_SET_VALUE_TYPE>) xmcda.criteriaSets.get(id);
	}

	@Override
	public String dimensionID(CriteriaSet<CRITERIA_SET_VALUE_TYPE> criteriaSet)
	{
		return criteriaSet.id();
	}

	@Override
	public CriteriaSetsMatrix<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		Matrix<CriteriaSet<CRITERIA_SET_VALUE_TYPE>, VALUE_TYPE> matrix = super.fromXML(xmcda, startElement, eventReader);
		CriteriaSetsMatrix<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE> criteriaSetsMatrix = Factory.criteriaSetsMatrix();

		criteriaSetsMatrix.setId(matrix.id());
		criteriaSetsMatrix.setName(matrix.name());
		criteriaSetsMatrix.setMcdaConcept(matrix.mcdaConcept());
		criteriaSetsMatrix.setDescription(matrix.getDescription());
		criteriaSetsMatrix.setValuation(matrix.getValuation());
		criteriaSetsMatrix.putAll(matrix);
		return criteriaSetsMatrix;
	}

	public void toXML(List<CriteriaSetsMatrix<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE>> matricesList, XMLStreamWriter writer)
		    throws XMLStreamException
	{
		if (matricesList == null || matricesList.size() == 0)
		    return;

		for (CriteriaSetsMatrix<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE> matrix: matricesList)
			toXML(matrix, writer);
	}

	public void toXML(CriteriaSetsMatrix<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE> matrix, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		super.toXML(matrix, writer);
	}
}
