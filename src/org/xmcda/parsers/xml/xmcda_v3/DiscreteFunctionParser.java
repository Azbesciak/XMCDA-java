package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.XMCDA;
import org.xmcda.value.DiscreteFunction;
import org.xmcda.value.Point;

public class DiscreteFunctionParser
{
	public static final String DISCRETE = "discrete";
	
	public DiscreteFunction<?,?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		DiscreteFunction function = new DiscreteFunction();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (DISCRETE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			if (PointParser.POINT.equals(startElement.getName().getLocalPart()))
				function.add(new PointParser().fromXML(xmcda, startElement, eventReader));
		}
		return function;
	}
	
	public void toXML(DiscreteFunction<?,?> function, XMLStreamWriter writer) throws XMLStreamException
    {
		if (function==null)
			return;
		
		writer.writeStartElement(DISCRETE);
		writer.writeln();
		for (Point point: function)
			new PointParser().toXML(point, writer);

		writer.writeEndElement();
		writer.writeln();
    }
}
