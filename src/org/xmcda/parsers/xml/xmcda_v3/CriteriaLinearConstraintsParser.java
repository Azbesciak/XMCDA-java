package org.xmcda.parsers.xml.xmcda_v3;


import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaLinearConstraints;
import org.xmcda.Criterion;
import org.xmcda.Factory;
import org.xmcda.LinearConstraint;
import org.xmcda.XMCDA;
import org.xmcda.parsers.xml.xmcda_v3.LinearConstraintParser.LinearConstraintParserHelper;

import java.util.List;

public class CriteriaLinearConstraintsParser <VALUE_TYPE>
{
	private class Helper
	    extends LinearConstraintParserHelper<Criterion>
	{
		@Override
		public Criterion buildObject(XMCDA xmcda, String id)
		{
			return xmcda.criteria.get(id);
		}
	}

	public static final String CRITERIA_LINEAR_CONSTRAINTS = CriteriaLinearConstraints.TAG;

	public static final String CRITERION_ID                = "criterionID";

	public LinearConstraintParserHelper<Criterion> helper()
	{
		return new Helper();
	}

	public CriteriaLinearConstraints<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CriteriaLinearConstraints<VALUE_TYPE> constraints = Factory.criteriaLinearConstraints();

		new CommonAttributesParser().handleAttributes(constraints, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERIA_LINEAR_CONSTRAINTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				constraints.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (VariablesParser.VARIABLES.equals(startElement.getName().getLocalPart()))
			{
				constraints.setVariables(new VariablesParser().fromXML(xmcda, startElement, eventReader));
			}
			if (LinearConstraintParser.CONSTRAINTS.equals(startElement.getName().getLocalPart()))
			{
				readConstraintsFromXML(xmcda, constraints, startElement, eventReader);
			}

		}
		return constraints;
	}

	public void readConstraintsFromXML(XMCDA xmcda, 
	                                   CriteriaLinearConstraints<VALUE_TYPE> constraints, StartElement startElement,
	                                   XMLEventReader eventReader) throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (LinearConstraintParser.CONSTRAINTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (LinearConstraintParser.CONSTRAINT.equals(startElement.getName().getLocalPart()))
			{
				constraints.add(new LinearConstraintParser<Criterion, VALUE_TYPE>().constraintFromXML(xmcda,
				                                                                                      helper(),
				                                                                                      startElement,
				                                                                                      eventReader));
			}
		}
	}

	public void toXML(List<CriteriaLinearConstraints<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CriteriaLinearConstraints<VALUE_TYPE> constraints: list)
			toXML(constraints, writer);
	}

	public void toXML(CriteriaLinearConstraints<VALUE_TYPE> constraints, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (constraints == null)
		    return;

		writer.writeStartElement(CRITERIA_LINEAR_CONSTRAINTS);
		new CommonAttributesParser().toXML(constraints, writer);
		writer.writeln();

		new DescriptionParser().toXML(constraints.getDescription(), writer);

		new VariablesParser().toXML(constraints.getVariables(), writer);

		writer.writeStartElement(LinearConstraintParser.CONSTRAINTS);
		writer.writeln();
		for (LinearConstraint<Criterion, VALUE_TYPE> constraint: constraints)
		{
			new LinearConstraintParser<Criterion, VALUE_TYPE>().toXML(CRITERION_ID, constraint, writer);
		}
		writer.writeEndElement(); // CONSTRAINTS
		writer.writeln();
		writer.writeEndElement(); // CRITERIA_LINEAR_CONSTRAINTS
		writer.writeln();
	}
}
