package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Factory;
import org.xmcda.QualitativeScale;
import org.xmcda.Scale;
import org.xmcda.Scale.PreferenceDirection;
import org.xmcda.XMCDA;
import org.xmcda.value.ValuedLabel;

/**
 * @author Sébastien Bigaret
 */
public class QualitativeScaleParser
{
	public static final String QUALITATIVE   = "qualitative";

	public static final String VALUED_LABELS = "valuedLabels";

	public QualitativeScale<?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		QualitativeScale scale = Factory.qualitativeScale();

		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (QUALITATIVE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (Scale.PREFERENCE_DIRECTION.equals(startElement.getName().getLocalPart()))
			{
				final String prefDir = Utils.getTextContent(startElement, eventReader).toUpperCase();
				scale.setPreferenceDirection(PreferenceDirection.valueOf(prefDir));
			}
			if (VALUED_LABELS.equals(startElement.getName().getLocalPart()))
			{
				valuedLabelsFromXML(xmcda, scale, startElement, eventReader);
			}
		}
		return scale;
	}

	/**
	 * Read the content of {@code <valuedLabels>} within qualitative scale.
	 * @param scale the qualitative scale the parsed valuedLabels will be added to.
	 * @param startElement
	 * @param eventReader
	 * @throws XMLStreamException
	 */
	protected void valuedLabelsFromXML(XMCDA xmcda, QualitativeScale<?> scale, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (VALUED_LABELS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (ValuedLabelParser.VALUED_LABEL.equals(startElement.getName().getLocalPart()))
			{
				scale.add(new ValuedLabelParser().fromXML(xmcda, startElement, eventReader));
			}
		}
	}

	public void toXML(QualitativeScale<?> scale, XMLStreamWriter writer) throws XMLStreamException
	{
		if (scale == null)
		    return;

		writer.writeStartElement(QUALITATIVE);
		writer.writeln();
		writer.writeElementChars(Scale.PREFERENCE_DIRECTION, scale.getPreferenceDirection().toString().toLowerCase());
		writer.writeStartElement(VALUED_LABELS);
		writer.writeln();

		for (ValuedLabel<?> valuedLabel: scale)
			new ValuedLabelParser().toXML(valuedLabel, writer);
		
		writer.writeEndElement(); // VALUED_LABELS
		writer.writeln();
		writer.writeEndElement(); // QUALITATIVE
		writer.writeln();
	}

}
