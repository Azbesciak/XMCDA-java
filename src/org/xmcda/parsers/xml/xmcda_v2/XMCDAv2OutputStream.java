package org.xmcda.parsers.xml.xmcda_v2;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class XMCDAv2OutputStream
    extends BufferedOutputStream
{
	final Version XMCDAv2Version;

	public XMCDAv2OutputStream(OutputStream out, Version version)
	{
		super(out);
		XMCDAv2Version = version;
	}

	public XMCDAv2OutputStream(OutputStream out, int size, Version version)
	{
		super(out, size < 8192 ? 8192 : size);
		XMCDAv2Version = version;
	}

	@Override
	public synchronized void write(int b) throws IOException
	{
		super.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException
	{
		super.write(b);
	}

	public Version getXMCDAVersion()
	{
	    return this.XMCDAv2Version;
	}

	@Override
	public synchronized void write(byte[] b, int off, int len) throws IOException
	{
		String s = new String(b, off, len);
		s = s.replace(Version.defaultVersion.namespace,
		              this.XMCDAv2Version.namespace);
		final byte[] c = s.getBytes();
		super.write(c, 0, c.length);
	}
}
