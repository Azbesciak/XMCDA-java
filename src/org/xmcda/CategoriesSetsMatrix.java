package org.xmcda;

import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesSetsMatrix<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE>
	extends Matrix<CategoriesSet<CATEGORIES_SET_VALUE_TYPE>, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "categoriesSetsMatrix";

	public CategoriesSetsMatrix()
	{
		super();
	}
}
