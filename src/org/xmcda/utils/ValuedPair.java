package org.xmcda.utils;

import org.xmcda.QualifiedValues;

public class ValuedPair <ELEMENT, VALUE>
{
	protected QualifiedValues<VALUE> values;

	protected ELEMENT                initial;

	protected ELEMENT                terminal;

	/**
	 * @return the values
	 */
	public QualifiedValues<VALUE> getValues()
	{
		return values;
	}

	/**
	 * @param values
	 *            the values to set
	 */
	public void setValues(QualifiedValues<VALUE> values)
	{
		this.values = values;
	}

	/**
	 * @return the initial
	 */
	public ELEMENT getInitial()
	{
		return initial;
	}

	/**
	 * @param initial
	 *            the initial to set
	 */
	public void setInitial(ELEMENT initial)
	{
		this.initial = initial;
	}

	/**
	 * @return the terminal
	 */
	public ELEMENT getTerminal()
	{
		return terminal;
	}

	/**
	 * @param terminal
	 *            the terminal to set
	 */
	public void setTerminal(ELEMENT terminal)
	{
		this.terminal = terminal;
	}

}
