/**
 * 
 */
package org.xmcda.utils;

import org.xmcda.value.Rational;

import java.util.HashMap;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class ValueConverters
{
	public static class ConversionException
	    extends Exception
	{
		private static final long serialVersionUID = 1L;

		public ConversionException()
		{
			super();
		}

		public ConversionException(String message)
		{
			super(message);
		}

		public ConversionException(Throwable cause)
		{
			super(cause);
		}

		public ConversionException(String message, Throwable cause)
		{
			super(message, cause);
		}
	}

	public static class ConversionError
	    extends ConversionException
	{
		private static final long serialVersionUID = 1L;

		public ConversionError()
		{
			super();
		}

		public ConversionError(String message)
		{
			super(message);
		}

		public ConversionError(Throwable cause)
		{
			super(cause);
		}

		public ConversionError(String message, Throwable cause)
		{
			super(message, cause);
		}
	}

	public static class NoConverterError
	    extends ConversionException
	{
		private static final long serialVersionUID = 1L;

		public NoConverterError()
		{
			super();
		}

		public NoConverterError(String message)
		{
			super(message);
		}

		public NoConverterError(Throwable cause)
		{
			super(cause);
		}

		public NoConverterError(String message, Throwable cause)
		{
			super(message, cause);
		}
	}

	public static class IdentityConverter<T>
	    implements IValueConverter<T,T>
	{
		@Override
		public T convert(T object) throws ConversionException
		{
			return object;
		}
	}

	public static class StringToDoubleConverter
	    implements IValueConverter<String, Double>
	{

		@Override
		public Double convert(String object) throws ConversionException
		{
			try
			{
				return Double.parseDouble(object);
			}
			catch (NumberFormatException | NullPointerException e)
			{
				throw new ConversionError(e);
			}
		}
	}

	public static class StringToIntegerConverter
	    implements IValueConverter<String, Integer>
	{

		@Override
		public Integer convert(String object) throws ConversionException
		{
			try
			{
				return Integer.parseInt(object);
			}
			catch (NumberFormatException | NullPointerException e)
			{
				throw new ConversionError(e);
			}
		}
	}

	public static class IntegerToDoubleConverter
	    implements IValueConverter<Integer, Double>
	{

		@Override
		public Double convert(Integer object) throws ConversionException
		{
			try
			{
				return object.doubleValue();
			}
			catch (NullPointerException e)
			{
				throw new ConversionError(e);
			}
		}
	}

	public static class RationalToDoubleConverter
	    implements IValueConverter<Rational, Double>
	{

		@Override
		public Double convert(Rational object) throws ConversionException
		{
			try
			{
				final int denominator = object.getDenominator();
				if ( denominator == 0 )
					throw new ConversionException("Rational denominator is zero");
				return ( ((Number)object.getNumerator()).doubleValue()) / ((Number)object.getDenominator()).doubleValue();
			}
			catch (NullPointerException e)
			{
				throw new ConversionError(e);
			}
		}
	}
	private static HashMap<Class, HashMap<Class, IValueConverter>> converters = new HashMap<>();

	static
	{
		register(String.class, Double.class, new StringToDoubleConverter());
		register(Integer.class, Double.class, new IntegerToDoubleConverter());
		register(String.class, Integer.class, new StringToIntegerConverter());
		register(Rational.class, Double.class, new RationalToDoubleConverter());

		register(Integer.class, String.class, new IValueConverter<Integer, String>()
		    { public String convert(Integer object) throws ConversionException{ return String.valueOf(object);} });
		register(Double.class, String.class, new IValueConverter<Double, String>()
		    { public String convert(Double object) throws ConversionException{ return String.valueOf(object);} });
		register(Boolean.class, String.class, new IValueConverter<Boolean, String>()
		    { public String convert(Boolean object) throws ConversionException{ return String.valueOf(object);} });
		register(Rational.class, String.class, new IValueConverter<Rational, String>()
		    { public String convert(Rational r) throws ConversionException{
		    	return String.valueOf(r.getNumerator()+" / "+r.getDenominator());} 
		    });
	}

	private ValueConverters() {}
	
	public static <SOURCE, DESTINATION> void register(Class<SOURCE> srcClass, Class<DESTINATION> dstClass,
	                                                  IValueConverter<SOURCE, DESTINATION> converter)
	{
		converters.computeIfAbsent(srcClass, x -> new HashMap()).put(dstClass, converter);
	}

	public static <SOURCE, DESTINATION> IValueConverter<SOURCE, DESTINATION> get(Class<SOURCE> srcClass,
	                                                                             Class<DESTINATION> dstClass)
	    throws NoConverterError
	{
		if (srcClass==dstClass)
			return new IdentityConverter();
		IValueConverter<SOURCE, DESTINATION> converter = null;
		try
		{
			converter = converters.get(srcClass).get(dstClass);
		}
		catch (NullPointerException e)
		{ /* ignore */}
		if (converter == null)
			throw new NoConverterError("Don't know how to convert "+srcClass + " to: " + dstClass);
		return converter;
	}

	@SuppressWarnings("unchecked")
	public static <SOURCE, DESTINATION> DESTINATION convert(SOURCE object, Class<DESTINATION> dstClass)
	    throws ConversionException
	{
		try
		{
			return ValueConverters.<SOURCE, DESTINATION> get((Class<SOURCE>) object.getClass(), dstClass).convert(object);
		}
		catch (RuntimeException e)
		{
			throw new ConversionError(e);
		}
	}
}
