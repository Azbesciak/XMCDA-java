/**
 *
 */
package org.xmcda.utils;

/**
 * @author Sébastien Bigaret
 */
public class Coord <X, Y>
{
	public final X x;

	public final Y y;

	public Coord(X x, Y y)
	{
		this.x = x;
		this.y = y;
	}

	static <X,Y> Coord<X,Y> of(X x, Y y)
	{
		return new Coord<>(x,y);
	}

	@Override
	public int hashCode()
	{
		if ( x == null )
			return y==null ? 0 : y.hashCode();
		if ( y == null )
			return x.hashCode();
		return x.hashCode() ^ y.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if ( ! ( obj instanceof Coord ) )
			return false;
		Coord<?,?> coord = (Coord<?,?>) obj;
		if ( this.x==null )
			return this.y==null ? coord.y==null : this.y.equals(coord.y);
		if ( this.y==null )
			return this.x.equals(coord.x);
	    return this.x.equals(coord.x) && this.y.equals(coord.y);
	}
}
