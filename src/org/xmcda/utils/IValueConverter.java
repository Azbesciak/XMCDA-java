/**
 * 
 */
package org.xmcda.utils;

import org.xmcda.utils.ValueConverters.ConversionException;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public interface IValueConverter <SOURCE, DESTINATION>
{
	public DESTINATION convert(SOURCE object) throws ConversionException;
}
