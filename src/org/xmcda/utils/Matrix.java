package org.xmcda.utils;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import org.xmcda.CommonAttributes;
import org.xmcda.Description;
import org.xmcda.HasDescription;
import org.xmcda.QualifiedValue;
import org.xmcda.QualifiedValues;
import org.xmcda.Scale;
import org.xmcda.utils.ValueConverters.ConversionException;

/**
 * @author Sébastien Bigaret
 */
public class Matrix<DIMENSION, VALUE_TYPE>
	extends LinkedHashMap<Coord<DIMENSION, DIMENSION>, QualifiedValues<VALUE_TYPE>>
	implements HasDescription, CommonAttributes, java.io.Serializable
{
    private static final long serialVersionUID = 1L;

    protected Scale valuation;

	/**
     * @return the valuation
     */
    public Scale getValuation()
    {
    	return valuation;
    }

	/**
     * @param valuation the valuation to set
     */
    public void setValuation(Scale valuation)
    {
    	this.valuation = valuation;
    }
    
    public QualifiedValues<VALUE_TYPE> get(DIMENSION row, DIMENSION column)
    {
    	return this.get(new Coord<DIMENSION, DIMENSION>(row, column));
    }

    public Set<DIMENSION> getRows()
    {
    	Set<DIMENSION> rows = new LinkedHashSet<DIMENSION>();
    	for (Coord<DIMENSION, DIMENSION> coord: this.keySet())
    		rows.add(coord.x);
    	return rows;
    }

    public Set<DIMENSION> getColumns()
    {
    	Set<DIMENSION> columns = new LinkedHashSet<DIMENSION>();
    	for (Coord<DIMENSION, DIMENSION> coord: this.keySet())
    		columns.add(coord.y);
    	return columns;
    }

	public void put(DIMENSION row, DIMENSION column, QualifiedValues<VALUE_TYPE> qvalues)
	{
		this.put(new Coord<>(row, column), qvalues);
	}

	public void put(DIMENSION row, DIMENSION column, QualifiedValue<VALUE_TYPE> qvalue)
	{
		this.put(new Coord<>(row, column), new QualifiedValues<>(qvalue));
	}

	public void put(DIMENSION row, DIMENSION column, VALUE_TYPE value)
	{
		this.put(new Coord<>(row, column), new QualifiedValues<>(new QualifiedValue<>(value)));
	}


	/**
	 * A performance table is said to be numeric if all its values are numeric
	 * 
	 * @return true if the performance table has numeric values only
	 */
	public boolean isNumeric()
	{
		return values().stream().allMatch(QualifiedValues::isNumeric);
	}

	public <U> void checkConversion(Class<U> clazz) throws ConversionException
	{
		for (QualifiedValues<VALUE_TYPE> values: values())
			values.checkConversion(clazz);
	}

	/**
	 * Converts the matrix' values to the specified type. If the conversion cannot be done,
	 * {@code ConversionException} is raised, and the values are left untouched.
	 * 
	 * @param <U> class of the converted values
	 * @param clazz the expected class of the converted values
	 * @return this object
	 * @throws ConversionException if at least one element cannot be converted; in this case, the values are left
	 *             unmodified.
	 */
	@SuppressWarnings("unchecked")
	public <U> Matrix<DIMENSION, U> convertTo(Class<U> clazz) throws ConversionException
	{
		// Try to convert all values: if a conversion fails, the values are not touched
		checkConversion(clazz);
		for (QualifiedValues<VALUE_TYPE> values: values())  // Now convert the values
			values.convertTo(clazz);
		return (Matrix<DIMENSION, U>) this;
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}
