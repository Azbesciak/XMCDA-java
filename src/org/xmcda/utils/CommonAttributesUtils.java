package org.xmcda.utils;

import java.util.Collection;

import org.xmcda.CommonAttributes;

public class CommonAttributesUtils
{
	public static <T extends CommonAttributes> T getElementWithMcdaConcept(Collection<T> collection, String mcdaConcept)
	{
		if (mcdaConcept==null)
		{
			for (T element: collection)
				if (element.mcdaConcept()==null)
					return element;
			return null;
		}
		else
		{
			for (T element: collection)
				if (mcdaConcept.equals(element.mcdaConcept()))
					return element;
			return null;
		}
	}

}
