package org.xmcda;

public class CategoriesLinearConstraints <VALUE_TYPE>
    extends LinearConstraints<Category, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long     serialVersionUID = 1L;

	public static final String TAG = "categoriesLinearConstraints";

	public CategoriesLinearConstraints()
	{ super(); }

}
