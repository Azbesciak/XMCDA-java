package org.xmcda;

import java.io.Serializable;
import java.util.Set;

/**
 * A hierarchy of criteriaSets.
 * 
 * @author Sébastien Bigaret
 */
@SuppressWarnings("rawtypes")
public class CriteriaSetsHierarchy extends CriteriaSetHierarchyNode
	implements HasDescription, CommonAttributes, XMCDARootElement, Serializable
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "criteriaSetsHierarchy";

	/**
	 * This node containing all the root nodes of the hierarchy.  Its {@link CriteriaSetHierarchyNode#getCriteriaSet()} is
	 * null because the object is used for its structure only.  That is the reason why this object is never exposed
	 * by the API, only its children (see {@link #getRoots()}, {@link #addRoot(CriteriaSetHierarchyNode)} etc.).
	 */
	private final CriteriaSetHierarchyNode root = this;

	public CriteriaSetsHierarchy() { super(); }
	
	/**
	 * Returns null.
	 */
	@Override
	public CriteriaSet getCriteriaSet()
	{
		return null;
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

	/**
	 * Returns the root nodes of this hierarchy. The returned set is unmodifiable, however if you
	 * {@link #addChild(CriteriaSet) add} or {@link #removeChild(CriteriaSet) remove} children from one of the returned
	 * root nodes, this will directly change this hierarchy as well. If you want to manipulate
	 * the hierarchy of the returned root nodes without affecting this hierarchy, call {@link #deepCopy()} on this
	 * object and then call this method on the copy instead. <br>
	 * This is an alias to {@link #getChildren()}.
	 * 
	 * @return the roots of this hierarchy.
	 */
	public Set<CriteriaSetHierarchyNode> getRootNodes()
	{
		return root.getChildren();
	}

	/**
	 * Adds a root node to this hierarchy if its criteriaSet is not already registered in an registered root node. <br>
	 * This is an alias to {@link #addChild(CriteriaSet)}
	 * 
	 * @param rootNode the root node to be added to this hierarchy.
	 * @return
	 */
	public boolean addRoot(CriteriaSetHierarchyNode rootNode)
	{
		return root.addChild(rootNode);
	}

	/**
	 * Adds a child to this node if the criteriaSet is not already registered as a root node. <br>
	 * This is an alias to {@link #addChild(CriteriaSet)}
	 */
	public boolean addRoot(CriteriaSet aCriteriaSet)
	{
		return root.addChild(new CriteriaSetHierarchyNode(aCriteriaSet));
	}
	
	/**
	 * Removes a root node from this node's children.<br>
	 * This is an alias to {@link #removeChild(CriteriaSetHierarchyNode)}.
	 * 
	 * @param aChild the element to be removed.
	 * @return {@code true} if the element was removed, {@code false} if it was not in this node's children.
	 */
	public boolean removeRoot(CriteriaSetHierarchyNode rootNode)
	{
		return root.removeChild(rootNode);
	}
	
	/**
	 * Removes from this hierarchy the root whose criteriaSet is the one supplied. <br>
	 * This is an alias to {@link #removeChild(CriteriaSet)}.
	 * 
	 * @param rootCriteriaSet the criteriaSet for the root to be removed.
	 * @return {@code true} if the corresponding root node was found and removed.
	 */
	public boolean removeRoot(CriteriaSet rootCriteriaSet)
	{
		return root.removeChild(rootCriteriaSet);
	}
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("CriteriaSetsHierarchy ");
		if (id() != null)
			sb.append("id:").append(id()).append(" ");

		sb.append(super.toString("<", ">"));
		return sb.toString();
	}

}
