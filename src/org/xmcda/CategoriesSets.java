package org.xmcda;


public class CategoriesSets <VALUE_TYPE>
    extends ReferenceableContainer<CategoriesSet<VALUE_TYPE>>
    implements HasDescription, CommonAttributes, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "categoriesSets";

	public CategoriesSets()
	{ super(); }

	@Override
	public Class<CategoriesSet<VALUE_TYPE>> elementClass()
	{
	    return (Class<CategoriesSet<VALUE_TYPE>>) new CategoriesSet<VALUE_TYPE>().getClass();
	}

	@Override
	public int hashCode()
	{
		return id().hashCode();
	}

	@Override
	public boolean equals(Object o)
	{
		if (!( o instanceof CategoriesSets ))
		    return false;
		if (this == o)
		    return true;

		CategoriesSets<?> sets = (CategoriesSets<?>) o;
		if (this.id() != null && this.id().equals(sets.id()))
		    return true;
		return sets.id() == null; // TODO CHECK content?
	}

}
