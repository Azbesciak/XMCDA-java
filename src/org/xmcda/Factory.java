package org.xmcda;

import org.xmcda.utils.Matrix;
import org.xmcda.value.FuzzyNumber;
import org.xmcda.value.PiecewiseLinearFunction;
import org.xmcda.value.Segment;
import org.xmcda.value.ValuedLabel;

/**
 * @author Sébastien Bigaret
 */
public abstract class Factory
{
	private Factory()
	{}

	public static <T> T build(Class<T> clazz)
	{
		if (clazz == ObjectsValues.class)
			return (T) new ObjectsValues();
		if (clazz == LabelledQValues.class)
			return (T) new LabelledQValues();
		throw new RuntimeException("Unimplemented");
	}

	public static <T> T build(Class<T> clazz, String id)
	{
		if (clazz == Alternative.class)
			return (T) new Alternative(id);
		if (clazz == AlternativesSet.class)
			return (T) new AlternativesSet(id);
		if (clazz == Criterion.class)
			return (T) new Criterion(id);
		if (clazz == CriteriaSet.class)
			return (T) new CriteriaSet(id);
		if (clazz == Category.class)
			return (T) new Category(id);
		if (clazz == CategoriesSet.class)
			return (T) new CategoriesSet(id);
		throw new RuntimeException("Unimplemented: " + clazz);
	}

	public static Alternative alternative(String id)
	{
		return new Alternative(id);
	}

	public static <VALUE_TYPE> AlternativeAssignment<VALUE_TYPE> alternativeAssignment()
    {
	    return new AlternativeAssignment<>();
    }

	public static Alternatives alternatives()
	{
		return new Alternatives();
	}

	public static <VALUE_TYPE> AlternativesAssignments<VALUE_TYPE> alternativesAssignments()
    {
	    return new AlternativesAssignments<>();
    }

	public static <VALUE_TYPE> AlternativesCriteriaValues<VALUE_TYPE> alternativesCriteriaValues()
    {
	    return new AlternativesCriteriaValues<>();
    }

	public static <VALUE_TYPE> AlternativesLinearConstraints<VALUE_TYPE> alternativesLinearConstraints()
	{
		return new AlternativesLinearConstraints<>();
	}

	public static <VALUE_TYPE> AlternativesMatrix<VALUE_TYPE> alternativesMatrix()
	{
		return new AlternativesMatrix<>();
	}

	public static <VALUE_TYPE> AlternativesSet<VALUE_TYPE> alternativesSet()
	{
		return new AlternativesSet<>();
	}

	public static <VALUE_TYPE> AlternativesSets<VALUE_TYPE> alternativesSets()
	{
		return new AlternativesSets<>();
	}

	public static <ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> AlternativesSetsLinearConstraints<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> alternativesSetsLinearConstraints()
	{
		return new AlternativesSetsLinearConstraints<>();
	}

	public static <ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> AlternativesSetsMatrix<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> alternativesSetsMatrix()
	{
		return new AlternativesSetsMatrix<>();
	}

	public static <ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE> AlternativesSetsValues<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE> alternativesSetsValues()
	{
		return new AlternativesSetsValues<>();
	}

	public static <VALUE_TYPE> AlternativesValues<VALUE_TYPE> alternativesValues()
	{
		return new AlternativesValues<>();
	}

	public static BibliographyEntry bibliographyEntry()
	{
		return new BibliographyEntry();
	}

	public static Categories categories()
	{
		return new Categories();
	}

	public static CategoriesInterval categoriesInterval()
    {
	    return new CategoriesInterval();
    }

	public static <VALUE_TYPE> CategoriesLinearConstraints<VALUE_TYPE> categoriesLinearConstraints()
	{
		return new CategoriesLinearConstraints<>();
	}

	public static <VALUE_TYPE> CategoriesMatrix<VALUE_TYPE> categoriesMatrix()
	{
		return new CategoriesMatrix<>();
	}

	public static <VALUE_TYPE> CategoriesProfiles<VALUE_TYPE> categoriesProfiles()
	{
		return new CategoriesProfiles<>();
	}

	public static <VALUE_TYPE> CategoriesSet<VALUE_TYPE> categoriesSet()
	{
		return new CategoriesSet<>();
	}

	public static <VALUE_TYPE> CategoriesSets<VALUE_TYPE> categoriesSets()
	{
		return new CategoriesSets<>();
	}

	public static <CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> CategoriesSetsLinearConstraints<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> categoriesSetsLinearConstraints()
	{
		return new CategoriesSetsLinearConstraints<>();
	}

	public static <CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> CategoriesSetsMatrix<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> categoriesSetsMatrix()
	{
		return new CategoriesSetsMatrix<>();
	}

	public static <CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE> CategoriesSetsValues<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE> categoriesSetsValues()
	{
		return new CategoriesSetsValues<>();
	}

	public static <VALUE_TYPE> CategoriesValues<VALUE_TYPE> categoriesValues()
	{
		return new CategoriesValues<>();
	}

	public static Category category(String id)
	{
		return new Category(id);
	}

	public static <VALUE_TYPE> CategoryProfile<VALUE_TYPE> categoryProfile(CategoryProfile.Type type)
	{
		return new CategoryProfile<>(type);
	}

	public static Criteria criteria()
	{
		return new Criteria();
	}

	public static CriteriaFunctions criteriaFunctions()
	{
		return new CriteriaFunctions();
	}

	public static CriteriaHierarchy criteriaHierarchy()
	{
		return new CriteriaHierarchy();
	}

	public static CriteriaSetHierarchyNode criteriaSetHierarchyNode(CriteriaSet criteriaSet)
	{
		return new CriteriaSetHierarchyNode(criteriaSet);
	}

	public static CriteriaSetsHierarchy criteriaSetsHierarchy()
	{
		return new CriteriaSetsHierarchy();
	}

	public static CriterionHierarchyNode criterionHierarchyNode(Criterion criterion)
	{
		return new CriterionHierarchyNode(criterion);
	}

	public static <VALUE_TYPE> CriteriaLinearConstraints<VALUE_TYPE> criteriaLinearConstraints()
	{
		return new CriteriaLinearConstraints<>();
	}

	public static <VALUE_TYPE> CriteriaMatrix<VALUE_TYPE> criteriaMatrix()
	{
		return new CriteriaMatrix<>();
	}

	public static CriteriaScales criteriaScales()
	{
		return new CriteriaScales();
	}

	public static <CRITERIASET_VALUE_TYPE> CriteriaSet<CRITERIASET_VALUE_TYPE> criteriaSet()
	{
		return new CriteriaSet<>();
	}

	public static <CRITERIASET_VALUE_TYPE> CriteriaSets<CRITERIASET_VALUE_TYPE> criteriaSets()
	{
		return new CriteriaSets<>();
	}

	public static <CRITERIASET_VALUE_TYPE, VALUE_TYPE> CriteriaSetsLinearConstraints<CRITERIASET_VALUE_TYPE, VALUE_TYPE> criteriaSetsLinearConstraints()
	{
		return new CriteriaSetsLinearConstraints<>();
	}

	public static <CRITERIA_SET_VALUE_TYPE, VALUE_TYPE> CriteriaSetsMatrix<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE> criteriaSetsMatrix()
	{
		return new CriteriaSetsMatrix<>();
	}

	public static <CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE> CriteriaSetsValues<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE> criteriaSetsValues()
	{
		return new CriteriaSetsValues<>();
	}

	public static <VALUE_TYPE> LabelledQValues<VALUE_TYPE> criteriaSetValues()
	{
		return Factory.<VALUE_TYPE>labelledQValues();
	}

	public static CriteriaThresholds criteriaThresholds()
	{
		return new CriteriaThresholds();
	}

	public static <VALUE_TYPE> CriteriaValues<VALUE_TYPE> criteriaValues()
	{
		return new CriteriaValues<>();
	}

	public static Criterion criterion(String id)
	{
		return new Criterion(id);
	}

	public static CriterionScales criterionScales()
	{
		return new CriterionScales();
	}

	public static CriterionThresholds criterionThresholds()
	{
		return new CriterionThresholds();
	}

	public static <VALUE_TYPE> LabelledQValues<VALUE_TYPE> criterionValues()
	{
		return Factory.<VALUE_TYPE>labelledQValues();
	}

	public static Description description()
	{
		return new Description();
	}

	public static <T1, T2> FuzzyNumber<T1, T2> fuzzyNumber()
	{
		return new FuzzyNumber<>();
	}

	public static <VALUE_TYPE> LabelledQValues<VALUE_TYPE> labelledQValues()
	{
		return new LabelledQValues<>();
	}

	public static <T, VALUE_TYPE> LinearConstraint<T, VALUE_TYPE> linearConstraint()
	{
		return new LinearConstraint<>();
	}

	public static <T, VALUE_TYPE> LinearConstraint.Element<T, VALUE_TYPE> linearConstraintElement()
	{
		return new LinearConstraint.Element<>();
	}

	public static LinearConstraint.Variable linearConstraintVariable()
	{
		return new LinearConstraint.Variable();
	}

	public static <DIMENSION, VALUE_TYPE> Matrix<DIMENSION, VALUE_TYPE> matrix()
    {
	    return new Matrix<>();
    }

	public static <VALUE_TYPE> ProgramParameter<VALUE_TYPE> programParameter()
    {
	    return new ProgramParameter<>();
    }

	public static <VALUE_TYPE> ProgramParameters<VALUE_TYPE> programParameters()
    {
	    return new ProgramParameters<>();
    }

	public static ProgramExecutionResult programExecutionResult()
	{
		return new ProgramExecutionResult();
	}

	public static NominalScale nominalScale()
	{
		return new NominalScale();
	}

	public static <T1, T2> PiecewiseLinearFunction <T1, T2> piecewiseLinearFunction()
	{
		return new PiecewiseLinearFunction<>();
	}

	public static <T> QualifiedValue<T> qualifiedValue()
	{
		return new QualifiedValue<>();
	}

	public static <T> QualifiedValues<T> qualifiedValues()
	{
		return new QualifiedValues<>();
	}

	public static <T> QualitativeScale<T> qualitativeScale()
	{
		return new QualitativeScale<>();
	}

	public static <T> QuantitativeScale<T> quantitativeScale()
	{
		return new QuantitativeScale<>();
	}

	public static <T1, T2> Segment <T1, T2> segment()
	{
		return new Segment<>();
	}

	public static <T> Threshold<T> threshold(QualifiedValue<T> constant)
	{
		return new Threshold<>(constant);
	}

	public static <T> Threshold<T> threshold(QualifiedValue<T> slope, QualifiedValue<T> intercept)
	{
		return new Threshold<>(slope, intercept);
	}

	public static <VALUE_TYPE> ValuedLabel<VALUE_TYPE> valuedLabel()
	{
		return new ValuedLabel<>();
	}
}
