package org.xmcda;

import java.io.Serializable;
import java.util.Set;

/**
 * A hierarchy of criteria.
 * 
 * @author Sébastien Bigaret
 */
public class CriteriaHierarchy extends CriterionHierarchyNode
	implements HasDescription, CommonAttributes, XMCDARootElement, Serializable
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "criteriaHierarchy";

	/**
	 * This node containing all the root nodes of the hierarchy. Its {@link CriterionHierarchyNode#getCriterion()} is
	 * null because the object is used for its structure only. That is the reason why this object is never exposed by
	 * the API, only its children (see {@link #getRoots()}, {@link #addRoot(CriterionHierarchyNode)} etc.).
	 */
	//private CriterionHierarchyNode root = new CriterionHierarchyNode();
	private final CriterionHierarchyNode root = this;

	public CriteriaHierarchy() { super(); }
	
	/**
	 * Returns null.
	 */
	@Override
	public Criterion getCriterion()
	{
		return null;
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

	/**
	 * Returns the root nodes of this hierarchy. The returned set is unmodifiable, however if you
	 * {@link #addChild(Criterion) add} or {@link #removeChild(Criterion) remove} children from one of the returned
	 * root nodes, this will directly change this hierarchy as well. If you want to manipulate
	 * the hierarchy of the returned root nodes without affecting this hierarchy, call {@link #deepCopy()} on this
	 * object and then call this method on the copy instead. <br>
	 * This is an alias to {@link #getChildren()}.
	 * 
	 * @return the roots of this hierarchy.
	 */
	public Set<CriterionHierarchyNode> getRootNodes()
	{
		return root.getChildren();
	}

	/**
	 * Adds a root node to this hierarchy if its criterion is not already registered in an registered root node. <br>
	 * This is an alias to {@link #addChild(Criterion)}
	 * 
	 * @param rootNode the root node to be added to this hierarchy.
	 * @return
	 */
	public boolean addRoot(CriterionHierarchyNode rootNode)
	{
		return root.addChild(rootNode);
	}

	/**
	 * Adds a child to this node if the criterion is not already registered as a root node. <br>
	 * This is an alias to {@link #addChild(Criterion)}
	 */
	public boolean addRoot(Criterion aCriterion)
	{
		return root.addChild(new CriterionHierarchyNode(aCriterion));
	}
	
	/**
	 * Removes a root node from this node's children.<br>
	 * This is an alias to {@link #removeChild(CriterionHierarchyNode)}.
	 * 
	 * @param aChild the element to be removed.
	 * @return {@code true} if the element was removed, {@code false} if it was not in this node's children.
	 */
	public boolean removeRoot(CriterionHierarchyNode rootNode)
	{
		return root.removeChild(rootNode);
	}
	
	/**
	 * Removes from this hierarchy the root whose criterion is the one supplied. <br>
	 * This is an alias to {@link #removeChild(Criterion)}.
	 * 
	 * @param rootCriterion the criterion for the root to be removed.
	 * @return {@code true} if the corresponding root node was found and removed.
	 */
	public boolean removeRoot(Criterion rootCriterion)
	{
		return root.removeChild(rootCriterion);
	}
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("CriteriaHierarchy ");
		if (id() != null)
			sb.append("id:").append(id()).append(" ");

		sb.append(super.toString("<", ">"));
		return sb.toString();
	}

}
