package org.xmcda;

public class CategoryProfile<VALUE_TYPE>
    implements HasDescription, CommonAttributes
{
	public static final String TAG = "categoryProfile";

	public static enum Type
	{
		CENTRAL, BOUNDING
	};

	public static class Profile<VALUE_TYPE>
	{
		Alternative alternative;
		QualifiedValues<VALUE_TYPE> values;
		/**
         * @return the alternative
         */
        public Alternative getAlternative()
        {
        	return alternative;
        }
		/**
         * @param alternative the alternative to set
         */
        public void setAlternative(Alternative alternative)
        {
        	this.alternative = alternative;
        }
		/**
         * @return the values
         */
        public QualifiedValues<VALUE_TYPE> getValues()
        {
        	return values;
        }
		/**
         * @param values the values to set
         */
        public void setValues(QualifiedValues<VALUE_TYPE> values)
        {
        	this.values = values;
        }
	}

	private Type     type;

	protected Category category;

	protected Profile<VALUE_TYPE>  centralProfile;

	protected Profile<VALUE_TYPE>  lowerBound;

	protected Profile<VALUE_TYPE>  upperBound;

	public CategoryProfile(Type type)
	{
		super();
		setType(type);
	}

	public CategoryProfile(String type)
	{
		super();
		setType(Type.valueOf(type));
	}

	/**
	 * Returns the type of this CategoryProfile.
	 * @return the type.  It is never {@code null}.
	 */
	public Type getType()
	{
		return type;
	}

	/**
	 * @param type
	 *            the type to set. It may not be null
	 */
	public void setType(Type type)
	{
		if (type == null)
		    throw new NullPointerException("type is not allowed to be null");
		this.type = type;
	}

	/**
	 * @return the category
	 */
	public Category getCategory()
	{
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(Category category)
	{
		this.category = category;
	}

	/**
	 * @return the central profile
	 */
	public Profile<VALUE_TYPE> getCentralProfile()
	{
		return centralProfile;
	}

	/**
	 * @param centralProfile
	 *            the centralProfile to set
	 */
	public void setCentralProfile(Profile<VALUE_TYPE> centralProfile)
	{
		this.centralProfile = centralProfile;
	}

	/**
	 * @return the lowerBound
	 */
	public Profile<VALUE_TYPE> getLowerBound()
	{
		return lowerBound;
	}

	/**
	 * @param lowerBound
	 *            the lowerBound to set
	 */
	public void setLowerBound(Profile<VALUE_TYPE> lowerBound)
	{
		this.lowerBound = lowerBound;
	}

	/**
	 * @return the upperBound
	 */
	public Profile<VALUE_TYPE> getUpperBound()
	{
		return upperBound;
	}

	/**
	 * @param upperBound
	 *            the upperBound to set
	 */
	public void setUpperBound(Profile<VALUE_TYPE> upperBound)
	{
		this.upperBound = upperBound;
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	@Override
	public String id()
	{
		return id;
	}

	@Override
	public void setId(String id)
	{
		this.id = id;
	}

	@Override
	public String name()
	{
		return name;
	}

	@Override
	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	@Override
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	@Override
	public void setDescription(Description description)
	{
		this.description = description;
	}

	@Override
	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}
