/**
 * 
 */
package org.xmcda;

import org.xmcda.utils.PerformanceTableCoord;
import org.xmcda.utils.ValueConverters.ConversionException;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Sébastien Bigaret
 */
public class PerformanceTable <T>
    extends LinkedHashMap<PerformanceTableCoord, QualifiedValues<T>>
    implements CommonAttributes, HasDescription, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "performanceTable";

	public PerformanceTable()
	{ super(); }

	public QualifiedValues<T> put(Alternative a, Criterion c, QualifiedValues<T> qvalues)
	{
		return this.put(new PerformanceTableCoord(a, c), qvalues);
	}

	public QualifiedValues<T> put(Alternative a, Criterion c, QualifiedValue<T> qvalue)
	{
		return this.put(new PerformanceTableCoord(a, c), new QualifiedValues<T>(qvalue));
	}

	public QualifiedValues<T> put(Alternative a, Criterion c, T value)
	{
		return this.put(new PerformanceTableCoord(a, c), new QualifiedValues<T>(new QualifiedValue<T>(value)));
	}

	public QualifiedValues<T> get(Alternative a, Criterion c)
	{
		return this.get(new PerformanceTableCoord(a, c));
	}

	/**
	 * Returns the evaluation of the supplied alternative on the supplied criterion, as a {@link QualifiedValue}.
	 * 
	 * @param a
	 *            the alternative
	 * @param c
	 *            the criterion
	 * @return the requested qualified value, or {@code null} if there is no such evaluation in the performance table
	 * @throws IllegalStateException
	 *             if the evaluation contains more than one value
	 */
	public QualifiedValue<T> getQValue(Alternative a, Criterion c)
	{
		QualifiedValues<T> values = this.get(new PerformanceTableCoord(a, c));
		if (values == null) return null;
		if (values.size() > 1) throw new IllegalStateException("More than one value at the requested coordinates"); // TODO
																													// check
																													// exc.
		return values.get(0);
	}

	/**
	 * Returns the evaluation of the supplied alternative on the supplied criterion.
	 * 
	 * @param a
	 *            the alternative
	 * @param c
	 *            the criterion
	 * @return the requested evaluation, or {@code null} if there is no such evaluation in the performance table
	 * @throws IllegalStateException
	 *             if the evaluation contains more than one value
	 */
	public T getValue(Alternative a, Criterion c)
	{
		QualifiedValue<T> qValue = getQValue(a, c);
		return qValue == null ? null : qValue.getValue();
	}

	/**
	 * Sets the evaluation of the supplied alternative on the supplied criterion.
	 * 
	 * @param a
	 *            the alternative
	 * @param c
	 *            the criterion
	 * @param value
	 *            the value to set
	 */
	public void setValue(Alternative a, Criterion c, T value)
	{
		this.put(a, c, value);
	}

	public Set<Alternative> getAlternatives()
	{
		Set<Alternative> alternatives = new LinkedHashSet<Alternative>();
		for (PerformanceTableCoord coord: this.keySet())
			alternatives.add(coord.x);
		return alternatives;
	}

	public Set<Criterion> getCriteria()
	{
		Set<Criterion> criteria = new LinkedHashSet<Criterion>();
		for (PerformanceTableCoord coord: this.keySet())
			criteria.add(coord.y);
		return criteria;
	}


	public PerformanceTableCoord getCoord(Alternative alternative, Criterion criterion)
	{
		for (PerformanceTableCoord coord: this.keySet())
			if (coord.x.equals(alternative) && coord.y.equals(criterion))
				return coord;
		return null;
	}

	/**
	 * A performance table is said to be numeric if all its values are numeric
	 * 
	 * @return true if the performance table has numeric values only
	 */
	public boolean isNumeric()
	{
		for (QualifiedValues<T> values: values())
			if (!values.isNumeric()) return false;
		return true;

	}

	public boolean hasMissingValues()
	{
		return this.size() != this.getAlternatives().size() * this.getCriteria().size();
	}

	public PerformanceTable<Double> asDouble() throws ConversionException
	{
		return this.convertTo(Double.class);
	}

	@SuppressWarnings("unchecked")
	public <U> PerformanceTable<U> convertTo(Class<U> clazz) throws ConversionException
	{
		for (QualifiedValues<T> values: values())
		{
			values.convertTo(clazz);
		}
		return (PerformanceTable<U>) this;
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String mcdaConcept;

	public String id()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;

	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}
