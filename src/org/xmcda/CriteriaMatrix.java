package org.xmcda;

import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaMatrix<VALUE_TYPE>
	extends Matrix<Criterion, VALUE_TYPE>
	implements XMCDARootElement
{
    private static final long serialVersionUID = 1L;

	public static final String TAG = "criteriaMatrix";

	public CriteriaMatrix()
	{
		super();
	}
}
