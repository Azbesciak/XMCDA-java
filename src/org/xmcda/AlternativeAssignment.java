package org.xmcda;


/**
 * xmcda:type:alternativeAssignment
 * @author Sébastien Bigaret
 */
public class AlternativeAssignment <VALUE_TYPE>
    implements HasDescription, CommonAttributes, java.io.Serializable
{
	private static final long           serialVersionUID = 1L;

	public static final String          TAG              = "alternativeAssignment";

	private Alternative                 alternative;

	private Category                    category;

	private CategoriesSet<?>            categoriesSet;

	private CategoriesInterval          categoryInterval;

	private QualifiedValues<VALUE_TYPE> values;

	/**
	 * @return the alternative
	 */
	public Alternative getAlternative()
	{
		return alternative;
	}

	/**
	 * @param alternative
	 *            the alternative to set
	 */
	public void setAlternative(Alternative alternative)
	{
		this.alternative = alternative;
	}

	/**
	 * @return the category
	 */
	public Category getCategory()
	{
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(Category category)
	{
		this.category = category;
	}

	/**
	 * @return the categoriesSet
	 */
	public CategoriesSet<?> getCategoriesSet()
	{
		return categoriesSet;
	}

	/**
	 * @param categoriesSet
	 *            the categoriesSet to set
	 */
	public void setCategoriesSet(CategoriesSet<?> categoriesSet)
	{
		this.categoriesSet = categoriesSet;
	}

	/**
	 * @return the categoryInterval
	 */
	public CategoriesInterval getCategoryInterval()
	{
		return categoryInterval;
	}

	/**
	 * @param categoryInterval
	 *            the categoryInterval to set
	 */
	public void setCategoryInterval(CategoriesInterval categoryInterval)
	{
		this.categoryInterval = categoryInterval;
	}

	/**
	 * @return the values
	 */
	public QualifiedValues<VALUE_TYPE> getValues()
	{
		return values;
	}

	/**
	 * @param values
	 *            the values to set
	 */
	public void setValues(QualifiedValues<VALUE_TYPE> values)
	{
		this.values = values;
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}
