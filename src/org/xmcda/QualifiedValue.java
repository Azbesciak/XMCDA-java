/**
 *
 */
package org.xmcda;

import java.io.Serializable;

import org.xmcda.utils.ValueConverters;
import org.xmcda.utils.ValueConverters.ConversionException;

/*
<xs:element name="integer" type="xs:int"/>
<xs:element name="real" type="xs:float"/>
<xs:element name="interval" type="xmcda:interval"/>
<xs:element name="rational" type="xmcda:rational"/>
<xs:element name="label" type="xs:string"/>
<xs:element name="boolean" type="xs:boolean"/>
<xs:element name="NA" type="xmcda:NA"/>
<xs:element name="valuedLabel" type="xmcda:valuedLabel"/>
<xs:element name="fuzzyNumber" type="xmcda:fuzzyNumber"/>
<xs:element name="valuedLabel" type="xmcda:valuedLabel"/>
*/

/**
 * Hold an evaluation of a alternative on a criterion, along with a description and the XMCDA so-called
 * {@link CommonAttributes "common attributes"}.
 *
 * @author Sébastien Bigaret
 */
public class QualifiedValue <T>
    implements CommonAttributes, HasDescription, Serializable
{
	public enum XMCDATypes
	{
		INTEGER("integer"),
		REAL("real"),
		LABEL("label"),
		BOOLEAN("boolean"),
		NA("NA"),
		RATIONAL("rational"),
		INTERVAL("interval"),
		FUZZY_NUMBER("fuzzyNumber"),
		VALUED_LABEL("valuedLabel");

		private String tag;

		XMCDATypes(String tag)
		{
			if (tag==null || "".equals(tag)) // watchdog
				throw new IllegalArgumentException("XMCDA tag cannot be null");
			this.tag = tag;
		}

		/**
		 * Return the tag associated to this xmcda type.
		 * @return the tag associated to this xmcda type.
		 */
		public String getTag()
		{
			return tag;
		}

		/**
		 * Return the XMCDA type associated to the supplied value ({@link #INTEGER} for integers, etc.)
		 * @param value the value for which the XMCDA type is requested (it cannot be {@code null})
		 * @return the XMCDA type associated to the supplied value
		 * @throws IllegalStateException
		 * @throws NullPointerException
		 * @see #getTag() to get the associated XMCDA tag
		 */
		public static XMCDATypes getXMCDAType(Object value) throws IllegalStateException, NullPointerException
		{
			if ( value == null )
				throw new NullPointerException("value cannot be null");

			if (value instanceof Integer)
				return INTEGER;

			if (value instanceof Double || value instanceof Float)
				return REAL;

			if (value instanceof String)
				return LABEL;

			if (value instanceof Boolean)
				return BOOLEAN;

			if (value instanceof org.xmcda.value.NA)
				return NA;

			if (value instanceof org.xmcda.value.Rational)
				return RATIONAL;

			if (value instanceof org.xmcda.value.Interval)
				return INTERVAL;

			if (value instanceof org.xmcda.value.FuzzyNumber)
				return FUZZY_NUMBER;

			if (value instanceof org.xmcda.value.ValuedLabel)
				return VALUED_LABEL;

			throw new IllegalStateException("Invalid class " + value.getClass().getCanonicalName());
		}
	}
    private static final long serialVersionUID = 1L;

    private T value;

	public QualifiedValue() { super(); }

	public QualifiedValue(T value)
	{
		super();
		this.value = value;
	}

	public T getValue()
	{
		return value;
	}

	public void setValue(T value)
	{
		this.value = value;
	}
	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	@Override
	public String id()
	{
		return id;
	}

	@Override
	public void setId(String id)
	{
		this.id = id;
	}

	@Override
	public String name()
	{
		return name;
	}

	@Override
	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	@Override
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;

	@Override
	public void setDescription(Description description)
	{
		this.description = description;
	}

	@Override
	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

	/**
	 * Checks that the conversion can be done but leave the object untouched.
	 * 
	 * @param <DESTINATION> the class to which the value should be converted
	 * @param value the value to be converted
	 * @param toClass the class to which the value should be converted
	 * @throws ConversionException raised if the conversion cannot be done
	 * @see #convertTo(Class)
	 */
	static public <DESTINATION> void checkConversion(QualifiedValue<?> value, Class<DESTINATION> toClass)
	    throws ConversionException
	{
		ValueConverters.convert(value.value, toClass);
	}

	public <DESTINATION> QualifiedValue<DESTINATION> convertTo(Class<DESTINATION> clazz) throws ConversionException
	{
		@SuppressWarnings("unchecked")
		final QualifiedValue<DESTINATION> u = (QualifiedValue<DESTINATION>) this;
		u.value = ValueConverters.convert(this.value, clazz);
		return u;
	}

	/**
	 * Converts this value to double. This a equivalent to {@code this.convertTo(Double.class)}.
	 *
	 * @return this object converted to {@link QualifiedValue QualifiedValue&lt;Double>}
	 * @throws ConversionException if the value could not be converted to a double. Note that this may happen even if
	 *             {@link #isNumeric()} is {@code True}: converting a rational with a denominator equal to zero throw
	 *             a ConversionException.
	 */
	public QualifiedValue<Double> convertToDouble() throws ConversionException
	{
		return this.convertTo(Double.class);
	}

	/**
	 * Return {@code true} if the value is of type integer, float, double or {@link Rational}
	 * @return {@code true} if the value belongs to a sub-type of {@code XMCDA:numeric}.
	 */
	public boolean isNumeric()
	{
		if (this.value instanceof Integer)
			return true;
		if (this.value instanceof Float)
			return true;
		if (this.value instanceof Double)
			return true;
		if (this.value instanceof org.xmcda.value.Rational)
			return true;
		if (this.value instanceof String)
		{
			try
			{
				Double.parseDouble((String) this.value);
			}
			catch (ClassCastException | NumberFormatException e)
			{
				return false;
			}
			return true;
		}
		return false;
	}


	/**
	 * Return the XMCDA tag for this value.
	 * Returned value are the one valid within the XMCDA type "value": "integer", "real", "interval",
	 * "rational, "label", "boolean", "NA", "fuzzyNumber" and "valuedLabel".
	 * @return the XMCDA tag for this value
	 * @throws NullPointerException if {@link #getValue()} is {@code null}.
	 * @throws IllegalArgumentException if the {@link #getValue()} is an instance of an invalid class for an XMCDA
	 * value.
	 */
	public String getXMCDATag() throws IllegalStateException, NullPointerException
	{
		return this.getXMCDAType().getTag();
	}

	/**
	 * Return the XMCDA {@link XMCDATypes} for this value.
	 * @return the XMCDA type for this value
	 * @throws NullPointerException if {@link #getValue()} is {@code null}.
	 * @throws IllegalArgumentException if the {@link #getValue()} is an instance of an invalid class for a XMCDA
	 * value.
	 */
	public XMCDATypes getXMCDAType() throws IllegalStateException, NullPointerException
	{
		return XMCDATypes.getXMCDAType(this.value);
	}
}
