package org.xmcda;


public class CriteriaSets <VALUE_TYPE>
    extends ReferenceableContainer<CriteriaSet<VALUE_TYPE>>
    implements HasDescription, CommonAttributes, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "criteriaSets";

	public CriteriaSets()
	{ super(); }

	@Override
	public Class<CriteriaSet<VALUE_TYPE>> elementClass()
	{
	    return (Class<CriteriaSet<VALUE_TYPE>>) new CriteriaSet<VALUE_TYPE>().getClass();
	}

	@Override
	public int hashCode()
	{
		return id().hashCode();
	}

	@Override
	public boolean equals(Object o)
	{
		if (!( o instanceof CriteriaSets ))
		    return false;
		if (this == o)
		    return true;

		CriteriaSets<?> sets = (CriteriaSets<?>) o;
		if (this.id() != null && this.id().equals(sets.id()))
		    return true;
		return sets.id() == null; // TODO CHECK content?
	}

}
