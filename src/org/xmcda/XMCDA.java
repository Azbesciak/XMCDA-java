/**
 *
 */
package org.xmcda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Sébastien Bigaret
 *
 */
public class XMCDA
{
	/**
	 * Maps ROOT_TAGS to their respective elements. It is lazily computed once, then cached.
	 * @see #rootElements()
	 */
	private Map<String, Object> rootElements = null;

	/**
	 * This is the list of all possible root XMCDA tags, i.e. the xml tags that are directly under the root tag
	 * {@code <xmcda>}.
	 */
	// We use a List inserted of String[] so that operator such as {@code .contains()} can be easily used.
	public static final List<String> ROOT_TAGS = Collections.unmodifiableList(Arrays.asList(
			Alternatives.TAG, AlternativesSets.TAG,
			Criteria.TAG, CriteriaSets.TAG,
			Categories.TAG, CategoriesSets.TAG,
			PerformanceTable.TAG,

			AlternativesValues.TAG, AlternativesSetsValues.TAG, AlternativesLinearConstraints.TAG,
			AlternativesSetsLinearConstraints.TAG, AlternativesMatrix.TAG, AlternativesSetsMatrix.TAG,

			CriteriaFunctions.TAG, CriteriaScales.TAG, CriteriaThresholds.TAG, CriteriaValues.TAG,
			CriteriaSetsValues.TAG, CriteriaLinearConstraints.TAG, CriteriaSetsLinearConstraints.TAG,
			CriteriaMatrix.TAG, CriteriaSetsMatrix.TAG, CriteriaHierarchy.TAG, CriteriaSetsHierarchy.TAG,

			AlternativesCriteriaValues.TAG,

			CategoriesProfiles.TAG,
			AlternativesAssignments.TAG,
			CategoriesValues.TAG,
			CategoriesSetsValues.TAG,
			CategoriesLinearConstraints.TAG,
			CategoriesSetsLinearConstraints.TAG,
			CategoriesMatrix.TAG,
			CategoriesSetsMatrix.TAG,

			ProgramParameters.TAG,
			ProgramExecutionResult.TAG
	));

	final public Alternatives alternatives = new Alternatives();
	final public AlternativesSets<?> alternativesSets = new AlternativesSets();
	final public Criteria criteria = new Criteria();
	final public CriteriaSets<?> criteriaSets = new CriteriaSets();
	final public Categories categories = new Categories();
	final public CategoriesSets<?> categoriesSets = new CategoriesSets();

	final public ArrayList<PerformanceTable<?>> performanceTablesList = new ArrayList<PerformanceTable<?>>();

	final public ArrayList<AlternativesValues<?>> alternativesValuesList = new ArrayList<AlternativesValues<?>>();
	final public ArrayList<AlternativesSetsValues<?, ?>> alternativesSetsValuesList = new ArrayList<AlternativesSetsValues<?, ?>>();
	final public ArrayList<AlternativesLinearConstraints< ?>> alternativesLinearConstraintsList = new ArrayList<AlternativesLinearConstraints<?>>();
	final public ArrayList<AlternativesSetsLinearConstraints<?, ?>> alternativesSetsLinearConstraintsList = new ArrayList<AlternativesSetsLinearConstraints<?, ?>>();
	final public ArrayList<AlternativesMatrix<?>> alternativesMatricesList = new ArrayList<AlternativesMatrix<?>>();
	final public ArrayList<AlternativesSetsMatrix<?, ?>> alternativesSetsMatricesList = new ArrayList<AlternativesSetsMatrix<?, ?>>();

	final public ArrayList<CriteriaFunctions> criteriaFunctionsList = new ArrayList<CriteriaFunctions>();
	final public ArrayList<CriteriaScales> criteriaScalesList = new ArrayList<CriteriaScales>();
	final public ArrayList<CriteriaThresholds> criteriaThresholdsList = new ArrayList<CriteriaThresholds>();
	final public ArrayList<CriteriaValues<?>> criteriaValuesList = new ArrayList<CriteriaValues<?>>();
	final public ArrayList<CriteriaSetsValues<?, ?>> criteriaSetsValuesList = new ArrayList<CriteriaSetsValues<?, ?>>();
	final public ArrayList<CriteriaLinearConstraints<?>> criteriaLinearConstraintsList = new ArrayList<CriteriaLinearConstraints<?>>();
	final public ArrayList<CriteriaSetsLinearConstraints<?, ?>> criteriaSetsLinearConstraintsList = new ArrayList<CriteriaSetsLinearConstraints<?, ?>>();
	final public ArrayList<CriteriaMatrix<?>> criteriaMatricesList = new ArrayList<CriteriaMatrix<?>>();
	final public ArrayList<CriteriaSetsMatrix<?, ?>> criteriaSetsMatricesList = new ArrayList<CriteriaSetsMatrix<?, ?>>();
	final public ArrayList<CriteriaHierarchy> criteriaHierarchiesList = new ArrayList<CriteriaHierarchy>();
	final public ArrayList<CriteriaSetsHierarchy> criteriaSetsHierarchiesList = new ArrayList<CriteriaSetsHierarchy>();

	final public ArrayList<AlternativesCriteriaValues<?>> alternativesCriteriaValuesList = new ArrayList<AlternativesCriteriaValues<?>>();

	final public ArrayList<CategoriesProfiles<?>> categoriesProfilesList = new ArrayList<CategoriesProfiles<?>>();
	final public ArrayList<AlternativesAssignments<?>> alternativesAssignmentsList = new ArrayList<AlternativesAssignments<?>>();
	final public ArrayList<CategoriesValues<?>> categoriesValuesList = new ArrayList<CategoriesValues<?>>();
	final public ArrayList<CategoriesSetsValues<?, ?>> categoriesSetsValuesList = new ArrayList<CategoriesSetsValues<?, ?>>();
	final public ArrayList<CategoriesLinearConstraints<?>> categoriesLinearConstraintsList = new ArrayList<CategoriesLinearConstraints<?>>();
	final public ArrayList<CategoriesSetsLinearConstraints<?, ?>> categoriesSetsLinearConstraintsList = new ArrayList<CategoriesSetsLinearConstraints<?, ?>>();
	final public ArrayList<CategoriesMatrix<?>> categoriesMatricesList = new ArrayList<CategoriesMatrix<?>>();
	final public ArrayList<CategoriesSetsMatrix<?, ?>> categoriesSetsMatricesList = new ArrayList<CategoriesSetsMatrix<?, ?>>();

	final public ArrayList<ProgramParameters<?>> programParametersList = new ArrayList<ProgramParameters<?>>();
	final public ArrayList<ProgramExecutionResult> programExecutionResultsList = new ArrayList<ProgramExecutionResult>();

	/**
	 * Return a map mapping the root tags (as defined in {@link #ROOT_TAGS}) to their respective elements. The size
	 * of the returned map is always equal to {@code ROOT_TAGS.size()}.
	 * @return a map with root tags and their corresponding elements
	 */
	public Map<String, Object> rootElements()
	{
		if ( rootElements != null )
		{
			return rootElements;
		}
		rootElements = new HashMap<>(ROOT_TAGS.size());
		//@formatter:off
		rootElements.put(Alternatives.TAG,                      alternatives);
		rootElements.put(AlternativesSets.TAG,                  alternativesSets);
		rootElements.put(Criteria.TAG,                          criteria);
		rootElements.put(CriteriaSets.TAG,                      criteriaSets);
		rootElements.put(Categories.TAG,                        categories);
		rootElements.put(CategoriesSets.TAG,                    categoriesSets);

		rootElements.put(PerformanceTable.TAG,                  performanceTablesList);

		rootElements.put(AlternativesValues.TAG,                alternativesValuesList);
		rootElements.put(AlternativesSetsValues.TAG,            alternativesSetsValuesList);
		rootElements.put(AlternativesLinearConstraints.TAG,     alternativesLinearConstraintsList);
		rootElements.put(AlternativesSetsLinearConstraints.TAG, alternativesSetsLinearConstraintsList);
		rootElements.put(AlternativesMatrix.TAG,                alternativesMatricesList);
		rootElements.put(AlternativesSetsMatrix.TAG,            alternativesSetsMatricesList);

		rootElements.put(CriteriaFunctions.TAG,                 criteriaFunctionsList);
		rootElements.put(CriteriaScales.TAG,                    criteriaScalesList);
		rootElements.put(CriteriaThresholds.TAG,                criteriaThresholdsList);
		rootElements.put(CriteriaValues.TAG,                    criteriaValuesList);
		rootElements.put(CriteriaSetsValues.TAG,                criteriaSetsValuesList);
		rootElements.put(CriteriaLinearConstraints.TAG,         criteriaLinearConstraintsList);
		rootElements.put(CriteriaSetsLinearConstraints.TAG,     criteriaSetsLinearConstraintsList);
		rootElements.put(CriteriaMatrix.TAG,                    criteriaMatricesList);
		rootElements.put(CriteriaSetsMatrix.TAG,                criteriaSetsMatricesList);
		rootElements.put(CriteriaHierarchy.TAG,                 criteriaHierarchiesList);
		rootElements.put(CriteriaSetsHierarchy.TAG,             criteriaSetsHierarchiesList);

		rootElements.put(AlternativesCriteriaValues.TAG,        alternativesCriteriaValuesList);

		rootElements.put(CategoriesProfiles.TAG,                categoriesProfilesList);
		rootElements.put(AlternativesAssignments.TAG,           alternativesAssignmentsList);
		rootElements.put(CategoriesValues.TAG,                  categoriesValuesList);
		rootElements.put(CategoriesSetsValues.TAG,              categoriesSetsValuesList);
		rootElements.put(CategoriesLinearConstraints.TAG,       categoriesLinearConstraintsList);
		rootElements.put(CategoriesSetsLinearConstraints.TAG,   categoriesSetsLinearConstraintsList);
		rootElements.put(CategoriesMatrix.TAG,                  categoriesMatricesList);
		rootElements.put(CategoriesSetsMatrix.TAG,              categoriesSetsMatricesList);

		rootElements.put(ProgramParameters.TAG,                 programParametersList);
		rootElements.put(ProgramExecutionResult.TAG,            programExecutionResultsList);
		//@formatter:on

		return rootElements;
	}

	/**
	 * Returns a map mapping the root tags (as defined in {@link #ROOT_TAGS}) to the number of elements it contains.
	 * This can be handy in different situations, such as when loading a file to determine which root tags have been
	 * added (see {@link #difference(Map, Map)}). The size of the returned map is always equal to
	 * {@code ROOT_TAGS.size()}.
	 * @return a map with the root tags and the number of their respective elements.
	 */
	public Map<String, Integer> rootElementsSize()
	{
		HashMap<String, Integer> rootElementsSize = new HashMap<>(ROOT_TAGS.size());
		//@formatter:off
		//
		// Copy-pasted from below. far simpler than using reflection just for that
		//
		rootElementsSize.put(Alternatives.TAG,                      alternatives.size());
		rootElementsSize.put(AlternativesSets.TAG,                  alternativesSets.size());
		rootElementsSize.put(Criteria.TAG,                          criteria.size());
		rootElementsSize.put(CriteriaSets.TAG,                      criteriaSets.size());
		rootElementsSize.put(Categories.TAG,                        categories.size());
		rootElementsSize.put(CategoriesSets.TAG,                    categoriesSets.size());

		rootElementsSize.put(PerformanceTable.TAG,                  performanceTablesList.size());

		rootElementsSize.put(AlternativesValues.TAG,                alternativesValuesList.size());
		rootElementsSize.put(AlternativesSetsValues.TAG,            alternativesSetsValuesList.size());
		rootElementsSize.put(AlternativesLinearConstraints.TAG,     alternativesLinearConstraintsList.size());
		rootElementsSize.put(AlternativesSetsLinearConstraints.TAG, alternativesSetsLinearConstraintsList.size());
		rootElementsSize.put(AlternativesMatrix.TAG,                alternativesMatricesList.size());
		rootElementsSize.put(AlternativesSetsMatrix.TAG,            alternativesSetsMatricesList.size());

		rootElementsSize.put(CriteriaFunctions.TAG,                 criteriaFunctionsList.size());
		rootElementsSize.put(CriteriaScales.TAG,                    criteriaScalesList.size());
		rootElementsSize.put(CriteriaThresholds.TAG,                criteriaThresholdsList.size());
		rootElementsSize.put(CriteriaValues.TAG,                    criteriaValuesList.size());
		rootElementsSize.put(CriteriaSetsValues.TAG,                criteriaSetsValuesList.size());
		rootElementsSize.put(CriteriaLinearConstraints.TAG,         criteriaLinearConstraintsList.size());
		rootElementsSize.put(CriteriaSetsLinearConstraints.TAG,     criteriaSetsLinearConstraintsList.size());
		rootElementsSize.put(CriteriaMatrix.TAG,                    criteriaMatricesList.size());
		rootElementsSize.put(CriteriaSetsMatrix.TAG,                criteriaSetsMatricesList.size());
		rootElementsSize.put(CriteriaHierarchy.TAG,                 criteriaHierarchiesList.size());
		rootElementsSize.put(CriteriaSetsHierarchy.TAG,             criteriaSetsHierarchiesList.size());

		rootElementsSize.put(AlternativesCriteriaValues.TAG,        alternativesCriteriaValuesList.size());

		rootElementsSize.put(CategoriesProfiles.TAG,                categoriesProfilesList.size());
		rootElementsSize.put(AlternativesAssignments.TAG,           alternativesAssignmentsList.size());
		rootElementsSize.put(CategoriesValues.TAG,                  categoriesValuesList.size());
		rootElementsSize.put(CategoriesSetsValues.TAG,              categoriesSetsValuesList.size());
		rootElementsSize.put(CategoriesLinearConstraints.TAG,       categoriesLinearConstraintsList.size());
		rootElementsSize.put(CategoriesSetsLinearConstraints.TAG,   categoriesSetsLinearConstraintsList.size());
		rootElementsSize.put(CategoriesMatrix.TAG,                  categoriesMatricesList.size());
		rootElementsSize.put(CategoriesSetsMatrix.TAG,              categoriesSetsMatricesList.size());

		rootElementsSize.put(ProgramParameters.TAG,                 programParametersList.size());
		rootElementsSize.put(ProgramExecutionResult.TAG,            programExecutionResultsList.size());
		//@formatter:on

		return rootElementsSize;
	}

	/**
	 * Return a map of root XMCDA tags whose number of element in the initial and final status are different.
	 * Equivalent to {@link #difference(Map, Map, boolean) difference(initialStatus, finalStatus, false)}
	 * @param initialStatus
	 * @param finalStatus
	 * @return the differences between the two status
	 * @see #difference(Map, Map, boolean)
	 */
	public static Map<String, Integer> difference(Map<String, Integer> initialStatus,
	                                              Map<String, Integer> finalStatus)
	{
		return difference(initialStatus, finalStatus, false);
	}

	/**
	 * Compares two different status as returned by {@link #rootElementsSize()} and returns the difference of
	 * elements declared in each tag between the two status. The difference is calculated on the basis of the content
	 * of {@code finalStatus}.  When both maps comes from {@link #rootElementsSize()} they obviously have the same
	 * size and the set of keys. If it not the case, it may happen that a tag/key in the {@code finalStatus} is not
	 * present in {@code initialStatus}, it defaults to zero.
	 * <br/>
	 * This may be useful when loading different files one after the other e.g., to check which (and how
	 * many) rootElements have been added.
	 * @param initialStatus the initial status
	 * @param finalStatus   the final status
	 * @param includeAll    if {@code False}, every tag in final Status is returned with its difference, even when it
	 *                      is zero.  When {@code true}, only tags with a non-zero difference are included in the
	 *                      returned map.
	 * @return a map associating tags in status to the difference between the two parameters.
	 */
	public static Map<String, Integer> difference(Map<String, Integer> initialStatus,
	                                              Map<String, Integer> finalStatus,
	                                              boolean includeAll)
	{
		HashMap<String, Integer> diffMap = new HashMap<>();
		for ( String tag: finalStatus.keySet() )
		{
			final int diff = finalStatus.get(tag) - initialStatus.getOrDefault(tag, 0);
			if ( diff != 0 || includeAll )
				diffMap.put(tag, diff);
		}
		return diffMap;
	}
}
