package org.xmcda;

public class AlternativesLinearConstraints <VALUE_TYPE>
    extends LinearConstraints<Alternative, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long     serialVersionUID = 1L;

	public static final String TAG = "alternativesLinearConstraints";

	public AlternativesLinearConstraints()
	{ super(); }

}
