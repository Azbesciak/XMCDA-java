/**
 * 
 */
package org.xmcda;

import java.io.Serializable;


/**
 * xmcda:type:category
 * 
 * @author Sébastien Bigaret
 */
public class Category
    implements HasDescription, Referenceable, Serializable
{
	private static final long serialVersionUID = 1L;

	public static CreationObserver creationObserver = Referenceable.defaultCreationObserver;

	private boolean           isActive         = true;

	public Category(String id)
	{
		super();
		setId(id);
		if ( creationObserver != null )
			creationObserver.objectCreated(this);
	}

	public Category(String id, String name)
	{
		super();
		setId(id);
		setName(name);
		if ( creationObserver != null )
			creationObserver.objectCreated(this);
	}

	public void merge(Referenceable object)
	{
		if (!( object instanceof Category ))
		    throw new IllegalArgumentException("Argument should be a Category");
		Category category = (Category) object;
		if (!this.id().equals(category.id()))
		    throw new IllegalArgumentException("Parameter's id() should be the same as this'");
		this.isActive = category.isActive;
		this.setDescription(category.getDescription());
		this.setName(category.name());
		this.setMcdaConcept(category.mcdaConcept());
	}

	public boolean isActive()
	{
		return isActive;
	}

	public void setIsActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * Two categories are equal if they have the same id
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null || !( obj instanceof Category ))
		    return false;
		final Category category = (Category) obj;
		return this.id().equals(category.id());
	}

	@Override
	public int hashCode()
	{
		return this.id().hashCode();
	}

	@Override
	public String toString()
	{
		StringBuffer str = new StringBuffer("Category id:");
		str.append(id());
		if (name() != null)
		    str.append(" [name:").append(name()).append("]");
		return str.toString();
	}

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

	// Referenceable (start)

	//   - implements CommonAttributes
	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	void _setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	//   Referenceable specific part
	private Object           marker;

	private transient Object container;

	public void setId(String id)
	{
		if (this.container != null)
			throw new IllegalStateException("Cannot change this object's id while it's in a container");
		_setId(id);
	}

	public void setContainer(Object container)
	{
		if ( container == null)
		{
			this.container = null;
			return;
		}
		if ( this.container != null && this.container != container )
			throw new IllegalStateException("this object already belongs to a other alternatives");
		this.container = container;
	}

	public Object getContainer()
	{
		return this.container;
	}

	public void setMarker(Object marker)
	{
		this.marker = marker;
	}

	public Object getMarker()
	{
		return this.marker;
	}

	// Referenceable (end)

}
