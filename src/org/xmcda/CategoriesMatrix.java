package org.xmcda;

import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesMatrix<VALUE_TYPE>
	extends Matrix<Category, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long  serialVersionUID = 1L;

	public static final String TAG = "categoriesMatrix";

	public CategoriesMatrix()
	{
		super();
	}
}
