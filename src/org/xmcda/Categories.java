package org.xmcda;

import java.util.ArrayList;
import java.util.List;

/**
 * xmcda:type:categories
 * 
 * @author Sébastien Bigaret
 */
public class Categories
    extends ReferenceableContainer<Category>
    implements CommonAttributes, HasDescription, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "categories";

	public Categories()
	{ super(); }

	@Override
	public Class<Category> elementClass()
	{
	    return Category.class;
	}

	public List<Category> getActiveCategories()
	{
		ArrayList<Category> activeCategories = new ArrayList<Category>(this);
		for (Category category: this)
			if (!category.isActive())
			    activeCategories.remove(category);
		return activeCategories;
	}


	public int getNumberOfActiveCategories()
	{
		int nb_active = 0;
		for (Category category: this)
			if (category.isActive())
			    nb_active++;
		return nb_active;
	}

	public void merge(Categories categories)
	{
		for (Category category: categories)
			this.merge(category);
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (Category category: this)
			sb.append(category.toString()).append(", ");
		sb.append("]");
		return sb.toString();
	}

}
