package org.xmcda;

import java.io.Serializable;

/**
 * @author Sébastien Bigaret
 */
public class Alternative
    implements Referenceable, HasDescription, Serializable
{
	private static final long serialVersionUID = 1L;

	public static CreationObserver creationObserver = Referenceable.defaultCreationObserver;

	// default: real (true)
	private boolean           isReal           = true;

	// default: true
	private boolean           isActive         = true;

	public Alternative(String id)
	{
		super();
		setId(id);
		if ( creationObserver != null )
			creationObserver.objectCreated(this);
	}

	public void merge(Referenceable object)
	{
		if (!( object instanceof Alternative ))
		    throw new IllegalArgumentException("Argument should be an Alternative");
		Alternative alternative = (Alternative) object;
		if (!this.id().equals(alternative.id()))
		    throw new IllegalArgumentException("Parameter's id() should be the same as this'");
		this.isReal = alternative.isReal;
		this.isActive = alternative.isActive;
		this.setDescription(alternative.getDescription());
		this.setName(alternative.name());
		this.setMcdaConcept(alternative.mcdaConcept());
	}

	/**
	 * @return
	 * @see #isFictive()
	 * @see #setIsReal(boolean)
	 */
	public boolean isReal()
	{
		return isReal;
	}

	/**
	 * @return
	 * @see #isReal()
	 * @see #setIsReal(boolean)
	 */
	public boolean isFictive()
	{
		return !isReal;
	}

	/**
	 * @param isReal
	 * @see #isReal()
	 * @see #setIsReal(boolean)
	 */
	public void setIsReal(boolean isReal)
	{
		this.isReal = isReal;
	}

	/**
	 * @return
	 * @see #activate()
	 * @see #deactivate()
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 */
	public void setIsActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @see #isActive()
	 * @see #activate()
	 */
	public void deactivate()
	{
		isActive = false;
	}

	/**
	 * @see #isActive()
	 * @see #deactivate()
	 */
	public void activate()
	{
		isActive = true;
	}

	/**
	 * Two alternatives are equal if they have the same id
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null || !( obj instanceof Alternative ))
		    return false;
		final Alternative alt = (Alternative) obj;
		return this.id().equals(alt.id());
	}

	@Override
	public int hashCode()
	{
		return this.id().hashCode();
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer("Alternative id:");
		sb.append(id()).append(" name:").append(name()).append(" mcdaConcept:").append(mcdaConcept());
		sb.append(" isActive:").append(isActive).append(" isReal:").append(isReal);
		return sb.toString();
	}

	public static void main(String[] args)
	{
		Alternative a1 = new Alternative("a01");
		Alternative a2 = new Alternative("a02");
		a1.setName("name01");
		a2.setName("name02");
		System.out.println(a1.name());
		System.out.println(a2.name());
		System.out.println(a1.name());
	}

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

	// Referenceable (start)

	//   - implements CommonAttributes
	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	void _setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	//   Referenceable specific part
	private Object           marker;

	private transient Object container;

	public void setId(String id)
	{
		if (this.container != null)
			throw new IllegalStateException("Cannot change this object's id while it's in a container");
		_setId(id);
	}

	public void setContainer(Object container)
	{
		if ( container == null)
		{
			this.container = null;
			return;
		}
		if ( this.container != null && this.container != container )
			throw new IllegalStateException("this object already belongs to a other alternatives");
		this.container = container;
	}

	public Object getContainer()
	{
		return this.container;
	}

	public void setMarker(Object marker)
	{
		this.marker = marker;
	}

	public Object getMarker()
	{
		return this.marker;
	}

	// Referenceable (end)

}
