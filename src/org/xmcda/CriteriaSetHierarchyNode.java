package org.xmcda;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.StringJoiner;

/**
 * A node of a hierarchy of criteria, representing a {@link #getCriteriaSet() criterion} and its {@link #getChildren()}.
 * 
 * @author Sébastien Bigaret
 */
@SuppressWarnings("rawtypes")
public class CriteriaSetHierarchyNode
    implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	/** The criteriaSet attached to this node. */
	private final CriteriaSet criteriaSet;

	private final Set<CriteriaSetHierarchyNode> nodes = new LinkedHashSet<>();
	
	public static final String TAG = "criteriaSetsHierarchy";

	/**
	 * Builds a new node.
	 * 
	 * @param aCriteriaSet the criteriaSet attached to the new node
	 * @throw {@link NullPointerException} if aCriteriaSet is {@code null}.
	 */
	public CriteriaSetHierarchyNode(CriteriaSet aCriteriaSet)
	{
		if ( aCriteriaSet == null )
			throw new NullPointerException("Parameter aCriteriaSet should not be null");
		this.criteriaSet = aCriteriaSet;
	}

	/**
	 * Builds a root node.  This is used by {@link CriteriaSetsHierarchy} to create the "super" node containing all
	 * the root nodes. A node object created by this method must not be exposed through the public API since
	 * the public API expects a node's criteriaSet to be non-null.
	 */
	CriteriaSetHierarchyNode()
	{
		this.criteriaSet = null;
	}

	/**
	 * Returns the criteriaSet attached to this node.
	 * @return the criteriaSet for this node.
	 */
	public CriteriaSet getCriteriaSet()
	{
		return criteriaSet;
	}

	/**
	 * Adds a child to this node if its criteriaSet is not already registered as a child.
	 * @param aNode the node to be added
	 * @return true if this node did not already contain the specified node's criteriaSet in its children.
	 */
	public boolean addChild(CriteriaSetHierarchyNode aNode)
	{
		if (nodes.stream().anyMatch((node) -> node.getCriteriaSet().equals(aNode.getCriteriaSet())))
			return false;
		return nodes.add(aNode);
	}

	/**
	 * Adds a child to this node if the criterion is not already registered as a child.
	 * @param aNode the node to be added
	 * @return true if the node did not already contain the criterion in its children.
	 */
	public boolean addChild(CriteriaSet aCriteriaSet)
	{
		return this.addChild(new CriteriaSetHierarchyNode(aCriteriaSet));
	}
	
	/**
	 * Removes a child from this node's children.
	 * 
	 * @param aChild the node to be removed.
	 * @return {@code true} if the element was removed, {@code false} if it was not in this node's children.
	 */
	public boolean removeChild(CriteriaSetHierarchyNode aChild)
	{
		return nodes.remove(aChild);
	}

	/**
	 * Removes from this node's children the child whose criteriaSet is the one supplied.
	 * 
	 * @param aChild the criteriaSet for the node to be removed.
	 * @return {@code true} if the corresponding child could be found and removed.
	 */
	public boolean removeChild(CriteriaSet aCriteriaSet)
	{
		return nodes.removeIf((node) -> node.criteriaSet.equals(aCriteriaSet));
	}

	/**
	 * Returns the children of this node. The returned set is unmodifiable, however if you {@link #addChild(CriteriaSet)
	 * add} or {@link #removeChild(CriteriaSet) remove} children from one of the returned set's nodes, this will
	 * directly change the sub-hierarchy held by this node as well. If you want to manipulate the hierarchy of the
	 * returned nodes without affecting this node's hierarchy, call {@link #deepCopy()} on this object and then call
	 * this method on the copy instead.
	 * 
	 * @return this node's children.
	 */
	public Set<CriteriaSetHierarchyNode> getChildren()
	{
		return Collections.unmodifiableSet(nodes);
	}

	/**
	 * Returns the child node for the supplied criteria set.
	 * 
	 * @param aCriteriaSet the criterion to search in this node's children
	 * @return the corresponding child node, or {@code null} if no such child could be found.
	 */
	public CriteriaSetHierarchyNode getChild(CriteriaSet aCriteriaSet)
	{
		return nodes.stream().filter((node) -> node.getCriteriaSet().equals(aCriteriaSet)).findFirst().orElse(null);
	}

	/**
	 * Returns the child node with the supplied criteria set ID.
	 * 
	 * @param aCriterionSetID the ID of a criteriaSet to search in this node's children
	 * @return the corresponding child node, or {@code null} if no such child could be found.
	 */
	public CriteriaSetHierarchyNode getChild(String aCriteriaSetID)
	{
		return nodes.stream().filter((node) -> node.getCriteriaSet().id().equals(aCriteriaSetID)).findFirst().orElse(null);
	}

	public boolean isLeaf()
	{
		return nodes.isEmpty();
	}

	/**
	 * Returns a deep copy of this node. The returned node and its children, and their children etc. can then be
	 * modified without affecting this node and the hierarchy underneath.
	 * 
	 * @return a deep copy of this node.
	 */
	public CriteriaSetHierarchyNode deepCopy()
	{
		CriteriaSetHierarchyNode copy = new CriteriaSetHierarchyNode(this.criteriaSet);
		this.nodes.stream().forEachOrdered(node -> copy.nodes.add((CriteriaSetHierarchyNode)node.deepCopy()));
		return copy;
	}

	@Override
	public String toString()
	{
		return toString(":<", ">");
	}
	
	protected String toString(String start, String end)
	{
		StringBuffer sb = new StringBuffer();
		if (criteriaSet!=null) // it is the case when called from CriteriaSetsHierarchy.toString()
			sb.append(criteriaSet.id());
		StringJoiner sj = new StringJoiner(",", start, end).setEmptyValue("");
		getChildren().stream().forEachOrdered(node -> sj.add(node.toString()));
		sb.append(sj.toString());
		return sb.toString();
	}
}
