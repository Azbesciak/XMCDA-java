package org.xmcda.converters.v2_v3;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;


/**
 * A simplified transformation of categoriesMatrices: the two only example are outputs
 * which only have categoryID in rows and columns.  These 2 programs are: 
 * <ul>
 * <li> (PUT) RORUTA-PairwiseOutrankingIndices,
 * <li> (PUT) RORUTA-PairwiseOutrankingIndicesHierarchical.
 * </ul>
 * @author Sébastien Bigaret
 */
public class CategoriesMatrixConverter
    extends Converter
{
	public static final String CATEGORIES_COMPARISONS = "categoriesComparisons";
	public static final String CATEGORIES_MATRIX      = org.xmcda.CategoriesMatrix.TAG;
	public static final String PAIRS                    = "pairs";
	public static final String PAIR                     = "pair";

	public CategoriesMatrixConverter()
	{
		super(CATEGORIES_MATRIX); // ajouter un constructeur super(tag_v2, tag_v3) ?
	}

	// v2 -> v3

	public void convertTo_v3(org.xmcda.v2.CategoriesMatrix value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORIES_MATRIX, value.getId());

		org.xmcda.CategoriesMatrix categoriesMatrix_v3 = Factory.categoriesMatrix();
		// defaultAttributes
		categoriesMatrix_v3.setId(value.getId());
		categoriesMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		categoriesMatrix_v3.setName(value.getName());
		categoriesMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			categoriesMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));

		// from here it diverges from categoriesComparisons
		// même pb pour categoriesMatrix qui mélange les 3 categoryID, categoriesSetId et categoriesSet
		
		// row
		for (org.xmcda.v2.CategoriesMatrix.Row row_v2: value.getRow())
		{
			// TODO warnings id, name, mcdaConcept, description ignored
			final String row_categoryID = row_v2.getCategoryID();
			final String row_categorySetID = row_v2.getCategoriesSetID();
			final org.xmcda.v2.CategoriesSet row_categoriesSet_v2 = row_v2.getCategoriesSet();

			if (row_categorySetID != null || row_categoriesSet_v2 != null)
			{
				getWarnings().throwUnimplemented();
			}
			final org.xmcda.Category row_category = xmcda_v3.categories.get(row_categoryID, true);
			// TODO ignore warnings for inner description, id etc...
			for (org.xmcda.v2.CategoriesMatrix.Row.Column column_v2: row_v2.getColumn())
			{
				final String column_categoryID = column_v2.getCategoryID();
				final String column_categorySetID = column_v2.getCategoriesSetID();
				final org.xmcda.v2.CategoriesSet column_categoriesSet_v2 = row_v2.getCategoriesSet();

				if (column_categorySetID != null || column_categoriesSet_v2 != null)
				{
					getWarnings().throwUnimplemented();
				}

				final org.xmcda.Category column_category = xmcda_v3.categories.get(column_categoryID, true);

				QualifiedValue value_v3;
				if ( column_v2.getValue()==null )
				{
					// no supplied value, value [0..1] in v2 : give n/a
					value_v3 = Factory.qualifiedValue();
					value_v3.setValue(org.xmcda.value.NA.na);
				}
				else
				{
				    value_v3 = new QualifiedValueConverter().convertTo_v3(column_v2.getValue(), xmcda_v3);
				}
				final org.xmcda.utils.Coord<org.xmcda.Category,org.xmcda.Category> coord_v3 = new org.xmcda.utils.Coord<org.xmcda.Category,org.xmcda.Category>(row_category, column_category);
				final org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
				values_v3.add(value_v3);
				categoriesMatrix_v3.put(coord_v3, values_v3);
				
			}
		}
		
		getWarnings().popTag(); // CATEGORIES_MATRIX
		xmcda_v3.categoriesMatricesList.add(categoriesMatrix_v3);
	}
	
	
	// v3 -> v2
	public void convertTo_v2(List<org.xmcda.CategoriesMatrix<?>> categoriesMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CategoriesMatrix categoriesMatrix_v3: categoriesMatrices_v3)
		{
			convertTo_v2(categoriesMatrix_v3, xmcda_v2);
		}
	}

	public void convertTo_v2(org.xmcda.CategoriesMatrix categoriesMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(CATEGORIES_MATRIX, categoriesMatrix_v3.id());

		org.xmcda.v2.CategoriesMatrix categoriesMatrix_v2 = new org.xmcda.v2.CategoriesMatrix();
		List<JAXBElement<?>> alts = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		alts.add(new JAXBElement<org.xmcda.v2.CategoriesMatrix>(new QName(CATEGORIES_MATRIX),
		                                                          org.xmcda.v2.CategoriesMatrix.class, categoriesMatrix_v2));


		// default attributes
		categoriesMatrix_v2.setId(categoriesMatrix_v3.id());
		categoriesMatrix_v2.setName(categoriesMatrix_v3.name());
		categoriesMatrix_v2.setMcdaConcept(categoriesMatrix_v3.mcdaConcept());

		// valuation: scale
		categoriesMatrix_v2.setValuation(new ScaleConverter().convertTo_v2(categoriesMatrix_v3.getValuation()));

		// rows
		for (Object _coord: categoriesMatrix_v3.keySet())
		{
			final org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			final org.xmcda.v2.CategoriesMatrix.Row row_v2 = new org.xmcda.v2.CategoriesMatrix.Row();
			categoriesMatrix_v2.getRow().add(row_v2);
			
			final org.xmcda.v2.CategoriesMatrix.Row.Column col_v2 = new org.xmcda.v2.CategoriesMatrix.Row.Column();
			row_v2.getColumn().add(col_v2);
			
			row_v2.setCategoryID( ((org.xmcda.Category) coord.x).id());
			col_v2.setCategoryID( ((org.xmcda.Category) coord.y).id());
			// values
			final org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			final org.xmcda.QualifiedValues values_v3 = (org.xmcda.QualifiedValues) categoriesMatrix_v3.get(_coord);
			if (values_v3 != null && (!values_v3.isEmpty()))
			{
				org.xmcda.QualifiedValue value = (org.xmcda.QualifiedValue) values_v3.get(0);
				final org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value);
				col_v2.setValue(value_v2);
			}

		}
		getWarnings().popTag(); // CATEGORIES_MATRIX
	}

}
