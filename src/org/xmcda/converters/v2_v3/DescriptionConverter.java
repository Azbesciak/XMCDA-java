package org.xmcda.converters.v2_v3;

import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import org.xmcda.Factory;

public class DescriptionConverter
    extends Converter
{
	public static final String DESCRIPTION            = "description";

	public static final String AUTHOR                 = "author";

	public static final String COMMENT                = "comment";

	public static final String KEYWORDS               = "keywords";

	public static final String CREATION_DATE          = "creationDate";

	public static final String LAST_MODIFICATION_DATE = "lastModificationDate";

	public DescriptionConverter()
	{
		super(DESCRIPTION);
	}

	public org.xmcda.Description convertTo_v3(org.xmcda.v2.Description description)
	{
		if (description == null)
		    return null;

		getWarnings().pushTag(DESCRIPTION);
		/*
		 * IGNORED: title, subTitle, subsubTitle, user, version, shortName, abstract, stakeholders
		 */
		org.xmcda.Description description_v3 = Factory.description();

		if (description.getTitle() != null)
		    getWarnings().elementIgnored("title");
		if (description.getSubTitle() != null)
		    getWarnings().elementIgnored("subTitle");
		if (description.getSubSubTitle() != null)
		    getWarnings().elementIgnored("subSubTitle");

		for (JAXBElement<?> element: description.getUserOrAuthorOrVersion())
		{
			final String currentName = element.getName().getLocalPart();
			Object value = element.getValue();
			if (value == null)
			    continue;

			if (COMMENT.equals(currentName))
				description_v3.setComment((String) value);
			else if (AUTHOR.equals(currentName))
			{
				description_v3.getAuthors().add((String) value);
			}
			else if (CREATION_DATE.equals(currentName))
			{
				description_v3.setCreationDate(( (XMLGregorianCalendar) value ).toGregorianCalendar());
			}
			else if (LAST_MODIFICATION_DATE.equals(currentName))
			{
				description_v3.setLastModificationDate(( (XMLGregorianCalendar) value ).toGregorianCalendar());
			}
			else if (KEYWORDS.equals(currentName))
			{
				for (String kw: ( (String) value ).split(","))
					description_v3.getKeywords().add(kw.trim());
			}
			else if (BibliographyConverter.BIBLIOGRAPHY.equals(currentName))
			{
				description_v3.setBibliography(new BibliographyConverter()
				        .convertTo_v3((org.xmcda.v2.Bibliography) value));
			}
			else
			{
				getWarnings().elementIgnored(currentName);
			}
		}
		getWarnings().popTag(); // DESCRIPTION
		return description_v3;
	}

	public org.xmcda.v2.Description convertTo_v2(org.xmcda.Description description_v3)
	{
		if (description_v3 == null)
		    return null;

		final DatatypeFactory dtf;
		try
		{
			dtf = DatatypeFactory.newInstance();
		}
		catch (DatatypeConfigurationException e)
		{
			return null; // TODO
		}
		getWarnings().pushTag(DESCRIPTION);

		/*
		 * IGNORED: title, subTitle, subsubTitle, user, version, shortName, abstract, stakeholders
		 */
		org.xmcda.v2.Description description_v2 = new org.xmcda.v2.Description();

		// author
		List<JAXBElement<?>> list_v2 = description_v2.getUserOrAuthorOrVersion();
		for (String author: description_v3.getAuthors())
			list_v2.add(new JAXBElement<String>(new QName(AUTHOR), String.class, author));

		// comment
		if (description_v3.getComment() != null)
		    list_v2.add(new JAXBElement<String>(new QName(COMMENT), String.class, description_v3.getComment()));

		// keywords
		if (description_v3.getKeywords() != null && description_v3.getKeywords().size() > 0)
		{
			StringBuffer kws = new StringBuffer();
			for (String keyword: description_v3.getKeywords())
			{
				kws.append(keyword).append(",");
			}
			final String kws_v2 = kws.substring(0, kws.length() - 1);
			list_v2.add(new JAXBElement<String>(new QName(KEYWORDS), String.class, kws_v2));
		}

		// creationDate
		if (description_v3.getCreationDate() != null)
		{
			final XMLGregorianCalendar _creationDate = dtf.newXMLGregorianCalendar(description_v3.getCreationDate());
			list_v2.add(new JAXBElement<XMLGregorianCalendar>(new QName(CREATION_DATE), XMLGregorianCalendar.class,
			                                                  _creationDate));
		}

		// lastModificationDate
		if (description_v3.getLastModificationDate() != null)
		{
			final GregorianCalendar _date = description_v3.getLastModificationDate();
			final XMLGregorianCalendar _lastModificationDate = dtf.newXMLGregorianCalendar(_date);
			list_v2.add(new JAXBElement<XMLGregorianCalendar>(new QName(LAST_MODIFICATION_DATE),
			                                                  XMLGregorianCalendar.class, _lastModificationDate));
		}

		// bibliography
		if (description_v3.getBibliography() != null)
		{
			final org.xmcda.v2.Bibliography bib_v2 = new BibliographyConverter().convertTo_v2(description_v3
			        .getBibliography());
			list_v2.add(new JAXBElement<org.xmcda.v2.Bibliography>(new QName(BibliographyConverter.BIBLIOGRAPHY),
			                                                           org.xmcda.v2.Bibliography.class, bib_v2));
		}
		getWarnings().popTag(); // DESCRIPTION
		return description_v2;
	}
}
