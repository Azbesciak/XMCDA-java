package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;


public class CategoriesSetConverter
    extends Converter
{
	public static final String CATEGORIES_SET = "categoriesSet";

	public CategoriesSetConverter()
	{
		super(CATEGORIES_SET);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
    public org.xmcda.CategoriesSet convertTo_v3(org.xmcda.v2.CategoriesSet categoriesSet_v2, org.xmcda.XMCDA xmcda_v3)
	{
		// TODO ignorés pour le moment: value et values
		String categoriesSetID = categoriesSet_v2.getId();
		getWarnings().pushTag(CATEGORIES_SET, categoriesSetID);

		if (categoriesSetID == null || "".equals(categoriesSetID))
		{
			// we are called by with a categoriesSet with no ID, which may happen when the categoriesSet is embedded in another structure,
			// such as in <categoriesValues>
			// we need to find one which is not already assigned: categoriesSet must have an ID in v3
			final String baseID="categoriesSet_generatedID_";
			int idx=1;
			categoriesSetID = baseID+idx;
			while (xmcda_v3.categoriesSets.contains(categoriesSetID))
			{
				idx = idx+1;
				categoriesSetID = baseID+idx;
			}
		}
		if (!categoriesSet_v2.getValueOrValues().isEmpty())
			getWarnings().elementIgnored("value", "value ou values ignored on categoriesSet: they are NOT translated into v3 categoriesSetsValues by this converter");
		org.xmcda.CategoriesSet categoriesSet_v3 = org.xmcda.Factory.categoriesSet();
		categoriesSet_v3.setId(categoriesSetID);
		categoriesSet_v3.setName(categoriesSet_v2.getName());
		categoriesSet_v3.setMcdaConcept(categoriesSet_v2.getMcdaConcept());
		
		categoriesSet_v3.setDescription(new DescriptionConverter().convertTo_v3(categoriesSet_v2.getDescription()));
		
		for (org.xmcda.v2.CategoriesSet.Element element: categoriesSet_v2.getElement())
		{
			getWarnings().pushTag("element");
			// ignoring: description, rank, value, values
			if (element.getDescription()!=null)
				getWarnings().elementIgnored("description");
			categoriesSet_v3.put(xmcda_v3.categories.get(element.getCategoryID()),
			                   values_convertTo_v3(element.getRankOrValueOrValues(), xmcda_v3));
			getWarnings().popTag(); // "element"
		}
		getWarnings().popTag(); // CATEGORIES_SET
		xmcda_v3.categoriesSets.add(categoriesSet_v3);
		return categoriesSet_v3;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    protected org.xmcda.QualifiedValues values_convertTo_v3(List<JAXBElement<?>> values, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.QualifiedValues values_v3 = org.xmcda.Factory.qualifiedValues();


		for (JAXBElement element: values)
		{
			getWarnings().pushTag(element.getName().getLocalPart());
			if ("rank".equals(element.getName().getLocalPart()))
				values_v3.add(new QualifiedValueConverter().convertTo_v3((org.xmcda.v2.Value)element.getValue(), xmcda_v3));
			if ("value".equals(element.getName().getLocalPart()))
				values_v3.add(new QualifiedValueConverter().convertTo_v3((org.xmcda.v2.Value)element.getValue(), xmcda_v3));
			if ("values".equals(element.getName().getLocalPart()))
			{
				final org.xmcda.v2.Values values_v2 = (org.xmcda.v2.Values) element.getValue();
				getWarnings().setTagID(values_v2.getId());
				if (values_v2.getId()!=null)
					getWarnings().attributeIgnored("id");
				if (values_v2.getName()!=null)
					getWarnings().attributeIgnored("name");
				if (values_v2.getMcdaConcept()!=null)
					getWarnings().attributeIgnored("mcdaConcept");
				if (values_v2.getDescription()!=null)
					getWarnings().elementIgnored("description");

				for (org.xmcda.v2.Value value: values_v2.getValue())
					values_v3.add(new QualifiedValueConverter().convertTo_v3(value, xmcda_v3));
			}
			getWarnings().popTag();
		}
		return values_v3;
	}
	
	public <T> org.xmcda.v2.CategoriesSet convertTo_v2(org.xmcda.CategoriesSet<T> categoriesSet_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		org.xmcda.v2.CategoriesSet categoriesSet_v2 = new org.xmcda.v2.CategoriesSet();

		getWarnings().pushTag(CATEGORIES_SET, categoriesSet_v3.id());

		// default attributes
		categoriesSet_v2.setId(categoriesSet_v3.id());
		categoriesSet_v2.setName(categoriesSet_v3.name());
		categoriesSet_v2.setMcdaConcept(categoriesSet_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = categoriesSet_v3.getDescription();
		categoriesSet_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// elements
		for (org.xmcda.Category category_v3: categoriesSet_v3.keySet())
		{
			getWarnings().pushTag("category", category_v3.id());

			org.xmcda.v2.CategoriesSet.Element element = new org.xmcda.v2.CategoriesSet.Element();
			element.setCategoryID(category_v3.id());

			getWarnings().pushTag("values");
			final Object _v = ValuesConverter.convertTo_v2(categoriesSet_v3.get(category_v3)); // may be null
			if (_v instanceof org.xmcda.v2.Value)
				element.getRankOrValueOrValues()
				        .add(new JAXBElement<>(new QName("value"), org.xmcda.v2.Value.class, (org.xmcda.v2.Value)_v));
			else if (_v instanceof org.xmcda.v2.Values)
			    element.getRankOrValueOrValues()
			            .add(new JAXBElement<>(new QName("values"), org.xmcda.v2.Values.class, (org.xmcda.v2.Values)_v));
			getWarnings().popTag(); // "values"

			categoriesSet_v2.getElement().add(element);

			getWarnings().popTag(); // "category"
		}
		getWarnings().popTag(); // CATEGORIES_SET
		return categoriesSet_v2;
	}
}
