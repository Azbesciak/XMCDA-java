/**
 * 
 */
package org.xmcda.converters.v2_v3;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class FuzzyLabelConverter
extends Converter
{
	public static final String FUZZY_LABEL = "fuzzyLabel";
	public static final String VALUED_LABEL = "valuedLabel";

	public FuzzyLabelConverter()
	{
		super(FUZZY_LABEL);
	}

	public org.xmcda.value.ValuedLabel convertTo_v3(org.xmcda.v2.FuzzyLabel fuzzyLabel_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(FUZZY_LABEL);

		// description: IGNORED 
		if ( fuzzyLabel_v2.getDescription() != null )
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);
		
		// FuzzyLabel
		// there is no <value/> around in v2.x, so we need to build one to convert it
		// QualifiedValue already handles ValuedLabels
		org.xmcda.v2.Value value_v2 = new org.xmcda.v2.Value();
		value_v2.setFuzzyLabel(fuzzyLabel_v2);
		getWarnings().popTag(); // FUZZY_LABEL
		return (org.xmcda.value.ValuedLabel) (new QualifiedValueConverter().convertTo_v3(value_v2, xmcda_v3).getValue());
	}

	public org.xmcda.v2.FuzzyLabel convertTo_v2(org.xmcda.value.ValuedLabel label_v3)
	{
		getWarnings().pushTag(VALUED_LABEL);
		org.xmcda.v2.FuzzyLabel fuzzyLabel_v2 = new org.xmcda.v2.FuzzyLabel();
		
		// Label
		fuzzyLabel_v2.setLabel(label_v3.getLabel());
		
		// Rank
		fuzzyLabel_v2.setFuzzyNumber(new QualifiedValueConverter().convertTo_v2(label_v3.getValue()).getFuzzyNumber());

		getWarnings().popTag(); // RANKED_LABEL
		return fuzzyLabel_v2;
	}
}
