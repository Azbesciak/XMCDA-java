/**
 * 
 */
package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.xmcda.Factory;
import org.xmcda.ProgramExecutionResult.Status;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class MethodMessagesConverter
extends Converter
{
	public static final String METHOD_MESSAGES = "methodMessages";
	public static final String PROGRAM_EXECUTION_RESULTS = org.xmcda.ProgramExecutionResult.TAG;
	
	public MethodMessagesConverter()
	{
		super(METHOD_MESSAGES);
	}

	// v2 -> v3

	public void convertTo_v3(org.xmcda.v2.MethodMessages msgs_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(METHOD_MESSAGES, msgs_v2.getId());

		org.xmcda.ProgramExecutionResult result_v3 = Factory.programExecutionResult();

		// Default attributes
		result_v3.setId(msgs_v2.getId());
		result_v3.setName(msgs_v2.getMcdaConcept());
		result_v3.setMcdaConcept(msgs_v2.getMcdaConcept());
		// description: see below
		result_v3.updateStatus(org.xmcda.ProgramExecutionResult.Status.OK); // default, see below
		
		for (JAXBElement _obj: msgs_v2.getDescriptionOrErrorMessageOrLogMessage())
		{
			if (_obj.getValue() instanceof org.xmcda.v2.Description)
			{
				result_v3.setDescription(new DescriptionConverter().convertTo_v3((org.xmcda.v2.Description) _obj.getValue()));
			}
			else if (_obj.getValue() instanceof org.xmcda.v2.Message)
			{
				final String name = _obj.getName().getLocalPart(); // errorMessage, logMessage, message
				final org.xmcda.v2.Message msg_v2 = (org.xmcda.v2.Message) _obj.getValue();
				org.xmcda.Message msg_v3 = new org.xmcda.Message(); // TODO factory
				msg_v3.setId(msg_v2.getId());
				msg_v3.setName(msg_v2.getName());
				msg_v3.setMcdaConcept(msg_v2.getMcdaConcept());
				msg_v3.setDescription(new DescriptionConverter().convertTo_v3(msg_v2.getDescription()));
				
				msg_v3.setText(msg_v2.getText());

				if ("message".equals(name))
				{
					msg_v3.setLevel(org.xmcda.Message.Level.INFO);
				}
				else if ("logMessage".equals(name))
				{
					msg_v3.setLevel(org.xmcda.Message.Level.DEBUG);
				}
				else if ("errorMessage".equals(name))
				{
					msg_v3.setLevel(org.xmcda.Message.Level.ERROR);
					result_v3.updateStatus(org.xmcda.ProgramExecutionResult.Status.ERROR);
				}
				result_v3.add(msg_v3);
			}
		}
		getWarnings().popTag(); // METHOD_MESSAGES
		xmcda_v3.programExecutionResultsList.add(result_v3);
	}

	// v3 -> v2

	public void convertProgramExecutionResultsTo_v2(List<org.xmcda.ProgramExecutionResult> results_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.ProgramExecutionResult result_v3: results_v3)
		{
			convertProgramExecutionResultTo_v2(result_v3, xmcda_v2);
		}
	}

	public void convertProgramExecutionResultTo_v2(org.xmcda.ProgramExecutionResult result_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(PROGRAM_EXECUTION_RESULTS);

		org.xmcda.v2.MethodMessages msgs_v2 = new org.xmcda.v2.MethodMessages();

		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.MethodMessages>(new QName(METHOD_MESSAGES),
                org.xmcda.v2.MethodMessages.class, msgs_v2));

		// Default attributes
		msgs_v2.setId(result_v3.id());
		msgs_v2.setName(result_v3.name());
		msgs_v2.setMcdaConcept(result_v3.mcdaConcept());
		if ( result_v3.getDescription() != null )
		{
			msgs_v2.getDescriptionOrErrorMessageOrLogMessage().add(new JAXBElement<org.xmcda.v2.Description>(new QName("description"),
					org.xmcda.v2.Description.class, new DescriptionConverter().convertTo_v2(result_v3.getDescription())));
		}
		
		convertExecutionStatusTo_v2(result_v3.getStatus(), msgs_v2);
		
		// messages
		for (org.xmcda.Message msg_v3: result_v3)
		{
			org.xmcda.v2.Message msg_v2 = new org.xmcda.v2.Message();

			msg_v2.setId(msg_v3.id());
			msg_v2.setName(msg_v3.name());
			msg_v2.setMcdaConcept(msg_v3.mcdaConcept());
			msg_v2.setDescription(new DescriptionConverter().convertTo_v2(msg_v3.getDescription()));

			msg_v2.setText(msg_v3.getText());

			String qname = "logMessage";
			if (org.xmcda.Message.Level.DEBUG.equals(msg_v3.getLevel()))
				qname="logMessage";
			else if (org.xmcda.Message.Level.INFO.equals(msg_v3.getLevel()))
				qname="message";
			else if (org.xmcda.Message.Level.ERROR.equals(msg_v3.getLevel()))
				qname="errorMessage";
			msgs_v2.getDescriptionOrErrorMessageOrLogMessage().add(new JAXBElement<org.xmcda.v2.Message>(new QName(qname),
					org.xmcda.v2.Message.class, msg_v2));
		}

		getWarnings().popTag(); // PROGRAM_EXECUTION_RESULTS
	}

	/**
	 * Converts an XMCDA v3 execution status into a XMCDA v2 methodMessages' logMessage or an errorMessage, if and
	 * only if {@link XMCDAConverter.ToV2ConverterParameters#convertExecutionStatusToMethodMessages()} is true.
	 * 
	 * @param executionStatus_v3 the execution status to convert.
	 * @param msgs_v2 The XMCDA v2 methodMessages into which the log/errorMessage is appended.
	 */
	protected void convertExecutionStatusTo_v2(org.xmcda.ProgramExecutionResult.Status executionStatus_v3,
	                                           org.xmcda.v2.MethodMessages msgs_v2)
	{
		if (!XMCDAConverter.toV2Parameters.get().convertExecutionStatusToMethodMessages())
		    return;

		final org.xmcda.v2.Message execStatus_v2 = new org.xmcda.v2.Message();

		execStatus_v2.setId("executionStatus");
		execStatus_v2.setName("execution status");
		String execStatus_qname = "logMessage";
		switch (executionStatus_v3)
		{
			case OK:
				execStatus_qname = "logMessage";
				execStatus_v2.setText("Ok");
				break;
			case WARNING:
				execStatus_qname = "logMessage";
				execStatus_v2.setText("Warning");
				break;
			case ERROR:
				execStatus_qname = "errorMessage";
				execStatus_v2.setText("Error");
				break;
			case TERMINATED:
				execStatus_qname = "errorMessage";
				execStatus_v2.setText("Terminated");
				break;
		}
		msgs_v2.getDescriptionOrErrorMessageOrLogMessage()
		        .add(new JAXBElement<org.xmcda.v2.Message>(new QName(execStatus_qname), org.xmcda.v2.Message.class,
		                                                   execStatus_v2));
	}
}
