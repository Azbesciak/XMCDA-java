package org.xmcda.converters.v2_v3;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesSetsConverter
extends Converter
{
	public static final String CATEGORIES_SETS = org.xmcda.CategoriesSets.TAG;;

	public CategoriesSetsConverter()
    {
		super(CATEGORIES_SETS);
    }

	public void convertTo_v3(org.xmcda.v2.CategoriesSets value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORIES_SETS, value.getId());
		
		org.xmcda.CategoriesSets categoriesSets_v3 = xmcda_v3.categoriesSets;

		// defaultAttributes
		categoriesSets_v3.setId(value.getId());
		categoriesSets_v3.setMcdaConcept(value.getMcdaConcept());
		categoriesSets_v3.setName(value.getName());

		// description
		categoriesSets_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));
		
		// categoriesSet 0..*
		for (org.xmcda.v2.CategoriesSet categoriesSet: value.getCategoriesSet())
		{
			xmcda_v3.categoriesSets.add(new CategoriesSetConverter().convertTo_v3(categoriesSet, xmcda_v3));
		}

		getWarnings().popTag(); // CATEGORIES_SETS
	}

	public <T> void convertTo_v2(final org.xmcda.CategoriesSets<T> categoriesSets_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		if (categoriesSets_v3==null || categoriesSets_v3.isVoid())
			return;

		getWarnings().pushTag(CATEGORIES_SETS, categoriesSets_v3.id());
		
		org.xmcda.v2.CategoriesSets categoriesSets_v2 = new org.xmcda.v2.CategoriesSets();
		List<JAXBElement<?>> crits = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		crits.add(new JAXBElement<org.xmcda.v2.CategoriesSets>(new QName(CATEGORIES_SETS), org.xmcda.v2.CategoriesSets.class,
				categoriesSets_v2));

		// default attributes
		categoriesSets_v2.setId(categoriesSets_v3.id());
		categoriesSets_v2.setName(categoriesSets_v3.name());
		categoriesSets_v2.setMcdaConcept(categoriesSets_v3.mcdaConcept());

		// description
		categoriesSets_v2.setDescription(new DescriptionConverter().convertTo_v2(categoriesSets_v3.getDescription()));

		// criteria
		CategoriesSetConverter categoriesSetConverter = new CategoriesSetConverter();

		for (org.xmcda.CategoriesSet<T> categoriesSet_v3: categoriesSets_v3)
		{
			categoriesSets_v2.getCategoriesSet().add(categoriesSetConverter.convertTo_v2(categoriesSet_v3, xmcda_v2));
		}
		getWarnings().popTag(); // CRITERIA
	}

	
}
