package org.xmcda.converters.v2_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

public class CriteriaValuesConverter
    extends Converter
{
	public static final String CRITERIA_VALUES = org.xmcda.CriteriaValues.TAG;
	public static final String CRITERION_VALUE = "criterionValue";

	public CriteriaValuesConverter()
	{
		super(CRITERIA_VALUES);
	}

	// v2 -> v3
	public void convertTo_v3(org.xmcda.v2.CriterionValue value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CRITERION_VALUE, value.getId());
		org.xmcda.v2.CriteriaValues values = new org.xmcda.v2.CriteriaValues();
		values.setId(value.getId());
		values.setName(value.getName());
		values.setMcdaConcept(value.getMcdaConcept());
		values.getCriterionValue().add(value);
		// we do not nullify the value's id, name or mcdaConcept, it has just been copied into the surrounding
		// criteriaValues for convenience but it was really defined on the criterionValue
		this.convertTo_v3(values, xmcda_v3);
		this.getWarnings().popTag(); // CRITERION_VALUE
	}

	/**
	 * @param value
	 * @param xmcda_v3
	 */
	public void convertTo_v3(org.xmcda.v2.CriteriaValues value, org.xmcda.XMCDA xmcda_v3)
	{
		if (value.getCriterionValue().size() == 0)
		{
			// NB not valid wrt xmcda v2 schema
			return;
		}
		getWarnings().pushTag(CRITERIA_VALUES, value.getId());

		// we suppose all values is of the same type of the first one; for example, there are just criterionIDs
		// This is what was intended in v2, even if not enforced by the schema
		final org.xmcda.v2.CriterionValue criterionValue_v2 = value.getCriterionValue().get(0);

		if (criterionValue_v2.getCriterionID() != null)
		{
			xmcda_v3.criteriaValuesList.add(criteriaValues_v3_criterionID(value, xmcda_v3));
		}
		else if (criterionValue_v2.getCriteriaSetID() != null)
		{
			xmcda_v3.criteriaSetsValuesList.add(criteriaValues_v3_criteriaSetID(value, xmcda_v3)); // unimplemented
		}
		else if (criterionValue_v2.getCriteriaSet() != null)
		{
			xmcda_v3.criteriaSetsValuesList.add(criteriaValues_v3_criteriaSet(value, xmcda_v3));
		}
		else
		{
			// do nothing: anything else won't be valid wrt xmcda v2 schema;
		}
		this.getWarnings().popTag(); // CRITERIA_VALUES
	}

	/**
	 * Convert v2 criteria values containing &lt;criterionID&gt; only
	 *
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	protected <T> org.xmcda.CriteriaValues<T> criteriaValues_v3_criterionID(org.xmcda.v2.CriteriaValues value,
	                                                                        org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.CriteriaValues<T> criteriaValues_v3 = org.xmcda.Factory.criteriaValues();

		// defaultAttributes
		criteriaValues_v3.setId(value.getId());
		criteriaValues_v3.setMcdaConcept(value.getMcdaConcept());
		criteriaValues_v3.setName(value.getName());

		// description
		criteriaValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// criterionValue 1..*
		for (org.xmcda.v2.CriterionValue criterionValue_v2: value.getCriterionValue())
		{
			final String criterionID = criterionValue_v2.getCriterionID();
			if (criterionID == null)
			{
				getWarnings().elementIgnored("criterionSetID or criteriaSet",
				                             "element found in a list which is supposed to contain criterionID only");
				continue;
			}

			final org.xmcda.LabelledQValues<T> criterionValues = ValuesConverter
			        .valueOrValues_convertTo_v3(criterionValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			criteriaValues_v3.put(xmcda_v3.criteria.get(criterionID), criterionValues);

			criterionValues.setId(criterionValue_v2.getId());
			criterionValues.setName(criterionValue_v2.getName());
			criterionValues.setMcdaConcept(criterionValue_v2.getMcdaConcept());
			criterionValues.setDescription(new DescriptionConverter().convertTo_v3(criterionValue_v2.getDescription()));
		}
		return criteriaValues_v3;
	}


	/**
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected org.xmcda.CriteriaSetsValues criteriaValues_v3_criteriaSetID(org.xmcda.v2.CriteriaValues value,
	                                                                       org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.CriteriaSetsValues criteriaSetsValues_v3 = org.xmcda.Factory.criteriaSetsValues();

		// defaultAttributes
		criteriaSetsValues_v3.setId(value.getId());
		criteriaSetsValues_v3.setMcdaConcept(value.getMcdaConcept());
		criteriaSetsValues_v3.setName(value.getName());

		// description
		criteriaSetsValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// criterionValue 1..*
		for (org.xmcda.v2.CriterionValue criterionValue_v2: value.getCriterionValue())
		{

			final String criteriaSetID = criterionValue_v2.getCriteriaSetID();
			if (criteriaSetID == null)
			{
				getWarnings()
				        .elementIgnored("criterionID or criteriaSet",
				                        "element found in a list which is supposed to contain criterionSetID only");
				continue;
			}
			org.xmcda.CriteriaSet criteriaSet_v3 = xmcda_v3.criteriaSets.get(criteriaSetID);

			final org.xmcda.LabelledQValues criterionValues = ValuesConverter
			        .valueOrValues_convertTo_v3(criterionValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			criteriaSetsValues_v3.put(criteriaSet_v3, criterionValues);

			criterionValues.setId(criterionValue_v2.getId());
			criterionValues.setName(criterionValue_v2.getName());
			criterionValues.setMcdaConcept(criterionValue_v2.getMcdaConcept());
			criterionValues.setDescription(new DescriptionConverter().convertTo_v3(criterionValue_v2.getDescription()));
		}
		return criteriaSetsValues_v3;
	}


	/**
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected org.xmcda.CriteriaSetsValues criteriaValues_v3_criteriaSet(org.xmcda.v2.CriteriaValues value,
	                                                                     org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.CriteriaSetsValues criteriaSetsValues_v3 = org.xmcda.Factory.criteriaSetsValues();

		// defaultAttributes
		criteriaSetsValues_v3.setId(value.getId());
		criteriaSetsValues_v3.setMcdaConcept(value.getMcdaConcept());
		criteriaSetsValues_v3.setName(value.getName());

		// description
		criteriaSetsValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));
		// criterionValue 1..*
		for (org.xmcda.v2.CriterionValue criterionValue_v2: value.getCriterionValue())
		{

			final org.xmcda.v2.CriteriaSet criteriaSet = criterionValue_v2.getCriteriaSet();
			if (criteriaSet == null)
			{
				getWarnings().elementIgnored("criterionID or criteriaSetID",
				                             "element found in a list which is supposed to contain criterionID only");
				continue;
			}
			org.xmcda.CriteriaSet criteriaSet_v3 = new CriteriaSetConverter().convertTo_v3(criteriaSet, xmcda_v3);
			final org.xmcda.LabelledQValues criterionValues = ValuesConverter
			        .valueOrValues_convertTo_v3(criterionValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			criteriaSetsValues_v3.put(criteriaSet_v3, criterionValues);
			xmcda_v3.criteriaSets.add(criteriaSet_v3);

			criterionValues.setId(criterionValue_v2.getId());
			criterionValues.setName(criterionValue_v2.getName());
			criterionValues.setMcdaConcept(criterionValue_v2.getMcdaConcept());
			if (criterionValue_v2.getDescription() != null)
				// if it is null, do not override a value that may have been already set by valueOrValues_convertTo_v3(), i.e. a description in <values>
				criterionValues.setDescription(new DescriptionConverter().convertTo_v3(criterionValue_v2.getDescription()));
		}
		return criteriaSetsValues_v3;
	}

    // v3 -> v2
    // we will never build XMCDA v2 criteriaValues with criteriaSet within, only criteriaValues with criteriaSetID
	public void convertTo_v2(List<org.xmcda.CriteriaValues<?>> criteriaValues_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CriteriaValues<?> criteriaValue: criteriaValues_v3)
		{
			org.xmcda.v2.CriteriaValues critValues_v2 = convertTo_v2(criteriaValue);
			List<JAXBElement<?>> critValuesList_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			critValuesList_v2.add(new JAXBElement<>(new QName(CRITERIA_VALUES), org.xmcda.v2.CriteriaValues.class, critValues_v2));
		}
	}

	protected <VALUE_TYPE> org.xmcda.v2.CriteriaValues convertTo_v2(org.xmcda.CriteriaValues<VALUE_TYPE> criteriaValues_v3)
	{
		org.xmcda.v2.CriteriaValues criteriaValues_v2 = new org.xmcda.v2.CriteriaValues();

		getWarnings().pushTag(CRITERIA_VALUES, criteriaValues_v3.id());

		// default attributes
		criteriaValues_v2.setId(criteriaValues_v3.id());
		criteriaValues_v2.setName(criteriaValues_v3.name());
		criteriaValues_v2.setMcdaConcept(criteriaValues_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = criteriaValues_v3.getDescription();
		criteriaValues_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// criterionValue
		for (Entry<org.xmcda.Criterion, org.xmcda.LabelledQValues<VALUE_TYPE>> critValue_v3: criteriaValues_v3.entrySet())
		{
			getWarnings().pushTag("criterionValue", critValue_v3.getValue().id());
			org.xmcda.v2.CriterionValue criterionValue_v2 = new org.xmcda.v2.CriterionValue();
			criterionValue_v2.setId(critValue_v3.getValue().id());
			criterionValue_v2.setName(critValue_v3.getValue().name());
			criterionValue_v2.setMcdaConcept(critValue_v3.getValue().mcdaConcept());
			criterionValue_v2.setDescription(new DescriptionConverter().convertTo_v2(critValue_v3.getValue().getDescription()));

			criterionValue_v2.setCriterionID(critValue_v3.getKey().id());

			org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			for (org.xmcda.QualifiedValue<VALUE_TYPE> value_v3: critValue_v3.getValue())
			{
				org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value_v3);
				if ( ! XMCDAConverter.toV2Parameters.get().omitValuesInCriteriaValues() )
					values_v2.getValue().add(value_v2);
				else
				{
					criterionValue_v2.getValueOrValues().add(value_v2);
				}
			}
			if ( ! XMCDAConverter.toV2Parameters.get().omitValuesInCriteriaValues() )
			{
				criterionValue_v2.getValueOrValues().add(values_v2);
			}
			criteriaValues_v2.getCriterionValue().add(criterionValue_v2);
			getWarnings().popTag(); // "criterionValue"
		}
		getWarnings().popTag(); // CRITERIA_VALUES
		return criteriaValues_v2;
	}

	public void criteriaSetsValues_convertTo_v2(List<org.xmcda.CriteriaSetsValues<?,?>> criteriaSetsValues_v3, org.xmcda.v2.XMCDA xmcda_v2, org.xmcda.XMCDA xmcda_v3)
	{
		for (org.xmcda.CriteriaSetsValues<?,?> criteriaSetsValues: criteriaSetsValues_v3)
		{
			org.xmcda.v2.CriteriaValues critValues_v2 = convertTo_v2(criteriaSetsValues, xmcda_v2, xmcda_v3);
			List<JAXBElement<?>> critValuesList_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			critValuesList_v2.add(new JAXBElement<>(new QName(CRITERIA_VALUES), org.xmcda.v2.CriteriaValues.class, critValues_v2));
		}
	}

	protected <CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE> org.xmcda.v2.CriteriaValues convertTo_v2(org.xmcda.CriteriaSetsValues<CRITERIA_SETS_VALUE_TYPE, VALUE_TYPE> criteriaSetsValues_v3, org.xmcda.v2.XMCDA xmcda_v2, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.v2.CriteriaValues criteriaValues_v2 = new org.xmcda.v2.CriteriaValues();

		getWarnings().pushTag(org.xmcda.CriteriaSetsValues.TAG, criteriaSetsValues_v3.id());

		// default attributes
		criteriaValues_v2.setId(criteriaSetsValues_v3.id());
		criteriaValues_v2.setName(criteriaSetsValues_v3.name());
		criteriaValues_v2.setMcdaConcept(criteriaSetsValues_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = criteriaSetsValues_v3.getDescription();
		criteriaValues_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// criteriaSetValue -> criterionValue
		for (Entry<org.xmcda.CriteriaSet<CRITERIA_SETS_VALUE_TYPE>, org.xmcda.LabelledQValues<VALUE_TYPE>> critValue_v3: criteriaSetsValues_v3.entrySet())
		{
			getWarnings().pushTag("criteriaSetValue", critValue_v3.getValue().id());
			org.xmcda.v2.CriterionValue criterionValue_v2 = new org.xmcda.v2.CriterionValue();
			criterionValue_v2.setId(critValue_v3.getValue().id());
			criterionValue_v2.setName(critValue_v3.getValue().name());
			criterionValue_v2.setMcdaConcept(critValue_v3.getValue().mcdaConcept());
			criterionValue_v2.setDescription(new DescriptionConverter().convertTo_v2(critValue_v3.getValue().getDescription()));

			//criterionValue_v2.setCriteriaSetID(critValue_v3.getKey().id());
			final String criteriaSetID_v3 = critValue_v3.getKey().id();
			final org.xmcda.CriteriaSet<?> criteriaSet_v3 = xmcda_v3.criteriaSets.get(criteriaSetID_v3);
			criterionValue_v2.setCriteriaSet(new CriteriaSetConverter().convertTo_v2(criteriaSet_v3, xmcda_v2));

			getWarnings().pushTag("values");
			org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			for (org.xmcda.QualifiedValue<VALUE_TYPE> value_v3: critValue_v3.getValue())
			{
				org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value_v3);
				values_v2.getValue().add(value_v2);
			}
			getWarnings().popTag(); // "values"

			criterionValue_v2.getValueOrValues().add(values_v2);
			criteriaValues_v2.getCriterionValue().add(criterionValue_v2);
			getWarnings().popTag(); // "criteriaSetValue"
		}
		getWarnings().popTag(); // "criteriaSetsValues" / org.xmcda.CriteriaSetsValues.TAG
		return criteriaValues_v2;
	}
}
