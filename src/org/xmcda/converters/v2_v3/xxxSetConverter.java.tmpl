package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;


public class McdaObjects_SetConverter
    extends Converter
{
	public static final String MCDA_OBJECTS__SET = "mcdaObjects_Set";

	public McdaObjects_SetConverter()
	{
		super(MCDA_OBJECTS__SET);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
    public org.xmcda.McdaObjects_Set convertTo_v3(org.xmcda.v2.McdaObjects_Set mcdaObjects_Set_v2, org.xmcda.XMCDA xmcda_v3)
	{
		// TODO ignorés pour le moment: value et values
		String mcdaObjects_SetID = mcdaObjects_Set_v2.getId();
		getWarnings().pushTag(MCDA_OBJECTS__SET, mcdaObjects_SetID);

		if (mcdaObjects_SetID == null || "".equals(mcdaObjects_SetID))
		{
			// we are called by with a mcdaObjects_Set with no ID, which may happen when the mcdaObjects_Set is embedded in another structure,
			// such as in <mcdaObjects_Values>
			// we need to find one which is not already assigned: mcdaObjects_Set must have an ID in v3
			final String baseID="mcdaObjects_Set_generatedID_";
			int idx=1;
			mcdaObjects_SetID = baseID+idx;
			while (xmcda_v3.mcdaObjects_Sets.contains(mcdaObjects_SetID))
			{
				idx = idx+1;
				mcdaObjects_SetID = baseID+idx;
			}
		}
		if (!mcdaObjects_Set_v2.getValueOrValues().isEmpty())
			getWarnings().elementIgnored("value", "value ou values ignored on mcdaObjects_Set: they are NOT translated into v3 mcdaObjects_SetsValues by this converter");
		org.xmcda.McdaObjects_Set mcdaObjects_Set_v3 = org.xmcda.Factory.mcdaObjects_Set();
		mcdaObjects_Set_v3.setId(mcdaObjects_SetID);
		mcdaObjects_Set_v3.setName(mcdaObjects_Set_v2.getName());
		mcdaObjects_Set_v3.setMcdaConcept(mcdaObjects_Set_v2.getMcdaConcept());
		
		mcdaObjects_Set_v3.setDescription(new DescriptionConverter().convertTo_v3(mcdaObjects_Set_v2.getDescription()));
		
		for (org.xmcda.v2.McdaObjects_Set.Element element: mcdaObjects_Set_v2.getElement())
		{
			getWarnings().pushTag("element");
			// ignoring: description, rank, value, values
			if (element.getDescription()!=null)
				getWarnings().elementIgnored("description");
			mcdaObjects_Set_v3.put(xmcda_v3.mcdaObjects_.get(element.getMcdaObject_ID()),
			                   values_convertTo_v3(element.getRankOrValueOrValues(), xmcda_v3));
			getWarnings().popTag(); // "element"
		}
		getWarnings().popTag(); // MCDA_OBJECTS__SET
		xmcda_v3.mcdaObjects_Sets.add(mcdaObjects_Set_v3);
		return mcdaObjects_Set_v3;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    protected org.xmcda.QualifiedValues values_convertTo_v3(List<JAXBElement<?>> values, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.QualifiedValues values_v3 = org.xmcda.Factory.qualifiedValues();


		for (JAXBElement element: values)
		{
			getWarnings().pushTag(element.getName().getLocalPart());
			if ("rank".equals(element.getName().getLocalPart()))
				values_v3.add(new QualifiedValueConverter().convertTo_v3((org.xmcda.v2.Value)element.getValue(), xmcda_v3));
			if ("value".equals(element.getName().getLocalPart()))
				values_v3.add(new QualifiedValueConverter().convertTo_v3((org.xmcda.v2.Value)element.getValue(), xmcda_v3));
			if ("values".equals(element.getName().getLocalPart()))
			{
				final org.xmcda.v2.Values values_v2 = (org.xmcda.v2.Values) element.getValue();
				getWarnings().setTagID(values_v2.getId());
				if (values_v2.getId()!=null)
					getWarnings().attributeIgnored("id");
				if (values_v2.getName()!=null)
					getWarnings().attributeIgnored("name");
				if (values_v2.getMcdaConcept()!=null)
					getWarnings().attributeIgnored("mcdaConcept");
				if (values_v2.getDescription()!=null)
					getWarnings().elementIgnored("description");

				for (org.xmcda.v2.Value value: values_v2.getValue())
					values_v3.add(new QualifiedValueConverter().convertTo_v3(value, xmcda_v3));
			}
			getWarnings().popTag();
		}
		return values_v3;
	}
	
	public <T> org.xmcda.v2.McdaObjects_Set convertTo_v2(org.xmcda.McdaObjects_Set<T> mcdaObjects_Set_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		org.xmcda.v2.McdaObjects_Set mcdaObjects_Set_v2 = new org.xmcda.v2.McdaObjects_Set();

		getWarnings().pushTag(MCDA_OBJECTS__SET, mcdaObjects_Set_v3.id());

		// default attributes
		mcdaObjects_Set_v2.setId(mcdaObjects_Set_v3.id());
		mcdaObjects_Set_v2.setName(mcdaObjects_Set_v3.name());
		mcdaObjects_Set_v2.setMcdaConcept(mcdaObjects_Set_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = mcdaObjects_Set_v3.getDescription();
		mcdaObjects_Set_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// elements
		for (org.xmcda.McdaObject_ mcdaObject__v3: mcdaObjects_Set_v3.keySet())
		{
			getWarnings().pushTag("mcdaObject_", mcdaObject__v3.id());

			org.xmcda.v2.McdaObjects_Set.Element element = new org.xmcda.v2.McdaObjects_Set.Element();
			element.setMcdaObject_ID(mcdaObject__v3.id());

			getWarnings().pushTag("values");
			final Object _v = ValuesConverter.convertTo_v2(mcdaObjects_Set_v3.get(mcdaObject__v3)); // may be null
			if (_v instanceof org.xmcda.v2.Value)
				element.getRankOrValueOrValues()
				        .add(new JAXBElement<>(new QName("value"), org.xmcda.v2.Value.class, (org.xmcda.v2.Value)_v));
			else if (_v instanceof org.xmcda.v2.Values)
			    element.getRankOrValueOrValues()
			            .add(new JAXBElement<>(new QName("values"), org.xmcda.v2.Values.class, (org.xmcda.v2.Values)_v));
			getWarnings().popTag(); // "values"

			mcdaObjects_Set_v2.getElement().add(element);

			getWarnings().popTag(); // "mcdaObject_"
		}
		getWarnings().popTag(); // MCDA_OBJECTS__SET
		return mcdaObjects_Set_v2;
	}
}
