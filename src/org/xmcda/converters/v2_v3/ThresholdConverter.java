/**
 * 
 */
package org.xmcda.converters.v2_v3;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class ThresholdConverter
extends Converter
{
	public static String THRESHOLD = "threshold";

	public ThresholdConverter()
	{
		super(THRESHOLD);
	}
	
	public org.xmcda.Threshold convertTo_v3(org.xmcda.v2.Function threshold_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(THRESHOLD, threshold_v2.getId());
		org.xmcda.value.Function function_v3 = new FunctionConverter().convertTo_v3(threshold_v2, xmcda_v3);

		org.xmcda.Threshold threshold_v3;
		
		if (function_v3 instanceof org.xmcda.value.ConstantFunction)
		{
			final org.xmcda.value.ConstantFunction const_v3 = (org.xmcda.value.ConstantFunction) function_v3;
			threshold_v3 = org.xmcda.Factory.threshold(new org.xmcda.QualifiedValue(const_v3.getValue()));

			threshold_v3.setId(threshold_v2.getId());
			threshold_v3.setName(threshold_v2.getName());
			threshold_v3.setMcdaConcept(threshold_v2.getMcdaConcept());
			// no description here in v3
		}
		else if (function_v3 instanceof org.xmcda.value.AffineFunction)
		{
			final org.xmcda.value.AffineFunction affine_v3 = (org.xmcda.value.AffineFunction) function_v3;
			threshold_v3 = org.xmcda.Factory.threshold(new org.xmcda.QualifiedValue(affine_v3.getSlope()),
			                                           new org.xmcda.QualifiedValue(affine_v3.getIntercept())
			                                           );
			threshold_v3.setId(threshold_v2.getId());
			threshold_v3.setName(threshold_v2.getName());
			threshold_v3.setMcdaConcept(threshold_v2.getMcdaConcept());
			// no description here in v3
			//threshold_v3.setDescription(new DescriptionConverter().convertTo_v3(threshold_v2.getDescription()));
		}
		else
			throw new RuntimeException("threshold can only contain a constant or affine function");
		getWarnings().popTag(); // THRESHOLD
		return threshold_v3;
	}

	public org.xmcda.v2.Function convertTo_v2(org.xmcda.Threshold threshold_v3)
	{
		getWarnings().pushTag(THRESHOLD, threshold_v3.id());

		org.xmcda.v2.Function func_v2 = new org.xmcda.v2.Function();
		func_v2.setId(threshold_v3.id());
		func_v2.setName(threshold_v3.name());
		func_v2.setMcdaConcept(threshold_v3.mcdaConcept());

		if (threshold_v3.isConstant())
		{
			func_v2.setConstant(V2ValueToNumericValueConverter.convert(new QualifiedValueConverter().convertTo_v2(threshold_v3.getConstant())));
		}
		else
		{ /* Affine */
			org.xmcda.v2.Function.Linear linear_v2 = new org.xmcda.v2.Function.Linear();
			linear_v2.setIntercept(new V2ValueToNumericValueConverter().convert(new QualifiedValueConverter().convertTo_v2(threshold_v3.getIntercept())));
			linear_v2.setSlope(V2ValueToNumericValueConverter.convert(new QualifiedValueConverter().convertTo_v2(threshold_v3.getSlope())));

			func_v2.setLinear(linear_v2);
		}
		getWarnings().popTag(); // THRESHOLD
		return func_v2;
	}
}
