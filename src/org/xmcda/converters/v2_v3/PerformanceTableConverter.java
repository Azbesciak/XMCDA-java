package org.xmcda.converters.v2_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

/**
 * @author Sébastien Bigaret
 */
public class PerformanceTableConverter
extends Converter
{
	public static final String PERFORMANCE_TABLE = org.xmcda.PerformanceTable.TAG;
	public PerformanceTableConverter()
	{
		super(PERFORMANCE_TABLE);
	}

	public <T> void convertTo_v3(org.xmcda.v2.PerformanceTable perfTable_v2, org.xmcda.XMCDA xmcda_v3)
	{
		final String id = perfTable_v2.getId();
		getWarnings().pushTag(PERFORMANCE_TABLE, id);

		// defaultAttributes
		org.xmcda.PerformanceTable<T> perfTable_v3 = new org.xmcda.PerformanceTable<T>();
		perfTable_v3.setId(id);
		perfTable_v3.setName(perfTable_v2.getName());
		perfTable_v3.setMcdaConcept(perfTable_v2.getMcdaConcept());

		xmcda_v3.performanceTablesList.add(perfTable_v3);

		// description
		perfTable_v3.setDescription(new DescriptionConverter().convertTo_v3(perfTable_v2.getDescription()));
		if (perfTable_v2.getDescription() != null && perfTable_v2.getDescription().getTitle() != null && perfTable_v3.name()==null)
		{
			perfTable_v3.setName(perfTable_v2.getDescription().getTitle());
		}

		for (org.xmcda.v2.AlternativeOnCriteriaPerformances row: perfTable_v2.getAlternativePerformances())
		{
			String alternativeID = row.getAlternativeID();
			getWarnings().pushTag("alternativesPerformances", alternativeID);
			if (row.getDescription()!=null)
				getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);
			if (row.getId()!= null && row.getId().length()>0)
				getWarnings().attributeIgnored("id");
			if (row.getName()!= null && row.getName().length()>0)
				getWarnings().attributeIgnored("name");
			if (row.getMcdaConcept()!= null && row.getMcdaConcept().length()>0)
				getWarnings().attributeIgnored("mcdaConcept");
			for (org.xmcda.v2.AlternativeOnCriteriaPerformances.Performance cell: row.getPerformance())
			{
				final String criterionID = cell.getCriterionID();
				getWarnings().pushTag("performance", criterionID);
				if (cell.getAttributeID() != null)
				{
					getWarnings().elementIgnored("attributeID");
					continue;
				}
				final org.xmcda.QualifiedValue<T> value = new QualifiedValueConverter().convertTo_v3(cell.getValue(), xmcda_v3);
				perfTable_v3.put(xmcda_v3.alternatives.get(alternativeID),
				                 xmcda_v3.criteria.get(criterionID),
				                 value);
				getWarnings().popTag(); // "performance"
			}
			getWarnings().popTag(); // "alternativesPerformances"
		}
		getWarnings().popTag(); // PERFORMANCE_TABLE
	}

	public void convertTo_v2(List<org.xmcda.PerformanceTable<?>> perfTables_v3,
	                             org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.PerformanceTable<?> perfTable_v3: perfTables_v3)
		{
			org.xmcda.v2.PerformanceTable perfTable_v2 = convertTo_v2(perfTable_v3, xmcda_v2);
			List<JAXBElement<?>> perfTables_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			perfTables_v2.add(new JAXBElement<org.xmcda.v2.PerformanceTable>(new QName(PERFORMANCE_TABLE), org.xmcda.v2.PerformanceTable.class,
			                                                     perfTable_v2));
		}
	}

	public <T> org.xmcda.v2.PerformanceTable convertTo_v2(org.xmcda.PerformanceTable<T> perfTable_v3,
	                                                          org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(PERFORMANCE_TABLE, perfTable_v3.id());

		org.xmcda.v2.PerformanceTable perfTable_v2 = new org.xmcda.v2.PerformanceTable();

		perfTable_v2.setId(perfTable_v3.id());
		perfTable_v2.setName(perfTable_v3.name());
		perfTable_v2.setMcdaConcept(perfTable_v3.mcdaConcept());

		perfTable_v2.setDescription(new DescriptionConverter().convertTo_v2(perfTable_v3.getDescription()));

		for (Entry<org.xmcda.utils.PerformanceTableCoord, org.xmcda.QualifiedValues<T>> entry: perfTable_v3.entrySet())
		{
			final org.xmcda.QualifiedValues<?> values_v3 = entry.getValue();
			if (values_v3==null || values_v3.size()==0)
				continue;
			if (values_v3.size()>1)
				throw new RuntimeException("more than one value in a performance table"); // TODO error

			final String alternativeID = entry.getKey().x.id();
			getWarnings().pushTag("alternativesPerformances", alternativeID);

			org.xmcda.v2.AlternativeOnCriteriaPerformances row_v2 = getRow_v2(perfTable_v2, alternativeID);

			org.xmcda.v2.AlternativeOnCriteriaPerformances.Performance cell_v2 =
					new org.xmcda.v2.AlternativeOnCriteriaPerformances.Performance();
			final String criterionID = entry.getKey().y.id();
			getWarnings().pushTag("performance", criterionID);
			cell_v2.setCriterionID(criterionID);
			cell_v2.setValue(new QualifiedValueConverter().convertTo_v2(values_v3.get(0)));
			row_v2.getPerformance().add(cell_v2);
			getWarnings().popTag(); // "performance"

			getWarnings().popTag(); // "alternativesPerformances"
		}
		getWarnings().popTag(); // PERFORMANCE_TABLE
		return perfTable_v2;
	}

	private org.xmcda.v2.AlternativeOnCriteriaPerformances getRow_v2(org.xmcda.v2.PerformanceTable pv2,
	                                                                     String alternativeID)
	{
		for (org.xmcda.v2.AlternativeOnCriteriaPerformances row: pv2.getAlternativePerformances())
			if (alternativeID.equals(row.getAlternativeID()))
				return row;
		org.xmcda.v2.AlternativeOnCriteriaPerformances row =
				new org.xmcda.v2.AlternativeOnCriteriaPerformances();
		row.setAlternativeID(alternativeID);
		pv2.getAlternativePerformances().add(row);
		return row;
	}
}
