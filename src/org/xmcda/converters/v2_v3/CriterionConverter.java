package org.xmcda.converters.v2_v3;

import java.util.List;


/**
 * @author Sébastien Bigaret
 */
public class CriterionConverter
    extends Converter
{
	public static final String CRITERION = "criterion";

	public static final String ACTIVE    = "active";

	public CriterionConverter()
	{
		super(CRITERION);
	}

	public void convertTo_v3(org.xmcda.v2.Criterion criterion_v2, org.xmcda.XMCDA xmcda_v3,
	                         org.xmcda.CriteriaScales critScales_v3,
	                         org.xmcda.CriteriaFunctions critFuncs_v3,
	                         org.xmcda.CriteriaThresholds critThresholds_v3
	                         )
	{
		final String id = criterion_v2.getId();
		getWarnings().pushTag(CRITERION, id);

		// defaultAttributes
		org.xmcda.Criterion criterion_v3 = xmcda_v3.criteria.get(id);
		criterion_v3.setName(criterion_v2.getName());
		criterion_v3.setMcdaConcept(criterion_v2.getMcdaConcept());

		criterion_v3.setIsActive(true);

		// description
		criterion_v3.setDescription(new DescriptionConverter().convertTo_v3(criterion_v2.getDescription()));

		// active: handled here
		// scale, criterionFunction, thresholds: will be handled by their respective CriteriaXXXConverters
		// attributeReference, criterionReference: IGNORED
		for (Object _value: criterion_v2.getActiveOrScaleOrCriterionFunction())
		{
			if (_value == null)
			    continue;

			if (_value instanceof Boolean) // active
			{
				criterion_v3.setIsActive((Boolean) _value);
			}
			else if (_value instanceof org.xmcda.v2.AttributeReference)
			{
				getWarnings().elementIgnored("attributeReference", Warnings.ABSENT_IN_V3_0);
			}
			else if (_value instanceof org.xmcda.v2.CriterionReference)
			{
				getWarnings().elementIgnored("criterionReference", Warnings.ABSENT_IN_V3_0);
			}
			else if (_value instanceof org.xmcda.v2.CriterionValue)
			{
				getWarnings()
				        .elementRecommendation("criterionValue",
				                               "criterionValue should not be used: please use the separate XMCDA v2 root tag <criteriaValues> instead");
			}
			else if (_value instanceof org.xmcda.v2.Scale)
			{
				org.xmcda.CriterionScales crit_scale_v3 = critScales_v3.get(criterion_v3);
				if (crit_scale_v3 == null)
				{
					crit_scale_v3 = org.xmcda.Factory.criterionScales();
					critScales_v3.put(criterion_v3, crit_scale_v3);
				}
				crit_scale_v3.add(new ScaleConverter().convertTo_v3((org.xmcda.v2.Scale)_value, xmcda_v3));
			}
			else if (_value instanceof org.xmcda.v2.Function)
			{
				org.xmcda.CriterionFunctions crit_func_v3 = critFuncs_v3.get(criterion_v3);
				if (crit_func_v3 == null)
				{
					crit_func_v3 = new org.xmcda.CriterionFunctions();
					critFuncs_v3.put(criterion_v3, crit_func_v3);
				}
				crit_func_v3.add(new FunctionConverter().convertTo_v3((org.xmcda.v2.Function)_value, xmcda_v3));
			}
			else if (_value instanceof org.xmcda.v2.Thresholds)
			{
				org.xmcda.CriterionThresholds crit_th_v3 = critThresholds_v3.get(criterion_v3);
				if (crit_th_v3 == null)
				{
					crit_th_v3 = org.xmcda.Factory.criterionThresholds();
					critThresholds_v3.put(criterion_v3, crit_th_v3);
				}
				// consider there is only one, or id, etc... will be overriddden and everything put in a single criterionThresholds
				new ThresholdsConverter().convertTo_v3((org.xmcda.v2.Thresholds)_value, crit_th_v3, xmcda_v3);
			}
		}
		getWarnings().popTag(); // CRITERION
	}

	public org.xmcda.v2.Criterion convertTo_v2(org.xmcda.Criterion criterion_v3,
	                                               org.xmcda.CriterionScales criterionScales_v3,
	                                               org.xmcda.CriterionFunctions criterionFunctions_v3,
	                                               org.xmcda.CriterionThresholds criterionThresholds_v3)
	{
		getWarnings().pushTag(CRITERION, criterion_v3.id());

		// ABSENT in v3: attributeReference, criterionReference
		// criterionValue: their are put in XMCDA v2 criteriaValues instead

		org.xmcda.v2.Criterion criterion_v2 = new org.xmcda.v2.Criterion();
		criterion_v2.setId(criterion_v3.id());
		criterion_v2.setName(criterion_v3.name());
		criterion_v2.setMcdaConcept(criterion_v3.mcdaConcept());

		criterion_v2.setDescription(new DescriptionConverter().convertTo_v2(criterion_v3.getDescription()));
		
		criterion_v2.getActiveOrScaleOrCriterionFunction().add(criterion_v3.isActive());

		if (criterionScales_v3 != null)
		{
			for (org.xmcda.Scale criterionScale_v3: criterionScales_v3)
			{
				final org.xmcda.v2.Scale scale_v2 = new ScaleConverter().convertTo_v2(criterionScale_v3);
				criterion_v2.getActiveOrScaleOrCriterionFunction().add(scale_v2);
			}
		}
		if (criterionFunctions_v3 != null)
		{
			for (org.xmcda.value.Function criterionFunction_v3: criterionFunctions_v3)
			{
				final org.xmcda.v2.Function func_v2 = new FunctionConverter().convertTo_v2(criterion_v2, criterionFunction_v3);
				final List<Object> critFunctionList_v2 = criterion_v2.getActiveOrScaleOrCriterionFunction();
				//critFunctionList_v2.add(new JAXBElement<org.xmcda.v2.Function>(new QName(FUNCTION), org.xmcda.v2.Function.class, func_v2));
				critFunctionList_v2.add(func_v2);
			}
		}
		if (criterionThresholds_v3 != null)
		{
			final org.xmcda.v2.Thresholds thresholds_v2 = new ThresholdsConverter().convertTo_v2(criterionThresholds_v3);
			criterion_v2.getActiveOrScaleOrCriterionFunction().add(thresholds_v2);
		}
		getWarnings().popTag(); // CRITERION
		return criterion_v2;
	}

}
