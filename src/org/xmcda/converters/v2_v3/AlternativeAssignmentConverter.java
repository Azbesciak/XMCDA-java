package org.xmcda.converters.v2_v3;

public class AlternativeAssignmentConverter
    extends Converter
{
	public static final String ALTERNATIVE_ASSIGNMENT  = "alternativeAssignment";

	public static final String ALTERNATIVE_AFFECTATION = "alternativeAffectation";

	public AlternativeAssignmentConverter()
	{
		super(ALTERNATIVE_ASSIGNMENT);
	}

	/**
	 * Converts a XMVDA v2 {@code <alternativeAffectation>} into a XMCDA v3 {@code <alternativeAssignment>}. Not all
	 * {@code <alternativeAffectation>}s can be converted: those referring to a {@code <alternativeSet>} or to a
	 * {@code <alternativesSetID>} cannot be converted (there are no shuch things in XMCDA v3); in this case the
	 * method returns {@code null}.
	 *
	 * @param affectation_v2 the XMCDA v2 alternative affectation to convert
	 * @param xmcda_v3 the XMCDA object in which the conversion searches for objects (or creates them)
	 * @return the converted XMCDA v3 {@link org.xmcda.AlternativeAssignment alternative assignment}, or {@code null}
	 *         if it cannot be converted
	 */
	public <T> org.xmcda.AlternativeAssignment<T> convertTo_v3(org.xmcda.v2.AlternativeAffectation affectation_v2,
	                                                           org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(ALTERNATIVE_AFFECTATION, affectation_v2.getId());

		handle_errors:
		{
			if (affectation_v2.getAlternativesSet() != null)
				getWarnings().elementIgnored("alternativeSet", Warnings.ABSENT_IN_V3_0);
			else if (affectation_v2.getAlternativesSetID() != null)
				getWarnings().elementIgnored("alternativeSetID", Warnings.ABSENT_IN_V3_0);
			else
				break handle_errors;
			getWarnings().popTag(); // ALTERNATIVES_AFFECTATIONS
			return null;
		}

		org.xmcda.AlternativeAssignment<T> alternativeAssignment = new org.xmcda.AlternativeAssignment<>();

		// defaultAttributes
		alternativeAssignment.setId(affectation_v2.getId());
		alternativeAssignment.setName(affectation_v2.getName());
		alternativeAssignment.setMcdaConcept(affectation_v2.getMcdaConcept());

		// description
		alternativeAssignment
		        .setDescription(new DescriptionConverter().convertTo_v3(affectation_v2.getDescription()));

		// convert alternative
		// alternativeID
		final String alternativeID = affectation_v2.getAlternativeID();
		alternativeAssignment.setAlternative(xmcda_v3.alternatives.get(alternativeID));
		convertTo_v3_categoryXXX(affectation_v2, alternativeAssignment, xmcda_v3);

		// value 0..*, values 0..*
		alternativeAssignment.setValues(ValuesConverter.valueOrValues_convertTo_v3(affectation_v2.getValue(),
		                                                                           xmcda_v3, getWarnings()));
		alternativeAssignment.getValues().addAll(ValuesConverter.valueOrValues_convertTo_v3(affectation_v2.getValues(),
		                                                                                    xmcda_v3, getWarnings()));

		getWarnings().popTag(); // ALTERNATIVES_AFFECTATIONS
		return alternativeAssignment;
	}

	private <T> void convertTo_v3_categoryXXX(org.xmcda.v2.AlternativeAffectation affectation_v2,
	                                          org.xmcda.AlternativeAssignment<T> assignment_v3,
	                                          org.xmcda.XMCDA xmcda_v3)
	{
		if (affectation_v2.getCategoryID() != null)
		{
			assignment_v3.setCategory(xmcda_v3.categories.get(affectation_v2.getCategoryID()));
			return;
		}
		if (affectation_v2.getCategoriesSetID() != null)
		{
			final org.xmcda.CategoriesSet<?> cS = xmcda_v3.categoriesSets.get(affectation_v2.getCategoriesSetID());
			assignment_v3.setCategoriesSet(cS);
			return;
		}
		if (affectation_v2.getCategoriesSet() != null)
		{
			final org.xmcda.CategoriesSet cS = new CategoriesSetConverter()
			        .convertTo_v3(affectation_v2.getCategoriesSet(), xmcda_v3);
			xmcda_v3.categoriesSets.add(cS);
			return;
		}
		if (affectation_v2.getCategoriesInterval() != null)
		{
			getWarnings().pushTag(org.xmcda.parsers.xml.xmcda_v3.CategoriesIntervalParser.CATEGORIES_INTERVAL,
			                      affectation_v2.getId());
			final org.xmcda.v2.CategoriesInterval catInterval_v2 = affectation_v2.getCategoriesInterval();
			final org.xmcda.CategoriesInterval catInterval_v3 = new org.xmcda.CategoriesInterval();
			assignment_v3.setCategoryInterval(catInterval_v3);

			catInterval_v3.setId(affectation_v2.getId());
			catInterval_v3.setName(affectation_v2.getName());
			catInterval_v3.setMcdaConcept(affectation_v2.getMcdaConcept());

			if (catInterval_v2.getDescription() != null)
			    getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

			if ( affectation_v2.getCategoriesInterval().getLowerBound() != null )
			    catInterval_v3.setLowerBound(xmcda_v3.categories.get(catInterval_v2.getLowerBound().getCategoryID()));

			if ( affectation_v2.getCategoriesInterval().getUpperBound() != null )
				catInterval_v3.setUpperBound(xmcda_v3.categories.get(catInterval_v2.getUpperBound().getCategoryID()));
			getWarnings().popTag();
		}
	}

	/* v3 -> v2 */

	public <VALUE_TYPE> org.xmcda.v2.AlternativeAffectation
	        convertTo_v2(org.xmcda.AlternativeAssignment<VALUE_TYPE> alternativeAssignment_v3)
	{
		org.xmcda.v2.AlternativeAffectation alternativeAffectation_v2 = new org.xmcda.v2.AlternativeAffectation();

		getWarnings().pushTag(ALTERNATIVE_ASSIGNMENT, alternativeAssignment_v3.id());

		// default attributes
		alternativeAffectation_v2.setId(alternativeAssignment_v3.id());
		alternativeAffectation_v2.setName(alternativeAssignment_v3.name());
		alternativeAffectation_v2.setMcdaConcept(alternativeAssignment_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = alternativeAssignment_v3.getDescription();
		alternativeAffectation_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// alternative
		if (alternativeAssignment_v3.getAlternative() != null)
		    // this is a degenerate case (and semantically invalid), not sure we should continue TODO
		    alternativeAffectation_v2.setAlternativeID(alternativeAssignment_v3.getAlternative().id());

		// category / categoriesSet / categoriesInterval
		if (alternativeAssignment_v3.getCategory() != null)
		{
			alternativeAffectation_v2.setCategoryID(alternativeAssignment_v3.getCategory().id());
		}
		else if (alternativeAssignment_v3.getCategoriesSet() != null)
		{
			/*
			 * we choose to convert to v2/categoriesSetID, the categoriesSet will be transformed on its own into its
			 * v2 equivalent
			 */
			alternativeAffectation_v2.setCategoriesSetID(alternativeAssignment_v3.getCategoriesSet().id());
		}
		else if (alternativeAssignment_v3.getCategoryInterval() != null)
		{
			final org.xmcda.CategoriesInterval categoriesInterval_v3 = alternativeAssignment_v3.getCategoryInterval();
			final org.xmcda.v2.CategoriesInterval categoriesInterval_v2 = new org.xmcda.v2.CategoriesInterval();
			alternativeAffectation_v2.setCategoriesInterval(categoriesInterval_v2);

			categoriesInterval_v2.setId(categoriesInterval_v3.id());
			categoriesInterval_v2.setName(categoriesInterval_v3.name());
			categoriesInterval_v2.setMcdaConcept(categoriesInterval_v3.mcdaConcept());

			categoriesInterval_v2
			        .setDescription(new DescriptionConverter().convertTo_v2(categoriesInterval_v3.getDescription()));

			org.xmcda.v2.CategoriesIntervalBound bound_v2;
			if (categoriesInterval_v3.getLowerBound() != null)
			{
				bound_v2 = new org.xmcda.v2.CategoriesIntervalBound();
				bound_v2.setCategoryID(categoriesInterval_v3.getLowerBound().id());
				categoriesInterval_v2.setLowerBound(bound_v2);
			}
			if (categoriesInterval_v3.getUpperBound() != null)
			{
				bound_v2 = new org.xmcda.v2.CategoriesIntervalBound();
				bound_v2.setCategoryID(categoriesInterval_v3.getUpperBound().id());
				categoriesInterval_v2.setUpperBound(bound_v2);
			}
		}

		// values
		if (alternativeAssignment_v3.getValues() != null)
		{
			Object _v = ValuesConverter.convertTo_v2(alternativeAssignment_v3.getValues());
			if (_v instanceof org.xmcda.v2.Value)
				alternativeAffectation_v2.getValue().add((org.xmcda.v2.Value) _v);
			else
				alternativeAffectation_v2.getValues().add((org.xmcda.v2.Values) _v);
		}

		getWarnings().popTag(); // ALTERNATIVE_ASSIGNMENT
		return alternativeAffectation_v2;
	}

}
