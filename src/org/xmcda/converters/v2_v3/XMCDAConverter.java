package org.xmcda.converters.v2_v3;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.xmcda.XMCDA;

public class XMCDAConverter
{
	private static final String V2 = "--v2";

	private static final String V3 = "--v3";

	/**
	 * This holds the parameters controlling some aspects the conversion of XMCDA objects to XMCDA v2<br/>
	 * By default, omitValues...() methods return the {@link default #getDefault() value} (which is equal to
	 * {@code false} when the parameters are initialized). Setting them to a non-nil boolean value superseeds the default behaviour; setting them to
	 * {@code null} re-installs the default value.<br/>
	 * The default value is a (non-nil) boolean value<br/>
	 */
	public static class ToV2ConverterParameters
	{
		protected boolean omit_values_in_xxxValues = true;
		protected Boolean omit_values_in_criteriaValues;
		protected Boolean omit_values_in_alternativesValues;
		protected Boolean omit_values_in_categoriesValues;
		protected boolean convert_execution_status_to_methodMessages = true;
			
		protected ToV2ConverterParameters() { reset(); }

		public boolean getDefault()
		{
			return omit_values_in_xxxValues;
		}

		public void setDefault(boolean bool)
		{
			omit_values_in_xxxValues = bool;
		}

		/**
		 * Resets the parameters as it is at creation time.
		 */
		public void reset()
		{
			omit_values_in_xxxValues = true;
			omit_values_in_alternativesValues = null;
			omit_values_in_categoriesValues = null;
			omit_values_in_criteriaValues = null;
		}

		/**
		 * Tells the {@link MethodMessagesConverter} whether XMCDA v3 execution messages should be converted into
		 * methodMessages in XMCDA v2.
		 * @parameter bool a boolean
		 */
		public void setConvertExecutionStatusToMethodMessages(boolean bool)
		{
			convert_execution_status_to_methodMessages = bool;
		}
		
		public void setOmitValuesInAlternativesValues(Boolean bool)
		{
			omit_values_in_alternativesValues = bool;
		}

		public void setOmitValuesInCriteriaValues(Boolean bool)
		{
			omit_values_in_criteriaValues = bool;
		}

		public void setOmitValuesInCategoriesValues(Boolean bool)
		{
			omit_values_in_categoriesValues = bool;
		}

		/**
		 * Tells the {@link MethodMessagesConverter} whether XMCDA v3 execution messages should be converted into
		 * methodMessages in XMCDA v2.
		 * @return a boolean
		 */
		public boolean convertExecutionStatusToMethodMessages()
		{
			return convert_execution_status_to_methodMessages;
		}
		
		public boolean omitValuesInAlternativesValues()
		{
			return omit_values_in_alternativesValues != null
			     ? omit_values_in_alternativesValues
			     : omit_values_in_xxxValues;
		}

		public boolean omitValuesInCategoriesValues()
		{
			return omit_values_in_categoriesValues != null
			     ? omit_values_in_categoriesValues
			     : omit_values_in_xxxValues;
		}

		public boolean omitValuesInCriteriaValues()
		{
			return omit_values_in_criteriaValues != null
			     ? omit_values_in_criteriaValues
			     : omit_values_in_xxxValues;
		}

	}

	public static ThreadLocal<ToV2ConverterParameters> toV2Parameters =
		new ThreadLocal<ToV2ConverterParameters>() {
			@Override
			protected ToV2ConverterParameters initialValue()
			{
				return new ToV2ConverterParameters();
			}
	};

	public static org.xmcda.XMCDA convertTo_v3(org.xmcda.v2.XMCDA xmcda_v2)
	{
		org.xmcda.XMCDA xmcda_v3 = new org.xmcda.XMCDA();
		return convertTo_v3(xmcda_v2, xmcda_v3);
	}

	public static org.xmcda.XMCDA convertTo_v3(org.xmcda.v2.XMCDA xmcda_v2, org.xmcda.XMCDA xmcda_v3)
	{
		for (JAXBElement<?> element: xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters())
		{
			if (element.getValue() instanceof org.xmcda.v2.Alternatives)
			{
				new AlternativesConverter().convertTo_v3((org.xmcda.v2.Alternatives) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.Criteria)
			{
				new CriteriaConverter().convertTo_v3((org.xmcda.v2.Criteria) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.Categories)
			{
				new CategoriesConverter().convertTo_v3((org.xmcda.v2.Categories) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.AlternativesSets)
			{
				new AlternativesSetsConverter().convertTo_v3((org.xmcda.v2.AlternativesSets) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CriteriaSets)
			{
				new CriteriaSetsConverter().convertTo_v3((org.xmcda.v2.CriteriaSets) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CategoriesSets)
			{
				new CategoriesSetsConverter().convertTo_v3((org.xmcda.v2.CategoriesSets) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.PerformanceTable)
			{
				new PerformanceTableConverter().convertTo_v3((org.xmcda.v2.PerformanceTable) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.AlternativesValues)
			{
				new AlternativesValuesConverter().convertTo_v3((org.xmcda.v2.AlternativesValues) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CriterionValue)
			{
				new CriteriaValuesConverter().convertTo_v3((org.xmcda.v2.CriterionValue) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CriteriaValues)
			{
				new CriteriaValuesConverter().convertTo_v3((org.xmcda.v2.CriteriaValues) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CategoriesValues)
			{
				new CategoriesValuesConverter().convertTo_v3((org.xmcda.v2.CategoriesValues) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.AlternativesAffectations)
			{
				new AlternativesAssignmentsConverter().convertTo_v3((org.xmcda.v2.AlternativesAffectations) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.AlternativesComparisons)
			{
				new AlternativesComparisonsConverter().convertTo_v3((org.xmcda.v2.AlternativesComparisons) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CriteriaComparisons)
			{
				new CriteriaComparisonsConverter().convertTo_v3((org.xmcda.v2.CriteriaComparisons) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CategoriesComparisons)
			{
				new CategoriesComparisonsConverter().convertTo_v3((org.xmcda.v2.CategoriesComparisons) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CriteriaMatrix)
			{
				new CriteriaMatrixConverter().convertTo_v3((org.xmcda.v2.CriteriaMatrix) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.CategoriesProfiles)
			{
				new CategoriesProfilesConverter().convertTo_v3((org.xmcda.v2.CategoriesProfiles) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.Hierarchy)
			{
				new HierarchyConverter().convertTo_v3((org.xmcda.v2.Hierarchy) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.MethodParameters)
			{
				new MethodParametersConverter().convertTo_v3((org.xmcda.v2.MethodParameters) element.getValue(), xmcda_v3);
			}
			if (element.getValue() instanceof org.xmcda.v2.MethodMessages)
			{
				new MethodMessagesConverter().convertTo_v3((org.xmcda.v2.MethodMessages) element.getValue(), xmcda_v3);
			}
	}
		return xmcda_v3;
	}

	public static org.xmcda.v2.XMCDA convertTo_v2(org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.v2.XMCDA xmcda_v2 = new org.xmcda.v2.XMCDA();

		if (xmcda_v3.alternatives != null && xmcda_v3.alternatives.size() != 0)
		    new AlternativesConverter().convertTo_v2(xmcda_v3.alternatives, xmcda_v2);
		/*
		if (xmcda_v3.alternativesValuesList != null && xmcda_v3.alternativesValuesList.size() != 0)
		    new AlternativesValuesConverter().convertTo_v2(xmcda_v3.alternativesValuesList, xmcda_v2);
		*/
		if (xmcda_v3.criteria != null && xmcda_v3.criteria.size() != 0)
		    new CriteriaConverter().convertTo_v2(xmcda_v3.criteria,
		                                         xmcda_v3.criteriaScalesList,
		                                         xmcda_v3.criteriaFunctionsList,
		                                         xmcda_v3.criteriaThresholdsList,
		                                         xmcda_v2);

		if (xmcda_v3.categories != null && xmcda_v3.categories.size() != 0)
			new CategoriesConverter().convertTo_v2(xmcda_v3.categories, xmcda_v2, xmcda_v3);

		if (xmcda_v3.alternativesSets != null && xmcda_v3.alternativesSets.size() != 0)
			new AlternativesSetsConverter().convertTo_v2(xmcda_v3.alternativesSets, xmcda_v2);
		if (xmcda_v3.criteriaSets != null && xmcda_v3.criteriaSets.size() != 0)
			new CriteriaSetsConverter().convertTo_v2(xmcda_v3.criteriaSets, xmcda_v2);
		if (xmcda_v3.categoriesSets != null && xmcda_v3.categoriesSets.size() != 0)
			new CategoriesSetsConverter().convertTo_v2(xmcda_v3.categoriesSets, xmcda_v2);

		if (xmcda_v3.performanceTablesList != null && xmcda_v3.performanceTablesList.size() != 0)
			new PerformanceTableConverter().convertTo_v2(xmcda_v3.performanceTablesList, xmcda_v2);

		if (xmcda_v3.alternativesMatricesList != null && xmcda_v3.alternativesMatricesList.size() != 0 )
			// to criteriaComparisons
			new AlternativesComparisonsConverter().convertAlternativesMatricesTo_v2(xmcda_v3.alternativesMatricesList, xmcda_v2);

		if (xmcda_v3.alternativesSetsMatricesList != null && xmcda_v3.alternativesSetsMatricesList.size() != 0 )
			// to criteriaComparisons
			new AlternativesComparisonsConverter().convertAlternativesSetsMatricesTo_v2(xmcda_v3.alternativesSetsMatricesList, xmcda_v2);

		if (xmcda_v3.alternativesValuesList != null && xmcda_v3.alternativesValuesList.size() != 0)
			new AlternativesValuesConverter().convertTo_v2(xmcda_v3.alternativesValuesList, xmcda_v2);

		if (xmcda_v3.alternativesAssignmentsList != null && xmcda_v3.alternativesAssignmentsList.size() != 0)
			new AlternativesAssignmentsConverter().convertTo_v2(xmcda_v3.alternativesAssignmentsList, xmcda_v2);

		if (xmcda_v3.criteriaValuesList != null && xmcda_v3.criteriaValuesList.size() != 0)
			new CriteriaValuesConverter().convertTo_v2(xmcda_v3.criteriaValuesList, xmcda_v2);

		if (xmcda_v3.categoriesValuesList != null && xmcda_v3.categoriesValuesList.size() != 0)
			new CategoriesValuesConverter().convertTo_v2(xmcda_v3.categoriesValuesList, xmcda_v2);

		if (xmcda_v3.criteriaSetsValuesList != null && xmcda_v3.criteriaSetsValuesList.size() != 0)
			new CriteriaValuesConverter().criteriaSetsValues_convertTo_v2(xmcda_v3.criteriaSetsValuesList, xmcda_v2, xmcda_v3);

		if (xmcda_v3.criteriaMatricesList != null && xmcda_v3.criteriaMatricesList.size() != 0 )
			// to criteriaComparisons, unconditionally: no WS use v2 criteriaMatrices
			new CriteriaComparisonsConverter().convertCriteriaMatricesTo_v2(xmcda_v3.criteriaMatricesList, xmcda_v2);
		    //new CriteriaMatrixConverter().convertTo_v2(xmcda_v3.criteriaMatricesList, xmcda_v2);

		if (xmcda_v3.criteriaSetsMatricesList != null && xmcda_v3.criteriaSetsMatricesList.size() != 0 )
			// To criteriaComparisons for testing it
			new CriteriaComparisonsConverter().convertCriteriaSetsMatricesTo_v2(xmcda_v3.criteriaSetsMatricesList, xmcda_v2);

		if (xmcda_v3.criteriaHierarchiesList != null && xmcda_v3.criteriaHierarchiesList.size() != 0 )
			new HierarchyConverter().convertTo_v2(xmcda_v3.criteriaHierarchiesList, xmcda_v2);

		if (xmcda_v3.criteriaSetsHierarchiesList != null && xmcda_v3.criteriaSetsHierarchiesList.size() != 0 )
			new HierarchyConverter().convertTo_v2(xmcda_v3.criteriaSetsHierarchiesList, xmcda_v2);

		if (xmcda_v3.categoriesMatricesList != null && xmcda_v3.categoriesMatricesList.size() != 0 )
			new CategoriesComparisonsConverter().convertCategoriesMatricesTo_v2(xmcda_v3.categoriesMatricesList, xmcda_v2);

		if (xmcda_v3.categoriesSetsMatricesList != null && xmcda_v3.categoriesSetsMatricesList.size() != 0 )
			new CategoriesComparisonsConverter().convertCategoriesSetsMatricesTo_v2(xmcda_v3.categoriesSetsMatricesList, xmcda_v2);

		if (xmcda_v3.categoriesProfilesList != null && xmcda_v3.categoriesProfilesList.size() != 0 )
			new CategoriesProfilesConverter().convertTo_v2(xmcda_v3.categoriesProfilesList, xmcda_v2);

		if (xmcda_v3.programParametersList != null && !xmcda_v3.programParametersList.isEmpty() )
			new MethodParametersConverter().convertProgramParametersTo_v2(xmcda_v3.programParametersList, xmcda_v2);

		if (xmcda_v3.programExecutionResultsList != null && !xmcda_v3.programExecutionResultsList.isEmpty() )
			new MethodMessagesConverter().convertProgramExecutionResultsTo_v2(xmcda_v3.programExecutionResultsList, xmcda_v2);

		return xmcda_v2;
	}

	private static void replaceTag(Set<String> tags, String target, String... replacement)
	{
		if ( tags.remove(target) )
			for (String $replacement: replacement)
				tags.add($replacement);
	}

	private static String[] transformRootTags(Set<String> rootTags, String targetVersion)
	{
		Set <String> tags = new HashSet<>(rootTags);
		if (V2.equals(targetVersion))
		{
			replaceTag(tags, "alternativesMatrix", "alternativesComparisons");
			replaceTag(tags, "alternativesAssignments", "alternativesAffectations");
			replaceTag(tags, "criteriaMatrix", "criteriaComparisons");
			replaceTag(tags, "criteriaSetsMatrix", "criteriaComparisons");
			replaceTag(tags, "criteriaFunctions", "criteria");
			replaceTag(tags, "criteriaScales", "criteria");
			replaceTag(tags, "criteriaSetsValues", "criteriaValues");
			replaceTag(tags, "criteriaThresholds", "criteria");
			replaceTag(tags, "categoriesMatrix", "categoriesComparisons");
			replaceTag(tags, "categoriesValues", "categories"); // categories' ranks
			replaceTag(tags, "programParameters", "methodParameters");
			replaceTag(tags, "programExecutionResult", "methodMessages");
		}
		else if (V3.equals(targetVersion))
		{
			replaceTag(tags, "alternativesComparisons", "alternativesMatrix");
			replaceTag(tags, "alternativesValues", "alternativesValues", "alternativesSets", "alternativesSetsValues");
			replaceTag(tags, "alternativesAffectations", "alternativesAssignments");
			replaceTag(tags, "criteriaComparisons", "criteriaMatrix", "criteriaSets", "criteriaSetsMatrix");
			replaceTag(tags, "criteria", "criteria", "criteriaFunctions", "criteriaScales", "criteriaThresholds");
			replaceTag(tags, "criterionValue", "criteriaValues", "criteriaSets", "criteriaSetsValues");
			replaceTag(tags, "criteriaValues", "criteriaValues", "criteriaSets", "criteriaSetsValues");
			replaceTag(tags, "categories", "categories", "categoriesValues"); // categories' ranks
			replaceTag(tags, "categoriesComparisons", "categoriesMatrix");
			replaceTag(tags, "methodParameters", "programParameters");
			replaceTag(tags, "methodMessages", "programExecutionResult");
		}
		else if (targetVersion == null)
			throw new NullPointerException("Invalid null value for parameter targetVersion");
		else
			throw new IllegalArgumentException("Unknown value ("+targetVersion+") for parameter targetVersion");

		return tags.toArray(new String[tags.size()]);
	}

	private static void printErrorAndExit(String errorMessage, int exitStatus)
	{
		System.err.println(errorMessage);
		System.exit(exitStatus);
	}
	
	private static final String USAGE = "Usage: XMCDAConverter [--no-values] [--input-tags-only] --[v2|v3] file_in.xml --[v2|v3] file_out.xml";

	public static void main(String[] args) throws Exception
	{
		List<String> v2files = new ArrayList<>();
		List<String> v3files = new ArrayList<>();

		Boolean to_v3 = null;
		boolean restrict_tags = false;
		boolean verbose = false;

		List<String> files = null;

		for (int idx = 0; idx < args.length; idx++)
		{
			if ("-v".equals(args[idx]) || "--verbose".equals(args[idx]))
			{
				verbose = true;
				continue;
			}
			if ("--no-values".equals(args[idx]))
			{
				toV2Parameters.get().setDefault(true);
				continue;
			}
			if ("--input-tags-only".equals(args[idx]))
			{
				restrict_tags = true;
				continue;
			}
			if ("--use-value-when-possible".equals(args[idx]))
			{
				ValuesConverter.setEmbedMultipleXMCDAv2ValueInValues(true);
				continue;
			}
			if ("--v2".equals(args[idx]))
			{
				if (to_v3 == null)
					to_v3 = true;
				files = v2files;
				continue;
			}
			if ("--v3".equals(args[idx]))
			{
				if (to_v3 == null)
					to_v3 = false;
				files = v3files;
				continue;
			}
			if (files == null)
				printErrorAndExit(USAGE, -1);
			files.add(args[idx]);
		}

		if (v2files.size() != v3files.size())
			printErrorAndExit("Error: there must be the same number of input files and output files", -2);

		org.xmcda.parsers.xml.xmcda_v3.XMCDAParser parser = new org.xmcda.parsers.xml.xmcda_v3.XMCDAParser();

		final Iterator<String> infiles;
		final Iterator<String> outfiles;
		if (to_v3) {
			infiles = v2files.iterator();
			outfiles = v3files.iterator();
		}
		else {
			infiles = v3files.iterator();
			outfiles = v2files.iterator();
		}
		while (infiles.hasNext())
		{
			final org.xmcda.XMCDA xmcda;
			final org.xmcda.v2.XMCDA xmcda_v2;

			// Read
			Set<String> readRootTags = new HashSet<>();
			if (to_v3)
			{
				String currentFile = infiles.next();
				try {
					xmcda_v2 = org.xmcda.parsers.xml.xmcda_v2.XMCDAParser.readXMCDA(currentFile);
				}
				catch (Exception e) {
					System.err.println("Failed to read file "+currentFile+": "+e.getMessage());
					if (verbose)
						e.printStackTrace();
					continue;
				}
				if ( restrict_tags )
					readRootTags = org.xmcda.parsers.xml.xmcda_v2.XMCDAParser.rootTags(xmcda_v2);
				xmcda = XMCDAConverter.convertTo_v3(xmcda_v2);
			}
			else
			{
				String currentFile = infiles.next();
				try {
					xmcda = parser.readXMCDA(new XMCDA(), new File(currentFile), readRootTags);
				}
				catch (Exception e) {
					System.err.println("Failed to read file "+currentFile+": "+e.getMessage());
					if (verbose)
						e.printStackTrace();
					continue;
				}
			}

			String[] restrict_to_tags = {};
			if ( restrict_tags )
				restrict_to_tags = transformRootTags(readRootTags, to_v3 ? V3 : V2);

			// Write
			final String outfile = outfiles.next();
			try {
				if (to_v3)
					parser.writeXMCDA(xmcda, outfile, restrict_to_tags);
				else
					org.xmcda.parsers.xml.xmcda_v2.XMCDAParser.writeXMCDA(XMCDAConverter.convertTo_v2(xmcda),
					                                                      new File(outfile),
					                                                      restrict_to_tags);
			}
			catch (Exception e) {
				System.err.println("Failed to write file "+outfile+": "+e.getMessage());
				if (verbose)
					e.printStackTrace();
				continue;
			}
		}
	}

}
