package org.xmcda.converters.v2_v3;

import org.xmcda.Factory;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Sébastien Bigaret
 */
public class CriteriaConverter
    extends Converter
{
	public static final String CRITERIA = org.xmcda.Criteria.TAG;

	public CriteriaConverter()
	{
		super(CRITERIA);
	}

	public void convertTo_v3(org.xmcda.v2.Criteria value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CRITERIA, value.getId());
		
		org.xmcda.Criteria criteria_v3 = xmcda_v3.criteria;
		// defaultAttributes
		criteria_v3.setId(value.getId());
		criteria_v3.setMcdaConcept(value.getMcdaConcept());
		criteria_v3.setName(value.getName());

		// description
		criteria_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// criterion 0..*
		org.xmcda.CriteriaFunctions critFuncs_v3 = Factory.criteriaFunctions();
		org.xmcda.CriteriaThresholds critThresholds_v3 = Factory.criteriaThresholds();
		org.xmcda.CriteriaScales critScales_v3 = Factory.criteriaScales();

		for (org.xmcda.v2.Criterion criterion: value.getCriterion())
		{
			// only create criterion w/ default attributes, description & active
			new CriterionConverter().convertTo_v3(criterion, xmcda_v3, critScales_v3, critFuncs_v3, critThresholds_v3);
		}
		if (!critScales_v3.isEmpty())
			xmcda_v3.criteriaScalesList.add(critScales_v3);
		if (!critFuncs_v3.isEmpty())
			xmcda_v3.criteriaFunctionsList.add(critFuncs_v3);
		if (!critThresholds_v3.isEmpty())
			xmcda_v3.criteriaThresholdsList.add(critThresholds_v3);
		getWarnings().popTag(); // CRITERIA
	}

	public void convertTo_v2(org.xmcda.Criteria criteria_v3,
	                         ArrayList<org.xmcda.CriteriaScales> criteriaScalesList_v3,
	                         ArrayList<org.xmcda.CriteriaFunctions> criteriaFuncsList_v3,
	                         ArrayList<org.xmcda.CriteriaThresholds> criteriaThresholdsList_v3,
	                         org.xmcda.v2.XMCDA xmcda_v2)
	{
		if (criteria_v3==null || criteria_v3.isVoid())
			return;

		getWarnings().pushTag(CRITERIA, criteria_v3.id());
		
		org.xmcda.v2.Criteria criteria_v2 = new org.xmcda.v2.Criteria();
		List<JAXBElement<?>> crits = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		crits.add(new JAXBElement<org.xmcda.v2.Criteria>(new QName(CRITERIA), org.xmcda.v2.Criteria.class,
		                                                     criteria_v2));

		// default attributes
		criteria_v2.setId(criteria_v3.id());
		criteria_v2.setName(criteria_v3.name());
		criteria_v2.setMcdaConcept(criteria_v3.mcdaConcept());

		criteria_v2.setDescription(new DescriptionConverter().convertTo_v2(criteria_v3.getDescription()));

		// criterion
		CriterionConverter criterionConverter = new CriterionConverter();
		
		// criteria: scales
		org.xmcda.CriteriaScales criteriaScales_v3 = null;
		if ( criteriaScalesList_v3!=null && !criteriaScalesList_v3.isEmpty() )
		{
			// explicitely convert the first criteriaScales only
			criteriaScales_v3 = criteriaScalesList_v3.get(0);
			if (criteriaScalesList_v3.size()>1)
				getWarnings().elementIgnored(org.xmcda.CriteriaScales.TAG, "Only the first criteriaScales is taken into account in the translation");
		}

		// criteria: functions
		org.xmcda.CriteriaFunctions criteriaFuncs_v3 = null;
		if (criteriaFuncsList_v3 != null && !criteriaFuncsList_v3.isEmpty())
		{
			// explicitely convert the first criteriaFunctions only
			criteriaFuncs_v3 = criteriaFuncsList_v3.get(0);
			if (criteriaFuncsList_v3.size()>1)
				getWarnings().elementIgnored(org.xmcda.CriteriaFunctions.TAG, "Only the first criteriaFunctions is taken into account in the translation");
		}
		
		// criteria: thresholds
		org.xmcda.CriteriaThresholds criteriaThresholds_v3 = null;
		if (criteriaThresholdsList_v3!=null && !criteriaThresholdsList_v3.isEmpty())
		{
			// explicitely convert the first criteriaThresholds only
			criteriaThresholds_v3 = criteriaThresholdsList_v3.get(0);
			if (criteriaThresholdsList_v3.size()>1)
				getWarnings().elementIgnored(org.xmcda.CriteriaThresholds.TAG, "Only the first criteriaThresholds is taken into account in the translation");
		}
		
		for (org.xmcda.Criterion criterion_v3: criteria_v3)
		{
			org.xmcda.CriterionFunctions criterionFuncs_v3 = null;
			if ( criteriaFuncs_v3 != null )
				criterionFuncs_v3 = criteriaFuncs_v3.get(criterion_v3);
			org.xmcda.CriterionThresholds criterionThresholds_v3 = null;
			if ( criteriaThresholds_v3 != null )
				criterionThresholds_v3 = criteriaThresholds_v3.get(criterion_v3);
			org.xmcda.CriterionScales criterionScales_v3 = null;
			if ( criteriaScales_v3 != null )
				criterionScales_v3 = criteriaScales_v3.get(criterion_v3);
			criteria_v2.getCriterion().add(criterionConverter.convertTo_v2(criterion_v3,
			                                                               criterionScales_v3,
			                                                               criterionFuncs_v3,
			                                                               criterionThresholds_v3)); // TODO
		}
		getWarnings().popTag(); // CRITERIA
	}

}
