/**
 * 
 */
package org.xmcda.converters.v2_v3;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class FunctionConverter
extends Converter
{
	public static String FUNCTION = "function";
	
	public FunctionConverter()
	{
		super(FUNCTION);
	}

	public org.xmcda.value.Function convertTo_v3(org.xmcda.v2.Function func_v2, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.value.Function func_v3 = null;

		if (func_v2.getConstant() != null)
		{
			org.xmcda.value.ConstantFunction cfunc_v3 = new org.xmcda.value.ConstantFunction();
			func_v3 = cfunc_v3;
			cfunc_v3.setValue(new QualifiedValueConverter().convertTo_v3(func_v2.getConstant(), xmcda_v3).getValue());
		}
		else if (func_v2.getLinear() != null)
		{
			org.xmcda.value.AffineFunction affine_v3 = new org.xmcda.value.AffineFunction();
			func_v3 = affine_v3;
			affine_v3.setIntercept(new QualifiedValueConverter().convertTo_v3(V2ValueToNumericValueConverter.convert(func_v2.getLinear().getIntercept()),
			                                                                  xmcda_v3).getValue());
			affine_v3.setSlope(new QualifiedValueConverter().convertTo_v3(V2ValueToNumericValueConverter.convert(func_v2.getLinear().getSlope()),
			                                                              xmcda_v3).getValue());
		}
		else if (func_v2.getPiecewiseLinear() != null)
		{
			org.xmcda.value.PiecewiseLinearFunction plf_v3 = new org.xmcda.value.PiecewiseLinearFunction();
			func_v3 = plf_v3;
			org.xmcda.v2.Function.PiecewiseLinear plf_v2 = func_v2.getPiecewiseLinear();
			for (org.xmcda.v2.Function.PiecewiseLinear.Segment segment_v2: plf_v2.getSegment())
			{
				org.xmcda.value.Segment segment_v3 = new org.xmcda.value.Segment();
				segment_v3.setHead(new org.xmcda.value.EndPoint(new org.xmcda.value.Point()));
				segment_v3.getHead().setAbscissa(new QualifiedValueConverter().convertTo_v3(segment_v2.getHead().getAbscissa(), xmcda_v3));
				segment_v3.getHead().setOrdinate(new QualifiedValueConverter().convertTo_v3(segment_v2.getHead().getOrdinate(), xmcda_v3));
				segment_v3.setTail(new org.xmcda.value.EndPoint(new org.xmcda.value.Point()));
				segment_v3.getTail().setAbscissa(new QualifiedValueConverter().convertTo_v3(segment_v2.getTail().getAbscissa(), xmcda_v3));
				segment_v3.getTail().setOrdinate(new QualifiedValueConverter().convertTo_v3(segment_v2.getTail().getOrdinate(), xmcda_v3));
				plf_v3.add(segment_v3);
			}
		}
		else if (func_v2.getPoints() != null)
		{
			org.xmcda.value.DiscreteFunction df_v3 = new org.xmcda.value.DiscreteFunction();
			func_v3 = df_v3;
			for (org.xmcda.v2.Point point_v2: func_v2.getPoints().getPoint())
			{
				org.xmcda.value.Point point_v3 = new org.xmcda.value.Point();
				point_v3.setAbscissa(new QualifiedValueConverter().convertTo_v3(point_v2.getAbscissa(), xmcda_v3));
				point_v3.setOrdinate(new QualifiedValueConverter().convertTo_v3(point_v2.getOrdinate(), xmcda_v3));
				df_v3.add(point_v3);
			}
		}

		if (func_v3 != null)
		{
			func_v3.setId(func_v2.getId());
			func_v3.setName(func_v2.getName());
			func_v3.setMcdaConcept(func_v2.getMcdaConcept());
		}

		return func_v3;
	}
	
	public org.xmcda.v2.Function convertTo_v2(org.xmcda.v2.Criterion criterion_v2, org.xmcda.value.Function criterionFunction_v3)
	{
		if (criterionFunction_v3==null)
			return null;
		org.xmcda.value.Function func_v3 = criterionFunction_v3;
		
		org.xmcda.v2.Function func_v2 = new org.xmcda.v2.Function();
		func_v2.setId(func_v3.id());
		func_v2.setName(func_v3.name());
		func_v2.setMcdaConcept(func_v3.mcdaConcept());

		if (func_v3 instanceof org.xmcda.value.ConstantFunction)
		{
			final org.xmcda.value.ConstantFunction const_func_v3 = (org.xmcda.value.ConstantFunction) func_v3;
			org.xmcda.QualifiedValue qv = org.xmcda.Factory.qualifiedValue();
			qv.setValue(const_func_v3.getValue());
			func_v2.setConstant(new V2ValueToNumericValueConverter().convert(new QualifiedValueConverter().convertTo_v2(qv)));
		}
		else if (func_v3 instanceof org.xmcda.value.AffineFunction)
		{
			final org.xmcda.value.AffineFunction<?> affine_func_v3 = (org.xmcda.value.AffineFunction) func_v3;
			org.xmcda.v2.Function.Linear linear_v2 = new org.xmcda.v2.Function.Linear();

			org.xmcda.QualifiedValue qv = org.xmcda.Factory.qualifiedValue();
			qv.setValue(affine_func_v3.getIntercept());
			linear_v2.setIntercept(new V2ValueToNumericValueConverter().convert(new QualifiedValueConverter().convertTo_v2(qv)));
			qv.setValue(affine_func_v3.getSlope());
			linear_v2.setSlope(new V2ValueToNumericValueConverter().convert(new QualifiedValueConverter().convertTo_v2(qv)));

			func_v2.setLinear(linear_v2);
		}
		else if (func_v3 instanceof org.xmcda.value.PiecewiseLinearFunction)
		{
			final org.xmcda.value.PiecewiseLinearFunction<?,?> pf_func_v3 = (org.xmcda.value.PiecewiseLinearFunction) func_v3;
			org.xmcda.v2.Function.PiecewiseLinear linear_v2 = new org.xmcda.v2.Function.PiecewiseLinear();

			for (org.xmcda.value.Segment segment_v3: pf_func_v3)
			{
				org.xmcda.v2.Function.PiecewiseLinear.Segment s_v2 = new org.xmcda.v2.Function.PiecewiseLinear.Segment();

				/* head */
				s_v2.setHead(new org.xmcda.v2.Point());
				s_v2.getHead().setAbscissa(new QualifiedValueConverter().convertTo_v2(segment_v3.getHead().getAbscissa()));
				s_v2.getHead().setOrdinate(new QualifiedValueConverter().convertTo_v2(segment_v3.getHead().getOrdinate()));

				/* tail */
				s_v2.setTail(new org.xmcda.v2.Point());
				s_v2.getTail().setAbscissa(new QualifiedValueConverter().convertTo_v2(segment_v3.getTail().getAbscissa()));
				s_v2.getTail().setOrdinate(new QualifiedValueConverter().convertTo_v2(segment_v3.getTail().getOrdinate()));

				linear_v2.getSegment().add(s_v2);
			}
			func_v2.setPiecewiseLinear(linear_v2);
		}
		
		else if (func_v3 instanceof org.xmcda.value.DiscreteFunction)
		{
			final org.xmcda.value.DiscreteFunction<?, ?> discrete_func_v3 = (org.xmcda.value.DiscreteFunction) func_v3;
			org.xmcda.v2.Function.Points discrete_v2 = new org.xmcda.v2.Function.Points();
			
			for (org.xmcda.value.Point point_v3: discrete_func_v3)
			{
				org.xmcda.v2.Point point_v2 = new org.xmcda.v2.Point();
				point_v2.setAbscissa(new QualifiedValueConverter().convertTo_v2(point_v3.getAbscissa()));
				point_v2.setOrdinate(new QualifiedValueConverter().convertTo_v2(point_v3.getOrdinate()));
				discrete_v2.getPoint().add(point_v2);
			}
			func_v2.setPoints(discrete_v2);
		}
		
		return func_v2;
	}
}
