package org.xmcda.converters.v2_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

public class AlternativesValuesConverter
    extends Converter
{
	public static final String ALTERNATIVES_VALUES = org.xmcda.AlternativesValues.TAG;

	public AlternativesValuesConverter()
	{
		super(ALTERNATIVES_VALUES);
	}

	// v2 -> v3
	/**
	 * @param value
	 * @param xmcda_v3
	 */
	public void convertTo_v3(org.xmcda.v2.AlternativesValues value, org.xmcda.XMCDA xmcda_v3)
	{
		if (value.getAlternativeValue().size() == 0)
		{
			// NB not valid wrt xmcda v2 schema
			return;
		}
		getWarnings().pushTag(ALTERNATIVES_VALUES, value.getId());

		// we suppose all values is of the same type of the first one; for example, there are just alternativeIDs
		// This is what was intended in v2, even if not enforced by the schema
		final org.xmcda.v2.AlternativeValue alternativeValue_v2 = value.getAlternativeValue().get(0);

		if (alternativeValue_v2.getAlternativeID() != null)
		{
			xmcda_v3.alternativesValuesList.add(alternativesValues_v3_alternativeID(value, xmcda_v3));
		}
		else if (alternativeValue_v2.getAlternativesSetID() != null)
		{
			xmcda_v3.alternativesSetsValuesList.add(alternativesValues_v3_alternativesSetID(value, xmcda_v3)); // unimplemented
		}
		else if (alternativeValue_v2.getAlternativesSet() != null)
		{
			xmcda_v3.alternativesSetsValuesList.add(alternativesValues_v3_alternativesSet(value, xmcda_v3));
		}
		else
		{
			// do nothing: anything else won't be valid wrt xmcda v2 schema;
		}
		this.getWarnings().popTag(); // ALTERNATIVES_VALUES
	}

	/**
	 * Convert v2 alternatives values containing &lt;alternativeID&gt; only
	 *
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	protected <T> org.xmcda.AlternativesValues<T> alternativesValues_v3_alternativeID(org.xmcda.v2.AlternativesValues value,
	                                                                        org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.AlternativesValues<T> alternativesValues_v3 = org.xmcda.Factory.alternativesValues();

		// defaultAttributes
		alternativesValues_v3.setId(value.getId());
		alternativesValues_v3.setMcdaConcept(value.getMcdaConcept());
		alternativesValues_v3.setName(value.getName());

		// description
		alternativesValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// alternativeValue 1..*
		for (org.xmcda.v2.AlternativeValue alternativeValue_v2: value.getAlternativeValue())
		{
			final String alternativeID = alternativeValue_v2.getAlternativeID();
			if (alternativeID == null)
			{
				getWarnings().elementIgnored("alternativeSetID or alternativesSet",
				                             "element found in a list which is supposed to contain alternativeID only");
				continue;
			}

			final org.xmcda.LabelledQValues<T> alternativeValues = ValuesConverter
			        .valueOrValues_convertTo_v3(alternativeValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			alternativesValues_v3.put(xmcda_v3.alternatives.get(alternativeID), alternativeValues);

			alternativeValues.setId(alternativeValue_v2.getId());
			alternativeValues.setName(alternativeValue_v2.getName());
			alternativeValues.setMcdaConcept(alternativeValue_v2.getMcdaConcept());
			alternativeValues.setDescription(new DescriptionConverter().convertTo_v3(alternativeValue_v2.getDescription()));
		}
		return alternativesValues_v3;
	}


	/**
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected org.xmcda.AlternativesSetsValues alternativesValues_v3_alternativesSetID(org.xmcda.v2.AlternativesValues value,
	                                                                       org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.AlternativesSetsValues alternativesSetsValues_v3 = org.xmcda.Factory.alternativesSetsValues();

		// defaultAttributes
		alternativesSetsValues_v3.setId(value.getId());
		alternativesSetsValues_v3.setMcdaConcept(value.getMcdaConcept());
		alternativesSetsValues_v3.setName(value.getName());

		// description
		alternativesSetsValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// alternativeValue 1..*
		for (org.xmcda.v2.AlternativeValue alternativeValue_v2: value.getAlternativeValue())
		{

			final String alternativesSetID = alternativeValue_v2.getAlternativesSetID();
			if (alternativesSetID == null)
			{
				getWarnings()
				        .elementIgnored("alternativeID or alternativesSet",
				                        "element found in a list which is supposed to contain alternativeSetID only");
				continue;
			}
			org.xmcda.AlternativesSet alternativesSet_v3 = xmcda_v3.alternativesSets.get(alternativesSetID);

			final org.xmcda.LabelledQValues alternativeValues = ValuesConverter
			        .valueOrValues_convertTo_v3(alternativeValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			alternativesSetsValues_v3.put(alternativesSet_v3, alternativeValues);

			alternativeValues.setId(alternativeValue_v2.getId());
			alternativeValues.setName(alternativeValue_v2.getName());
			alternativeValues.setMcdaConcept(alternativeValue_v2.getMcdaConcept());
			alternativeValues.setDescription(new DescriptionConverter().convertTo_v3(alternativeValue_v2.getDescription()));
		}
		return alternativesSetsValues_v3;
	}


	/**
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected org.xmcda.AlternativesSetsValues alternativesValues_v3_alternativesSet(org.xmcda.v2.AlternativesValues value,
	                                                                     org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.AlternativesSetsValues alternativesSetsValues_v3 = org.xmcda.Factory.alternativesSetsValues();

		// defaultAttributes
		alternativesSetsValues_v3.setId(value.getId());
		alternativesSetsValues_v3.setMcdaConcept(value.getMcdaConcept());
		alternativesSetsValues_v3.setName(value.getName());

		// description
		alternativesSetsValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));
		// alternativeValue 1..*
		for (org.xmcda.v2.AlternativeValue alternativeValue_v2: value.getAlternativeValue())
		{

			final org.xmcda.v2.AlternativesSet alternativesSet = alternativeValue_v2.getAlternativesSet();
			if (alternativesSet == null)
			{
				getWarnings().elementIgnored("alternativeID or alternativesSetID",
				                             "element found in a list which is suppoed to contain alternativeID only");
				continue;
			}
			org.xmcda.AlternativesSet alternativesSet_v3 = new AlternativesSetConverter().convertTo_v3(alternativesSet, xmcda_v3);
			final org.xmcda.LabelledQValues alternativeValues = ValuesConverter
			        .valueOrValues_convertTo_v3(alternativeValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			alternativesSetsValues_v3.put(alternativesSet_v3, alternativeValues);
			xmcda_v3.alternativesSets.add(alternativesSet_v3);

			alternativeValues.setId(alternativeValue_v2.getId());
			alternativeValues.setName(alternativeValue_v2.getName());
			alternativeValues.setMcdaConcept(alternativeValue_v2.getMcdaConcept());
			if (alternativeValue_v2.getDescription() != null)
				// if it is null, do not override a value that may have been already set by valueOrValues_convertTo_v3(), i.e. a description in <values>
				alternativeValues.setDescription(new DescriptionConverter().convertTo_v3(alternativeValue_v2.getDescription()));
		}
		return alternativesSetsValues_v3;
	}

    // v3 -> v2
    // we will never build XMCDA v2 alternativesValues with alternativesSet within, only alternativesValues with alternativesSetID
	public void convertTo_v2(List<org.xmcda.AlternativesValues<?>> alternativesValues_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.AlternativesValues<?> alternativesValue: alternativesValues_v3)
		{
			org.xmcda.v2.AlternativesValues critValues_v2 = convertTo_v2(alternativesValue);
			List<JAXBElement<?>> critValuesList_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			critValuesList_v2.add(new JAXBElement<>(new QName(ALTERNATIVES_VALUES), org.xmcda.v2.AlternativesValues.class, critValues_v2));
		}
	}

	protected <VALUE_TYPE> org.xmcda.v2.AlternativesValues convertTo_v2(org.xmcda.AlternativesValues<VALUE_TYPE> alternativesValues_v3)
	{
		org.xmcda.v2.AlternativesValues alternativesValues_v2 = new org.xmcda.v2.AlternativesValues();

		getWarnings().pushTag(ALTERNATIVES_VALUES, alternativesValues_v3.id());

		// default attributes
		alternativesValues_v2.setId(alternativesValues_v3.id());
		alternativesValues_v2.setName(alternativesValues_v3.name());
		alternativesValues_v2.setMcdaConcept(alternativesValues_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = alternativesValues_v3.getDescription();
		alternativesValues_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// alternativeValue
		for (Entry<org.xmcda.Alternative, org.xmcda.LabelledQValues<VALUE_TYPE>> critValue_v3: alternativesValues_v3.entrySet())
		{
			getWarnings().pushTag("alternativeValue", critValue_v3.getValue().id());
			org.xmcda.v2.AlternativeValue alternativeValue_v2 = new org.xmcda.v2.AlternativeValue();
			alternativeValue_v2.setId(critValue_v3.getValue().id());
			alternativeValue_v2.setName(critValue_v3.getValue().name());
			alternativeValue_v2.setMcdaConcept(critValue_v3.getValue().mcdaConcept());
			alternativeValue_v2.setDescription(new DescriptionConverter().convertTo_v2(critValue_v3.getValue().getDescription()));

			alternativeValue_v2.setAlternativeID(critValue_v3.getKey().id());

			org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			for (org.xmcda.QualifiedValue<VALUE_TYPE> value_v3: critValue_v3.getValue())
			{
				org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value_v3);
				if ( ! XMCDAConverter.toV2Parameters.get().omitValuesInAlternativesValues() )
					values_v2.getValue().add(value_v2);
				else
				{
					alternativeValue_v2.getValueOrValues().add(value_v2);
				}
			}
			if ( ! XMCDAConverter.toV2Parameters.get().omitValuesInAlternativesValues() )
			{
				alternativeValue_v2.getValueOrValues().add(values_v2);
			}
			alternativesValues_v2.getAlternativeValue().add(alternativeValue_v2);
			getWarnings().popTag(); // "alternativeValue"
		}
		getWarnings().popTag(); // ALTERNATIVES_VALUES
		return alternativesValues_v2;
	}

	public void alternativesSetsValues_convertTo_v2(List<org.xmcda.AlternativesSetsValues<?,?>> alternativesSetsValues_v3, org.xmcda.v2.XMCDA xmcda_v2, org.xmcda.XMCDA xmcda_v3)
	{
		for (org.xmcda.AlternativesSetsValues<?,?> alternativesSetsValues: alternativesSetsValues_v3)
		{
			org.xmcda.v2.AlternativesValues critValues_v2 = convertTo_v2(alternativesSetsValues, xmcda_v2, xmcda_v3);
			List<JAXBElement<?>> critValuesList_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			critValuesList_v2.add(new JAXBElement<>(new QName(ALTERNATIVES_VALUES), org.xmcda.v2.AlternativesValues.class, critValues_v2));
		}
	}

	protected <ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE> org.xmcda.v2.AlternativesValues convertTo_v2(org.xmcda.AlternativesSetsValues<ALTERNATIVES_SETS_VALUE_TYPE, VALUE_TYPE> alternativesSetsValues_v3, org.xmcda.v2.XMCDA xmcda_v2, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.v2.AlternativesValues alternativesValues_v2 = new org.xmcda.v2.AlternativesValues();

		getWarnings().pushTag(org.xmcda.AlternativesSetsValues.TAG, alternativesSetsValues_v3.id());

		// default attributes
		alternativesValues_v2.setId(alternativesSetsValues_v3.id());
		alternativesValues_v2.setName(alternativesSetsValues_v3.name());
		alternativesValues_v2.setMcdaConcept(alternativesSetsValues_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = alternativesSetsValues_v3.getDescription();
		alternativesValues_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// alternativesSetValue -> alternativeValue
		for (Entry<org.xmcda.AlternativesSet<ALTERNATIVES_SETS_VALUE_TYPE>, org.xmcda.LabelledQValues<VALUE_TYPE>> critValue_v3: alternativesSetsValues_v3.entrySet())
		{
			getWarnings().pushTag("alternativesSetValue", critValue_v3.getValue().id());
			org.xmcda.v2.AlternativeValue alternativeValue_v2 = new org.xmcda.v2.AlternativeValue();
			alternativeValue_v2.setId(critValue_v3.getValue().id());
			alternativeValue_v2.setName(critValue_v3.getValue().name());
			alternativeValue_v2.setMcdaConcept(critValue_v3.getValue().mcdaConcept());
			alternativeValue_v2.setDescription(new DescriptionConverter().convertTo_v2(critValue_v3.getValue().getDescription()));

			//alternativeValue_v2.setAlternativesSetID(critValue_v3.getKey().id());
			final String alternativesSetID_v3 = critValue_v3.getKey().id();
			final org.xmcda.AlternativesSet<?> alternativesSet_v3 = xmcda_v3.alternativesSets.get(alternativesSetID_v3);
			alternativeValue_v2.setAlternativesSet(new AlternativesSetConverter().convertTo_v2(alternativesSet_v3, xmcda_v2));

			getWarnings().pushTag("values");
			org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			for (org.xmcda.QualifiedValue<VALUE_TYPE> value_v3: critValue_v3.getValue())
			{
				org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value_v3);
				values_v2.getValue().add(value_v2);
			}
			getWarnings().popTag(); // "values"

			alternativeValue_v2.getValueOrValues().add(values_v2);
			alternativesValues_v2.getAlternativeValue().add(alternativeValue_v2);
			getWarnings().popTag(); // "alternativesSetValue"
		}
		getWarnings().popTag(); // "alternativesSetsValues" / org.xmcda.AlternativesSetsValues.TAG
		return alternativesValues_v2;
	}
}
