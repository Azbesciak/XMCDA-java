package org.xmcda.converters.v2_v3;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class Warnings
extends ArrayList<Warnings>
{
	private static ThreadLocal<Warnings> threadLocalWarning = ThreadLocal.withInitial(()->new Warnings("xmcda"));

	public static Warnings getThreadLocal()
	{
		return threadLocalWarning.get();
	}

	private static final long serialVersionUID = 1L;

	private static class Pair
	{
		String object;

		String reason;

		Pair(String object)
		{
			this.object = object;
		}

		Pair(String object, String reason)
		{
			this.object = object;
			this.reason = reason;
		}

		@Override
		public String toString()
		{
			return "("+this.object==null?"null":this.object+","+this.reason==null?"null":this.reason+")";
		}
	}

	public static final String ATTRIBUTE               = "attribute";

	public static final String ELEMENT                 = "element";

	public static final String IGNORED                 = "ignored";

	public static final String ALL_BUT_FIRST_IGNORED   = "Only the first element is taken into account";

	public static final String ABSENT_IN_V3_0          = "Concept does not exist in XMCDA v3";
	public static final String ABSENT_IN_V2_0          = "Concept does not exist in XMCDA v2";

	private Deque<String[]>    tagsStack               = new ArrayDeque<>();

	private List<Pair>         ignoredAttributes       = new ArrayList<>();

	private List<Pair>         ignoredElements         = new ArrayList<>();

	private List<Pair>         elementsRecommendations = new ArrayList<>();

	public Warnings(String tag)
	{
		this.pushTag(tag);
	}

	/**
	 * Returns the current tag
	 * @return the current tag
	 */
	public String getTag()
	{
		return this.tagsStack.peekLast()[0];
	}

	public String getTagChain()
	{
		StringBuffer chain = new StringBuffer();
		for ( String[] tag: this.tagsStack )
		{
			chain.append("<").append(tag[0]);
			if (tag[1]!=null)
				chain.append(" id=\"").append(tag[1]).append("\"");
			chain.append(">");
		}
		return chain.toString();
	}

	/**
	 * Returns the "id" of the current tag; it may be {@code null}.
	 * @return the id of the current tag
	 */
	public String getTagID()
	{
		return this.tagsStack.peekLast()[1];
	}

	/**
	 * Removes the current tag from the stack
	 * @return the tag that has been removed, along with its ID
	 */
	public String[] popTag()
	{
		return this.tagsStack.removeLast();
	}

	/**
	 * Pushes a new tag onto the tags' stack, making it the current tag. The current tag's {@link #getTagID() ID} is set
	 * to @code null}.
	 *
	 * @param tag the new current tag
	 * @see #getTag()
	 */
	public void pushTag(String tag)
	{
		this.pushTag(tag, null);
	}

	/**
	 * Pushes a new tag onto the tag's stack, along with its associated ID.
	 * @param tag the new current tag
	 * @param id the new current tag's ID. It may be null.
	 */
	public void pushTag(String tag, String id)
	{
		this.tagsStack.addLast(new String[]{tag, id});
	}

	/**
	 * Sets or overrides the tagID for the current tag
	 * @param tagID
	 */
	public void setTagID(String tagID)
	{
		this.tagsStack.peekLast()[1] = tagID;
	}

	public void attributeIgnored(String attributeName)
	{
		System.err.println(getTagChain() + " ignored attribute: " + attributeName);
		this.ignoredAttributes.add(new Pair(attributeName));
	}

	public void attributeIgnored(String attributeName, String reason)
	{
		System.err.println(getTagChain() + " ignored attribute: " + attributeName + " reason: "+reason);
		this.ignoredAttributes.add(new Pair(attributeName));
	}

	public void elementIgnored(String elementName)
	{
		System.err.println(getTagChain() + " ignored element: " + elementName);
		this.ignoredElements.add(new Pair(elementName));
	}

	public void elementIgnored(String elementName, String reason)
	{
		System.err.println(getTagChain() + " ignored element: " + elementName + " reason: "+ reason);
		this.ignoredElements.add(new Pair(elementName, reason));
	}

	public void elementRecommendation(String elementName, String recommendation)
	{
		this.elementsRecommendations.add(new Pair(elementName, recommendation));
	}

	public void elementRequiredIsAbsent_defaultApplied(String elementName, String value)
	{
		System.err.println(getTagChain() + " required element absent: " + elementName + ", using default: "+ value);
		// TODO
	}

	public void elementUnimplemented(String elementName)
	{
		this.ignoredElements.add(new Pair(elementName, "not implemented, please contact the maintainer if you need it"));
	}

	public void throwUnimplemented()
	{
		throw new RuntimeException("unimplemented");
	}
}
