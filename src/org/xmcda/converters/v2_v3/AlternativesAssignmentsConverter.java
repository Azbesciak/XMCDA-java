package org.xmcda.converters.v2_v3;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.xmcda.AlternativesAssignments;

public class AlternativesAssignmentsConverter
extends Converter
{
	public static final String ALTERNATIVES_ASSIGNMENTS = org.xmcda.AlternativeAssignment.TAG;

	public static final String ALTERNATIVES_AFFECTATIONS = "alternativesAffectations";

	public AlternativesAssignmentsConverter()
	{
		super(ALTERNATIVES_ASSIGNMENTS);
	}

	public void convertTo_v3(org.xmcda.v2.AlternativesAffectations affectations, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(ALTERNATIVES_AFFECTATIONS, affectations.getId());
		org.xmcda.AlternativesAssignments<?> altAssgnmts = new org.xmcda.AlternativesAssignments<>();

		// defaultAttributes
		altAssgnmts.setId(affectations.getId());
		altAssgnmts.setName(affectations.getName());
		altAssgnmts.setMcdaConcept(affectations.getMcdaConcept());

		// description
		altAssgnmts.setDescription(new DescriptionConverter().convertTo_v3(affectations.getDescription()));

		for (org.xmcda.v2.AlternativeAffectation alternativeAssignment_v2: affectations.getAlternativeAffectation())
		{
			altAssgnmts.add(new AlternativeAssignmentConverter().convertTo_v3(alternativeAssignment_v2, xmcda_v3));
		}

		xmcda_v3.alternativesAssignmentsList.add(altAssgnmts);
		this.getWarnings().popTag(); // ALTERNATIVES_AFFECTATIONS
	}

	public void convertTo_v2(ArrayList<AlternativesAssignments<?>> alternativesAssignmentsList,
	                         org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.AlternativesAssignments<?> alternativesAssignments: alternativesAssignmentsList)
		{
			org.xmcda.v2.AlternativesAffectations critValues_v2 = convertTo_v2(alternativesAssignments);
			List<JAXBElement<?>> critValuesList_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			critValuesList_v2.add(new JAXBElement<org.xmcda.v2.AlternativesAffectations>(new QName(ALTERNATIVES_AFFECTATIONS), org.xmcda.v2.AlternativesAffectations.class, critValues_v2));
		}
	}

	public <VALUE_TYPE> org.xmcda.v2.AlternativesAffectations convertTo_v2(org.xmcda.AlternativesAssignments<VALUE_TYPE> alternativesAssignments_v3)
	{
		org.xmcda.v2.AlternativesAffectations alternativesAffectations_v2 = new org.xmcda.v2.AlternativesAffectations();

		getWarnings().pushTag(ALTERNATIVES_ASSIGNMENTS, alternativesAssignments_v3.id());

		// default attributes
		alternativesAffectations_v2.setId(alternativesAssignments_v3.id());
		alternativesAffectations_v2.setName(alternativesAssignments_v3.name());
		alternativesAffectations_v2.setMcdaConcept(alternativesAssignments_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = alternativesAssignments_v3.getDescription();
		alternativesAffectations_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		for (org.xmcda.AlternativeAssignment<VALUE_TYPE> alternativeAssignment: alternativesAssignments_v3)
		{
			final org.xmcda.v2.AlternativeAffectation affectation = new AlternativeAssignmentConverter().convertTo_v2(alternativeAssignment);
			if ( affectation != null )
				alternativesAffectations_v2.getAlternativeAffectation().add(affectation);
		}

		getWarnings().popTag(); // ALTERNATIVES_ASSIGNMENTS
		return alternativesAffectations_v2;
	}
}
