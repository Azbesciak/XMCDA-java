package org.xmcda.converters.v2_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

public class CategoriesValuesConverter
    extends Converter
{
	public static final String CATEGORIES_VALUES = org.xmcda.CategoriesValues.TAG;

	public CategoriesValuesConverter()
	{
		super(CATEGORIES_VALUES);
	}

	// v2 -> v3
	/**
	 * @param value
	 * @param xmcda_v3
	 */
	public void convertTo_v3(org.xmcda.v2.CategoriesValues value, org.xmcda.XMCDA xmcda_v3)
	{
		if (value.getCategoryValue().size() == 0)
		{
			// NB not valid wrt xmcda v2 schema
			return;
		}
		getWarnings().pushTag(CATEGORIES_VALUES, value.getId());

		// we suppose all values is of the same type of the first one; for example, there are just categoryIDs
		// This is what was intended in v2, even if not enforced by the schema
		final org.xmcda.v2.CategoryValue categoryValue_v2 = value.getCategoryValue().get(0);

		if (categoryValue_v2.getCategoryID() != null)
		{
			xmcda_v3.categoriesValuesList.add(categoriesValues_v3_categoryID(value, xmcda_v3));
		}
		else if (categoryValue_v2.getCategoriesSetID() != null)
		{
			xmcda_v3.categoriesSetsValuesList.add(categoriesValues_v3_categoriesSetID(value, xmcda_v3)); // unimplemented
		}
		else if (categoryValue_v2.getCategoriesSet() != null)
		{
			xmcda_v3.categoriesSetsValuesList.add(categoriesValues_v3_categoriesSet(value, xmcda_v3));
		}
		else
		{
			// do nothing: anything else won't be valid wrt xmcda v2 schema;
		}
		this.getWarnings().popTag(); // CATEGORIES_VALUES
	}

	/**
	 * Convert v2 categories values containing &lt;categoryID&gt; only
	 *
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	protected <T> org.xmcda.CategoriesValues<T> categoriesValues_v3_categoryID(org.xmcda.v2.CategoriesValues value,
	                                                                        org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.CategoriesValues<T> categoriesValues_v3 = org.xmcda.Factory.categoriesValues();

		// defaultAttributes
		categoriesValues_v3.setId(value.getId());
		categoriesValues_v3.setMcdaConcept(value.getMcdaConcept());
		categoriesValues_v3.setName(value.getName());

		// description
		categoriesValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// categoryValue 1..*
		for (org.xmcda.v2.CategoryValue categoryValue_v2: value.getCategoryValue())
		{
			final String categoryID = categoryValue_v2.getCategoryID();
			if (categoryID == null)
			{
				getWarnings().elementIgnored("categorySetID or categoriesSet",
				                             "element found in a list which is supposed to contain categoryID only");
				continue;
			}

			final org.xmcda.LabelledQValues<T> categoryValues = ValuesConverter
			        .valueOrValues_convertTo_v3(categoryValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			categoriesValues_v3.put(xmcda_v3.categories.get(categoryID), categoryValues);

			categoryValues.setId(categoryValue_v2.getId());
			categoryValues.setName(categoryValue_v2.getName());
			categoryValues.setMcdaConcept(categoryValue_v2.getMcdaConcept());
			categoryValues.setDescription(new DescriptionConverter().convertTo_v3(categoryValue_v2.getDescription()));
		}
		return categoriesValues_v3;
	}


	/**
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected org.xmcda.CategoriesSetsValues categoriesValues_v3_categoriesSetID(org.xmcda.v2.CategoriesValues value,
	                                                                       org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.CategoriesSetsValues categoriesSetsValues_v3 = org.xmcda.Factory.categoriesSetsValues();

		// defaultAttributes
		categoriesSetsValues_v3.setId(value.getId());
		categoriesSetsValues_v3.setMcdaConcept(value.getMcdaConcept());
		categoriesSetsValues_v3.setName(value.getName());

		// description
		categoriesSetsValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// categoryValue 1..*
		for (org.xmcda.v2.CategoryValue categoryValue_v2: value.getCategoryValue())
		{

			final String categoriesSetID = categoryValue_v2.getCategoriesSetID();
			if (categoriesSetID == null)
			{
				getWarnings()
				        .elementIgnored("categoryID or categoriesSet",
				                        "element found in a list which is supposed to contain categorySetID only");
				continue;
			}
			org.xmcda.CategoriesSet categoriesSet_v3 = xmcda_v3.categoriesSets.get(categoriesSetID);

			final org.xmcda.LabelledQValues categoryValues = ValuesConverter
			        .valueOrValues_convertTo_v3(categoryValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			categoriesSetsValues_v3.put(categoriesSet_v3, categoryValues);

			categoryValues.setId(categoryValue_v2.getId());
			categoryValues.setName(categoryValue_v2.getName());
			categoryValues.setMcdaConcept(categoryValue_v2.getMcdaConcept());
			categoryValues.setDescription(new DescriptionConverter().convertTo_v3(categoryValue_v2.getDescription()));
		}
		return categoriesSetsValues_v3;
	}


	/**
	 * @param value
	 * @param xmcda_v3
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected org.xmcda.CategoriesSetsValues categoriesValues_v3_categoriesSet(org.xmcda.v2.CategoriesValues value,
	                                                                     org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.CategoriesSetsValues categoriesSetsValues_v3 = org.xmcda.Factory.categoriesSetsValues();

		// defaultAttributes
		categoriesSetsValues_v3.setId(value.getId());
		categoriesSetsValues_v3.setMcdaConcept(value.getMcdaConcept());
		categoriesSetsValues_v3.setName(value.getName());

		// description
		categoriesSetsValues_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));
		// categoryValue 1..*
		for (org.xmcda.v2.CategoryValue categoryValue_v2: value.getCategoryValue())
		{

			final org.xmcda.v2.CategoriesSet categoriesSet = categoryValue_v2.getCategoriesSet();
			if (categoriesSet == null)
			{
				getWarnings().elementIgnored("categoryID or categoriesSetID",
				                             "element found in a list which is suppoed to contain categoryID only");
				continue;
			}
			org.xmcda.CategoriesSet categoriesSet_v3 = new CategoriesSetConverter().convertTo_v3(categoriesSet, xmcda_v3);
			final org.xmcda.LabelledQValues categoryValues = ValuesConverter
			        .valueOrValues_convertTo_v3(categoryValue_v2.getValueOrValues(), xmcda_v3, this.getWarnings());
			categoriesSetsValues_v3.put(categoriesSet_v3, categoryValues);
			xmcda_v3.categoriesSets.add(categoriesSet_v3);

			categoryValues.setId(categoryValue_v2.getId());
			categoryValues.setName(categoryValue_v2.getName());
			categoryValues.setMcdaConcept(categoryValue_v2.getMcdaConcept());
			if (categoryValue_v2.getDescription() != null)
				// if it is null, do not override a value that may have been already set by valueOrValues_convertTo_v3(), i.e. a description in <values>
				categoryValues.setDescription(new DescriptionConverter().convertTo_v3(categoryValue_v2.getDescription()));
		}
		return categoriesSetsValues_v3;
	}

    // v3 -> v2
    // we will never build XMCDA v2 categoriesValues with categoriesSet within, only categoriesValues with categoriesSetID
	public void convertTo_v2(List<org.xmcda.CategoriesValues<?>> categoriesValues_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CategoriesValues<?> categoriesValue: categoriesValues_v3)
		{
			org.xmcda.v2.CategoriesValues critValues_v2 = convertTo_v2(categoriesValue);
			List<JAXBElement<?>> critValuesList_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			critValuesList_v2.add(new JAXBElement<>(new QName(CATEGORIES_VALUES), org.xmcda.v2.CategoriesValues.class, critValues_v2));
		}
	}

	protected <VALUE_TYPE> org.xmcda.v2.CategoriesValues convertTo_v2(org.xmcda.CategoriesValues<VALUE_TYPE> categoriesValues_v3)
	{
		org.xmcda.v2.CategoriesValues categoriesValues_v2 = new org.xmcda.v2.CategoriesValues();

		getWarnings().pushTag(CATEGORIES_VALUES, categoriesValues_v3.id());

		// default attributes
		categoriesValues_v2.setId(categoriesValues_v3.id());
		categoriesValues_v2.setName(categoriesValues_v3.name());
		categoriesValues_v2.setMcdaConcept(categoriesValues_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = categoriesValues_v3.getDescription();
		categoriesValues_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// categoryValue
		for (Entry<org.xmcda.Category, org.xmcda.LabelledQValues<VALUE_TYPE>> critValue_v3: categoriesValues_v3.entrySet())
		{
			getWarnings().pushTag("categoryValue", critValue_v3.getValue().id());
			org.xmcda.v2.CategoryValue categoryValue_v2 = new org.xmcda.v2.CategoryValue();
			categoryValue_v2.setId(critValue_v3.getValue().id());
			categoryValue_v2.setName(critValue_v3.getValue().name());
			categoryValue_v2.setMcdaConcept(critValue_v3.getValue().mcdaConcept());
			categoryValue_v2.setDescription(new DescriptionConverter().convertTo_v2(critValue_v3.getValue().getDescription()));

			categoryValue_v2.setCategoryID(critValue_v3.getKey().id());

			org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			for (org.xmcda.QualifiedValue<VALUE_TYPE> value_v3: critValue_v3.getValue())
			{
				org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value_v3);
				if ( ! XMCDAConverter.toV2Parameters.get().omitValuesInCategoriesValues() )
					values_v2.getValue().add(value_v2);
				else
				{
					categoryValue_v2.getValueOrValues().add(value_v2);
				}
			}
			if ( ! XMCDAConverter.toV2Parameters.get().omitValuesInCategoriesValues() )
			{
				categoryValue_v2.getValueOrValues().add(values_v2);
			}
			categoriesValues_v2.getCategoryValue().add(categoryValue_v2);
			getWarnings().popTag(); // "categoryValue"
		}
		getWarnings().popTag(); // CATEGORIES_VALUES
		return categoriesValues_v2;
	}

	public void categoriesSetsValues_convertTo_v2(List<org.xmcda.CategoriesSetsValues<?,?>> categoriesSetsValues_v3, org.xmcda.v2.XMCDA xmcda_v2, org.xmcda.XMCDA xmcda_v3)
	{
		for (org.xmcda.CategoriesSetsValues<?,?> categoriesSetsValues: categoriesSetsValues_v3)
		{
			org.xmcda.v2.CategoriesValues critValues_v2 = convertTo_v2(categoriesSetsValues, xmcda_v2, xmcda_v3);
			List<JAXBElement<?>> critValuesList_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			critValuesList_v2.add(new JAXBElement<>(new QName(CATEGORIES_VALUES), org.xmcda.v2.CategoriesValues.class, critValues_v2));
		}
	}

	protected <CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE> org.xmcda.v2.CategoriesValues convertTo_v2(org.xmcda.CategoriesSetsValues<CATEGORIES_SETS_VALUE_TYPE, VALUE_TYPE> categoriesSetsValues_v3, org.xmcda.v2.XMCDA xmcda_v2, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.v2.CategoriesValues categoriesValues_v2 = new org.xmcda.v2.CategoriesValues();

		getWarnings().pushTag(org.xmcda.CategoriesSetsValues.TAG, categoriesSetsValues_v3.id());

		// default attributes
		categoriesValues_v2.setId(categoriesSetsValues_v3.id());
		categoriesValues_v2.setName(categoriesSetsValues_v3.name());
		categoriesValues_v2.setMcdaConcept(categoriesSetsValues_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = categoriesSetsValues_v3.getDescription();
		categoriesValues_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// categoriesSetValue -> categoryValue
		for (Entry<org.xmcda.CategoriesSet<CATEGORIES_SETS_VALUE_TYPE>, org.xmcda.LabelledQValues<VALUE_TYPE>> critValue_v3: categoriesSetsValues_v3.entrySet())
		{
			getWarnings().pushTag("categoriesSetValue", critValue_v3.getValue().id());
			org.xmcda.v2.CategoryValue categoryValue_v2 = new org.xmcda.v2.CategoryValue();
			categoryValue_v2.setId(critValue_v3.getValue().id());
			categoryValue_v2.setName(critValue_v3.getValue().name());
			categoryValue_v2.setMcdaConcept(critValue_v3.getValue().mcdaConcept());
			categoryValue_v2.setDescription(new DescriptionConverter().convertTo_v2(critValue_v3.getValue().getDescription()));

			//categoryValue_v2.setCategoriesSetID(critValue_v3.getKey().id());
			final String categoriesSetID_v3 = critValue_v3.getKey().id();
			final org.xmcda.CategoriesSet<?> categoriesSet_v3 = xmcda_v3.categoriesSets.get(categoriesSetID_v3);
			categoryValue_v2.setCategoriesSet(new CategoriesSetConverter().convertTo_v2(categoriesSet_v3, xmcda_v2));

			getWarnings().pushTag("values");
			org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			for (org.xmcda.QualifiedValue<VALUE_TYPE> value_v3: critValue_v3.getValue())
			{
				org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value_v3);
				values_v2.getValue().add(value_v2);
			}
			getWarnings().popTag(); // "values"

			categoryValue_v2.getValueOrValues().add(values_v2);
			categoriesValues_v2.getCategoryValue().add(categoryValue_v2);
			getWarnings().popTag(); // "categoriesSetValue"
		}
		getWarnings().popTag(); // "categoriesSetsValues" / org.xmcda.CategoriesSetsValues.TAG
		return categoriesValues_v2;
	}
}
