package org.xmcda.converters.v2_v3;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaSetsConverter
extends Converter
{
	public static final String CRITERIA_SETS = org.xmcda.CriteriaSets.TAG;

	public CriteriaSetsConverter()
    {
		super(CRITERIA_SETS);
    }

	public void convertTo_v3(org.xmcda.v2.CriteriaSets value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CRITERIA_SETS, value.getId());
		
		org.xmcda.CriteriaSets criteriaSets_v3 = xmcda_v3.criteriaSets;

		// defaultAttributes
		criteriaSets_v3.setId(value.getId());
		criteriaSets_v3.setMcdaConcept(value.getMcdaConcept());
		criteriaSets_v3.setName(value.getName());

		// description
		criteriaSets_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));
		
		// criteriaSet 0..*
		for (org.xmcda.v2.CriteriaSet criteriaSet: value.getCriteriaSet())
		{
			xmcda_v3.criteriaSets.add(new CriteriaSetConverter().convertTo_v3(criteriaSet, xmcda_v3));
		}

		getWarnings().popTag(); // CRITERIA_SETS
	}

	public <T> void convertTo_v2(final org.xmcda.CriteriaSets<T> criteriaSets_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		if (criteriaSets_v3==null || criteriaSets_v3.isVoid())
			return;

		getWarnings().pushTag(CRITERIA_SETS, criteriaSets_v3.id());
		
		org.xmcda.v2.CriteriaSets criteriaSets_v2 = new org.xmcda.v2.CriteriaSets();
		List<JAXBElement<?>> crits = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		crits.add(new JAXBElement<org.xmcda.v2.CriteriaSets>(new QName(CRITERIA_SETS), org.xmcda.v2.CriteriaSets.class,
				criteriaSets_v2));

		// default attributes
		criteriaSets_v2.setId(criteriaSets_v3.id());
		criteriaSets_v2.setName(criteriaSets_v3.name());
		criteriaSets_v2.setMcdaConcept(criteriaSets_v3.mcdaConcept());

		// description
		criteriaSets_v2.setDescription(new DescriptionConverter().convertTo_v2(criteriaSets_v3.getDescription()));

		// criteria
		CriteriaSetConverter criteriaSetConverter = new CriteriaSetConverter();

		for (org.xmcda.CriteriaSet<T> criteriaSet_v3: criteriaSets_v3)
		{
			criteriaSets_v2.getCriteriaSet().add(criteriaSetConverter.convertTo_v2(criteriaSet_v3, xmcda_v2));
		}
		getWarnings().popTag(); // CRITERIA
	}

	
}
