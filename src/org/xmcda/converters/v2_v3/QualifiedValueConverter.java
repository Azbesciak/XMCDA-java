package org.xmcda.converters.v2_v3;

import java.math.BigInteger;

import org.xmcda.Factory;

// @SuppressWarnings("rawtypes")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class QualifiedValueConverter
    extends Converter
{
	public static final String VALUE = "value";

	public QualifiedValueConverter()
	{
		super(VALUE);
	}

	/**
	 * Converts a XMCDA v2 value into XMCDA v3.
	 * 
	 * @param value_v2 the value to be converted
	 * @param xmcda_v3 the converted value, or {@code null} if the value could not be converted (because it does not
	 *            exist in XMCDA v3).
	 * @return
	 */
	public <T> org.xmcda.QualifiedValue<T> convertTo_v3(org.xmcda.v2.Value value_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(VALUE, value_v2.getId());
		org.xmcda.QualifiedValue qvalue = Factory.<T>qualifiedValue();

		qvalue.setId(value_v2.getId());
		qvalue.setName(value_v2.getName());
		qvalue.setMcdaConcept(value_v2.getMcdaConcept());
		qvalue.setDescription(new DescriptionConverter().convertTo_v3(value_v2.getDescription()));

		// image, imageRef: not in XMCDA v3

		if (value_v2.getInteger() != null)
		{
			qvalue.setValue(value_v2.getInteger());
		}
		else if (value_v2.getReal() != null)
		{
			qvalue.setValue(value_v2.getReal());
		}
		else if (value_v2.getInterval() != null)
		{
			org.xmcda.v2.Interval iv2 = value_v2.getInterval();
			org.xmcda.value.Interval interval = new org.xmcda.value.Interval();
			getWarnings().elementIgnored("interval:description");
			if (iv2.getLowerBound() != null)
				interval.setLowerBound(this.convertTo_v3(iv2.getLowerBound(), xmcda_v3));
			if (iv2.getUpperBound() != null)
				interval.setUpperBound(this.convertTo_v3(iv2.getUpperBound(), xmcda_v3));
			interval.setIsLeftClosed(true);
			interval.setIsRightClosed(true);
			qvalue.setValue(interval);
		}

		else if (value_v2.getRational() != null)
		{
			org.xmcda.value.Rational rv3 = new org.xmcda.value.Rational();
			rv3.setNumerator(value_v2.getRational().getNumerator());
			rv3.setDenominator(value_v2.getRational().getDenominator());
			qvalue.setValue(rv3);
		}

		else if (value_v2.getLabel() != null)
		{
			qvalue.setValue(value_v2.getLabel());
		}

		else if (value_v2.getRankedLabel() != null)
		{
			org.xmcda.value.ValuedLabel vlabel_v3 = new org.xmcda.value.ValuedLabel();
			// description: ignored
			getWarnings().elementIgnored("rankedLabel:description");
			vlabel_v3.setLabel(value_v2.getRankedLabel().getLabel());
			org.xmcda.QualifiedValue v = Factory.qualifiedValue();
			v.setValue(new Integer(value_v2.getRankedLabel().getRank().intValue()));
			vlabel_v3.setValue(v);
			qvalue.setValue(vlabel_v3);
		}

		else if (value_v2.isBoolean() != null)
		{
			qvalue.setValue(value_v2.isBoolean());
		}

		else if (value_v2.getNA() != null)
		{
			qvalue.setValue(org.xmcda.value.NA.na);
		}

		else if (value_v2.getImage() != null)
		{
			getWarnings().elementIgnored("image");
			qvalue = null;
		}

		else if (value_v2.getImageRef() != null)
		{
			getWarnings().elementIgnored("imageRef");
			qvalue = null;
		}

		else if (value_v2.getFuzzyLabel() != null)
		{
			final org.xmcda.v2.FuzzyLabel fuzzyLabel_v2 = value_v2.getFuzzyLabel();
			final org.xmcda.value.ValuedLabel vlabel_v3 = new org.xmcda.value.ValuedLabel();
			// description: ignored
			getWarnings().elementIgnored("fuzzyLabel:description", Warnings.ABSENT_IN_V3_0);
			vlabel_v3.setLabel(value_v2.getFuzzyLabel().getLabel());
			org.xmcda.QualifiedValue v = Factory.qualifiedValue();

			v.setValue(convertFuzzyNumberTo_v3(fuzzyLabel_v2.getFuzzyNumber(), xmcda_v3));
			vlabel_v3.setValue(v);
			qvalue.setValue(vlabel_v3);
		}

		else if (value_v2.getFuzzyNumber() != null)
		{
			final org.xmcda.v2.FuzzyNumber fuzzyNumber_v2 = value_v2.getFuzzyNumber();
			final org.xmcda.value.FuzzyNumber fuzzyNumber_v3 = convertFuzzyNumberTo_v3(fuzzyNumber_v2, xmcda_v3);
			qvalue.setValue(fuzzyNumber_v3);
		}
		getWarnings().popTag(); // VALUE
		return qvalue;
	}

	public org.xmcda.value.Segment convertSegmentTo_v3(org.xmcda.v2.Point p1, org.xmcda.v2.Point p2,
	                                                   org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.value.Segment segment_v3 = new org.xmcda.value.Segment();
		org.xmcda.value.Point p1_v3 = new org.xmcda.value.Point();
		org.xmcda.value.Point p2_v3 = new org.xmcda.value.Point();
		p1_v3.setAbscissa(this.convertTo_v3(p1.getAbscissa(), xmcda_v3));
		p1_v3.setOrdinate(this.convertTo_v3(p1.getOrdinate(), xmcda_v3));
		p2_v3.setAbscissa(this.convertTo_v3(p2.getAbscissa(), xmcda_v3));
		p2_v3.setOrdinate(this.convertTo_v3(p2.getOrdinate(), xmcda_v3));
		segment_v3.setHead(new org.xmcda.value.EndPoint(p1_v3));
		segment_v3.setTail(new org.xmcda.value.EndPoint(p2_v3));
		return segment_v3;
	}

	public org.xmcda.value.FuzzyNumber convertFuzzyNumberTo_v3(org.xmcda.v2.FuzzyNumber fuzzyNumber_v2,
	                                                           org.xmcda.XMCDA xmcda_v3)
	{
		final org.xmcda.value.FuzzyNumber fuzzyNumber_v3 = new org.xmcda.value.FuzzyNumber();

		// description: IGNORED
		final org.xmcda.v2.Description desc_v2 = fuzzyNumber_v2.getDescription();
		if (desc_v2 != null)
		    getWarnings().elementIgnored("fuzzyNumber/description", Warnings.ABSENT_IN_V3_0);

		final org.xmcda.value.PiecewiseLinearFunction<?, ?> function_v3 = new org.xmcda.value.PiecewiseLinearFunction();
		if (fuzzyNumber_v2.getTrapezoidal() != null)
		{
			org.xmcda.v2.FuzzyNumber.Trapezoidal tv2 = fuzzyNumber_v2.getTrapezoidal();
			function_v3.add(this.convertSegmentTo_v3(tv2.getPoint1(), tv2.getPoint2(), xmcda_v3));
			function_v3.add(this.convertSegmentTo_v3(tv2.getPoint2(), tv2.getPoint3(), xmcda_v3));
			function_v3.add(this.convertSegmentTo_v3(tv2.getPoint3(), tv2.getPoint4(), xmcda_v3));
			fuzzyNumber_v3.setFunction(function_v3);
		}
		else if (fuzzyNumber_v2.getTriangular() != null)
		{
			org.xmcda.v2.FuzzyNumber.Triangular tv2 = fuzzyNumber_v2.getTriangular();
			function_v3.add(this.convertSegmentTo_v3(tv2.getPoint1(), tv2.getPoint2(), xmcda_v3));
			function_v3.add(this.convertSegmentTo_v3(tv2.getPoint2(), tv2.getPoint3(), xmcda_v3));
			fuzzyNumber_v3.setFunction(function_v3);
		}
		return fuzzyNumber_v3;
	}

	/* to v2 */
	public org.xmcda.v2.Value convertTo_v2(org.xmcda.QualifiedValue<?> value_v3)
	{
		getWarnings().pushTag(VALUE, value_v3.id());

		org.xmcda.v2.Value value_v2 = new org.xmcda.v2.Value();

		value_v2.setId(value_v3.id());
		value_v2.setName(value_v3.name());
		value_v2.setMcdaConcept(value_v3.mcdaConcept());
		value_v2.setDescription(new DescriptionConverter().convertTo_v2(value_v3.getDescription()));

		final Object value = value_v3.getValue();

		if (value instanceof Integer)
			value_v2.setInteger((Integer) value);
		else if (value instanceof Double)
			value_v2.setReal((Double) value);
		else if (value instanceof Float)
			value_v2.setReal(((Float) value).doubleValue());
		else if (value instanceof org.xmcda.value.Interval)
		{
			final org.xmcda.value.Interval<?> iv3 = (org.xmcda.value.Interval<?>) value;
			org.xmcda.v2.Interval iv2 = new org.xmcda.v2.Interval();
			if (iv3.getLowerBound() != null)
				iv2.setLowerBound(convertTo_v2(iv3.getLowerBound()));
			if (iv3.getUpperBound() != null)
				iv2.setUpperBound(convertTo_v2(iv3.getUpperBound()));
			if (!iv3.isLeftClosed())
				getWarnings().elementIgnored("interval/leftclosed", Warnings.ABSENT_IN_V2_0);
			if (!iv3.isRightClosed())
				getWarnings().elementIgnored("interval/rightClosed", Warnings.ABSENT_IN_V2_0);
			value_v2.setInterval(iv2);
		}
		else if (value instanceof org.xmcda.value.Rational)
		{
			final org.xmcda.value.Rational rv3 = (org.xmcda.value.Rational) value;
			final org.xmcda.v2.Rational rv2 = new org.xmcda.v2.Rational();
			rv2.setNumerator(rv3.getNumerator());
			rv2.setDenominator(rv3.getDenominator());
			value_v2.setRational(rv2);
		}
		else if (value instanceof String)
		{
			value_v2.setLabel((String) value);
		}
		else if (value instanceof Boolean)
		{
			value_v2.setBoolean((Boolean) value);
		}
		else if (value instanceof org.xmcda.value.NA)
		{
			value_v2.setNA("");
		}
		else if (value instanceof org.xmcda.value.FuzzyNumber)
		{
			value_v2.setFuzzyNumber(convertFuzzyNumberTo_v2((org.xmcda.value.FuzzyNumber)value));
		}
		else if (value instanceof org.xmcda.value.ValuedLabel)
		{
			final org.xmcda.value.ValuedLabel vl_v3 = (org.xmcda.value.ValuedLabel) value;
			if (vl_v3.getValue().getValue() instanceof Integer)
			{
				// TODO if description, etc warning
				// ranked label
				org.xmcda.v2.RankedLabel rl_v2 = new org.xmcda.v2.RankedLabel();
				rl_v2.setLabel(vl_v3.getLabel());
				rl_v2.setRank(new BigInteger( vl_v3.getValue().getValue().toString() ));
				value_v2.setRankedLabel(rl_v2);
			}
			else if (vl_v3.getValue().getValue() instanceof org.xmcda.value.FuzzyNumber)
			{
				// fuzzyLabel
				org.xmcda.v2.FuzzyLabel fl_v2 = new org.xmcda.v2.FuzzyLabel();
				fl_v2.setLabel(vl_v3.getLabel());
				fl_v2.setFuzzyNumber(convertFuzzyNumberTo_v2((org.xmcda.value.FuzzyNumber)vl_v3.getValue().getValue()));
				value_v2.setFuzzyLabel(fl_v2);
			}
			else
			{
			    getWarnings().elementIgnored("valuedLabel w/ type:"+vl_v3.getValue().getValue().getClass(), Warnings.ABSENT_IN_V2_0);
			}
		}
		getWarnings().popTag(); // VALUE
		return value_v2;
	}

	public org.xmcda.v2.FuzzyNumber convertFuzzyNumberTo_v2(org.xmcda.value.FuzzyNumber fuzzyNumber_v3)
	{
		org.xmcda.v2.FuzzyNumber fuzzy_v2 = new org.xmcda.v2.FuzzyNumber();

		// no description
		if ( ! ( fuzzyNumber_v3.getFunction() instanceof org.xmcda.value.PiecewiseLinearFunction ) )
		{
			getWarnings().elementIgnored("fuzzyNumber: not piecewise", Warnings.ABSENT_IN_V2_0);
			return fuzzy_v2; // TODO check
		}
		final org.xmcda.value.PiecewiseLinearFunction<?,?> pl_v3 = (org.xmcda.value.PiecewiseLinearFunction<?,?>) fuzzyNumber_v3.getFunction();
		if (pl_v3.size() == 3)
		{
			// trapezoidal
			// TODO check tail(-1)=head(0)
			org.xmcda.v2.FuzzyNumber.Trapezoidal trapezoidal = new org.xmcda.v2.FuzzyNumber.Trapezoidal();
			trapezoidal.setPoint1(convertPointTo_v2(pl_v3.get(0).getHead()));
			trapezoidal.setPoint2(convertPointTo_v2(pl_v3.get(0).getTail()));
			trapezoidal.setPoint3(convertPointTo_v2(pl_v3.get(1).getTail()));
			trapezoidal.setPoint4(convertPointTo_v2(pl_v3.get(2).getTail()));
			fuzzy_v2.setTrapezoidal(trapezoidal);
		}
		else if (pl_v3.size() == 2)
		{
			// triangular
			// TODO check tail(-1)=head(0)
			org.xmcda.v2.FuzzyNumber.Triangular triangle = new org.xmcda.v2.FuzzyNumber.Triangular();
			triangle.setPoint1(convertPointTo_v2(pl_v3.get(0).getHead()));
			triangle.setPoint2(convertPointTo_v2(pl_v3.get(0).getTail()));
			triangle.setPoint3(convertPointTo_v2(pl_v3.get(1).getTail()));
			fuzzy_v2.setTriangular(triangle);
		}
		else if (pl_v3.size() < 2)
		{
			getWarnings().elementIgnored("fuzzyNumber: less than 2 segments", Warnings.ABSENT_IN_V2_0);
			return fuzzy_v2; // TODO check
		}
		else if (pl_v3.size() > 3)
		{
			getWarnings().elementIgnored("fuzzyNumber: more than 3 segments", Warnings.ABSENT_IN_V2_0);
			return fuzzy_v2; // TODO check
		}
		return fuzzy_v2;
	}

	protected org.xmcda.v2.Point convertPointTo_v2(org.xmcda.value.EndPoint point_v3)
	{
		org.xmcda.v2.Point point_v2 = new org.xmcda.v2.Point();
		point_v2.setAbscissa(convertTo_v2(point_v3.getAbscissa()));
		point_v2.setOrdinate(convertTo_v2(point_v3.getOrdinate()));
		return point_v2;
	}

}
