/**
 * 
 */
package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class McdaObjects_ComparisonsConverter
extends Converter
{
	public static final String MCDA_OBJECTS__COMPARISONS = "mcdaObjects_Comparisons";
	public static final String PAIRS                    = "pairs";
	public static final String PAIR                     = "pair";

	public McdaObjects_ComparisonsConverter()
	{
		super(MCDA_OBJECTS__COMPARISONS); // ajouter un constructeur super(tag_v2, tag_v3) ?
	}
	
	// v2 -> v3
	
	public void convertTo_v3(org.xmcda.v2.McdaObjects_Comparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		// determine whether we convert mcdaObjects_ or mcdaObjects_Sets/mcdaObjects_SetIDs
		final org.xmcda.v2.McdaObjects_Comparisons.Pairs pairs = value.getPairs();
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			convertMcdaObjects_ComparisonsTo_v3(value, xmcda_v3);
			return;
		}
		org.xmcda.v2.McdaObjects_Comparisons.Pairs.Pair pair = pairs.getPair().get(0);
		String initial_id = pair.getInitial().getMcdaObject_ID();
		if ( initial_id != null )
		{
			// either only mcdaObjects_Sets ...
			convertMcdaObjects_ComparisonsTo_v3(value, xmcda_v3);
		}
		else
		{
			// ... or mcdaObjects_Sets / mcdaObjects_SetIDs
			convertMcdaObjects_SetsComparisonsTo_v3(value, xmcda_v3);
		}
	}

	// Implementation note:
	// convertMcdaObjects_ComparisonsTo_v3 & convertMcdaObjects_SetsComparisonsTo_v3 are very similar
	// Changes in one probably means changing the other one 
	public void convertMcdaObjects_SetsComparisonsTo_v3(org.xmcda.v2.McdaObjects_Comparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(MCDA_OBJECTS__COMPARISONS, value.getId());
		
		org.xmcda.McdaObjects_SetsMatrix<?,?> mcdaObjects_SetsMatrix_v3 = Factory.mcdaObjects_SetsMatrix();
		// defaultAttributes
		mcdaObjects_SetsMatrix_v3.setId(value.getId());
		mcdaObjects_SetsMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		mcdaObjects_SetsMatrix_v3.setName(value.getName());
		mcdaObjects_SetsMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			mcdaObjects_SetsMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));

		// pair 0..*
		if (value.getPairs().getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

		// 
		final org.xmcda.v2.McdaObjects_Comparisons.Pairs pairs = value.getPairs();

		// TODO v2 accepts empty Matrix
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			getWarnings().elementUnimplemented("empty v2"); // TODO c'est pas unimplemented qu'il faut
			return;
		}

		getWarnings().pushTag(PAIRS, value.getId());

		/* comparisonType is present in examples of weightsFromCondorcetAndPreferences only
		 * and it is not used.  It is not present in v3.  The idea is that, if something like that
		 * is necessary, it should be passed as a methodParameter.
		 */
		final String comparisonType = value.getComparisonType();
		if ( comparisonType!=null && !"".equals(comparisonType) )
			getWarnings().elementIgnored("comparisonType");

		// pair[0..*]
		for (org.xmcda.v2.McdaObjects_Comparisons.Pairs.Pair pair: pairs.getPair())
		{
			/* one of them */
			org.xmcda.McdaObjects_Set<?> initial = null;
			
			String initial_id = pair.getInitial().getMcdaObject_ID();
			String initial_set_id = pair.getInitial().getMcdaObjects_SetID();
			org.xmcda.v2.McdaObjects_Set initial_set = pair.getInitial().getMcdaObjects_Set();
			
			if ( initial_id != null )
			{
				throw new IllegalArgumentException("mcdaObject_ID unexpected");
			}
			else if ( initial_set_id != null )
			{
				initial = xmcda_v3.mcdaObjects_Sets.get(initial_set_id, true);
			}
			else if ( initial_set != null )
			{
				// the converter already assigns a generated id if there was none
				initial = new McdaObjects_SetConverter().convertTo_v3(initial_set, xmcda_v3);
				xmcda_v3.mcdaObjects_Sets.add((org.xmcda.McdaObjects_Set)initial);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no mcdaObjects_SetID nor mcdaObjects_Set in initial");
			/* */
			org.xmcda.McdaObjects_Set<?> terminal = null;

			String terminal_id = pair.getTerminal().getMcdaObject_ID();
			String terminal_set_id = pair.getTerminal().getMcdaObjects_SetID();
			org.xmcda.v2.McdaObjects_Set terminal_set = pair.getTerminal().getMcdaObjects_Set();
			
			if ( terminal_id != null )
			{
				throw new IllegalArgumentException("mcdaObject_ID unexpected");
			}
			else if ( terminal_set_id != null )
			{
				terminal = xmcda_v3.mcdaObjects_Sets.get(terminal_set_id, true);
			}
			else if ( terminal_set != null )
			{
				// the converter already assigns a generated id if there was none
				terminal = new McdaObjects_SetConverter().convertTo_v3(terminal_set, xmcda_v3);
				xmcda_v3.mcdaObjects_Sets.add((org.xmcda.McdaObjects_Set)terminal);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no mcdaObjects_SetID nor mcdaObjects_Set in initial");

			// mcdaObjects_SetsMatrix en v3 ne contient que des mcdaObject_ID row/column
			List<Object> pairValue_v2 = pair.getValueOrValues();
			org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
			if ( pairValue_v2==null || pairValue_v2.size()==0 )
			{
				// no value: assign N/A
				QualifiedValue tmpValue = Factory.qualifiedValue();
				tmpValue.setValue(org.xmcda.value.NA.na);
				values_v3.add(tmpValue);
			}
			else
			{
				// iterate on valueOrValues, convert them and and add them to values_v3
				for (Object valueOrValues: pairValue_v2)
				{
					if (valueOrValues instanceof org.xmcda.v2.Value)
					{
						org.xmcda.v2.Value tmpValue_v2 = (org.xmcda.v2.Value) valueOrValues;
						values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
					}
					else
					{
						org.xmcda.v2.Values tmpValues_v2 = (org.xmcda.v2.Values) valueOrValues;
						// TODO ignore description mcdaConcept etc.
						for (org.xmcda.v2.Value tmpValue_v2 : tmpValues_v2.getValue())
						{
							values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
						}
					}
				}
			}
			mcdaObjects_SetsMatrix_v3.put(new org.xmcda.utils.Coord(initial, terminal), values_v3);
		}
		getWarnings().popTag(); // PAIRS

		getWarnings().popTag(); // MCDA_OBJECTS__COMPARISONS
		xmcda_v3.mcdaObjects_SetsMatricesList.add(mcdaObjects_SetsMatrix_v3);
	}

	// Implementation note:
	// convertMcdaObjects_ComparisonsTo_v3 & convertMcdaObjects_SetsComparisonsTo_v3 are very similar
	// Changes in one probably means changing the other one 
	public void convertMcdaObjects_ComparisonsTo_v3(org.xmcda.v2.McdaObjects_Comparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(MCDA_OBJECTS__COMPARISONS, value.getId());

		org.xmcda.McdaObjects_Matrix<?> mcdaObjects_Matrix_v3 = Factory.mcdaObjects_Matrix();
		// defaultAttributes
		mcdaObjects_Matrix_v3.setId(value.getId());
		mcdaObjects_Matrix_v3.setMcdaConcept(value.getMcdaConcept());
		mcdaObjects_Matrix_v3.setName(value.getName());
		mcdaObjects_Matrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			mcdaObjects_Matrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));
		
		// pair 0..*
		if (value.getPairs().getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

		// 
		final org.xmcda.v2.McdaObjects_Comparisons.Pairs pairs = value.getPairs();

		// TODO v2 accepts empty Matrix
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			getWarnings().elementUnimplemented("empty v2"); // TODO c'est pas unimplemented qu'il faut
			return;
		}

		getWarnings().pushTag(PAIRS, value.getId());

		/* comparisonType is present in examples of weightsFromCondorcetAndPreferences only
		 * and it is not used.  It is not present in v3.  The idea is that, if something like that
		 * is necessary, it should be passed as a programParameter.
		 */
		final String comparisonType = value.getComparisonType();
		if ( comparisonType!=null && !"".equals(comparisonType) )
			getWarnings().elementIgnored("comparisonType");

		// pair[0..*]
		for (org.xmcda.v2.McdaObjects_Comparisons.Pairs.Pair pair: pairs.getPair())
		{
			/* one of them */
			Object initial = null;
			
			String initial_id = pair.getInitial().getMcdaObject_ID();
			String initial_set_id = pair.getInitial().getMcdaObjects_SetID();
			org.xmcda.v2.McdaObjects_Set initial_set = pair.getInitial().getMcdaObjects_Set();
			
			if ( initial_id != null )
			{
				initial = xmcda_v3.mcdaObjects_.get(initial_id, true);
			}
			else if ( initial_set_id != null )
			{
				throw new IllegalArgumentException("mcdaObjects_SetID unexpected");
				//initial = xmcda_v3.mcdaObjects_Sets.get(initial_set_id, true);
			}
			else if ( initial_set != null )
			{
				throw new IllegalArgumentException("mcdaObjects_Set unexpected");
				//initial = new McdaObjects_SetConverter().convertTo_v3(initial_set, xmcda_v3);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no mcdaObjects_ID in initial");
			/* */
			Object terminal = null;

			String terminal_id = pair.getTerminal().getMcdaObject_ID();
			String terminal_set_id = pair.getTerminal().getMcdaObjects_SetID();
			org.xmcda.v2.McdaObjects_Set terminal_set = pair.getTerminal().getMcdaObjects_Set();
			
			if ( terminal_id != null )
			{
				terminal = xmcda_v3.mcdaObjects_.get(terminal_id, true);
			}
			else if ( terminal_set_id != null )
			{
				throw new IllegalArgumentException("mcdaObjects_SetID unexpected");
				//terminal = xmcda_v3.mcdaObjects_Sets.get(terminal_set_id, true);
			}
			else if ( terminal_set != null )
			{
				throw new IllegalArgumentException("mcdaObjects_Set unexpected");
				//terminal = new McdaObjects_SetConverter().convertTo_v3(terminal_set, xmcda_v3);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no mcdaObjects_ID in terminal");

			// mcdaObjects_Matrix en v3 ne contient que des mcdaObject_ID row/column
			List<Object> pairValue_v2 = pair.getValueOrValues();
			org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
			if ( pairValue_v2==null || pairValue_v2.size()==0 )
			{
				// no value: assign N/A
				QualifiedValue tmpValue = Factory.qualifiedValue();
				tmpValue.setValue(org.xmcda.value.NA.na);
				values_v3.add(tmpValue);
			}
			else
			{
				// iterate on valueOrValues, convert them and and add them to values_v3
				for (Object valueOrValues: pairValue_v2)
				{
					if (valueOrValues instanceof org.xmcda.v2.Value)
					{
						org.xmcda.v2.Value tmpValue_v2 = (org.xmcda.v2.Value) valueOrValues;
						values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
					}
					else
					{
						org.xmcda.v2.Values tmpValues_v2 = (org.xmcda.v2.Values) valueOrValues;
						// TODO ignore description mcdaConcept etc.
						for (org.xmcda.v2.Value tmpValue_v2 : tmpValues_v2.getValue())
						{
							values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
						}
					}
				}
			}
			mcdaObjects_Matrix_v3.put(new org.xmcda.utils.Coord(initial, terminal), values_v3);
		}
		getWarnings().popTag(); // PAIRS

		getWarnings().popTag(); // MCDA_OBJECTS__COMPARISONS
		xmcda_v3.mcdaObjects_MatricesList.add(mcdaObjects_Matrix_v3);
	}

	// v3 -> v2

	public void convertMcdaObjects_MatricesTo_v2(List<org.xmcda.McdaObjects_Matrix<?>> mcdaObjects_Matrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.McdaObjects_Matrix mcdaObjects_Matrix_v3: mcdaObjects_Matrices_v3)
		{
			convertMcdaObjects_MatrixTo_v2(mcdaObjects_Matrix_v3, xmcda_v2);
		}
	}

	/**
	 * Converts a mcdaObjects_ matrix (v3) into a mcdaObjects_ comparison (v2)
	 * @param mcdaObjects_Matrix_v3
	 * @param xmcda_v2
	 */
	public void convertMcdaObjects_MatrixTo_v2(org.xmcda.McdaObjects_Matrix mcdaObjects_Matrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(org.xmcda.McdaObjects_Matrix.TAG);
		org.xmcda.v2.McdaObjects_Comparisons mcdaObjects_Comparisons_v2 = new org.xmcda.v2.McdaObjects_Comparisons();
		
		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.McdaObjects_Comparisons>(new QName(MCDA_OBJECTS__COMPARISONS),
		                                                              org.xmcda.v2.McdaObjects_Comparisons.class, mcdaObjects_Comparisons_v2));

		// default attributes
		mcdaObjects_Comparisons_v2.setId(mcdaObjects_Matrix_v3.id());
		mcdaObjects_Comparisons_v2.setName(mcdaObjects_Matrix_v3.name());
		mcdaObjects_Comparisons_v2.setMcdaConcept(mcdaObjects_Matrix_v3.mcdaConcept());

		// description
		mcdaObjects_Comparisons_v2.setDescription(new DescriptionConverter().convertTo_v2(mcdaObjects_Matrix_v3.getDescription()));

		// valuation
		mcdaObjects_Comparisons_v2.setValuation(new ScaleConverter().convertTo_v2(mcdaObjects_Matrix_v3.getValuation()));
		
		// rows
		org.xmcda.v2.McdaObjects_Comparisons.Pairs pairs_v2 = new org.xmcda.v2.McdaObjects_Comparisons.Pairs();
		mcdaObjects_Comparisons_v2.setPairs(pairs_v2);
		
		for (Object _coord: mcdaObjects_Matrix_v3.keySet())
		{
			org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			org.xmcda.v2.McdaObjects_Comparisons.Pairs.Pair pair_v2 = new org.xmcda.v2.McdaObjects_Comparisons.Pairs.Pair();
			org.xmcda.v2.McdaObject_Reference mcdaObject_Ref_initial = new org.xmcda.v2.McdaObject_Reference();
			mcdaObject_Ref_initial.setMcdaObject_ID( ((org.xmcda.McdaObject_) coord.x).id()) ;
			
			org.xmcda.v2.McdaObject_Reference mcdaObject_Ref_terminal = new org.xmcda.v2.McdaObject_Reference();
			mcdaObject_Ref_terminal.setMcdaObject_ID( ((org.xmcda.McdaObject_) coord.y).id()) ;

			pair_v2.setInitial(mcdaObject_Ref_initial);
			pair_v2.setTerminal(mcdaObject_Ref_terminal);

			// values
			final org.xmcda.QualifiedValues<?> values_v3 = (org.xmcda.QualifiedValues<?>) mcdaObjects_Matrix_v3.get(_coord);			
			final Object _v = ValuesConverter.convertTo_v2(values_v3); // may be null

			if (_v instanceof org.xmcda.v2.Value)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Value) _v);
			else if (_v instanceof org.xmcda.v2.Values)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Values) _v);

			pairs_v2.getPair().add(pair_v2);
		}
		getWarnings().popTag(); // org.xmcda.McdaObjects_Matrix.TAG
	}
	
	public void convertMcdaObjects_SetsMatricesTo_v2(List<org.xmcda.McdaObjects_SetsMatrix<?,?>> mcdaObjects_SetsMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.McdaObjects_SetsMatrix mcdaObjects_SetsMatrix_v3: mcdaObjects_SetsMatrices_v3)
		{
			convertMcdaObjects_SetsMatrixTo_v2(mcdaObjects_SetsMatrix_v3, xmcda_v2);
		}
	}

	/**
	 * Converts a mcdaObjects_Sets matrix (v3) into a mcdaObjects_ comparison (v2)
	 * @param mcdaObjects_Matrix_v3
	 * @param xmcda_v2
	 */
	public void convertMcdaObjects_SetsMatrixTo_v2(org.xmcda.McdaObjects_SetsMatrix mcdaObjects_SetsMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(org.xmcda.McdaObjects_Matrix.TAG);
		org.xmcda.v2.McdaObjects_Comparisons mcdaObjects_Comparisons_v2 = new org.xmcda.v2.McdaObjects_Comparisons();
		
		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.McdaObjects_Comparisons>(new QName(MCDA_OBJECTS__COMPARISONS),
		                                                              org.xmcda.v2.McdaObjects_Comparisons.class, mcdaObjects_Comparisons_v2));

		// default attributes
		mcdaObjects_Comparisons_v2.setId(mcdaObjects_SetsMatrix_v3.id());
		mcdaObjects_Comparisons_v2.setName(mcdaObjects_SetsMatrix_v3.name());
		mcdaObjects_Comparisons_v2.setMcdaConcept(mcdaObjects_SetsMatrix_v3.mcdaConcept());

		// description
		mcdaObjects_Comparisons_v2.setDescription(new DescriptionConverter().convertTo_v2(mcdaObjects_SetsMatrix_v3.getDescription()));

		// valuation
		mcdaObjects_Comparisons_v2.setValuation(new ScaleConverter().convertTo_v2(mcdaObjects_SetsMatrix_v3.getValuation()));
		
		// rows
		org.xmcda.v2.McdaObjects_Comparisons.Pairs pairs_v2 = new org.xmcda.v2.McdaObjects_Comparisons.Pairs();
		mcdaObjects_Comparisons_v2.setPairs(pairs_v2);
		
		for (Object _coord: mcdaObjects_SetsMatrix_v3.keySet())
		{
			org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			org.xmcda.v2.McdaObjects_Comparisons.Pairs.Pair pair_v2 = new org.xmcda.v2.McdaObjects_Comparisons.Pairs.Pair();
			org.xmcda.v2.McdaObject_Reference mcdaObject_Ref_initial = new org.xmcda.v2.McdaObject_Reference();
			mcdaObject_Ref_initial.setMcdaObjects_SetID( ((org.xmcda.McdaObjects_Set) coord.x).id() ) ;
			
			org.xmcda.v2.McdaObject_Reference mcdaObject_Ref_terminal = new org.xmcda.v2.McdaObject_Reference();
			mcdaObject_Ref_terminal.setMcdaObjects_SetID( ((org.xmcda.McdaObjects_Set) coord.y).id() ) ;

			pair_v2.setInitial(mcdaObject_Ref_initial);
			pair_v2.setTerminal(mcdaObject_Ref_terminal);

			// values
			final org.xmcda.QualifiedValues<?> values_v3 = (org.xmcda.QualifiedValues<?>) mcdaObjects_SetsMatrix_v3.get(_coord);			
			final Object _v = ValuesConverter.convertTo_v2(values_v3); // may be null

			if (_v instanceof org.xmcda.v2.Value)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Value) _v);
			else if (_v instanceof org.xmcda.v2.Values)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Values) _v);

			pairs_v2.getPair().add(pair_v2);
		}
		getWarnings().popTag(); // org.xmcda.McdaObjects_Matrix.TAG
	}
}


