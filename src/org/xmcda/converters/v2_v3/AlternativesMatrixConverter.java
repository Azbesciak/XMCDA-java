package org.xmcda.converters.v2_v3;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;


/**
 * A simplified transformation of alternativesMatrices: the two only example are outputs
 * which only have alternativeID in rows and columns.  These 2 programs are: 
 * <ul>
 * <li> (PUT) RORUTA-PairwiseOutrankingIndices,
 * <li> (PUT) RORUTA-PairwiseOutrankingIndicesHierarchical.
 * </ul>
 * @author Sébastien Bigaret
 */
public class AlternativesMatrixConverter
    extends Converter
{
	public static final String ALTERNATIVES_COMPARISONS = "alternativesComparisons";
	public static final String ALTERNATIVES_MATRIX      = org.xmcda.AlternativesMatrix.TAG;
	public static final String PAIRS                    = "pairs";
	public static final String PAIR                     = "pair";
	
	public AlternativesMatrixConverter()
	{
		super(ALTERNATIVES_MATRIX); // ajouter un constructeur super(tag_v2, tag_v3) ?
	}

	// v2 -> v3

	public void convertTo_v3(org.xmcda.v2.AlternativesMatrix value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(ALTERNATIVES_MATRIX, value.getId());

		org.xmcda.AlternativesMatrix alternativesMatrix_v3 = Factory.alternativesMatrix();
		// defaultAttributes
		alternativesMatrix_v3.setId(value.getId());
		alternativesMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		alternativesMatrix_v3.setName(value.getName());
		alternativesMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			alternativesMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));

		// from here it diverges from alternativesComparisons
		// même pb pour alternativesMatrix qui mélange les 3 alternativeID, alternativesSetId et alternativesSet
		
		// row
		for (org.xmcda.v2.AlternativesMatrix.Row row_v2: value.getRow())
		{
			// TODO warnings id, name, mcdaConcept, description ignored
			final String row_alternativeID = row_v2.getAlternativeID();
			final String row_alternativeSetID = row_v2.getAlternativesSetID();
			final org.xmcda.v2.AlternativesSet row_alternativesSet_v2 = row_v2.getAlternativesSet();

			if (row_alternativeSetID != null || row_alternativesSet_v2 != null)
			{
				getWarnings().throwUnimplemented();
			}
			final org.xmcda.Alternative row_alternative = xmcda_v3.alternatives.get(row_alternativeID, true);
			// TODO ignore warnings for inner description, id etc...
			for (org.xmcda.v2.AlternativesMatrix.Row.Column column_v2: row_v2.getColumn())
			{
				final String column_alternativeID = column_v2.getAlternativeID();
				final String column_alternativeSetID = column_v2.getAlternativesSetID();
				final org.xmcda.v2.AlternativesSet column_alternativesSet_v2 = row_v2.getAlternativesSet();

				if (column_alternativeSetID != null || column_alternativesSet_v2 != null)
				{
					getWarnings().throwUnimplemented();
				}

				final org.xmcda.Alternative column_alternative = xmcda_v3.alternatives.get(column_alternativeID, true);

				QualifiedValue value_v3;
				if ( column_v2.getValue()==null )
				{
					// no supplied value, value [0..1] in v2 : give n/a
					value_v3 = Factory.qualifiedValue();
					value_v3.setValue(org.xmcda.value.NA.na);
				}
				else
				{
				    value_v3 = new QualifiedValueConverter().convertTo_v3(column_v2.getValue(), xmcda_v3);
				}
				final org.xmcda.utils.Coord<org.xmcda.Alternative,org.xmcda.Alternative> coord_v3 = new org.xmcda.utils.Coord<org.xmcda.Alternative,org.xmcda.Alternative>(row_alternative, column_alternative);
				final org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
				values_v3.add(value_v3);
				alternativesMatrix_v3.put(coord_v3, values_v3);
				
			}
		}
		
		getWarnings().popTag(); // ALTERNATIVES_MATRIX
		xmcda_v3.alternativesMatricesList.add(alternativesMatrix_v3);
	}
	
	
	// v3 -> v2
	public void convertTo_v2(List<org.xmcda.AlternativesMatrix<?>> alternativesMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.AlternativesMatrix alternativesMatrix_v3: alternativesMatrices_v3)
		{
			convertTo_v2(alternativesMatrix_v3, xmcda_v2);
		}
	}

	public void convertTo_v2(org.xmcda.AlternativesMatrix alternativesMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(ALTERNATIVES_MATRIX, alternativesMatrix_v3.id());

		org.xmcda.v2.AlternativesMatrix alternativesMatrix_v2 = new org.xmcda.v2.AlternativesMatrix();
		List<JAXBElement<?>> alts = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		alts.add(new JAXBElement<org.xmcda.v2.AlternativesMatrix>(new QName(ALTERNATIVES_MATRIX),
		                                                          org.xmcda.v2.AlternativesMatrix.class, alternativesMatrix_v2));


		// default attributes
		alternativesMatrix_v2.setId(alternativesMatrix_v3.id());
		alternativesMatrix_v2.setName(alternativesMatrix_v3.name());
		alternativesMatrix_v2.setMcdaConcept(alternativesMatrix_v3.mcdaConcept());

		// valuation: scale
		alternativesMatrix_v2.setValuation(new ScaleConverter().convertTo_v2(alternativesMatrix_v3.getValuation()));

		// rows
		for (Object _coord: alternativesMatrix_v3.keySet())
		{
			final org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			final org.xmcda.v2.AlternativesMatrix.Row row_v2 = new org.xmcda.v2.AlternativesMatrix.Row();
			alternativesMatrix_v2.getRow().add(row_v2);
			
			final org.xmcda.v2.AlternativesMatrix.Row.Column col_v2 = new org.xmcda.v2.AlternativesMatrix.Row.Column();
			row_v2.getColumn().add(col_v2);
			
			row_v2.setAlternativeID( ((org.xmcda.Alternative) coord.x).id());
			col_v2.setAlternativeID( ((org.xmcda.Alternative) coord.y).id());
			// values
			final org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			final org.xmcda.QualifiedValues values_v3 = (org.xmcda.QualifiedValues) alternativesMatrix_v3.get(_coord);
			if (values_v3 != null && (!values_v3.isEmpty()))
			{
				org.xmcda.QualifiedValue value = (org.xmcda.QualifiedValue) values_v3.get(0);
				final org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value);
				col_v2.setValue(value_v2);
			}

		}
		getWarnings().popTag(); // ALTERNATIVES_MATRIX
	}

}
