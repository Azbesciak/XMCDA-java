package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

/**
 * @author Sébastien Bigaret
 */
public class AlternativeConverter
    extends Converter
{
	public static final String ALTERNATIVE = "alternative";

	public static final String ACTIVE    = "active";

	public static final String TYPE      = "type";

	public static final String REFERENCE = "reference";

	public AlternativeConverter()
	{
		super(ALTERNATIVE);
	}

	public void convertTo_v3(org.xmcda.v2.Alternative alternative_v2, org.xmcda.XMCDA xmcda_v3)
	{
		final String id = alternative_v2.getId();
		getWarnings().pushTag(ALTERNATIVE, alternative_v2.getId());

		// defaultAttributes
		org.xmcda.Alternative alternative_v3 = xmcda_v3.alternatives.get(id); // TODO if already exists, warnings

		alternative_v3.setName(alternative_v2.getName());
		alternative_v3.setMcdaConcept(alternative_v2.getMcdaConcept());

		// description, type, active
		// reference: IGNORED
		alternative_v3.setIsActive(true);
		alternative_v3.setIsReal(true);

		for (JAXBElement<?> element: alternative_v2.getDescriptionOrTypeOrActive())
		{
			final Object _value = element.getValue();
			if (_value == null)
			    continue;

			if (_value instanceof org.xmcda.v2.Description)
			{
				if (alternative_v3.getDescription() != null)
				    continue;
				alternative_v3.setDescription(new DescriptionConverter()
				        .convertTo_v3((org.xmcda.v2.Description) _value));
			}
			if (_value instanceof org.xmcda.v2.AlternativeType)
			{
				alternative_v3.setIsReal(( (org.xmcda.v2.AlternativeType) _value )
				        .equals(org.xmcda.v2.AlternativeType.REAL));
				continue;
			}
			if (ACTIVE.equals(element.getName().getLocalPart()))
			{
				alternative_v3.setIsActive((Boolean) _value);
				continue;
			}
			if (REFERENCE.equals(element.getName().getLocalPart()))
			{
				getWarnings().elementIgnored("reference");
				continue;
			}
		}
		getWarnings().popTag(); // ALTERNATIVE
	}

	public org.xmcda.v2.Alternative convertTo_v2(org.xmcda.Alternative alternative_v3)
	{
		getWarnings().pushTag(ALTERNATIVE, alternative_v3.id());

		// ABSENT in v3: reference
		org.xmcda.v2.Alternative alternative_v2 = new org.xmcda.v2.Alternative();

		// default attributes
		alternative_v2.setId(alternative_v3.id());
		alternative_v2.setName(alternative_v3.name());
		alternative_v2.setMcdaConcept(alternative_v3.mcdaConcept());

		List<JAXBElement<?>> list = alternative_v2.getDescriptionOrTypeOrActive();

		// description
		final org.xmcda.Description desc_v3 = alternative_v3.getDescription();
		if (desc_v3 != null)
		{
			org.xmcda.v2.Description desc_v2 = new DescriptionConverter().convertTo_v2(desc_v3);
			list.add(new JAXBElement<org.xmcda.v2.Description>(new QName(DescriptionConverter.DESCRIPTION),
			                                                       org.xmcda.v2.Description.class, desc_v2));
		}

		// type
		org.xmcda.v2.AlternativeType type = org.xmcda.v2.AlternativeType.REAL;
		if (alternative_v3.isFictive())
		    type = org.xmcda.v2.AlternativeType.FICTIVE;
		list.add(new JAXBElement<org.xmcda.v2.AlternativeType>(new QName(TYPE),
		                                                           org.xmcda.v2.AlternativeType.class, type));

		// active
		list.add(new JAXBElement<Boolean>(new QName(ACTIVE), Boolean.class, alternative_v3.isActive()));

		getWarnings().popTag(); // ALTERNATIVE
		return alternative_v2;
	}
}
