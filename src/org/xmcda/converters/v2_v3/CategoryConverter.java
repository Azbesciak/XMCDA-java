package org.xmcda.converters.v2_v3;

import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.utils.CommonAttributesUtils;



/**
 * @author Sébastien Bigaret
 */
public class CategoryConverter
    extends Converter
{
	public static final String CATEGORY = "criterion";

	public static final String ACTIVE    = "active";

	public static final String RANK    = "rank";

	public CategoryConverter()
	{
		super(CATEGORY);
	}

	public void convertTo_v3(org.xmcda.v2.Category category_v2, org.xmcda.XMCDA xmcda_v3)
	{
		final String id = category_v2.getId();
		getWarnings().pushTag(CATEGORY, id);

		// defaultAttributes
		org.xmcda.Category category_v3 = xmcda_v3.categories.get(id);
		category_v3.setName(category_v2.getName());
		category_v3.setMcdaConcept(category_v2.getMcdaConcept());

		category_v3.setIsActive(category_v2.isActive()==null?true : category_v2.isActive());

		// description
		category_v3.setDescription(new DescriptionConverter().convertTo_v3(category_v2.getDescription()));
		getWarnings().popTag(); // CATEGORY
	}
	
	public void convertRanksTo_v3(org.xmcda.v2.Category category_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORY, category_v2.getId());

		// search for criteriaValues w/ mcdaConcept="rank"
		boolean ranksCreated = false;
		org.xmcda.CategoriesValues ranks = CommonAttributesUtils.getElementWithMcdaConcept(xmcda_v3.categoriesValuesList, "rank");
		if (ranks==null)
		{
			ranks = Factory.categoriesValues();
			ranks.setMcdaConcept(RANK);
			ranksCreated = true;
		}

		if (category_v2.getRank()!=null)
		{
			final org.xmcda.QualifiedValue value_v3 =
					new QualifiedValueConverter().convertTo_v3(category_v2.getRank(), xmcda_v3);
			// if the rank value is ignored by the conversion, avoid the creation of an empty <values/> which is
			// invalid in XMCDA v3
			if (value_v3 != null)
			{
				org.xmcda.Category category = xmcda_v3.categories.get(category_v2.getId());
				org.xmcda.LabelledQValues lqvalues = Factory.labelledQValues();
				lqvalues.add(value_v3);
				ranks.put(category, lqvalues);
			}
		}
		if (ranksCreated && ranks.size()>0)
			xmcda_v3.categoriesValuesList.add(ranks);

		getWarnings().popTag(); // CATEGORY
	}

	public org.xmcda.v2.Category convertTo_v2(org.xmcda.Category category_v3, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORY, category_v3.id());

		org.xmcda.CategoriesValues<?> ranks = CommonAttributesUtils.getElementWithMcdaConcept(xmcda_v3.categoriesValuesList, "rank");
		org.xmcda.v2.Category category_v2 = new org.xmcda.v2.Category();
		category_v2.setId(category_v3.id());
		category_v2.setName(category_v3.name());
		category_v2.setMcdaConcept(category_v3.mcdaConcept());
		category_v2.setDescription(new DescriptionConverter().convertTo_v2(category_v3.getDescription()));
		category_v2.setActive(category_v3.isActive());
		
		LabelledQValues<?> values = ranks != null ? ranks.get(category_v3) : null;
		// todo warnings if >1
		if (values != null && values.size()==1)
			category_v2.setRank(new QualifiedValueConverter().convertTo_v2(values.get(0)));

		getWarnings().popTag(); // CATEGORY

		return category_v2;
	}

}
