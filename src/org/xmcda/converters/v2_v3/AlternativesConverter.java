package org.xmcda.converters.v2_v3;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;


/**
 * @author Sébastien Bigaret
 */
public class AlternativesConverter
    extends Converter
{
	public static final String ALTERNATIVES = org.xmcda.Alternatives.TAG;

	public AlternativesConverter()
	{
		super(ALTERNATIVES);
	}

	public void convertTo_v3(org.xmcda.v2.Alternatives value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(ALTERNATIVES, value.getId());
		org.xmcda.Alternatives alternatives_v3 = xmcda_v3.alternatives;
		// defaultAttributes
		alternatives_v3.setId(value.getId());
		alternatives_v3.setMcdaConcept(value.getMcdaConcept());
		alternatives_v3.setName(value.getName());

		// description
		// alternative 0..*
		for (Object element: value.getDescriptionOrAlternative())
		{
			final Object _value = element;
			if (_value != null && _value instanceof org.xmcda.v2.Description)
			{
				if (alternatives_v3.getDescription() != null)
				{
					getWarnings().elementIgnored(DescriptionConverter.DESCRIPTION, Warnings.ALL_BUT_FIRST_IGNORED);
					continue;
				}
				alternatives_v3.setDescription(new DescriptionConverter()
				        .convertTo_v3((org.xmcda.v2.Description) _value));
				continue;
			}
			if (_value != null && _value instanceof org.xmcda.v2.Alternative)
			{
				new AlternativeConverter().convertTo_v3((org.xmcda.v2.Alternative) _value, xmcda_v3);
				continue;
			}
		}
		getWarnings().popTag(); // ALTERNATIVES
	}

	public void convertTo_v2(org.xmcda.Alternatives alternatives_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(ALTERNATIVES, alternatives_v3.id());
		org.xmcda.v2.Alternatives alternatives_v2 = new org.xmcda.v2.Alternatives();
		List<JAXBElement<?>> alts = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		alts.add(new JAXBElement<org.xmcda.v2.Alternatives>(new QName(ALTERNATIVES),
		                                                        org.xmcda.v2.Alternatives.class, alternatives_v2));


		// default attributes
		alternatives_v2.setId(alternatives_v3.id());
		alternatives_v2.setName(alternatives_v3.name());
		alternatives_v2.setMcdaConcept(alternatives_v3.mcdaConcept());

		List<Object> list = alternatives_v2.getDescriptionOrAlternative();

		// description
		final org.xmcda.Description desc_v3 = alternatives_v3.getDescription();
		if (desc_v3 != null)
		    list.add(new DescriptionConverter().convertTo_v2(desc_v3));

		// alternative
		for (org.xmcda.Alternative alt_v3: alternatives_v3)
			list.add(new AlternativeConverter().convertTo_v2(alt_v3));

		getWarnings().popTag(); // ALTERNATIVES
	}

}
