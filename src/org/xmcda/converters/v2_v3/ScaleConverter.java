package org.xmcda.converters.v2_v3;

import org.xmcda.Factory;


/**
 * @author Sébastien Bigaret
 */
public class ScaleConverter
    extends Converter
{
	public static final String SCALE = "scale";

	public ScaleConverter()
	{
		super(SCALE);
	}

	protected static class NominalScaleConverter
	extends Converter
	{
		public NominalScaleConverter()
		{
			super(SCALE);
		}

		public org.xmcda.Scale convertTo_v3(org.xmcda.v2.Scale scale_v2)
		{
			final org.xmcda.Scale scale_v3 = Factory.nominalScale();
			org.xmcda.v2.Nominal nominal_v2 = scale_v2.getNominal();
			// ignore description TODO WARNING
			for (String label: nominal_v2.getLabel())
			{
				((org.xmcda.NominalScale) scale_v3).add(label);
			}
			return scale_v3;
		}

		public org.xmcda.v2.Scale convertTo_v2(org.xmcda.NominalScale scale_v3)
		{
			org.xmcda.v2.Scale scale_v2 = new org.xmcda.v2.Scale();
			scale_v2.setNominal(new org.xmcda.v2.Nominal());
			for (String label: scale_v3)
			{
				scale_v2.getNominal().getLabel().add(label);
			}
			return scale_v2;
		}
	}
	
	protected static class QualitativeScaleConverter
	extends Converter
	{
		public QualitativeScaleConverter()
		{
			super(SCALE);
		}

		public org.xmcda.Scale convertTo_v3(org.xmcda.v2.Scale scale_v2, org.xmcda.XMCDA xmcda_v3)
		{
			getWarnings().pushTag(SCALE, scale_v2.getId());
			org.xmcda.Scale scale_v3 = Factory.qualitativeScale();
			org.xmcda.QualitativeScale qualitativeScale_v3 = ((org.xmcda.QualitativeScale) scale_v3);
			
			org.xmcda.v2.Qualitative qualitative_v2 = scale_v2.getQualitative();

			// description: absent in 3.0
			if ( scale_v2.getDescription() != null )
				getWarnings().elementIgnored("description");
		
			// preferenceDirection
			qualitativeScale_v3.setPreferenceDirection(new PreferenceDirectionConverter().convertTo_v3(qualitative_v2.getPreferenceDirection()));

			// get ranked label ou fuzzy label, return valuesLabelsType
			for (org.xmcda.v2.RankedLabel rankedLabel_v2: qualitative_v2.getRankedLabel())
			{
				qualitativeScale_v3.add(new RankedLabelConverter().convertTo_v3(rankedLabel_v2));
			}
			for (org.xmcda.v2.FuzzyLabel fuzzyLabel_v2: qualitative_v2.getFuzzyLabel())
			{
				qualitativeScale_v3.add(new FuzzyLabelConverter().convertTo_v3(fuzzyLabel_v2, xmcda_v3));
			}

			getWarnings().popTag();
			return scale_v3;
		}

		public org.xmcda.v2.Scale convertTo_v2(org.xmcda.QualitativeScale<?> scale_v3)
		{
			getWarnings().pushTag(SCALE, scale_v3.id());
			final org.xmcda.v2.Scale scale_v2 = new org.xmcda.v2.Scale();
			final org.xmcda.v2.Qualitative qualitative_v2= new org.xmcda.v2.Qualitative();
			scale_v2.setQualitative(qualitative_v2);

			// preferenceDirection
			qualitative_v2.setPreferenceDirection(new PreferenceDirectionConverter().convertTo_v2(scale_v3.getPreferenceDirection()));

			// ranked labels or fuzzy labels
			for (org.xmcda.value.ValuedLabel<?> _label: scale_v3)
			{
				// the first getValue returns a QualifiedValue, we get its value
				if ( _label.getValue().getValue() instanceof org.xmcda.value.FuzzyNumber)
					qualitative_v2.getFuzzyLabel().add(new FuzzyLabelConverter().convertTo_v2(_label));
				else
					qualitative_v2.getRankedLabel().add(new RankedLabelConverter().convertTo_v2((org.xmcda.value.ValuedLabel<Integer>)_label));
			}
			
			getWarnings().popTag();
			return scale_v2;
		}
	}

	protected static class QuantitativeScaleConverter
	extends Converter
	{
		public QuantitativeScaleConverter()
		{
			super(SCALE);
		}

		public org.xmcda.Scale convertTo_v3(org.xmcda.v2.Scale scale_v2, org.xmcda.XMCDA xmcda_v3)
		{
			org.xmcda.Scale scale_v3 = Factory.quantitativeScale();
			org.xmcda.QuantitativeScale quantitativeScale_v3 = ((org.xmcda.QuantitativeScale) scale_v3);

			// preferenceDirection[1..1]
			quantitativeScale_v3.setPreferenceDirection(new PreferenceDirectionConverter().convertTo_v3(scale_v2.getQuantitative().getPreferenceDirection()));
			
			// min[0..1]
			if (scale_v2.getQuantitative().getMinimum() != null)
				quantitativeScale_v3.setMinimum(new QualifiedValueConverter().convertTo_v3(scale_v2.getQuantitative().getMinimum(), xmcda_v3));
			// max[0..1]
			if (scale_v2.getQuantitative().getMaximum() != null)
				quantitativeScale_v3.setMaximum(new QualifiedValueConverter().convertTo_v3(scale_v2.getQuantitative().getMaximum(), xmcda_v3));

			return scale_v3;
		}

		public org.xmcda.v2.Scale convertTo_v2(org.xmcda.Scale scale_v3)
		{
			org.xmcda.v2.Scale scale_v2 = new org.xmcda.v2.Scale();
			org.xmcda.QuantitativeScale quantitativeScale_v3 = ((org.xmcda.QuantitativeScale) scale_v3);
			
			final org.xmcda.v2.Quantitative quantitative_v2 = new org.xmcda.v2.Quantitative();
			scale_v2.setQuantitative(quantitative_v2);

			// preferenceDirection[1..1]
			quantitative_v2.setPreferenceDirection(new PreferenceDirectionConverter().convertTo_v2(quantitativeScale_v3.getPreferenceDirection()));
			
			// min[0..1]
			org.xmcda.v2.Value _value;
			if (quantitativeScale_v3.getMinimum()!=null)
			{
				_value = new QualifiedValueConverter().convertTo_v2(quantitativeScale_v3.getMinimum());
				org.xmcda.v2.NumericValue _nValue = V2ValueToNumericValueConverter.convert(_value);
				quantitative_v2.setMinimum(_nValue);
			}
			// max[0..1]
			if (quantitativeScale_v3.getMaximum()!=null)
			{
				_value = new QualifiedValueConverter().convertTo_v2(quantitativeScale_v3.getMaximum());
				org.xmcda.v2.NumericValue _nValue = V2ValueToNumericValueConverter.convert(_value);
				quantitative_v2.setMaximum(_nValue);
			}
			return scale_v2;
		}
	}

	public org.xmcda.Scale convertTo_v3(org.xmcda.v2.Scale scale_v2, org.xmcda.XMCDA xmcda_v3)
	{
		if ( scale_v2 == null )
			return null;
		
		final String id = scale_v2.getId();
		getWarnings().pushTag(SCALE, id);

		org.xmcda.Scale scale_v3 = null;

		// get the type of the scale
		if ( scale_v2.getNominal() != null )
		{
			scale_v3 = new NominalScaleConverter().convertTo_v3(scale_v2);
		}
		else if ( scale_v2.getQualitative() != null )
		{
			scale_v3 = new QualitativeScaleConverter().convertTo_v3(scale_v2, xmcda_v3);
		}
		else if ( scale_v2.getQuantitative() != null )
		{
			scale_v3 = new QuantitativeScaleConverter().convertTo_v3(scale_v2, xmcda_v3);
		}

		// defaultAttributes
		scale_v3.setId(scale_v2.getId());
		scale_v3.setName(scale_v2.getName());
		scale_v3.setMcdaConcept(scale_v2.getMcdaConcept());
		
		//comparisonType, criterionID: ignored ADD WARNINGS
		if ( scale_v2.getDescription() != null )
			getWarnings().elementIgnored("description");
		if ( scale_v2.getValuationType() != null )
			getWarnings().elementIgnored("valuationType");
		
		getWarnings().popTag(); // SCALE
		return scale_v3;
	}

	public org.xmcda.v2.Scale convertTo_v2(org.xmcda.Scale scale_v3)
	{
		if (scale_v3==null)
			return null;
		
		final String id = scale_v3.id();
		getWarnings().pushTag(SCALE, id);

		org.xmcda.v2.Scale scale_v2 = null;

		if ( scale_v3 instanceof org.xmcda.NominalScale )
		{
			scale_v2 = new NominalScaleConverter().convertTo_v2((org.xmcda.NominalScale)scale_v3);
		}
		else if ( scale_v3 instanceof org.xmcda.QualitativeScale )
		{
			scale_v2 = new QualitativeScaleConverter().convertTo_v2((org.xmcda.QualitativeScale)scale_v3);
		}
		else if ( scale_v3 instanceof org.xmcda.QuantitativeScale )
		{
			scale_v2 = new QuantitativeScaleConverter().convertTo_v2((org.xmcda.QuantitativeScale)scale_v3);
		}
	
		// defaultAttributes
		scale_v2.setId(scale_v3.id());
		scale_v2.setName(scale_v3.name());
		scale_v2.setMcdaConcept(scale_v3.mcdaConcept());
		
		getWarnings().popTag(); // SCALE
		return scale_v2;
	}
}
