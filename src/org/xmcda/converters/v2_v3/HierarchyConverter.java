package org.xmcda.converters.v2_v3;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.xmcda.CriteriaHierarchy;
import org.xmcda.CriteriaSetsHierarchy;

public class HierarchyConverter
	extends Converter
{

	public static final String HIERARCHY = "hierarchy";

	public static final String CRITERIA_HIERARCHY = org.xmcda.CriteriaHierarchy.TAG;

	public static final String NODES = org.xmcda.parsers.xml.xmcda_v3.CriteriaHierarchyParser.NODE;

	public static final String NODE = org.xmcda.parsers.xml.xmcda_v3.CriteriaHierarchyParser.NODE;

	protected enum XMCDAv2HierarchyTag
	{
		ATTRIBUTE_ID, ATTRIBUTE_SET_ID, ATTRIBUTES_SET,
		CRITERION_ID, CRITERIA_SET_ID, CRITERIA_SET,
		ALTERNATIVE_ID, ALTERNATIVES_SET_ID, ALTERNATIVES_SET,
		CATEGORY_ID, CATEGORIES_SET_ID, CATEGORIES_SET;
		
		static Set<XMCDAv2HierarchyTag> tagsForNode(org.xmcda.v2.Node node)
		{
			// Note: xmcda v2 schema allows a node to have different tags, <criterionID> with <criteriaSet> and
			// <alternativeID> e.g., but this was not the original intention and it is a non-sense.
			// Here we simply consider that this is not the case, and we do not check that a node holds only one sort
			// of sub-tags.
			Set<XMCDAv2HierarchyTag> tags = new LinkedHashSet<>();
			if (node.getAttributeID()!=null) tags.add(ATTRIBUTE_ID);
			if (node.getAttributeSetID()!=null) tags.add(ATTRIBUTE_SET_ID);
			if (node.getAttributesSet()!=null) tags.add(ATTRIBUTES_SET);
			if (node.getAlternativeID()!=null) tags.add(ALTERNATIVE_ID);
			if (node.getAlternativesSetID()!=null) tags.add(ALTERNATIVES_SET_ID);
			if (node.getAlternativesSet()!=null) tags.add(ALTERNATIVES_SET);
			if (node.getCriterionID()!=null) tags.add(CRITERION_ID);
			if (node.getCriteriaSetID()!=null) tags.add(CRITERIA_SET_ID);
			if (node.getCriteriaSet()!=null) tags.add(CRITERIA_SET);
			if (node.getCategoryID()!=null) tags.add(CATEGORY_ID);
			if (node.getCategoriesSetID()!=null) tags.add(CATEGORIES_SET_ID);
			if (node.getCategoriesSet()!=null) tags.add(CATEGORIES_SET);
			return tags;
		}
	}

	protected enum XMCDAv3HierarchyTag
	{
		CRITERION_ID, CRITERIA_SET_ID,
		ALTERNATIVE_ID, ALTERNATIVES_SET_ID,
		CATEGORY_ID, CATEGORIES_SET_ID;
	}
	
	public HierarchyConverter()
	{
		super(HIERARCHY);
	}

	/**
	 * For each type of tags X (i.e. attribute, criterion, alternative, category) examines the sub-set of tags present
	 * in the supplied sets and removes all elements except the one that will be used to convert v2 to v3. For example
	 * with criteria, if they are criteria sets or criteria set id, this will be
	 * 
	 * @param tags the set of xcda v2 tags
	 * @return the set of xmcda v3 tags to be used when converting XMCDA v2 to XMCDA v3. The returned set has no
	 *         guaranteed ordering.
	 * @throws IllegalArgument if parameters tags contains elements related to the XMCDA v2 {@code <attribute>} tag:
	 *             attributes are not present in XMCDA v3.
	 */
	protected static Set<XMCDAv3HierarchyTag> v3Tags(Set<XMCDAv2HierarchyTag> tags)
	{
		Set<XMCDAv3HierarchyTag> v3Tags = new HashSet<>();

		if (tags.contains(XMCDAv2HierarchyTag.ATTRIBUTE_ID) || tags.contains(XMCDAv2HierarchyTag.ATTRIBUTES_SET)
		    || tags.contains(XMCDAv2HierarchyTag.ALTERNATIVES_SET_ID))
			throw new IllegalArgumentException("Unimplemented: there are no attributes in XMCDA v3");
		if (tags.contains(XMCDAv2HierarchyTag.ALTERNATIVES_SET_ID)
		    || tags.contains(XMCDAv2HierarchyTag.ALTERNATIVES_SET))
			// convert everythin to sets, incl. <alternative>, if any
			v3Tags.add(XMCDAv3HierarchyTag.ALTERNATIVES_SET_ID);
		else if (tags.contains(XMCDAv2HierarchyTag.ALTERNATIVE_ID))
			v3Tags.add(XMCDAv3HierarchyTag.ALTERNATIVE_ID);

		if (tags.contains(XMCDAv2HierarchyTag.CRITERIA_SET_ID)
			    || tags.contains(XMCDAv2HierarchyTag.CRITERIA_SET))
				v3Tags.add(XMCDAv3HierarchyTag.CRITERIA_SET_ID);
			else if (tags.contains(XMCDAv2HierarchyTag.CRITERION_ID))
				v3Tags.add(XMCDAv3HierarchyTag.CRITERION_ID);

		if (tags.contains(XMCDAv2HierarchyTag.CATEGORIES_SET_ID)
			    || tags.contains(XMCDAv2HierarchyTag.CATEGORIES_SET))
				v3Tags.add(XMCDAv3HierarchyTag.CATEGORIES_SET_ID);
			else if (tags.contains(XMCDAv2HierarchyTag.CATEGORY_ID))
				v3Tags.add(XMCDAv3HierarchyTag.CATEGORY_ID);

		return v3Tags;
	}

	protected static Set<XMCDAv2HierarchyTag> analyseHierarchy(final org.xmcda.v2.Hierarchy hierarchy_v2)
	{
		Set<XMCDAv2HierarchyTag> nodesTags = new HashSet<>();
		for (org.xmcda.v2.Node node: hierarchy_v2.getNode())
			analyseNode(node, nodesTags);
		return nodesTags;
	}

	protected static void analyseNode(final org.xmcda.v2.Node node, final Set<XMCDAv2HierarchyTag> tags)
	{
		final Set<XMCDAv2HierarchyTag> nodeTags = XMCDAv2HierarchyTag.tagsForNode(node);
		//if (tags.isEmpty() && node.getNode()!=null);
		tags.addAll(nodeTags);
		
		for (org.xmcda.v2.Node child: node.getNode())
		{
			analyseNode(child, tags);
		}
	}

	/**
	 * 
	 * @param hierarchy_v2
	 * @return
	 * @throw IllegalArgumentException if the v2 hierarchy contains attributes (there is no such thing in XMCDA v3),
	 *        if it mixes alternatives, criteria and/or categories, or if the hierarchy is empty.
	 */
	static XMCDAv3HierarchyTag v3Tag(final org.xmcda.v2.Hierarchy hierarchy_v2)
	{
		final Set<XMCDAv2HierarchyTag> tags = analyseHierarchy(hierarchy_v2);
		
		if (tags.size() > 3) // this is wrong, always
			throw new IllegalArgumentException("XMCDA v2 hierarchy mixes alternatives, criteria and/or categories");

		Set<XMCDAv3HierarchyTag> v3Tags = v3Tags(tags);
		if (v3Tags.size() > 1)
			throw new IllegalArgumentException("XMCDA v2 hierarchy mixes alternatives, criteria and/or categories");
		if (v3Tags.size() == 0)
			// there is no way to know which one to use
			throw new IllegalArgumentException("An XMCDA v2 empty hierarchy cannot be converted to XMCDA v3");

		return v3Tags.iterator().next();
	}
	/**
	 * @param hierarchy_v2
	 * @param xmcda_v3
	 * @throw IllegalArgumentException if the v2 hierarchy contains attributes (there is no such thing in XMCDA v3),
	 *        if it mixes alternatives, criteria and/or categories, or if the hierarchy is empty.
	 */
	public void convertTo_v3(org.xmcda.v2.Hierarchy hierarchy_v2, org.xmcda.XMCDA xmcda_v3)
	{
		XMCDAv3HierarchyTag v3Tag = v3Tag(hierarchy_v2);

		switch (v3Tag) {
			case CRITERION_ID:
			case CRITERIA_SET_ID:
				new CriteriaHierarchyConverter().convertTo_v3(hierarchy_v2, xmcda_v3, v3Tag);
				break;
			case ALTERNATIVE_ID:
			case ALTERNATIVES_SET_ID:
				//getWarnings()
				break;
			case CATEGORY_ID:
			case CATEGORIES_SET_ID:
				break;
		}
	}
	
	/**
	 * Converts a XMCDA v3 list of hierarchies into its XMCDA v2 equivalent. The list must be made of elements all
	 * having the same class or superclass, ant this class must be one of {@link CriteriaHierarchy},
	 * {@link CriteriaSetsHierarchy}.
	 * 
	 * @param hierarchies_v3 the XMCDA v3 hierarchies to be converted
	 * @param xmcda_v2 the xmcda v2 object into which the converted hierarchies are added
	 * @throws IllegalArgumentException if the list is not made of elements all being subclasses of a unique
	 *             superclass, this unique superclass being one of CriteriaHierarchy, CriteriaSetsHierarchy.
	 */
	@SuppressWarnings("unchecked")
	public void convertTo_v2(List<?> hierarchies_v3, org.xmcda.v2.XMCDA xmcda_v2)
	throws IllegalArgumentException
	{
		if (hierarchies_v3.isEmpty())
			return;

		//Stream<?> classes = hierarchies_v3.stream().map(element -> element.getClass()).distinct();
		final String error = "Parameter hierarchies_v3 must be a List made of either CriteriaHierarchy only, or CriteriaSetsHierarchy only";

		Object[] classes =  hierarchies_v3.stream()
				.<Class<?>>map(clazz -> clazz instanceof CriteriaHierarchy
				     ? CriteriaHierarchy.class
				    		 : clazz instanceof CriteriaSetsHierarchy ? CriteriaSetsHierarchy.class : clazz.getClass())
				.distinct().toArray();

		if (classes.length != 1)
			throw new IllegalArgumentException(error);
		if (classes[0] == CriteriaHierarchy.class)
			new CriteriaHierarchyConverter().convertTo_v2((List<CriteriaHierarchy>) hierarchies_v3, xmcda_v2);
		else if (classes[0] == CriteriaSetsHierarchy.class)
			new CriteriaSetsHierarchyConverter().convertTo_v2((List<CriteriaSetsHierarchy>) hierarchies_v3, xmcda_v2);
		else
			throw new IllegalArgumentException(error);
	}

}
