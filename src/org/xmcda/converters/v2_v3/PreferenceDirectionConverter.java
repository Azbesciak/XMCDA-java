/**
 * 
 */
package org.xmcda.converters.v2_v3;

import org.xmcda.v2.PreferenceDirection;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class PreferenceDirectionConverter
extends Converter
{
	public PreferenceDirectionConverter()
	{
		super(org.xmcda.Scale.PREFERENCE_DIRECTION);
	}

	public org.xmcda.Scale.PreferenceDirection convertTo_v3(org.xmcda.v2.PreferenceDirection pref_v2)
	{
		getWarnings().pushTag(org.xmcda.Scale.PREFERENCE_DIRECTION);
		org.xmcda.Scale.PreferenceDirection ret = org.xmcda.Scale.PreferenceDirection.MAX;
		
		if ( PreferenceDirection.MIN.equals(pref_v2) )
		{
			ret = org.xmcda.Scale.PreferenceDirection.MIN;
		}
		else if ( PreferenceDirection.MAX.equals(pref_v2) )
		{
			ret = org.xmcda.Scale.PreferenceDirection.MAX;
		}
		else
		{
			// no value in v2: defaults to MAX in v3
			getWarnings().elementRequiredIsAbsent_defaultApplied(/*org.xmcda.v2*/"preferenceDirection", org.xmcda.Scale.PreferenceDirection.MAX.toString());
		}
		getWarnings().popTag(); // org.xmcda.Scale.PREFERENCE_DIRECTION
		return ret;
	}
	
	public org.xmcda.v2.PreferenceDirection convertTo_v2(org.xmcda.Scale.PreferenceDirection preference_v3)
	{
		getWarnings().pushTag(org.xmcda.Scale.PREFERENCE_DIRECTION);
		if (org.xmcda.Scale.PreferenceDirection.MIN.equals(preference_v3))
			return org.xmcda.v2.PreferenceDirection.MIN;
		if (org.xmcda.Scale.PreferenceDirection.MAX.equals(preference_v3))
			return org.xmcda.v2.PreferenceDirection.MAX;
		//à tester tout de suite la valeur par défaut d'un scale en v3 ou l'absence d'un scale en v3
		// faire un unittest
		getWarnings().popTag(); // org.xmcda.Scale.PREFERENCE_DIRECTION
		return null;
	}

}
