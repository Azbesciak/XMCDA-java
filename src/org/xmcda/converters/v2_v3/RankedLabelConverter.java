/**
 * 
 */
package org.xmcda.converters.v2_v3;


import org.xmcda.Factory;
import org.xmcda.QualifiedValue;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class RankedLabelConverter
extends Converter
{
	public static final String RANKED_LABEL = "rankedLabel";
	public static final String VALUED_LABEL = "valuedLabel";

	public RankedLabelConverter()
	{
		super(RANKED_LABEL);
	}

	public org.xmcda.value.ValuedLabel<Integer> convertTo_v3(org.xmcda.v2.RankedLabel rankedLabel_v2)
	{
		org.xmcda.value.ValuedLabel<Integer> valuedLabel_v3 = Factory.valuedLabel();
		getWarnings().pushTag(RANKED_LABEL);

		// description: IGNORED 
		if (rankedLabel_v2.getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);
		
		// Label
		valuedLabel_v3.setLabel(rankedLabel_v2.getLabel());
		
		// Rank
		QualifiedValue<Integer> value_v3 = Factory.qualifiedValue();
		value_v3.setValue(rankedLabel_v2.getRank().intValue());
		valuedLabel_v3.setValue(value_v3);

		getWarnings().popTag(); // RANKED_LABEL
		return valuedLabel_v3;
	}

	public org.xmcda.v2.RankedLabel convertTo_v2(org.xmcda.value.ValuedLabel<Integer> label_v3)
	{
		getWarnings().pushTag(RANKED_LABEL);
		
		final org.xmcda.v2.RankedLabel rankedLabel_v2 = new org.xmcda.v2.RankedLabel();

		// Label
		rankedLabel_v2.setLabel(label_v3.getLabel());

		// Rank
		rankedLabel_v2.setRank(java.math.BigInteger.valueOf(label_v3.getValue().getValue()));

		getWarnings().popTag(); // RANKED_LABEL
		return rankedLabel_v2;
	}
}
