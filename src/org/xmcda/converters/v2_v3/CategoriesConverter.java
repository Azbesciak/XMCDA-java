package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;


/**
 * @author Sébastien Bigaret
 */
public class CategoriesConverter
extends Converter
{
	public static final String CATEGORIES = org.xmcda.Categories.TAG;

	public CategoriesConverter()
    {
		super(CATEGORIES);
    }

	public void convertTo_v3(org.xmcda.v2.Categories value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORIES, value.getId());

		org.xmcda.Categories categories_v3 = xmcda_v3.categories;
		// defaultAttributes
		categories_v3.setId(value.getId());
		categories_v3.setMcdaConcept(value.getMcdaConcept());
		categories_v3.setName(value.getName());

		// description
		categories_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// category 0..*
		CategoryConverter categoryConverter = new CategoryConverter();
		for (org.xmcda.v2.Category category: value.getCategory())
		{
			categoryConverter.convertTo_v3(category, xmcda_v3);
			categoryConverter.convertRanksTo_v3(category, xmcda_v3);
		}

		getWarnings().popTag(); // CATEGORIES
	}

	public void convertTo_v2(org.xmcda.Categories categories_v3, org.xmcda.v2.XMCDA xmcda_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORIES, categories_v3.id());
		
		org.xmcda.v2.Categories categories_v2 = new org.xmcda.v2.Categories();
		List<JAXBElement<?>> crits = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		crits.add(new JAXBElement<org.xmcda.v2.Categories>(new QName(CATEGORIES), org.xmcda.v2.Categories.class,
                categories_v2));

		// default attributes
		categories_v2.setId(categories_v3.id());
		categories_v2.setName(categories_v3.name());
		categories_v2.setMcdaConcept(categories_v3.mcdaConcept());

		// description
		categories_v2.setDescription(new DescriptionConverter().convertTo_v2(categories_v3.getDescription()));

		// categories
		for (org.xmcda.Category cat_v3: categories_v3)
			categories_v2.getCategory().add(new CategoryConverter().convertTo_v2(cat_v3, xmcda_v3));

		getWarnings().popTag(); // CATEGORIES
	}

}
