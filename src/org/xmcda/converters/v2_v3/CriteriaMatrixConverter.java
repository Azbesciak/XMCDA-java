package org.xmcda.converters.v2_v3;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;


/**
 * A simplified transformation of criteriaMatrices: the two only example are outputs
 * which only have criterionID in rows and columns.  These 2 programs are: 
 * <ul>
 * <li> (PUT) RORUTA-PairwiseOutrankingIndices,
 * <li> (PUT) RORUTA-PairwiseOutrankingIndicesHierarchical.
 * </ul>
 * @author Sébastien Bigaret
 */
public class CriteriaMatrixConverter
    extends Converter
{
	public static final String CRITERIA_COMPARISONS = "criteriaComparisons";
	public static final String CRITERIA_MATRIX      = org.xmcda.CriteriaMatrix.TAG;
	public static final String PAIRS                    = "pairs";
	public static final String PAIR                     = "pair";
	
	public CriteriaMatrixConverter()
	{
		super(CRITERIA_MATRIX); // ajouter un constructeur super(tag_v2, tag_v3) ?
	}

	// v2 -> v3

	public void convertTo_v3(org.xmcda.v2.CriteriaMatrix value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CRITERIA_MATRIX, value.getId());

		org.xmcda.CriteriaMatrix criteriaMatrix_v3 = Factory.criteriaMatrix();
		// defaultAttributes
		criteriaMatrix_v3.setId(value.getId());
		criteriaMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		criteriaMatrix_v3.setName(value.getName());
		criteriaMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			criteriaMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));

		// from here it diverges from criteriaComparisons
		// même pb pour criteriaMatrix qui mélange les 3 criterionID, criteriaSetId et criteriaSet
		
		// row
		for (org.xmcda.v2.CriteriaMatrix.Row row_v2: value.getRow())
		{
			// TODO warnings id, name, mcdaConcept, description ignored
			final String row_criterionID = row_v2.getCriterionID();
			final String row_criterionSetID = row_v2.getCriteriaSetID();
			final org.xmcda.v2.CriteriaSet row_criteriaSet_v2 = row_v2.getCriteriaSet();

			if (row_criterionSetID != null || row_criteriaSet_v2 != null)
			{
				getWarnings().throwUnimplemented();
			}
			final org.xmcda.Criterion row_criterion = xmcda_v3.criteria.get(row_criterionID, true);
			// TODO ignore warnings for inner description, id etc...
			for (org.xmcda.v2.CriteriaMatrix.Row.Column column_v2: row_v2.getColumn())
			{
				final String column_criterionID = column_v2.getCriterionID();
				final String column_criterionSetID = column_v2.getCriteriaSetID();
				final org.xmcda.v2.CriteriaSet column_criteriaSet_v2 = row_v2.getCriteriaSet();

				if (column_criterionSetID != null || column_criteriaSet_v2 != null)
				{
					getWarnings().throwUnimplemented();
				}

				final org.xmcda.Criterion column_criterion = xmcda_v3.criteria.get(column_criterionID, true);

				QualifiedValue value_v3;
				if ( column_v2.getValue()==null )
				{
					// no supplied value, value [0..1] in v2 : give n/a
					value_v3 = Factory.qualifiedValue();
					value_v3.setValue(org.xmcda.value.NA.na);
				}
				else
				{
				    value_v3 = new QualifiedValueConverter().convertTo_v3(column_v2.getValue(), xmcda_v3);
				}
				final org.xmcda.utils.Coord<org.xmcda.Criterion,org.xmcda.Criterion> coord_v3 = new org.xmcda.utils.Coord<org.xmcda.Criterion,org.xmcda.Criterion>(row_criterion, column_criterion);
				final org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
				values_v3.add(value_v3);
				criteriaMatrix_v3.put(coord_v3, values_v3);
				
			}
		}
		
		getWarnings().popTag(); // CRITERIA_MATRIX
		xmcda_v3.criteriaMatricesList.add(criteriaMatrix_v3);
	}
	
	
	// v3 -> v2
	public void convertTo_v2(List<org.xmcda.CriteriaMatrix<?>> criteriaMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CriteriaMatrix criteriaMatrix_v3: criteriaMatrices_v3)
		{
			convertTo_v2(criteriaMatrix_v3, xmcda_v2);
		}
	}

	public void convertTo_v2(org.xmcda.CriteriaMatrix criteriaMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(CRITERIA_MATRIX, criteriaMatrix_v3.id());

		org.xmcda.v2.CriteriaMatrix criteriaMatrix_v2 = new org.xmcda.v2.CriteriaMatrix();
		List<JAXBElement<?>> alts = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		alts.add(new JAXBElement<org.xmcda.v2.CriteriaMatrix>(new QName(CRITERIA_MATRIX),
		                                                          org.xmcda.v2.CriteriaMatrix.class, criteriaMatrix_v2));


		// default attributes
		criteriaMatrix_v2.setId(criteriaMatrix_v3.id());
		criteriaMatrix_v2.setName(criteriaMatrix_v3.name());
		criteriaMatrix_v2.setMcdaConcept(criteriaMatrix_v3.mcdaConcept());

		// valuation: scale
		criteriaMatrix_v2.setValuation(new ScaleConverter().convertTo_v2(criteriaMatrix_v3.getValuation()));

		// rows
		for (Object _coord: criteriaMatrix_v3.keySet())
		{
			final org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			final org.xmcda.v2.CriteriaMatrix.Row row_v2 = new org.xmcda.v2.CriteriaMatrix.Row();
			criteriaMatrix_v2.getRow().add(row_v2);
			
			final org.xmcda.v2.CriteriaMatrix.Row.Column col_v2 = new org.xmcda.v2.CriteriaMatrix.Row.Column();
			row_v2.getColumn().add(col_v2);
			
			row_v2.setCriterionID( ((org.xmcda.Criterion) coord.x).id());
			col_v2.setCriterionID( ((org.xmcda.Criterion) coord.y).id());
			// values
			final org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
			final org.xmcda.QualifiedValues values_v3 = (org.xmcda.QualifiedValues) criteriaMatrix_v3.get(_coord);
			if (values_v3 != null && (!values_v3.isEmpty()))
			{
				org.xmcda.QualifiedValue value = (org.xmcda.QualifiedValue) values_v3.get(0);
				final org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value);
				col_v2.setValue(value_v2);
			}

		}
		getWarnings().popTag(); // CRITERIA_MATRIX
	}

}
