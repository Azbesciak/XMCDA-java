package org.xmcda;

import java.util.ArrayList;
import java.util.List;


/**
 * xmcda:type:criteria
 * @author Sébastien Bigaret
 */
public class Criteria
    extends ReferenceableContainer<Criterion>
    implements CommonAttributes, HasDescription, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "criteria";

	public Criteria()
	{ super(); }

	@Override
	public Class<Criterion> elementClass()
	{
	    return Criterion.class;
	}

	public List<Criterion> getActiveCriteria()
	{
		ArrayList<Criterion> activeCriteria = new ArrayList<Criterion>(this);
		for (Criterion criterion: this)
			if (!criterion.isActive())
			    activeCriteria.remove(criterion);
		return activeCriteria;
	}

	public int getNumberOfActiveCriteria()
	{
		int nb_active = 0;
		for (Criterion criterion: this)
			if (criterion.isActive())
			    nb_active++;
		return nb_active;
	}

	public void merge(Criteria criteria)
	{
		for (Criterion criterion: criteria)
			this.merge(criterion);
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (Criterion criterion: this)
			sb.append(criterion.toString()).append(", ");
		sb.append("]");
		return sb.toString();
	}

}
