//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.12.02 at 10:01:58 AM CET 
//


package org.xmcda.v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * A category profile.
 * 
 * <p>Java class for categoryProfile complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="categoryProfile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}description" minOccurs="0"/>
 *         &lt;element name="alternativeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;choice>
 *           &lt;element name="central">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;choice maxOccurs="unbounded" minOccurs="0">
 *                       &lt;element name="value" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}value"/>
 *                       &lt;element name="values" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}values"/>
 *                     &lt;/choice>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="limits">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="lowerCategory" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoryProfileBound" minOccurs="0"/>
 *                     &lt;element name="upperCategory" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoryProfileBound" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="value" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}value"/>
 *           &lt;element name="values" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}values"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.decision-deck.org/2019/XMCDA-2.2.3}defaultAttributes"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "categoryProfile", propOrder = {
    "description",
    "alternativeID",
    "central",
    "limits",
    "valueOrValues"
})
public class CategoryProfile {

    protected Description description;
    @XmlElement(required = true)
    protected String alternativeID;
    protected CategoryProfile.Central central;
    protected CategoryProfile.Limits limits;
    @XmlElements({
        @XmlElement(name = "value", type = Value.class),
        @XmlElement(name = "values", type = Values.class)
    })
    protected List<Object> valueOrValues;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "mcdaConcept")
    protected String mcdaConcept;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link Description }
     *     
     */
    public Description getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link Description }
     *     
     */
    public void setDescription(Description value) {
        this.description = value;
    }

    /**
     * Gets the value of the alternativeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternativeID() {
        return alternativeID;
    }

    /**
     * Sets the value of the alternativeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternativeID(String value) {
        this.alternativeID = value;
    }

    /**
     * Gets the value of the central property.
     * 
     * @return
     *     possible object is
     *     {@link CategoryProfile.Central }
     *     
     */
    public CategoryProfile.Central getCentral() {
        return central;
    }

    /**
     * Sets the value of the central property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryProfile.Central }
     *     
     */
    public void setCentral(CategoryProfile.Central value) {
        this.central = value;
    }

    /**
     * Gets the value of the limits property.
     * 
     * @return
     *     possible object is
     *     {@link CategoryProfile.Limits }
     *     
     */
    public CategoryProfile.Limits getLimits() {
        return limits;
    }

    /**
     * Sets the value of the limits property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryProfile.Limits }
     *     
     */
    public void setLimits(CategoryProfile.Limits value) {
        this.limits = value;
    }

    /**
     * Gets the value of the valueOrValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valueOrValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValueOrValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Value }
     * {@link Values }
     * 
     * 
     */
    public List<Object> getValueOrValues() {
        if (valueOrValues == null) {
            valueOrValues = new ArrayList<Object>();
        }
        return this.valueOrValues;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the mcdaConcept property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcdaConcept() {
        return mcdaConcept;
    }

    /**
     * Sets the value of the mcdaConcept property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcdaConcept(String value) {
        this.mcdaConcept = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;choice maxOccurs="unbounded" minOccurs="0">
     *           &lt;element name="value" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}value"/>
     *           &lt;element name="values" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}values"/>
     *         &lt;/choice>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "categoryID",
        "valueOrValues"
    })
    public static class Central {

        @XmlElement(required = true)
        protected String categoryID;
        @XmlElements({
            @XmlElement(name = "value", type = Value.class),
            @XmlElement(name = "values", type = Values.class)
        })
        protected List<Object> valueOrValues;

        /**
         * Gets the value of the categoryID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCategoryID() {
            return categoryID;
        }

        /**
         * Sets the value of the categoryID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCategoryID(String value) {
            this.categoryID = value;
        }

        /**
         * Gets the value of the valueOrValues property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the valueOrValues property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getValueOrValues().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Value }
         * {@link Values }
         * 
         * 
         */
        public List<Object> getValueOrValues() {
            if (valueOrValues == null) {
                valueOrValues = new ArrayList<Object>();
            }
            return this.valueOrValues;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="lowerCategory" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoryProfileBound" minOccurs="0"/>
     *         &lt;element name="upperCategory" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoryProfileBound" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lowerCategory",
        "upperCategory"
    })
    public static class Limits {

        protected CategoryProfileBound lowerCategory;
        protected CategoryProfileBound upperCategory;

        /**
         * Gets the value of the lowerCategory property.
         * 
         * @return
         *     possible object is
         *     {@link CategoryProfileBound }
         *     
         */
        public CategoryProfileBound getLowerCategory() {
            return lowerCategory;
        }

        /**
         * Sets the value of the lowerCategory property.
         * 
         * @param value
         *     allowed object is
         *     {@link CategoryProfileBound }
         *     
         */
        public void setLowerCategory(CategoryProfileBound value) {
            this.lowerCategory = value;
        }

        /**
         * Gets the value of the upperCategory property.
         * 
         * @return
         *     possible object is
         *     {@link CategoryProfileBound }
         *     
         */
        public CategoryProfileBound getUpperCategory() {
            return upperCategory;
        }

        /**
         * Sets the value of the upperCategory property.
         * 
         * @param value
         *     allowed object is
         *     {@link CategoryProfileBound }
         *     
         */
        public void setUpperCategory(CategoryProfileBound value) {
            this.upperCategory = value;
        }

    }

}
