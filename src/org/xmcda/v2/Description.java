//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.11.30 at 02:37:50 PM CET 
//


package org.xmcda.v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *  The generic description type is used to describe any individual in the XMCDA instance. The optional title, subTitle and subSubTitle elements, if present, must preceed all other elements as they are used by the default XSL transformation to HTML (xmcdaDefault.xsl) with the corresponding h1, h2 and h3 tags. The names of the elements suggest their standard use. In the default html transformation these description elements appear in the order of the instance with tagName and value. 
 * 
 * <p>Java class for description complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="description">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subSubTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="author" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *           &lt;element name="lastModificationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *           &lt;element name="shortName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="abstract" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="keywords" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="bibliography" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}bibliography" minOccurs="0"/>
 *           &lt;element name="stakeholders" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "description", propOrder = {
    "title",
    "subTitle",
    "subSubTitle",
    "userOrAuthorOrVersion"
})
@XmlSeeAlso({
    ProjectReference.class
})
public class Description {

    protected String title;
    protected String subTitle;
    protected String subSubTitle;
    @XmlElementRefs({
        @XmlElementRef(name = "version", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "lastModificationDate", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "shortName", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "keywords", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "stakeholders", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "user", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "comment", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "author", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "creationDate", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "abstract", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "bibliography", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> userOrAuthorOrVersion;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the subTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubTitle() {
        return subTitle;
    }

    /**
     * Sets the value of the subTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubTitle(String value) {
        this.subTitle = value;
    }

    /**
     * Gets the value of the subSubTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSubTitle() {
        return subSubTitle;
    }

    /**
     * Sets the value of the subSubTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSubTitle(String value) {
        this.subSubTitle = value;
    }

    /**
     * Gets the value of the userOrAuthorOrVersion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userOrAuthorOrVersion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserOrAuthorOrVersion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Bibliography }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getUserOrAuthorOrVersion() {
        if (userOrAuthorOrVersion == null) {
            userOrAuthorOrVersion = new ArrayList<JAXBElement<?>>();
        }
        return this.userOrAuthorOrVersion;
    }

}
