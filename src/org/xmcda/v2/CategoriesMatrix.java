//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.11.30 at 02:37:50 PM CET 
//


package org.xmcda.v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Generic type for a matrix on categories.
 * 
 * <p>Java class for categoriesMatrix complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="categoriesMatrix">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}description" minOccurs="0"/>
 *         &lt;element name="valuation" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}scale" minOccurs="0"/>
 *         &lt;element name="row" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="description" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}description" minOccurs="0"/>
 *                   &lt;choice minOccurs="0">
 *                     &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="categoriesSetID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="categoriesSet" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoriesSet"/>
 *                   &lt;/choice>
 *                   &lt;element name="column" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="description" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}description" minOccurs="0"/>
 *                             &lt;choice minOccurs="0">
 *                               &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="categoriesSetID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="categoriesSet" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoriesSet"/>
 *                             &lt;/choice>
 *                             &lt;element name="value" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}value" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attGroup ref="{http://www.decision-deck.org/2019/XMCDA-2.2.3}defaultAttributes"/>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.decision-deck.org/2019/XMCDA-2.2.3}defaultAttributes"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "categoriesMatrix", propOrder = {
    "description",
    "valuation",
    "row"
})
public class CategoriesMatrix {

    protected Description description;
    protected Scale valuation;
    @XmlElement(required = true)
    protected List<CategoriesMatrix.Row> row;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "mcdaConcept")
    protected String mcdaConcept;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link Description }
     *     
     */
    public Description getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link Description }
     *     
     */
    public void setDescription(Description value) {
        this.description = value;
    }

    /**
     * Gets the value of the valuation property.
     * 
     * @return
     *     possible object is
     *     {@link Scale }
     *     
     */
    public Scale getValuation() {
        return valuation;
    }

    /**
     * Sets the value of the valuation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Scale }
     *     
     */
    public void setValuation(Scale value) {
        this.valuation = value;
    }

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoriesMatrix.Row }
     * 
     * 
     */
    public List<CategoriesMatrix.Row> getRow() {
        if (row == null) {
            row = new ArrayList<CategoriesMatrix.Row>();
        }
        return this.row;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the mcdaConcept property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcdaConcept() {
        return mcdaConcept;
    }

    /**
     * Sets the value of the mcdaConcept property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcdaConcept(String value) {
        this.mcdaConcept = value;
    }


    /**
     * A row of a matrix.
     * 
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="description" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}description" minOccurs="0"/>
     *         &lt;choice minOccurs="0">
     *           &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;element name="categoriesSetID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;element name="categoriesSet" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoriesSet"/>
     *         &lt;/choice>
     *         &lt;element name="column" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="description" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}description" minOccurs="0"/>
     *                   &lt;choice minOccurs="0">
     *                     &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                     &lt;element name="categoriesSetID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                     &lt;element name="categoriesSet" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoriesSet"/>
     *                   &lt;/choice>
     *                   &lt;element name="value" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}value" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attGroup ref="{http://www.decision-deck.org/2019/XMCDA-2.2.3}defaultAttributes"/>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "description",
        "categoryID",
        "categoriesSetID",
        "categoriesSet",
        "column"
    })
    public static class Row {

        protected Description description;
        protected String categoryID;
        protected String categoriesSetID;
        protected CategoriesSet categoriesSet;
        @XmlElement(required = true)
        protected List<CategoriesMatrix.Row.Column> column;
        @XmlAttribute(name = "id")
        protected String id;
        @XmlAttribute(name = "name")
        protected String name;
        @XmlAttribute(name = "mcdaConcept")
        protected String mcdaConcept;

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link Description }
         *     
         */
        public Description getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link Description }
         *     
         */
        public void setDescription(Description value) {
            this.description = value;
        }

        /**
         * Gets the value of the categoryID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCategoryID() {
            return categoryID;
        }

        /**
         * Sets the value of the categoryID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCategoryID(String value) {
            this.categoryID = value;
        }

        /**
         * Gets the value of the categoriesSetID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCategoriesSetID() {
            return categoriesSetID;
        }

        /**
         * Sets the value of the categoriesSetID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCategoriesSetID(String value) {
            this.categoriesSetID = value;
        }

        /**
         * Gets the value of the categoriesSet property.
         * 
         * @return
         *     possible object is
         *     {@link CategoriesSet }
         *     
         */
        public CategoriesSet getCategoriesSet() {
            return categoriesSet;
        }

        /**
         * Sets the value of the categoriesSet property.
         * 
         * @param value
         *     allowed object is
         *     {@link CategoriesSet }
         *     
         */
        public void setCategoriesSet(CategoriesSet value) {
            this.categoriesSet = value;
        }

        /**
         * Gets the value of the column property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the column property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getColumn().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CategoriesMatrix.Row.Column }
         * 
         * 
         */
        public List<CategoriesMatrix.Row.Column> getColumn() {
            if (column == null) {
                column = new ArrayList<CategoriesMatrix.Row.Column>();
            }
            return this.column;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the mcdaConcept property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMcdaConcept() {
            return mcdaConcept;
        }

        /**
         * Sets the value of the mcdaConcept property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMcdaConcept(String value) {
            this.mcdaConcept = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="description" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}description" minOccurs="0"/>
         *         &lt;choice minOccurs="0">
         *           &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *           &lt;element name="categoriesSetID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *           &lt;element name="categoriesSet" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}categoriesSet"/>
         *         &lt;/choice>
         *         &lt;element name="value" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}value" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "description",
            "categoryID",
            "categoriesSetID",
            "categoriesSet",
            "value"
        })
        public static class Column {

            protected Description description;
            protected String categoryID;
            protected String categoriesSetID;
            protected CategoriesSet categoriesSet;
            protected Value value;

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link Description }
             *     
             */
            public Description getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link Description }
             *     
             */
            public void setDescription(Description value) {
                this.description = value;
            }

            /**
             * Gets the value of the categoryID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCategoryID() {
                return categoryID;
            }

            /**
             * Sets the value of the categoryID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCategoryID(String value) {
                this.categoryID = value;
            }

            /**
             * Gets the value of the categoriesSetID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCategoriesSetID() {
                return categoriesSetID;
            }

            /**
             * Sets the value of the categoriesSetID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCategoriesSetID(String value) {
                this.categoriesSetID = value;
            }

            /**
             * Gets the value of the categoriesSet property.
             * 
             * @return
             *     possible object is
             *     {@link CategoriesSet }
             *     
             */
            public CategoriesSet getCategoriesSet() {
                return categoriesSet;
            }

            /**
             * Sets the value of the categoriesSet property.
             * 
             * @param value
             *     allowed object is
             *     {@link CategoriesSet }
             *     
             */
            public void setCategoriesSet(CategoriesSet value) {
                this.categoriesSet = value;
            }

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link Value }
             *     
             */
            public Value getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link Value }
             *     
             */
            public void setValue(Value value) {
                this.value = value;
            }

        }

    }

}
