//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.11.30 at 02:37:50 PM CET 
//


package org.xmcda.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Generic value type with configurable named parameters
 * 
 * <p>Java class for value complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="value">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}description" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="integer" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *           &lt;element name="real" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *           &lt;element name="interval" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}interval"/>
 *           &lt;element name="rational" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}rational"/>
 *           &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="rankedLabel" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}rankedLabel"/>
 *           &lt;element name="boolean" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           &lt;element name="NA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="image" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *           &lt;element name="imageRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="fuzzyNumber" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}fuzzyNumber"/>
 *           &lt;element name="fuzzyLabel" type="{http://www.decision-deck.org/2019/XMCDA-2.2.3}fuzzyLabel"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.decision-deck.org/2019/XMCDA-2.2.3}defaultAttributes"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "value", propOrder = {
    "description",
    "integer",
    "real",
    "interval",
    "rational",
    "label",
    "rankedLabel",
    "_boolean",
    "na",
    "image",
    "imageRef",
    "fuzzyNumber",
    "fuzzyLabel"
})
@XmlSeeAlso({
    NumericValue.class
})
public class Value {

    protected Description description;
    protected Integer integer;
    protected Double real;
    protected Interval interval;
    protected Rational rational;
    protected String label;
    protected RankedLabel rankedLabel;
    @XmlElement(name = "boolean")
    protected Boolean _boolean;
    @XmlElement(name = "NA")
    protected String na;
    protected byte[] image;
    protected String imageRef;
    protected FuzzyNumber fuzzyNumber;
    protected FuzzyLabel fuzzyLabel;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "mcdaConcept")
    protected String mcdaConcept;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link Description }
     *     
     */
    public Description getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link Description }
     *     
     */
    public void setDescription(Description value) {
        this.description = value;
    }

    /**
     * Gets the value of the integer property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInteger() {
        return integer;
    }

    /**
     * Sets the value of the integer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInteger(Integer value) {
        this.integer = value;
    }

    /**
     * Gets the value of the real property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getReal() {
        return real;
    }

    /**
     * Sets the value of the real property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setReal(Double value) {
        this.real = value;
    }

    /**
     * Gets the value of the interval property.
     * 
     * @return
     *     possible object is
     *     {@link Interval }
     *     
     */
    public Interval getInterval() {
        return interval;
    }

    /**
     * Sets the value of the interval property.
     * 
     * @param value
     *     allowed object is
     *     {@link Interval }
     *     
     */
    public void setInterval(Interval value) {
        this.interval = value;
    }

    /**
     * Gets the value of the rational property.
     * 
     * @return
     *     possible object is
     *     {@link Rational }
     *     
     */
    public Rational getRational() {
        return rational;
    }

    /**
     * Sets the value of the rational property.
     * 
     * @param value
     *     allowed object is
     *     {@link Rational }
     *     
     */
    public void setRational(Rational value) {
        this.rational = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the rankedLabel property.
     * 
     * @return
     *     possible object is
     *     {@link RankedLabel }
     *     
     */
    public RankedLabel getRankedLabel() {
        return rankedLabel;
    }

    /**
     * Sets the value of the rankedLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link RankedLabel }
     *     
     */
    public void setRankedLabel(RankedLabel value) {
        this.rankedLabel = value;
    }

    /**
     * Gets the value of the boolean property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBoolean() {
        return _boolean;
    }

    /**
     * Sets the value of the boolean property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBoolean(Boolean value) {
        this._boolean = value;
    }

    /**
     * Gets the value of the na property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNA() {
        return na;
    }

    /**
     * Sets the value of the na property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNA(String value) {
        this.na = value;
    }

    /**
     * Gets the value of the image property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Sets the value of the image property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImage(byte[] value) {
        this.image = value;
    }

    /**
     * Gets the value of the imageRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageRef() {
        return imageRef;
    }

    /**
     * Sets the value of the imageRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageRef(String value) {
        this.imageRef = value;
    }

    /**
     * Gets the value of the fuzzyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link FuzzyNumber }
     *     
     */
    public FuzzyNumber getFuzzyNumber() {
        return fuzzyNumber;
    }

    /**
     * Sets the value of the fuzzyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link FuzzyNumber }
     *     
     */
    public void setFuzzyNumber(FuzzyNumber value) {
        this.fuzzyNumber = value;
    }

    /**
     * Gets the value of the fuzzyLabel property.
     * 
     * @return
     *     possible object is
     *     {@link FuzzyLabel }
     *     
     */
    public FuzzyLabel getFuzzyLabel() {
        return fuzzyLabel;
    }

    /**
     * Sets the value of the fuzzyLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link FuzzyLabel }
     *     
     */
    public void setFuzzyLabel(FuzzyLabel value) {
        this.fuzzyLabel = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the mcdaConcept property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcdaConcept() {
        return mcdaConcept;
    }

    /**
     * Sets the value of the mcdaConcept property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcdaConcept(String value) {
        this.mcdaConcept = value;
    }

}
