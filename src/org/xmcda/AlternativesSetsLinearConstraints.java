package org.xmcda;

public class AlternativesSetsLinearConstraints <ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE>
    extends LinearConstraints<AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE>, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long     serialVersionUID = 1L;

	public static final String TAG = "alternativesSetsLinearConstraints";

	public AlternativesSetsLinearConstraints()
	{ super(); }

}
