package org.xmcda;

import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesSetsMatrix<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE>
	extends Matrix<AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE>, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "alternativesSetsMatrix";

	public AlternativesSetsMatrix()
	{
		super();
	}
}
