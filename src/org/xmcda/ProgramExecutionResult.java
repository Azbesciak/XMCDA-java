package org.xmcda;

import java.util.ArrayList;

/**
 * The result of the execution of a program is defined by:
 * <ul>
 *     <li>its global {@link #getStatus()} status,</li>
 *     <li>a list of @{link {@link Message messages}.</li>
 * </ul>
 * Its status is normally deduced during the lifespan of this object:
 * <ul>
 * <li>it starts with the default value OK;</li>
 * <li>its seriouness is elevated to WARNING as soon as a warning message is added;</li>
 * <li>last, it is set to ERROR the first time an error message is added.</li>
 * </ul>
 * When using the different methods {@link #addDebug(String)}, {@link #addInfo(String)}, {@link #addWarning(String)}
 * and {@link #addError(String)}, the status code never "goes back".  More formally, status codes are ordered like
 * this: {@code OK < WARNING < ERROR < TERMINATED}; its value always goes up, e.g. from OK to ERROR but never fall
 * back to a smaller value.<br/>
 * <br/>
 * The TERMINATED value has a special behaviour: it can only be set (or replaced by an other status) by using
 * {@link #forceStatus(Status)}.  It is intended to signal that the program has been terminated by an external
 * mean, when possible (for example after receiving a signal from the operating system, or terminated by the user,
 * etc.).
 *
 * xmcda:type:executionResult
 * 
 * @author Sébastien Bigaret
 */
public class ProgramExecutionResult
    extends ArrayList<Message>
    implements HasDescription, CommonAttributes, java.io.Serializable, XMCDARootElement
{
	/**
	 * This enumerates the statuses that a program execution result {@link ProgramExecutionResult#getStatus() can have}.
	 * A program can use this to determine its own return status code. Both status {@code OK} and {@code WARNING} means
	 * that a program's execution is successful, even if a {@code WARNING} status tells that the user may have to
	 * check the outputs and/or the warning messages issued by the program.
	 */
	public static enum Status
	{
		/** Status OK means that neither {@link #addWarning(String)} nor {@link #addError(String)} have been called */
		OK,
		/** Status WARNING means that at least a warning has been signaled with {@link #addWarning(String)}, but that
		 * no {@link #addError(String) errors} have been signaled. */
		WARNING,
		/** Status ERROR means that at least an error has been signaled with {@link #addError(String)}. */
		ERROR,
		/** Status TERMINATED is a special one in that it is not set by any method but
		 * {@link #forceStatus(Status) forceStatus(Status.TERMINATED)} */
		TERMINATED;

		/**
		 * Returns the exit status corresponding to this status. A program typically use this for returning its own exit
		 * status.
		 * <ul>
		 * <li>{@code OK} and {@WARNING} status represents a successful execution, hence the return code is {@code 0}
		 * (zero),
		 * <li>{@code ERROR} returns 1,
		 * <li>{@code TERMINATED} return 2.
		 * 
		 * @return 0 for {@code OK} and {@code WARNING}, 1 for {@code ERROR} and 2 for {@code TERMINATED}.
		 */
		public int exitStatus()
		{
			switch (this)
			{
				case OK:
				case WARNING: return 0;
				case ERROR: return 1;
				case TERMINATED: return 2;
				default:
					throw new RuntimeException("Unknown status :" + this.toString());
			}
		}
	}

	private static final long serialVersionUID = 1L;

	public static final String TAG = "programExecutionResult";

	/**
	 * Helper method to determine this Status with respect to {@link Message.Level}.
	 * @param level a message level
	 * @return the status corresponding to the supplied level.
	 */
	private static Status statusForMessageLevel(Message.Level level)
	{
		Status status = Status.OK;
		switch (level)
		{
			case DEBUG:
			case INFO:
				break; // OK
			case WARNING:
				status = Status.WARNING;
				break;
			case ERROR:
				status = Status.ERROR;
				break;
		}
		return status;
	}

	/** The status is {@link Status#OK} by default. It must never be null. */
	private Status            status           = Status.OK;

	/**
	 * Builds a new object with no messages and a status equal to OK.
	 */
	public ProgramExecutionResult()
	{ super(); }

	/**
	 * Return the status of this execution result
	 * @return the status of this execution result
	 */
	public Status getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	private void setStatus(Status status)
	{
		this.status = status;
	}

	private void setStatus(String status)
	{
		setStatus(Status.valueOf(status.toUpperCase()));
	}

	/**
	 * Updates the status of this execution results.
	 * @param status the proposed status. It must not be {@code null}.
	 * @throws NullPointerException if status is {@code null}.
	 */
	public void updateStatus(Status status) throws NullPointerException
	{
		if ( status.compareTo(this.status) > 0 )
			this.status = status;
	}

	/**
	 * Examine all messages and set the appropriate status, except when the status is already set to TERMINATED, in
	 * which case it is left unchanged.  It must be called if this list of messages is modified, to reflect the
	 * changes.
	 * @return The updated status, or {@link Status#TERMINATED} when it already has this value.
	 */
	public Status refreshStatus()
	{
		if (this.status == Status.TERMINATED)
			return Status.TERMINATED;

		this.status = Status.OK;
		for (Message message: this)
			updateStatus(statusForMessageLevel(message.getLevel()));
		return this.status;
	}

	/**
	 * Do not make any checks and force the object's {@link #getStatus() status} to the one supplied.<br/>
	 * 	<b>WARNING!</b> Unless you have very good reason, you probably want to use {@link #updateStatus(Status)}.
	 * @param newStatus the new (non null) status
	 * @throws NullPointerException if status is {@code null}.
	 */
	public void forceStatus(Status newStatus) throws NullPointerException

	{
		if (newStatus==null)
			throw new NullPointerException("The status cannot be null");
		this.status = newStatus;
	}

	/**
	 * Adds a message to this object, along with its the degree of seriousness
	 * @param level the degree of seriouness of the supplied message
	 * @param text the message itself
	 * @return the created Message
	 */
	protected Message addMessage(Message.Level level, String text)
	{
		updateStatus(statusForMessageLevel(level));
		final Message msg = new Message(level, text);
		this.add(msg);
		return msg;
	}

	public Message addDebug(String text)
	{
		return addMessage(Message.Level.DEBUG, text);
	}

	public Message addInfo(String text)
	{
		return addMessage(Message.Level.INFO, text);
	}

	public Message addWarning(String text)
	{
		return addMessage(Message.Level.WARNING, text);
	}

	public Message addError(String text)
	{
		return addMessage(Message.Level.ERROR, text);
	}

	/**
	 * Equivalent to {@code getStatus().equals(Status.OK)}
	 * @return true if this object's status is equal to {@link Status#OK}, false otherwise.
	 */
	public boolean isOk()
	{
		return this.status.equals(Status.OK);
	}

	/**
	 * Equivalent to {@code getStatus().equals(Status.WARNING)}
	 * @return true if this object's status is equal to {@link Status#WARNING}, false otherwise.
	 */
	public boolean isWarning()
	{
		return this.status.equals(Status.WARNING);
	}

	/**
	 * Equivalent to {@code getStatus().equals(Status.ERROR)}
	 * @return true if this object's status is equal to {@link Status#ERROR}, false otherwise.
	 */
	public boolean isError()
	{
		return this.status.equals(Status.ERROR);
	}

	/**
	 * Equivalent to {@code getStatus().equals(Status.TERMINATED)}
	 * @return true if this object's status is equal to {@link Status#TERMINATED}, false otherwise.
	 */
	public boolean isTerminated()
	{
		return this.status.equals(Status.TERMINATED);
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	@Override
	public String id()
	{
		return id;
	}

	@Override
	public void setId(String id)
	{
		this.id = id;
	}

	@Override
	public String name()
	{
		return name;
	}

	@Override
	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	@Override
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	@Override
	public void setDescription(Description description)
	{
		this.description = description;
	}

	@Override
	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}
