package org.xmcda;

/**
 * This interface is implemented by all XMCDA classes which uses the so-called "common attribute" (named
 * "default attributes" in the XML Schema).<br/>
 * These attributes are:
 * <ul>
 * <li>an id</li>
 * <li>a name</li>
 * <li>a MCDA concept</li>
 * </ul>
 * For the exact definition of these fields and how they shold be used, please refer to XXX TODO référence
 * 
 * @author Sébastien Bigaret
 */
public interface CommonAttributes
{
	public String id();

	
	public void setId(String id);


	public String name();


	public void setName(String name);


	public String mcdaConcept();


	public void setMcdaConcept(String mcdaConcept);

}
