package org.xmcda;

import java.util.ArrayList;

/**
 * @author Sébastien Bigaret
 * @param <T>
 */
public class LinearConstraint <T, VALUE_TYPE>
implements CommonAttributes, HasDescription
{
	/**
	 * @author Sébastien Bigaret
	 * @param <T>
	 * @param <VALUE_TYPE>
	 */
	public static class Element <T, VALUE_TYPE>
	implements CommonAttributes
	{
		protected T                          unknown;

		protected String                     variableID; // TODO ref to Variable

		protected QualifiedValue<VALUE_TYPE> coefficient; // TODO y a-t-il vraiment une raison pour que le coef. ait le même type que rhs

		/**
		 * @return the unknown
		 */
		public T getUnknown()
		{
			return unknown;
		}

		/**
		 * @param unknown the unknown to set
		 */
		public void setUnknown(T unknown)
		{
			this.unknown = unknown;
		}

		/**
		 * @return the variable
		 */
		public String getVariableID()
		{
			return variableID;
		}

		/**
		 * @param variable the variable to set
		 */
		public void setVariableID(String variableID)
		{
			this.variableID = variableID;
		}

		/**
		 * @return the coefficient
		 */
		public QualifiedValue<VALUE_TYPE> getCoefficient()
		{
			return coefficient;
		}

		/**
		 * @param coefficient the coefficient to set
		 */
		public void setCoefficient(QualifiedValue<VALUE_TYPE> coefficient)
		{
			this.coefficient = coefficient;
		}

		// CommonAttributes (start)

		/** The id attribute allows to identify the underlying piece of data by a program. */
		private String            id;

		/** The name attribute contains the human readable name of the object or concept. */
		private String            name;

		/**
		 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
		 * make choices which will have an influence on the output. The documentation of the program should therefore
		 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
		 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
		 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
		 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
		 * mcdaConcepts, as this will reduce the compatibility between the various programs.
		 */
		private String            mcdaConcept;

		public String id()
		{
			return id;
		}

		public void setId(String id)
		{
			this.id = id;
		}

		public String name()
		{
			return name;
		}

		public String mcdaConcept()
		{
			return mcdaConcept;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public void setMcdaConcept(String mcdaConcept)
		{
			this.mcdaConcept = mcdaConcept;
		}

		// CommonAttributes (end)

	}

	public static class Variable
	implements Referenceable, HasDescription
	{
		// NB: there is no reason to have a creation observer here

		@Override
		public void merge(Referenceable object)
		{
			if (!( object instanceof Variable ))
				throw new IllegalArgumentException("Argument should be a Variable");
			Variable variable = (Variable) object;
			if (!this.id().equals(variable.id()))
				throw new IllegalArgumentException("Parameter's id() should be the same as this'");
			this.setDescription(variable.getDescription());
			this.setName(variable.name());
			this.setMcdaConcept(variable.mcdaConcept());
		}

		// HasDescription (start)

		private Description description;
		public void setDescription(Description description)
		{
			this.description = description;
		}

		public Description getDescription()
		{
			return description;
		}

		// HasDescription (end)

		// Referenceable (start)

		//   - implements CommonAttributes
		/** The id attribute allows to identify the underlying piece of data by a program. */
		private String            id;

		/** The name attribute contains the human readable name of the object or concept. */
		private String            name;

		/**
		 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
		 * make choices which will have an influence on the output. The documentation of the program should therefore
		 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
		 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
		 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
		 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
		 * mcdaConcepts, as this will reduce the compatibility between the various programs.
		 */
		private String            mcdaConcept;

		public String id()
		{
			return id;
		}

		void _setId(String id)
		{
			this.id = id;
		}

		public String name()
		{
			return name;
		}

		public String mcdaConcept()
		{
			return mcdaConcept;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public void setMcdaConcept(String mcdaConcept)
		{
			this.mcdaConcept = mcdaConcept;
		}

		//   Referenceable specific part
		private Object           marker;

		private transient Object container;

		public void setId(String id)
		{
			if (this.container != null)
				throw new IllegalStateException("Cannot change this object's id while it's in a container");
			_setId(id);
		}

		public void setContainer(Object container)
		{
			if ( container == null)
			{
				this.container = null;
				return;
			}
			if ( this.container != null && this.container != container )
				throw new IllegalStateException("this object already belongs to a other alternatives");
			this.container = container;
		}

		public Object getContainer()
		{
			return this.container;
		}

		public void setMarker(Object marker)
		{
			this.marker = marker;
		}

		public Object getMarker()
		{
			return this.marker;
		}

		// Referenceable (end)

	}

	public static enum Operator
	{
		GEQ, EQ, LEQ
	};

	protected ArrayList<Element<T, VALUE_TYPE>> elements;

	protected Operator operator;

	protected QualifiedValue<VALUE_TYPE>        rhs;

	protected QualifiedValues<?> values; // TODO un autre type pour celui-là?

	/**
	 * @return the elements
	 */
	public ArrayList<Element<T, VALUE_TYPE>> getElements()
	{
		return elements;
	}

	/**
	 * @param elements the elements to set
	 */
	public void setElements(ArrayList<Element<T, VALUE_TYPE>> elements)
	{
		this.elements = elements;
	}

	/**
	 * @return the operator
	 */
	public Operator getOperator()
	{
		return operator;
	}

	/**
	 * @param rhs the rhs to set
	 */
	public void setOperator(Operator operator)
	{
		this.operator = operator;
	}

	/**
	 * @return the rhs
	 */
	public QualifiedValue<VALUE_TYPE> getRhs()
	{
		return rhs;
	}

	/**
	 * @param rhs the rhs to set
	 */
	public void setRhs(QualifiedValue<VALUE_TYPE> rhs)
	{
		this.rhs = rhs;
	}

	/**
	 * @return the values
	 */
	public QualifiedValues<?> getValues()
	{
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(QualifiedValues<?> values)
	{
		this.values = values;
	}

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

	// Referenceable (start)

	//   - implements CommonAttributes
	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	void _setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	//   Referenceable specific part
	private Object           marker;

	private transient Object container;

	public void setId(String id)
	{
		if (this.container != null)
			throw new IllegalStateException("Cannot change this object's id while it's in a container");
		_setId(id);
	}

	public void setContainer(Object container)
	{
		if ( container == null)
		{
			this.container = null;
			return;
		}
		if ( this.container != null && this.container != container )
			throw new IllegalStateException("this object already belongs to a other alternatives");
		this.container = container;
	}

	public Object getContainer()
	{
		return this.container;
	}

	public void setMarker(Object marker)
	{
		this.marker = marker;
	}

	public Object getMarker()
	{
		return this.marker;
	}

	// Referenceable (end)

}
