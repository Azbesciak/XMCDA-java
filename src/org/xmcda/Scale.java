package org.xmcda;

/**
 * mcda:type:scale
 * 
 * @author Sébastien Bigaret
 */
public interface Scale extends CommonAttributes
{
	public static final String PREFERENCE_DIRECTION = "preferenceDirection";
	
	/**
	 * mcda:type:preferenceDirection
	 * 
	 * @author Sébastien Bigaret
	 */
	public static enum PreferenceDirection { MIN, MAX };
}
