package org.xmcda;

public class CriteriaLinearConstraints <VALUE_TYPE>
    extends LinearConstraints<Criterion, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long     serialVersionUID = 1L;

	public static final String TAG = "criteriaLinearConstraints";

	public CriteriaLinearConstraints()
	{ super(); }

}
