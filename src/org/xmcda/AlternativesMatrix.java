package org.xmcda;

import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesMatrix<VALUE_TYPE>
	extends Matrix<Alternative, VALUE_TYPE>
	implements XMCDARootElement
{
    private static final long serialVersionUID = 1L;

	public static final String TAG = "alternativesMatrix";

	public AlternativesMatrix()
	{
		super();
	}
}
