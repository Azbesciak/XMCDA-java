package org.xmcda;


public class AlternativesSets <VALUE_TYPE>
    extends ReferenceableContainer<AlternativesSet<VALUE_TYPE>>
    implements HasDescription, CommonAttributes, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "alternativesSets";

	public AlternativesSets()
	{ super(); }

	@Override
	public Class<AlternativesSet<VALUE_TYPE>> elementClass()
	{
	    return (Class<AlternativesSet<VALUE_TYPE>>) new AlternativesSet<VALUE_TYPE>().getClass();
	}

	@Override
	public int hashCode()
	{
		return id().hashCode();
	}

	@Override
	public boolean equals(Object o)
	{
		if (!( o instanceof AlternativesSets ))
		    return false;
		if (this == o)
		    return true;

		AlternativesSets<?> sets = (AlternativesSets<?>) o;
		if (this.id() != null && this.id().equals(sets.id()))
		    return true;
		return sets.id() == null; // TODO CHECK content?
	}

}
