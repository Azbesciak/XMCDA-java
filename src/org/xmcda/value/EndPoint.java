/**
 * 
 */
package org.xmcda.value;

/**
 * @author Sébastien Bigaret
 */
public class EndPoint <T1, T2>
    extends Point<T1, T2>
{
	boolean open = false;

	public EndPoint(Point<T1, T2> point)
	{
		super();
		this.setAbscissa(point.getAbscissa());
		this.setOrdinate(point.getOrdinate());
	}
	
	/**
	 * @return the open
	 */
	public boolean isOpen()
	{
		return open;
	}

	/**
	 * @param open
	 *            the open to set
	 */
	public void setOpen(boolean open)
	{
		this.open = open;
	}

}
