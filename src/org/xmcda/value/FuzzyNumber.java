/**
 * 
 */
package org.xmcda.value;


/**
 * @author Sébastien Bigaret
 */
public class FuzzyNumber <T1, T2>
{
	private MembershipFunction<T1, T2> function;

	/**
	 * @return the function
	 */
	public MembershipFunction<T1, T2> getFunction()
	{
		return function;
	}

	/**
	 * @param function
	 *            the function to set
	 */
	public void setFunction(MembershipFunction<T1, T2> function)
	{
		this.function = function;
	}

}
