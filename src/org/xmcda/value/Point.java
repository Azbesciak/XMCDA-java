/**
 * 
 */
package org.xmcda.value;

import org.xmcda.QualifiedValue;

/**
 * @author Sébastien Bigaret
 */
public class Point <T1, T2> // TODO direct access to value, à la PerformanceTable?
{
	private QualifiedValue<T1> abscissa;

	private QualifiedValue<T2> ordinate;

	/**
	 * @return the abscissa
	 */
	public QualifiedValue<T1> getAbscissa()
	{
		return abscissa;
	}

	/**
	 * @param abscissa
	 *            the abscissa to set
	 */
	public void setAbscissa(QualifiedValue<T1> abscissa)
	{
		this.abscissa = abscissa;
	}

	/**
	 * @return the ordinate
	 */
	public QualifiedValue<T2> getOrdinate()
	{
		return ordinate;
	}

	/**
	 * @param ordinate
	 *            the ordinate to set
	 */
	public void setOrdinate(QualifiedValue<T2> ordinate)
	{
		this.ordinate = ordinate;
	}
}
