/**
 * 
 */
package org.xmcda.value;

import org.xmcda.QualifiedValue;

/**
 * @author Sébastien Bigaret
 */
public class Interval <T>
{
	private QualifiedValue<T> lowerBound;

	private QualifiedValue<T> upperBound;

	private boolean           leftClosed  = true;

	private boolean           rightClosed = true;

	/**
	 * @return the lowerBound
	 */
	public QualifiedValue<T> getLowerBound()
	{
		return lowerBound;
	}

	/**
	 * @param lowerBound
	 *            the lowerBound to set
	 */
	public void setLowerBound(QualifiedValue<T> lowerBound)
	{
		this.lowerBound = lowerBound;
	}

	/**
	 * @return the upperBound
	 */
	public QualifiedValue<T> getUpperBound()
	{
		return upperBound;
	}

	/**
	 * @param upperBound
	 *            the upperBound to set
	 */
	public void setUpperBound(QualifiedValue<T> upperBound)
	{
		this.upperBound = upperBound;
	}

	/**
	 * Tells whether the interval is left-closed. An interval is said to be left-closed if its lower bound belongs to
	 * the interval.
	 * 
	 * @return whether the interval is left-closed
	 */
	public boolean isLeftClosed()
	{
		return leftClosed;
	}

	public void setIsLeftClosed(boolean leftClosed)
	{
		this.leftClosed = leftClosed;
	}

	/**
	 * Tells whether the interval is right-closed. An interval is said to be right-closed if its lower bound belongs to
	 * the interval.
	 * 
	 * @return whether the interval is right-closed
	 */
	public boolean isRightClosed()
	{
		return rightClosed;
	}

	public void setIsRightClosed(boolean rightClosed)
	{
		this.rightClosed = rightClosed;
	}

	/**
	 * Tells whether the interval is closed. An interval is said to be closed if its bounds belong to the interval.
	 * 
	 * @return whether the interval is closed
	 */
	public boolean isClosed()
	{
		return leftClosed && rightClosed;
	}
}
