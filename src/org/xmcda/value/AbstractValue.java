/**
 * 
 */
package org.xmcda.value;

/**
 * TODO AbstractValue voir si on utilise cette interface pour identifier tout ce qui est acceptable dans les
 * QualifiedValue, mais bof, vaut mieux une factory dans les converters
 * 
 * @author Sébastien Bigaret
 */
public interface AbstractValue
{
	public void toXml();
}
