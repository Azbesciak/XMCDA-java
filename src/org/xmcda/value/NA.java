/**
 * 
 */
package org.xmcda.value;

/**
 * Represent the XMCDA value: NotAvailable.  This is a singleton.
 * @author Sébastien Bigaret
 *
 */
public class NA
{
	/**
	 * The NotAvailable value.  This is a singleton.
	 */
	public static final NA na = new NA();

	private NA() {}	
}
