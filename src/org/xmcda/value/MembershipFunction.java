package org.xmcda.value;

/**
 * Restriction of the "function" type to functions compatible with fuzzy numbers.
 * 
 * XMCDA-tag:type:membershipFunction
 * @author Sébastien Bigaret
 */
public interface MembershipFunction <T1, T2> extends Function
{

}
