/**
 * 
 */
package org.xmcda.value;

/**
 * @author big
 */
public class Rational
{
	private int numerator;

	private int denominator;

	public Rational() {}

	public Rational(int numerator, int denominator)
	{
		this.numerator = numerator;
		this.denominator = denominator;
	}

	/**
	 * @return the numerator
	 */
	public int getNumerator()
	{
		return numerator;
	}

	/**
	 * @param numerator
	 *            the numerator to set
	 */
	public void setNumerator(int numerator)
	{
		this.numerator = numerator;
	}

	/**
	 * @return the denominator
	 */
	public int getDenominator()
	{
		return denominator;
	}

	/**
	 * @param denominator
	 *            the denominator to set
	 */
	public void setDenominator(int denominator)
	{
		this.denominator = denominator;
	}

}
