package org.xmcda.value;

import org.xmcda.QualifiedValue;

public class ValuedLabel <T>
{
	private String            label;

	private QualifiedValue<T> value;

	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label)
	{
		this.label = label;
	}

	/**
	 * @return the value
	 */
	public QualifiedValue<T> getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(QualifiedValue<T> value)
	{
		this.value = value;
	}

}
