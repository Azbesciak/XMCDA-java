
NB: pour le moment aucun convertisseur XML / POJO n'est réalisé

État d'avancement du convertisseur v2 <--> v3 du point de vue v2 puisque ça n'a de sens que du point de vue v2: conversion d'un format appelé à disparaître


XMCDA

<!-- XMCDA types declarations -->
[*]  defaultAttributes
[*]  defaultAttributesIDRequired

[*]  description
[*]  bibliography

<!-- alternatives -->
[*]  alternatives
[*]  alternative
[*]  alternativesSets
[*]  alternativesSet
[ ]  alternativesValues
[ ]  alternativeValue
[ ]  alternativeReference
[ ]  alternativesComparisons
[ ]  alternativesMatrix
[ ]  alternativesLinearConstraints

<!-- criteria -->
[*]  criteria
[*]  criterion
[ ]  criteriaFunctions
[ ]  criterionFunction
[ ]  criteriaThresholds
[ ]  criterionThreshold
[*]  criteriaSets
[*]  criteriaSet
[*]  criteriaValues
[*]  criterionValue
[*]  criteriaComparisons
[*]  criteriaMatrix
[ ]  criterionReference
[ ]  criteriaLinearConstraints

<!-- mixed types for alternatives and criteria -->
[ ]  alternativesCriteriaValues
[ ]  alternativeCriteriaValues

<!-- categories -->
[*]  categories
[*]  category
[*]  categoriesSets
[*]  categoriesSet
[ ]  categoriesValues
[ ]  categoryValue
[ ]  categoriesComparisons
[ ]  categoriesMatrix
[ ]  categoriesLinearConstraints
[ ]  categoryReference
[ ]  categoriesProfiles
[ ]  categoryProfile
[ ]  alternativesAssignments
[ ]  alternativeAssignment
[ ]  categoriesInterval

<!-- Other types -->
[*]  scale
[/]  valuationType  IGNORED in v3
[*]  nominal
[*]  qualitative
[*]  quantitative
[*]  preferenceDirection
[ ]  numericValue
[*]  value
[*]  values
[*]  interval
[*]  NA

[*]  rational
[*]  rankedLabel -> valuedLabel
[*]  fuzzyLabel  -> valuedLabel
[*]  fuzzyNumber
[*]  point
[ ]  function
[*]  performanceTable
[*]  alternativeOnCriteriaPerformances
