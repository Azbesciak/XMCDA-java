================
 xxxComparisons
================


alternativesComparisons
=======================

::

  ( grep -L 'active="0"' /home/big/Projets/diviz/descriptions/*.xml | while read f; do grep -l '<type>alternativesComparisons</type>' $f; done ) |wc -l
  82

  ITTB___alternativesRankingViaQualificationDistillation___1.1.xml
  ITTB___consistencyTest___1.0.xml
  ITTB___cutRelation___1.1.xml
  ITTB___plotAlternativesComparisons___1.1.xml
  ITTB___transitiveReductionAlternativesComparisons___1.0.xml
  J-MCDA___ElectreConcordance___0.5.1.xml
  J-MCDA___ElectreDiscordances___0.5.1.xml
  J-MCDA___ElectreOutranking___0.5.1.xml
  J-MCDA___PrometheeFlows___0.5.1.xml
  J-MCDA___PrometheePreference___0.5.1.xml
  J-MCDA___cutRelation___0.5.1.xml
  LJY___SRMPdisaggregationNoInconsistency___1.2.xml
criteriaID

  PUT___DEACCRPreferenceRelations___1.0.xml
  PUT___DEAvalueADDPreferenceRelations___1.0.xml
  PUT___ElectreComprehensiveDiscordanceIndex___0.2.0.xml
  PUT___ElectreConcordanceReinforcedPreference___0.1.0.xml
  PUT___ElectreConcordanceWithInteractions___0.2.0.xml
  PUT___ElectreConcordance___0.2.0.xml
  PUT___ElectreCredibilityWithCounterVeto___0.1.0.xml
  PUT___ElectreCredibility___0.2.0.xml
  PUT___ElectreCrispOutrankingAggregation___0.2.0.xml
  PUT___ElectreDiscordance___0.2.0.xml
  PUT___ElectreDistillationRank___0.1.0.xml
  PUT___ElectreDistillation___0.2.0.xml
  PUT___ElectreIVCredibility___0.2.0.xml
  PUT___ElectreIsDiscordanceBinary___0.1.0.xml
  PUT___ElectreIsFindKernel___0.2.0.xml
  PUT___ElectreNFSOutranking___0.2.0.xml
  PUT___ElectreTri-CClassAssignments___0.2.0.xml
  PUT___ElectreTri-rCClassAssignments___0.2.0.xml
  PUT___ElectreTriClassAssignments___0.2.0.xml
  PUT___RORUTA-ExtremeRanksHierarchical___1.0.xml
  PUT___RORUTA-ExtremeRanks___1.0.xml
  PUT___RORUTA-NecessaryAndPossiblePreferenceRelationsHierarchical___1.0.xml
  PUT___RORUTA-NecessaryAndPossiblePreferenceRelations___1.0.xml
  PUT___RORUTA-PairwiseOutrankingIndicesHierarchical___1.0.xml
  PUT___RORUTA-PairwiseOutrankingIndices___1.0.xml
  PUT___RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValueHierarchical___1.0.xml
  PUT___RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValue___1.0.xml
  PUT___RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValueHierarchical___1.0.xml
  PUT___RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue___1.0.xml
  PUT___RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValueHierarchical___1.0.xml
  PUT___RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue___1.0.xml
  PUT___RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical___1.0.xml
  PUT___RORUTA-PostFactum-RankRelatedImprovementOrMissingValue___1.0.xml
  PUT___RORUTA-PreferentialReductsForNecessaryRelationsHierarchical___1.0.xml
  PUT___RORUTA-PreferentialReductsForNecessaryRelations___1.0.xml
  PUT___RORUTA-RankAcceptabilityIndicesHierarchical___1.0.xml
  PUT___RORUTA-RankAcceptabilityIndices___1.0.xml
  PUT___RORUTA-RankRelatedPreferentialReductsHierarchical___1.0.xml
  PUT___RORUTA-RankRelatedPreferentialReducts___1.0.xml
  PUT___RORUTA-RepresentativeValueFunctionHierarchical___1.0.xml
  PUT___RORUTA-RepresentativeValueFunction___1.0.xml
  PUT___RORUTADIS-ExtremeClassCardinalities___0.1.xml
  PUT___RORUTADIS-NecessaryAssignment-basedPreferenceRelation___0.1.xml
  PUT___RORUTADIS-PossibleAndNecessaryAssignments___0.1.xml
  PUT___RORUTADIS-PostFactum-InvestigatePerformanceDeterioration___0.3.xml
  PUT___RORUTADIS-PostFactum-InvestigatePerformanceImprovement___0.3.xml
  PUT___RORUTADIS-PostFactum-InvestigateValueChange___0.3.xml
  PUT___RORUTADIS-PreferentialReducts___0.1.xml
  PUT___RORUTADIS-RepresentativeValueFunction___0.3.xml
  PUT___RORUTADIS-StochasticResults___0.1.xml
  PUT___cutRelationCrisp___0.1.0.xml
  PUT___plotAlternativesHasseDiagram___0.2.xml
  PyXMCDA___CondorcetRobustnessRelation___1.0.xml
  PyXMCDA___RubisConcordanceRelation___1.0.xml
  PyXMCDA___RubisOutrankingRelation___1.1.xml
  PyXMCDA___stableSorting___1.0.xml
  PyXMCDA___weightsFromCondorcetAndPreferences___1.0.xml
  RXMCDA___Promethee1Ranking___1.0.xml
  RXMCDA___alternativesRankingViaQualificationDistillation___1.0.xml
  RXMCDA___computeAlternativesQualification___1.0.xml
  RXMCDA___convertAlternativesRanksToAlternativesComparisons___1.0.xml
  RXMCDA___plotAlternativesComparisons___1.0.xml
  kappalab___linProgCapaIdent___1.0.xml
  kappalab___lsRankingCapaIdent___1.0.xml
  kappalab___miniVarCapaIdent___1.0.xml
  ws-Mcc___mccClustersRelationSummary___1.0.xml
  ws-Mcc___mccClusters___1.0.xml
  ws-Mcc___mccEvaluateClusters___1.0.xml
  ws-Mcc___mccPlotClusters___1.0.xml
  ws-Mcc___mccPreferenceRelation___1.0.xml

Ceux qui n'ont pas de répertoire 'tests'::

  ( grep -L 'active="0"' /home/big/Projets/diviz/descriptions/*.xml | while read f; do grep -l '<type>alternativesComparisons</type>' $f; done ) \
  | while read f; do name=${f##*/}; name=${name%___*}; provider=${name%%___*}; name=${name#*___}; dir=${name}-${provider}; \
      grep -l '<alternativesComparisons>' ${dir}/tests/*/*.xml \
      | while read testf; do grep -li set ${testf}; done; \
    done
weightsFromCondorcetAndPreferences-PyXMCDA/tests/in1/criteriaComparisons.xml





alternativesMatrix
==================

::

  ( grep -L 'active="0"' *.xml | while read f; do grep -l '<type>alternativesMatrix</type>' $f; done ) |wc -l
  2

  PUT___RORUTA-PairwiseOutrankingIndicesHierarchical___1.0.xml
  PUT___RORUTA-PairwiseOutrankingIndices___1.0.xml


criteriaComparisons
===================

Dans 9 services

::

  grep -l '<type>criteriaComparisons</type>' *.xml|wc -l
  9

  ITTB___plotCriteriaComparisons___1.0.xml
  ITTB___transitiveReductionCriteriaComparisons___1.0.xml
  PyXMCDA___weightsFromCondorcetAndPreferences___1.0.xml
  RXMCDA___generatePerformanceTableFromBayesianNet___1.0.xml
  RXMCDA___plotCriteriaComparisons___1.0.xml
  kappalab___leastSquaresCapaIdent___1.0.xml
  kappalab___linProgCapaIdent___1.0.xml
  kappalab___lsRankingCapaIdent___1.0.xml
  kappalab___miniVarCapaIdent___1.0.xml



  ( grep -L 'active="0"' /home/big/Projets/diviz/descriptions/*.xml | while read f; do grep -l '<type>criteriaComparisons</type>' $f; done ) \
  | while read f; do name=${f##*/}; name=${name%___*}; provider=${name%%___*}; name=${name#*___}; dir=${name}-${provider}; \
      grep -l '<criteriaComparisons>' ${dir}/tests/*/*.xml \
      | while read testf; do grep -li set ${testf}; done; \
    done




criteriaMatrix
==============

( grep -L 'active="0"' *.xml | while read f; do grep -l '<type>criteriaMatrix</type>' $f; done ) |wc -l
0


categoriesComparisons
=====================

::

  grep -l '<type>categoriesComparisons</type>' *.xml|wc -l
  6

  CppXMCDA___IRIS-testInconsistency___1.0.xml
  CppXMCDA___IRIS___1.0.xml
  ws-Mcc___mccClustersRelationSummary___1.0.xml
  ws-Mcc___mccClusters___1.0.xml
  ws-Mcc___mccEvaluateClusters___1.0.xml
  ws-Mcc___mccPlotClusters___1.0.xml

  ( grep -L 'active="0"' /home/big/Projets/diviz/descriptions/*.xml | while read f; do grep -l '<type>categoriesComparisons</type>' $f; done ) \
  | while read f; do name=${f##*/}; name=${name%___*}; provider=${name%%___*}; name=${name#*___}; dir=${name}-${provider}; \
      grep -l '<categoriesComparisons>' ${dir}/tests/*/*.xml \
      | while read testf; do grep -li set ${testf}; done; \
    done

categoriesMatrix
================

( grep -L 'active="0"' *.xml | while read f; do grep -l '<type>categoriesMatrix</type>' $f; done ) |wc -l
0
